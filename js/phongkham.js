var dataphongkham = [
 {
   "STT": 1,
   "Name": "Bác sĩ Hồ Sĩ Chương Khoa Sản",
   "address": "221 Nguyễn Tiểu La, phường 8, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7645646,
   "Latitude": 106.6660563
 },
 {
   "STT": 2,
   "Name": "Bác sĩ Huỳnh Ngọc Thiện Khoa Tim Mạch",
   "address": "255 - 257 Hòa Hảo, phường 4, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.762142,
   "Latitude": 106.670142
 },
 {
   "STT": 3,
   "Name": "Bác sĩ Huỳnh Xuân Nghiêm Khoa Sản",
   "address": "34 Phan Phú Tiên, phường 10, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7520661,
   "Latitude": 106.6657334
 },
 {
   "STT": 4,
   "Name": "Bác sĩ Lê Chí Dũng Khoa Chấn Thương Chỉnh Hình",
   "address": "436/2A Ba Tháng Hai, phường 12, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7773817,
   "Latitude": 106.6811498
 },
 {
   "STT": 5,
   "Name": "Bác sĩ Lê Hữu Phước  Khoa Nội Tổng Quát",
   "address": "274 Nguyễn Duy Dương, phường 4, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.763741,
   "Latitude": 106.670473
 },
 {
   "STT": 6,
   "Name": "Bác sĩ Lê Minh Đạo Khoa Thần Kinh",
   "address": "31C Trần Bình Trọng, phường 1, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7532147,
   "Latitude": 106.6820276
 },
 {
   "STT": 7,
   "Name": "Bác sĩ Lê Quang Nghĩa  Khoa Nội Tổng Quát",
   "address": "262/24 Nguyễn Tiểu La, phường 8, Quận 10, Hồ Chí Minh ",
   "Longtitude": 10.760335,
   "Latitude": 106.6672403
 },
 {
   "STT": 8,
   "Name": "Bác sĩ Lê Thị Hồng Liên  Khoa Nội Thần Kinh",
   "address": "247 Đào Duy Từ, phường 7, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7593009,
   "Latitude": 106.6617112
 },
 {
   "STT": 9,
   "Name": "Bác sĩ Lê Văn Đính Khoa Tai Mũi Họng",
   "address": "91 Ngô Quyền, phường 11, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7536945,
   "Latitude": 106.6664022
 },
 {
   "STT": 10,
   "Name": "Bác sĩ Lê Đình Minh Nhân  Nội Nhiễm",
   "address": "514 Nguyễn Chí Thanh, phường 7, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.758703,
   "Latitude": 106.6618963
 },
 {
   "STT": 11,
   "Name": "Bác sĩ Nguyễn Anh Võ Khoa Tai Mũi Họng",
   "address": "104/15 Thành Thái, phường 12, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.771207,
   "Latitude": 106.6654341
 },
 {
   "STT": 12,
   "Name": "Bác sĩ Nguyễn Bá Lưu Khoa Tai Mũi Họng",
   "address": "12 Lô E Chung cư Nguyễn Trãi, phường 8, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.749874,
   "Latitude": 106.645237
 },
 {
   "STT": 13,
   "Name": "Bác sĩ Nguyễn Bá Mỹ Nhi Khoa Sản",
   "address": "315 Cách Mạng Tháng Tám, phường 12, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7796317,
   "Latitude": 106.6780663
 },
 {
   "STT": 14,
   "Name": "Bác sĩ Nguyễn Công Tâm  Khoa Tim Mạch, Nội Tổng Quát",
   "address": "65 Thành Thái, phường 14, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7729751,
   "Latitude": 106.6648673
 },
 {
   "STT": 15,
   "Name": "Bác sĩ Nguyễn Hải Nam  Khoa Nội Tổng Quát",
   "address": "286 Hòa Hảo, phường 4, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.762269,
   "Latitude": 106.669627
 },
 {
   "STT": 16,
   "Name": "Bác sĩ Nguyễn Huy Dũng Khoa Lao Phổi",
   "address": "21 Mạc Thiên Tích, phường 11, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7550055,
   "Latitude": 106.6655542
 },
 {
   "STT": 17,
   "Name": "Bác sĩ Nguyễn Minh Chiền Khoa Thần Kinh",
   "address": "270G Võ Thị Sáu, phường 7, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.780359,
   "Latitude": 106.6844825
 },
 {
   "STT": 18,
   "Name": "Bác sĩ Nguyễn Phú Thọ Khoa Tim Mạch",
   "address": "177/7 Ba Tháng Hai, phường 11, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7755901,
   "Latitude": 106.6800888
 },
 {
   "STT": 19,
   "Name": "Bác sĩ Nguyễn Sào Trung  Khoa Ung Bướu & Nội Khoa",
   "address": "171 Nguyễn Duy Dương, phường 3, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.761688,
   "Latitude": 106.670623
 },
 {
   "STT": 20,
   "Name": "Bác sĩ Nguyễn Thị Ngọc Thu Khoa Răng Hàm Mặt",
   "address": "708 Sư Vạn Hạnh, phường 12, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7715341,
   "Latitude": 106.6698759
 },
 {
   "STT": 21,
   "Name": "Bác sĩ Nguyễn Trí Thức Khoa Lao",
   "address": "436B/42 Ba Tháng Hai, phường 12, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7766922,
   "Latitude": 106.6804467
 },
 {
   "STT": 22,
   "Name": "Bác sĩ Nguyễn Văn Quang Khoa Chỉnh Hình",
   "address": "JJ2, Cư xá Bắc Hải, Bạch Mã, phường 15, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7802432,
   "Latitude": 106.6620216
 },
 {
   "STT": 23,
   "Name": "Bác sĩ Nguyễn Văn Út  Khoa Da Liễu",
   "address": "468 Trần Hưng Đạo, phường 14, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7521393,
   "Latitude": 106.6582222
 },
 {
   "STT": 24,
   "Name": "Bác sĩ Phùng Minh Thủy  Khoa Nội Tổng Quát",
   "address": "8 Phạm Hữu Chí, phường 12, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7557495,
   "Latitude": 106.6584175
 },
 {
   "STT": 25,
   "Name": "Bác sĩ Trần Khải Quang  Khoa Nội Tổng Quát",
   "address": "165-167 Huỳnh Mẫn Đạt, phường 8, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7569101,
   "Latitude": 106.6758912
 },
 {
   "STT": 26,
   "Name": "Ác Sĩ Trần Thị Kim Xuyến Khoa Sản",
   "address": "159 Cao Đạt, phường 1, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7543541,
   "Latitude": 106.6835677
 },
 {
   "STT": 27,
   "Name": "Bác sĩ Trần Thiện Vĩnh Quân Khoa Sản",
   "address": "28 Phan Huy Chú, phường 10, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7506679,
   "Latitude": 106.6618649
 },
 {
   "STT": 28,
   "Name": "Bác sĩ Võ Thị Thúy Vân Khoa Nhi",
   "address": "57/8 An Dương Vương, phường 8, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7567375,
   "Latitude": 106.6722122
 },
 {
   "STT": 29,
   "Name": "Bác sĩ Võ Văn Sĩ Khoa Chấn Thương Chỉnh Hình",
   "address": "28B Mạc Cửu, phường 13, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7494651,
   "Latitude": 106.6598227
 },
 {
   "STT": 30,
   "Name": "Bác sĩ Võ Đức Chiến Khoa Lao Phổi",
   "address": "281/5D Trần Bình Trọng, phường 4, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7612513,
   "Latitude": 106.6787238
 },
 {
   "STT": 31,
   "Name": "Bác sĩ Vũ Anh Nhị Khoa Thần Kinh, Tai Mũi Họng",
   "address": "273/16 Tô Hiến Thành, phường 13, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.775781,
   "Latitude": 106.6668229
 },
 {
   "STT": 32,
   "Name": "Bác sĩ Đinh Sơn Thắng  Khoa Nội Tổng Quát",
   "address": "245 Trần Bình Trọng, phường 4, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7604497,
   "Latitude": 106.6797353
 },
 {
   "STT": 33,
   "Name": "Bảo Sanh Viện Phước Sơn",
   "address": "7 Lê Quý Đôn, phường Bình Thọ, Thủ Đức, Hồ Chí Minh",
   "Longtitude": 10.840756,
   "Latitude": 106.7672127
 },
 {
   "STT": 34,
   "Name": "Bệnh Viện Đa Khoa Hoàn Hảo Chi Nhánh I (Phòng khám Đa Khoa)",
   "address": "1B Hoàng Hữu Nam, phường Long Thạnh Mỹ, Quận 9, Hồ Chí Minh",
   "Longtitude": 10.8511519,
   "Latitude": 106.813926
 },
 {
   "STT": 35,
   "Name": "Bệnh Viện Đa Khoa Khu Vực Hóc Môn  Phòng khám Đa Khoa (Cơ Sở 2)",
   "address": "159A Nguyễn Ảnh Thủ, xã Trung Chánh, huyện Hóc Môn, Hồ Chí Minh",
   "Longtitude": 10.8556983,
   "Latitude": 106.6074134
 },
 {
   "STT": 36,
   "Name": "Bùi Huy Phụng Chuyên khoa Xương Khớp",
   "address": "424 Cao Thắng, phường 12, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7750849,
   "Latitude": 106.6725844
 },
 {
   "STT": 37,
   "Name": "Chi Nhánh Nha Khoa Việt Giao",
   "address": "120 Ngô Quyền, phường 5, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7618209,
   "Latitude": 106.6651224
 },
 {
   "STT": 38,
   "Name": "Cơ Sở Dịch Vụ Y Tế  Y sĩ Nguyễn Thị Thu Thủy",
   "address": "137 Đặng Chất, phường 2, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.7468044,
   "Latitude": 106.6844102
 },
 {
   "STT": 39,
   "Name": "Cơ Sở Y Học Cổ Truyền Hoàng Quân",
   "address": "70 Nguyễn Thị Tú, phường Bình Hưng Hòa B, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.8160057,
   "Latitude": 106.5982675
 },
 {
   "STT": 40,
   "Name": "Cơ Sở Y Học Cổ Truyền Minh Hoàng",
   "address": "1594 Tỉnh Lộ 10, phường Tân Tạo, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.7583708,
   "Latitude": 106.5808176
 },
 {
   "STT": 41,
   "Name": "Cơ Sở Điều Trị Giảm Cân  Bác sĩ Huỳnh Bá Tản",
   "address": "126/10 Sư Vạn Hạnh, phường 12, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7748226,
   "Latitude": 106.6682775
 },
 {
   "STT": 42,
   "Name": "Cơ Sở Dịch Vụ Y Tế Bác sĩ Bùi Tiến Dũng",
   "address": "177/1 Tôn Thất Thuyết, phường 15, Quận 4, Hồ Chí Minh",
   "Longtitude": 10.753513,
   "Latitude": 106.705798
 },
 {
   "STT": 43,
   "Name": "Elite Dental Group",
   "address": "57A Trần Quốc Thảo, phường 7, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.78045,
   "Latitude": 106.689041
 },
 {
   "STT": 44,
   "Name": "Family Medical Practice Vietnam Phòng khám Diamond Plaza",
   "address": "Diamond Plaza, 34 Lê Duẩn, phường Bến Nghé, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.7806206,
   "Latitude": 106.6983943
 },
 {
   "STT": 45,
   "Name": "Family Medical Practice Vietnam Phòng khám Quận 2",
   "address": "95 Thảo Điền, phường Thảo Điền, Quận 2, Hồ Chí Minh",
   "Longtitude": 10.8073544,
   "Latitude": 106.7333595
 },
 {
   "STT": 46,
   "Name": "Hà Nhân Chuyên khoa Xét Nghiệm",
   "address": "15 Thuận Kiều, phường 12, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7560169,
   "Latitude": 106.6581826
 },
 {
   "STT": 47,
   "Name": "Hệ Thống Nha Khoa Quốc Tế Việt Mỹ Cs1",
   "address": "A14 Bà Hom, phường 13, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.754821,
   "Latitude": 106.629595
 },
 {
   "STT": 48,
   "Name": "Huỳnh Thị Duy Hương Chuyên khoa Nhi & Nhi Sơ Sinh",
   "address": "320/6 Trần Bình Trọng, phường 4, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7628143,
   "Latitude": 106.6797665
 },
 {
   "STT": 49,
   "Name": "Labo Răng Giả Nha Khoa Hoa Sứ",
   "address": "95 Phó Đức Chính, phường 1, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.798659,
   "Latitude": 106.697372
 },
 {
   "STT": 50,
   "Name": "Lien Tam Genaral Clinic",
   "address": "Đường số 10, phường 5, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.7417001,
   "Latitude": 106.6706045
 },
 {
   "STT": 51,
   "Name": "Lương Y Đa Khoa Quốc Gia",
   "address": "109 Lê Quốc Hưng, phường 12, Quận 4, Hồ Chí Minh",
   "Longtitude": 10.764375,
   "Latitude": 106.704451
 },
 {
   "STT": 52,
   "Name": "Maple Healthcare Quận 5",
   "address": "10 Trần Phú, phường 4, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7634705,
   "Latitude": 106.6797687
 },
 {
   "STT": 53,
   "Name": "Maple Healthcare Quận 7",
   "address": "MD6 Nguyễn Lương Bằng, phường Tân Phú, Quận 7, Hồ Chí Minh",
   "Longtitude": 10.7328821,
   "Latitude": 106.7196919
 },
 {
   "STT": 54,
   "Name": "Nguyễn Thị Thanh Tâm  Chuyên khoa Nội Tổng Quát",
   "address": "613/16 Ba Tháng Hai, phường 8, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7654745,
   "Latitude": 106.6629695
 },
 {
   "STT": 55,
   "Name": "Nhà Bảo Sanh Bác sĩ Nguyễn Kim Quỹ",
   "address": "255 Phan Văn Hớn, phường Tân Thới Nhất, Quận 12, Hồ Chí Minh",
   "Longtitude": 10.8310509,
   "Latitude": 106.6124439
 },
 {
   "STT": 56,
   "Name": "Nhà Bảo Sanh Tham Lương",
   "address": "17 Trường Chinh, phường Tân Thới Nhất, Quận 12, Hồ Chí Minh",
   "Longtitude": 10.834056,
   "Latitude": 106.620423
 },
 {
   "STT": 57,
   "Name": "Nhà Bảo Sanh Thanh Bình",
   "address": "141 Trần Thủ Độ, phường Phú Thạnh, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.782307,
   "Latitude": 106.626158
 },
 {
   "STT": 58,
   "Name": "Nhà Hộ Sinh Hồng Phước",
   "address": "66L Đặng Nguyên Cẩn, phường 14, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.7573488,
   "Latitude": 106.6346475
 },
 {
   "STT": 59,
   "Name": "Nhà Hộ Sinh Minh Vân",
   "address": "7 Tỉnh Lộ 8, khu phố 1, thị trấn Củ Chi, huyện Củ Chi, Hồ Chí Minh",
   "Longtitude": 10.9747373,
   "Latitude": 106.4963302
 },
 {
   "STT": 60,
   "Name": "Nha Khoa & Spa Hoàn Vũ",
   "address": "791 Trần Hưng Đạo, phường 1, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7551527,
   "Latitude": 106.6813159
 },
 {
   "STT": 61,
   "Name": "Nha Khoa Bác sĩ Bùi Hữu Lâm & Bác sĩ Vũ Thị Ngọc Hương",
   "address": "287 Nguyễn Chí Thanh, phường 15, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7570972,
   "Latitude": 106.6557057
 },
 {
   "STT": 62,
   "Name": "Nha Khoa Bác sĩ Nguyễn Minh Phụng",
   "address": "216A Hậu Giang, phường 9, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.749909,
   "Latitude": 106.641588
 },
 {
   "STT": 63,
   "Name": "Nha Khoa Bác sĩ Nguyễn Phước Thọ",
   "address": "244 Bis Bà Hom, phường 13, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.754852,
   "Latitude": 106.6306994
 },
 {
   "STT": 64,
   "Name": "Nha Khoa Bác sĩ Nguyễn Thành Được",
   "address": "64 Đường số 11, cư xá Đài Ra Đa, phường 13, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.7531479,
   "Latitude": 106.6258792
 },
 {
   "STT": 65,
   "Name": "Nha Khoa Bác sĩ Nguyễn Thị Thu Lan",
   "address": "273B Hậu Giang, phường 5, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.749585,
   "Latitude": 106.642606
 },
 {
   "STT": 66,
   "Name": "Nha Khoa Bác sĩ Phạm Thị Hai",
   "address": "29/7 Quốc Lộ 22, xã Bà Điểm, huyện Hóc Môn, Hồ Chí Minh",
   "Longtitude": 10.8461405,
   "Latitude": 106.6129642
 },
 {
   "STT": 67,
   "Name": "Nha Khoa Bác sĩ Tại",
   "address": "551/30A Bến Phú Lâm, phường 9, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.7520314,
   "Latitude": 106.6419899
 },
 {
   "STT": 68,
   "Name": "Nha Khoa Bác sĩ Trúc",
   "address": "27S Bình Phú, phường 10, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.739682,
   "Latitude": 106.629158
 },
 {
   "STT": 69,
   "Name": "Nha Khoa Bác sĩ Khôi",
   "address": "107A Đường Số 154, phường Tân Phú, Quận 9, Hồ Chí Minh",
   "Longtitude": 10.8673561,
   "Latitude": 106.8083592
 },
 {
   "STT": 70,
   "Name": "Nha Khoa  Bác sĩ Kim Cương & Phòng Chẩn Trị Y Học Cổ Truyền  Lương Y Phùng Lưu Nhuận",
   "address": "282 Trần Phú, phường 8, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7567626,
   "Latitude": 106.6731952
 },
 {
   "STT": 71,
   "Name": "Nha Khoa Bác sĩ Lê Anh Ngọc",
   "address": "666/18C Đường 3 Tháng 2, phường 6, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7654745,
   "Latitude": 106.6629694
 },
 {
   "STT": 72,
   "Name": "Nha Khoa Bác sĩ Lê Văn Tâm",
   "address": "11 Trương Văn Ngư, phường Linh Tây, Thủ Đức, Hồ Chí Minh",
   "Longtitude": 10.850432,
   "Latitude": 106.7534183
 },
 {
   "STT": 73,
   "Name": "Nha Khoa Bác sĩ Ngô Quốc Hùng",
   "address": "51 Nguyễn Súy, phường Tân Quý, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.791417,
   "Latitude": 106.6223737
 },
 {
   "STT": 74,
   "Name": "Nha Khoa Bác sĩ Ngô Thị Thúy Lụa",
   "address": "153 Ngô Quyền, phường Hiệp Phú, Quận 9, Hồ Chí Minh",
   "Longtitude": 10.8448605,
   "Latitude": 106.7753061
 },
 {
   "STT": 75,
   "Name": "Nha Khoa Bác sĩ Nguyễn Thị Minh Sang",
   "address": "19/9 Nguyễn Thị Minh Khai, phường Bến Nghé, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.7869455,
   "Latitude": 106.7022543
 },
 {
   "STT": 76,
   "Name": "Nha Khoa Bác sĩ Nguyễn Văn Thiêm",
   "address": "85 Cách Mạng Tháng Tám, phường Bến Thành, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.7723388,
   "Latitude": 106.69147
 },
 {
   "STT": 77,
   "Name": "Nha Khoa Bác sĩ Nguyễn Văn Thu",
   "address": "897 Phạm Thế Hiển, phường 4, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.743589,
   "Latitude": 106.6720409
 },
 {
   "STT": 78,
   "Name": "Nha Khoa Bác sĩ Nguyễn Đình Bửu",
   "address": "B6/5 Lương Định Của, phường Bình An, Quận 2, Hồ Chí Minh",
   "Longtitude": 10.784354,
   "Latitude": 106.727647
 },
 {
   "STT": 79,
   "Name": "Nha Khoa Bác sĩ Sophie",
   "address": "203 Nguyễn Thị Định, phường Bình Trưng Tây, Quận 2, Hồ Chí Minh",
   "Longtitude": 10.79173,
   "Latitude": 106.753126
 },
 {
   "STT": 80,
   "Name": "Nha Khoa Bác sĩ Vương Gia Hiệp Vũ",
   "address": "108 Lê Sao, phường Phú Thạnh, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.782035,
   "Latitude": 106.62645
 },
 {
   "STT": 81,
   "Name": "Nha Khoa Bác sĩ Đỗ Thị Hồng Khánh",
   "address": "463 Nơ Trang Long, phường 13, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.8211993,
   "Latitude": 106.7055494
 },
 {
   "STT": 82,
   "Name": "Nha Khoa Bác sĩ Đông Hải",
   "address": "117 Đường 1011 Phạm Thế Hiển, phường 5, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.7410243,
   "Latitude": 106.6657233
 },
 {
   "STT": 83,
   "Name": "Nha Khoa 017",
   "address": "Chung cư CMT8 17 Trường Chinh, phường 8, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.793246,
   "Latitude": 106.652494
 },
 {
   "STT": 84,
   "Name": "Nha Khoa 07",
   "address": "401 Nguyễn Thị Thập, phường Tân Phong, Quận 7, Hồ Chí Minh",
   "Longtitude": 10.7396803,
   "Latitude": 106.7051805
 },
 {
   "STT": 85,
   "Name": "Nha Khoa 111 Bác sĩ Vũ Khắc Đoan",
   "address": "111 Phan Văn Trị, phường 2, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7570669,
   "Latitude": 106.6827758
 },
 {
   "STT": 86,
   "Name": "Nha Khoa 118",
   "address": "118 Lý Nam Đế, phường 7, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.7623014,
   "Latitude": 106.6590928
 },
 {
   "STT": 87,
   "Name": "Nha Khoa 126",
   "address": "126 Nguyễn Cư Trinh, phường Nguyễn Cư Trinh, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.763914,
   "Latitude": 106.689419
 },
 {
   "STT": 88,
   "Name": "Nha Khoa 179",
   "address": "179 Nguyễn Văn Nghi, phường 7, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8262613,
   "Latitude": 106.6820845
 },
 {
   "STT": 89,
   "Name": "Nha Khoa 195",
   "address": "195 Lãnh Binh Thăng, phường 12, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.762406,
   "Latitude": 106.652995
 },
 {
   "STT": 90,
   "Name": "Nha Khoa 2000 Cơ Sở 1",
   "address": "99 Hồ Hảo Hớn, phường Cô Giang, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.76259,
   "Latitude": 106.691006
 },
 {
   "STT": 91,
   "Name": "Nha Khoa 2000 Cơ Sở 2",
   "address": "502 Ngô Gia Tự, phường 9, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.758917,
   "Latitude": 106.6681963
 },
 {
   "STT": 92,
   "Name": "Nha Khoa 212",
   "address": "212 Điện Biên Phủ, phường 7, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.78287,
   "Latitude": 106.690063
 },
 {
   "STT": 93,
   "Name": "Nha Khoa 233",
   "address": "233 Nguyễn Thiện Thuật, phường 1, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.7690462,
   "Latitude": 106.6783978
 },
 {
   "STT": 94,
   "Name": "Nha Khoa 264 Phạm Cơ Điều",
   "address": "264 Phó Cơ Điều, phường 6, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.760548,
   "Latitude": 106.655851
 },
 {
   "STT": 95,
   "Name": "Nha Khoa 280",
   "address": "280 Lạc Long Quân, phường 10, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.766291,
   "Latitude": 106.642516
 },
 {
   "STT": 96,
   "Name": "Nha Khoa 288",
   "address": "288 Nguyễn Văn Đậu, phường 11, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.8138102,
   "Latitude": 106.6937753
 },
 {
   "STT": 97,
   "Name": "Nha Khoa 294 Hàng Xanh",
   "address": "294 Xô Viết Nghệ Tĩnh, phường 25, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.802576,
   "Latitude": 106.711512
 },
 {
   "STT": 98,
   "Name": "Nha Khoa 3 2",
   "address": "74 đường 3 tháng 2, phường 12, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7764392,
   "Latitude": 106.6803424
 },
 {
   "STT": 99,
   "Name": "Nha Khoa 3 Nhất",
   "address": "538 Ngô Gia Tự, phường 9, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.763653,
   "Latitude": 106.670914
 },
 {
   "STT": 100,
   "Name": "Nha Khoa 311",
   "address": "311 Đường số 7, phường Bình Trị Đông B, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.7516137,
   "Latitude": 106.6116051
 },
 {
   "STT": 101,
   "Name": "Nha Khoa 34",
   "address": "34 Nguyễn Trường Tộ, phường 12, Quận 4, Hồ Chí Minh",
   "Longtitude": 10.765568,
   "Latitude": 106.704583
 },
 {
   "STT": 102,
   "Name": "Nha Khoa 343",
   "address": "343 Tân Kỳ Tân Quý, phường Tân Quý, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.800857,
   "Latitude": 106.623332
 },
 {
   "STT": 103,
   "Name": "Nha Khoa 3d",
   "address": "889 Quốc Lộ 22, xã Tân An Hội, huyện Củ Chi, Hồ Chí Minh",
   "Longtitude": 10.9717383,
   "Latitude": 106.4810693
 },
 {
   "STT": 104,
   "Name": "Nha Khoa 3t",
   "address": "6 Nguyễn Cửu Đàm, phường Tân Sơn Nhì, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.7987068,
   "Latitude": 106.6314042
 },
 {
   "STT": 105,
   "Name": "Nha Khoa 44 Gia Phú",
   "address": "44 Gia Phú, phường 13, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7477162,
   "Latitude": 106.6547447
 },
 {
   "STT": 106,
   "Name": "Nha Khoa 440",
   "address": "440 Võ Văn Tần, phường 5, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.7694908,
   "Latitude": 106.683461
 },
 {
   "STT": 107,
   "Name": "Nha Khoa 471",
   "address": "471 Lê Văn Sỹ, phường 13, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.78699,
   "Latitude": 106.6757669
 },
 {
   "STT": 108,
   "Name": "Nha Khoa 49 Lương Trúc Đàm",
   "address": "49 Lương Trúc Đàm, phường Hiệp Tân, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.7747119,
   "Latitude": 106.629666
 },
 {
   "STT": 109,
   "Name": "Nha Khoa 68",
   "address": "68 Hàn Hải Nguyên, phường 8, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.758196,
   "Latitude": 106.6489129
 },
 {
   "STT": 110,
   "Name": "Nha Khoa 75",
   "address": "75 Trần Mai Ninh, phường 12, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.792574,
   "Latitude": 106.647675
 },
 {
   "STT": 111,
   "Name": "Nha Khoa 81",
   "address": "81 Nguyễn Tiểu La, phường 5, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7616735,
   "Latitude": 106.6666498
 },
 {
   "STT": 112,
   "Name": "Nha Khoa 811",
   "address": "811/1 Lũy Bán Bích, phường Tân Thành, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.789508,
   "Latitude": 106.63706
 },
 {
   "STT": 113,
   "Name": "Nha Khoa 83a Bà Hom",
   "address": "83A Bà Hom, phường 13, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.754348,
   "Latitude": 106.632109
 },
 {
   "STT": 114,
   "Name": "Nha Khoa 90",
   "address": "90 Đường số 11, phường 13, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.7532624,
   "Latitude": 106.6254584
 },
 {
   "STT": 115,
   "Name": "Nha Khoa 92",
   "address": "92 Vạn Kiếp, phường 3, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.799826,
   "Latitude": 106.694222
 },
 {
   "STT": 116,
   "Name": "Nha Khoa 94 Chợ Thiếc",
   "address": "94 Phó Cơ Điều, phường 4, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.758767,
   "Latitude": 106.657261
 },
 {
   "STT": 117,
   "Name": "Nha Khoa 98",
   "address": "416 Âu Cơ, phường 10, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.7783941,
   "Latitude": 106.6455969
 },
 {
   "STT": 118,
   "Name": "Nha Khoa Á Âu",
   "address": "260 Đặng Văn Bi, phường Bình Thọ, Thủ Đức, Hồ Chí Minh",
   "Longtitude": 10.850463,
   "Latitude": 106.75879
 },
 {
   "STT": 119,
   "Name": "Nha Khoa Á Âu",
   "address": "51 Tô Ngọc Vân, phường Linh Tây, Thủ Đức, Hồ Chí Minh",
   "Longtitude": 10.851612,
   "Latitude": 106.753024
 },
 {
   "STT": 120,
   "Name": "Nha Khoa Á Âu",
   "address": "937 Lê Đức Thọ, phường 16, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8515132,
   "Latitude": 106.662211
 },
 {
   "STT": 121,
   "Name": "Nha Khoa Á Châu",
   "address": "441B Hai Bà Trưng, phường 8, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.791131,
   "Latitude": 106.6876659
 },
 {
   "STT": 122,
   "Name": "Nha Khoa Á Đông",
   "address": "546 An Dương Vương, phường 10, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.7546774,
   "Latitude": 106.6242716
 },
 {
   "STT": 123,
   "Name": "Nha Khoa Á Đông",
   "address": "755 Đường 3 Tháng 2, phường 7, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7644269,
   "Latitude": 106.661315
 },
 {
   "STT": 124,
   "Name": "Nha Khoa Á Đông",
   "address": "196 Lãnh Binh Thăng, phường 13, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.762694,
   "Latitude": 106.653266
 },
 {
   "STT": 125,
   "Name": "Nha Khoa A.B.C",
   "address": "39 Thích Quảng Đức, phường 4, Phú Nhuận, Hồ Chí Minh",
   "Longtitude": 10.804624,
   "Latitude": 106.683017
 },
 {
   "STT": 126,
   "Name": "Nha Khoa A1",
   "address": "388 Lê Đức Thọ, phường 6, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.841806,
   "Latitude": 106.677169
 },
 {
   "STT": 127,
   "Name": "Nha Khoa AAA",
   "address": "204A Sư Vạn Hạnh, phường 9, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7599847,
   "Latitude": 106.6734802
 },
 {
   "STT": 128,
   "Name": "Nha Khoa ABC",
   "address": "785 Đường 3 Tháng 2, phường 7, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7640908,
   "Latitude": 106.660744
 },
 {
   "STT": 129,
   "Name": "Nha Khoa ADC",
   "address": "160 Hòa Hưng, phường 13, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7791979,
   "Latitude": 106.67407
 },
 {
   "STT": 130,
   "Name": "Nha Khoa Amy",
   "address": "166B Trần Đình Xu, phường Nguyễn Cư Trinh, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.7631789,
   "Latitude": 106.6875699
 },
 {
   "STT": 131,
   "Name": "Nha Khoa An Bình",
   "address": "700A Nguyễn Xiển, phường Long Thạnh Mỹ, Quận 9, Hồ Chí Minh",
   "Longtitude": 10.8524852,
   "Latitude": 106.8333492
 },
 {
   "STT": 132,
   "Name": "Nha Khoa An Bình",
   "address": "52A Tân Hòa Đông, phường 14, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.756276,
   "Latitude": 106.6327079
 },
 {
   "STT": 133,
   "Name": "Nha Khoa An Bình",
   "address": "295 Quốc lộ 50, phường 5, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.7348437,
   "Latitude": 106.6562539
 },
 {
   "STT": 134,
   "Name": "Nha Khoa An Bình Sài Gòn",
   "address": "110B An Bình, phường 5, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7520275,
   "Latitude": 106.6725673
 },
 {
   "STT": 135,
   "Name": "Nha Khoa An Dương Vương",
   "address": "351 An Dương Vương, phường 10, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.738983,
   "Latitude": 106.6223785
 },
 {
   "STT": 136,
   "Name": "Nha Khoa An Hòa",
   "address": "135B Trương Phước Phan, phường Bình Trị Đông, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.7641606,
   "Latitude": 106.613152
 },
 {
   "STT": 137,
   "Name": "Nha Khoa An Hội",
   "address": "441 Phan Huy Ích, phường 14, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8403508,
   "Latitude": 106.6376551
 },
 {
   "STT": 138,
   "Name": "Nha Khoa An Khang",
   "address": "366 Cách Mạng Tháng Tám, phường 10, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.7803257,
   "Latitude": 106.6765092
 },
 {
   "STT": 139,
   "Name": "Nha Khoa An Khang",
   "address": "49 đường số 3, phường Bình An, Quận 2, Hồ Chí Minh",
   "Longtitude": 10.7952731,
   "Latitude": 106.73464
 },
 {
   "STT": 140,
   "Name": "Nha Khoa An Lạc",
   "address": "481 Kinh Dương Vương, phường An Lạc, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.7380299,
   "Latitude": 106.6151321
 },
 {
   "STT": 141,
   "Name": "Nha Khoa An Mỹ",
   "address": "384 Phạm Hùng, phường 5, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.7364677,
   "Latitude": 106.6715358
 },
 {
   "STT": 142,
   "Name": "Nha Khoa An Nhiên",
   "address": "240 Phạm Văn Chí, phường 4, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.7447574,
   "Latitude": 106.6462204
 },
 {
   "STT": 143,
   "Name": "Nha Khoa An Nhơn",
   "address": "291 Nguyễn Oanh, phường 17, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8446609,
   "Latitude": 106.677546
 },
 {
   "STT": 144,
   "Name": "Nha Khoa An Phú",
   "address": "127 Đường Số 154, phường Tân Phú, Quận 9, Hồ Chí Minh",
   "Longtitude": 10.8672973,
   "Latitude": 106.8087506
 },
 {
   "STT": 145,
   "Name": "Nha Khoa An Phú",
   "address": "25 LK 5-6, phường Bình Hưng Hòa B, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.7895644,
   "Latitude": 106.5925579
 },
 {
   "STT": 146,
   "Name": "Nha Khoa An Phước",
   "address": "293 Nguyễn Oanh, phường 6, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.844661,
   "Latitude": 106.677546
 },
 {
   "STT": 147,
   "Name": "Nha Khoa An Phước",
   "address": "114 Phan Huy Ích, phường 15, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.835392,
   "Latitude": 106.6353424
 },
 {
   "STT": 148,
   "Name": "Nha Khoa Ân Tâm",
   "address": "63 Trần Quốc Hoàn, phường 4, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.8057684,
   "Latitude": 106.6641745
 },
 {
   "STT": 149,
   "Name": "Nha Khoa An Tâm Sài Gòn",
   "address": "11/9 Bông Sao, phường 5, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.740891,
   "Latitude": 106.66141
 },
 {
   "STT": 150,
   "Name": "Nha Khoa An Tâm Sài Gòn",
   "address": "44 Nguyễn Biểu, phường 1, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7544514,
   "Latitude": 106.6843321
 },
 {
   "STT": 151,
   "Name": "Nha Khoa An Tiêm",
   "address": "67 Trung Mỹ Tây 19, phường Trung Mỹ Tây, Quận 12, Hồ Chí Minh",
   "Longtitude": 10.8646411,
   "Latitude": 106.6141467
 },
 {
   "STT": 152,
   "Name": "Nha Khoa An Việt Cs2",
   "address": "251 Đường 9A Khu dân cư Trung Sơn, xã Bình Hưng, huyện Bình Chánh, Hồ Chí Minh",
   "Longtitude": 10.7323699,
   "Latitude": 106.6896965
 },
 {
   "STT": 153,
   "Name": "Nha Khoa Ân Đức",
   "address": "77/1 Tô Ngọc Vân, phường Thạnh Xuân, Quận 12, Hồ Chí Minh",
   "Longtitude": 10.8571224,
   "Latitude": 106.6707232
 },
 {
   "STT": 154,
   "Name": "Nha Khoa An Đức",
   "address": "92 Ngô Thị Thu Minh, phường 2, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.7972931,
   "Latitude": 106.6626002
 },
 {
   "STT": 155,
   "Name": "Nha Khoa Ánh Dương",
   "address": "362 Đường 3 Tháng 2, phường 12, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7694695,
   "Latitude": 106.6700833
 },
 {
   "STT": 156,
   "Name": "Nha Khoa Ánh Dương",
   "address": "8 Đường Số 4, phường An Lạc A, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.7487678,
   "Latitude": 106.6205609
 },
 {
   "STT": 157,
   "Name": "Nha Khoa Ánh Dương",
   "address": "225 Dương Bá Trạc, phường 1, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.744292,
   "Latitude": 106.6920648
 },
 {
   "STT": 158,
   "Name": "Nha Khoa Anh Huân",
   "address": "133 Nguyễn Hồng Đào, phường 14, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.7941765,
   "Latitude": 106.6426475
 },
 {
   "STT": 159,
   "Name": "Nha Khoa Anh Kiệt",
   "address": "905 Quốc Lộ 22, xã Tân An Hội, huyện Củ Chi, Hồ Chí Minh",
   "Longtitude": 10.972165,
   "Latitude": 106.480537
 },
 {
   "STT": 160,
   "Name": "Nha Khoa Anh Thư",
   "address": "566 Lê Quang Sung, phường 9, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.7521768,
   "Latitude": 106.6393225
 },
 {
   "STT": 161,
   "Name": "Nha Khoa Anh Trinh",
   "address": "646 Phạm Văn Chiêu, phường 13, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8528964,
   "Latitude": 106.6619809
 },
 {
   "STT": 162,
   "Name": "Nha Khoa Anh Tú",
   "address": "20 Nguyễn Văn Lượng, phường 17, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.838681,
   "Latitude": 106.6794369
 },
 {
   "STT": 163,
   "Name": "Nha Khoa Ánh Vy",
   "address": "373/57 Lý Thường Kiệt, phường 9, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.779813,
   "Latitude": 106.652688
 },
 {
   "STT": 164,
   "Name": "Nha Khoa Ao Sen",
   "address": "106 Đường Số 28, phường Bình Trị Đông B, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.747072,
   "Latitude": 106.611609
 },
 {
   "STT": 165,
   "Name": "Nha Khoa Apona",
   "address": "128 Nguyễn Công Trứ, phường Nguyễn Thái Bình, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.769277,
   "Latitude": 106.702012
 },
 {
   "STT": 166,
   "Name": "Nha Khoa Apple 1",
   "address": "475 Cách Mạng Tháng Tám, phường 13, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7820437,
   "Latitude": 106.6736061
 },
 {
   "STT": 167,
   "Name": "Nha Khoa Apt",
   "address": "195C Nguyễn Chí Thanh, phường 12, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7583136,
   "Latitude": 106.6620978
 },
 {
   "STT": 168,
   "Name": "Nha Khoa Asean",
   "address": "282 - 284 Phan Xích Long, phường 2, Phú Nhuận, Hồ Chí Minh",
   "Longtitude": 10.7971637,
   "Latitude": 106.6913887
 },
 {
   "STT": 169,
   "Name": "Nha Khoa Âu Lạc",
   "address": "626 Lạc Long Quân, phường 9, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.7775574,
   "Latitude": 106.649836
 },
 {
   "STT": 170,
   "Name": "Nha Khoa Bà Chiểu",
   "address": "332 Bạch Đằng, phường 14, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.803418,
   "Latitude": 106.6999945
 },
 {
   "STT": 171,
   "Name": "Nha Khoa Ba Lê 2",
   "address": "370 Lê Văn Sỹ, phường 14, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.787723,
   "Latitude": 106.67843
 },
 {
   "STT": 172,
   "Name": "Nha Khoa Ba Đình",
   "address": "268 Ba Đình, phường 10, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.746857,
   "Latitude": 106.664637
 },
 {
   "STT": 173,
   "Name": "Nha Khoa Bách Phước",
   "address": "5A Trường Chinh, phường Tân Thới Nhất, Quận 12, Hồ Chí Minh",
   "Longtitude": 10.842224,
   "Latitude": 106.615106
 },
 {
   "STT": 174,
   "Name": "Nha Khoa Bách Việt",
   "address": "241 Nguyễn Thiện Thuật, phường 1, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.769204,
   "Latitude": 106.6783404
 },
 {
   "STT": 175,
   "Name": "Nha Khoa Bách Việt",
   "address": "175 Nguyễn Cư Trinh, phường Nguyễn Cư Trinh, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.762951,
   "Latitude": 106.6882
 },
 {
   "STT": 176,
   "Name": "Nha Khoa Bàn Cờ",
   "address": "141/24 Bàn Cờ, phường 3, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.7709628,
   "Latitude": 106.6783452
 },
 {
   "STT": 177,
   "Name": "Nha Khoa Băng Thanh",
   "address": "529/3 Huỳnh Văn Bánh, phường 14, Phú Nhuận, Hồ Chí Minh",
   "Longtitude": 10.791793,
   "Latitude": 106.668969
 },
 {
   "STT": 178,
   "Name": "Nha Khoa Bảo An",
   "address": "757C Kha Vạn Cân, phường Linh Tây, Thủ Đức, Hồ Chí Minh",
   "Longtitude": 10.8516164,
   "Latitude": 106.7548344
 },
 {
   "STT": 179,
   "Name": "Nha Khoa Bảo Anh",
   "address": "428-430 Lê Văn Việt, phường Tăng Nhơn Phú A, Quận 9, Hồ Chí Minh",
   "Longtitude": 10.8456611,
   "Latitude": 106.7954887
 },
 {
   "STT": 180,
   "Name": "Nha Khoa Bảo Châu",
   "address": "28 Nguyễn Trọng Tuyển, phường 15, Phú Nhuận, Hồ Chí Minh",
   "Longtitude": 10.797273,
   "Latitude": 106.680425
 },
 {
   "STT": 181,
   "Name": "Nha Khoa Bảo Châu",
   "address": "247 Trần Bình Trọng, phường 4, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7604893,
   "Latitude": 106.6797235
 },
 {
   "STT": 182,
   "Name": "Nha Khoa Bảo Hân",
   "address": "1069 Phan Văn Trị, phường 10, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8309109,
   "Latitude": 106.6767403
 },
 {
   "STT": 183,
   "Name": "Nha Khoa Bảo Hân",
   "address": "106 Tôn Thất Thuyết, phường 15, Quận 4, Hồ Chí Minh",
   "Longtitude": 10.753421,
   "Latitude": 106.709436
 },
 {
   "STT": 184,
   "Name": "Nha Khoa Bảo Hiến",
   "address": "430 Quốc Lộ 13, phường Hiệp Bình Phước, Thủ Đức, Hồ Chí Minh",
   "Longtitude": 10.851039,
   "Latitude": 106.7198166
 },
 {
   "STT": 185,
   "Name": "Nha Khoa Bảo Nam",
   "address": "10 lâm Văn Bền, phường Tân Kiểng, Quận 7, Hồ Chí Minh",
   "Longtitude": 10.7502211,
   "Latitude": 106.7158806
 },
 {
   "STT": 186,
   "Name": "Nha Khoa Bảo Nghi",
   "address": "13 Đường D3, phường Phước Long B, Quận 9, Hồ Chí Minh",
   "Longtitude": 10.8062757,
   "Latitude": 106.778501
 },
 {
   "STT": 187,
   "Name": "Nha Khoa Bảo Ngọc",
   "address": "605 Nguyễn Duy Trinh, phường Bình Trưng Đông, Quận 2, Hồ Chí Minh",
   "Longtitude": 10.7883405,
   "Latitude": 106.7737634
 },
 {
   "STT": 188,
   "Name": "Nha Khoa Bảo Ngọc Cơ Sở 1",
   "address": "24/4 Nguyễn Cảnh Chân, phường Cầu Kho, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.7590273,
   "Latitude": 106.6884428
 },
 {
   "STT": 189,
   "Name": "Nha Khoa Bảo Ngọc Cơ Sở 2",
   "address": "177 Quốc Lộ 13, phường Hiệp Bình Chánh, Thủ Đức, Hồ Chí Minh",
   "Longtitude": 10.8508955,
   "Latitude": 106.7197145
 },
 {
   "STT": 190,
   "Name": "Nha Khoa Bảo Nha",
   "address": "232 Ba Tháng Hai, phường 12, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7738158,
   "Latitude": 106.6775064
 },
 {
   "STT": 191,
   "Name": "Nha Khoa Bảo Nhân",
   "address": "456 Nguyễn Thái Sơn, phường 5, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8273778,
   "Latitude": 106.6904551
 },
 {
   "STT": 192,
   "Name": "Nha Khoa Bảo Nhi",
   "address": "29 Lê Văn Việt, phường Hiệp Phú, Quận 9, Hồ Chí Minh",
   "Longtitude": 10.847529,
   "Latitude": 106.7761128
 },
 {
   "STT": 193,
   "Name": "Nha Khoa Bảo Tâm",
   "address": "324 Nguyễn Oanh, phường 17, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8403698,
   "Latitude": 106.6761063
 },
 {
   "STT": 194,
   "Name": "Nha Khoa Bảo Thi",
   "address": "15 Lê Văn Sỹ, phường 13, Phú Nhuận, Hồ Chí Minh",
   "Longtitude": 10.7902883,
   "Latitude": 106.6735661
 },
 {
   "STT": 195,
   "Name": "Nha Khoa Bảo Thu",
   "address": "36 Lê Văn Thọ, phường 11, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8377912,
   "Latitude": 106.6580046
 },
 {
   "STT": 196,
   "Name": "Nha Khoa Bảo Tín",
   "address": "283 Trường Chinh, phường Tân Thới Nhất, Quận 12, Hồ Chí Minh",
   "Longtitude": 10.8350763,
   "Latitude": 106.6198897
 },
 {
   "STT": 197,
   "Name": "Nha Khoa Bảo Việt Cs Lê Văn Việt",
   "address": "20-22 Lê Văn Việt, phường Hiệp Phú, Quận 9, Hồ Chí Minh",
   "Longtitude": 10.8478509,
   "Latitude": 106.7753504
 },
 {
   "STT": 198,
   "Name": "Nha Khoa Bảo Việt Cs Thống Nhất",
   "address": "169 Thống Nhất, phường Bình Thọ, Thủ Đức, Hồ Chí Minh",
   "Longtitude": 10.8491009,
   "Latitude": 106.771717
 },
 {
   "STT": 199,
   "Name": "Nha Khoa Bến Phú Định",
   "address": "6C Bến Phú Định, phường 16, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.7107403,
   "Latitude": 106.6196444
 },
 {
   "STT": 200,
   "Name": "Nha Khoa Beverly Hills Bích Ngọc",
   "address": "523 Lê Hồng Phong, phường 2, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.767149,
   "Latitude": 106.67435
 },
 {
   "STT": 201,
   "Name": "Nha Khoa Bf",
   "address": "155 Khu phố Nam Thiên 3, Phạm Thái Bường, phường Tân Phong, Quận 7, Hồ Chí Minh",
   "Longtitude": 10.7272352,
   "Latitude": 106.7058797
 },
 {
   "STT": 202,
   "Name": "Nha Khoa Bích Ngọc",
   "address": "137/4 Ngô Quyền, phường 6, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7594841,
   "Latitude": 106.6656346
 },
 {
   "STT": 203,
   "Name": "Nha Khoa Bình Chánh",
   "address": "2142 Đường Vĩnh Lộc, xã Vĩnh Lộc A, huyện Bình Chánh, Hồ Chí Minh",
   "Longtitude": 10.8110685,
   "Latitude": 106.5754831
 },
 {
   "STT": 204,
   "Name": "Nha Khoa Bình Hưng",
   "address": "10/1A Quốc lộ 50, xã Bình Hưng, huyện Bình Chánh, Hồ Chí Minh",
   "Longtitude": 10.6917747,
   "Latitude": 106.6548981
 },
 {
   "STT": 205,
   "Name": "Nha Khoa Bình Lợi",
   "address": "3054 Phạm Thế Hiển, phường 7, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.7068011,
   "Latitude": 106.622899
 },
 {
   "STT": 206,
   "Name": "Nha Khoa Bình Minh",
   "address": "54B Tân Quý, phường Tân Quý, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.7961316,
   "Latitude": 106.6198946
 },
 {
   "STT": 207,
   "Name": "Nha Khoa Bình Phú",
   "address": "43B Cư Xá Phú Lâm D, phường 10, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.740872,
   "Latitude": 106.631844
 },
 {
   "STT": 208,
   "Name": "Nha Khoa Bình Phú",
   "address": "43 Lý Chiêu Hoàng, phường 10, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.739413,
   "Latitude": 106.631083
 },
 {
   "STT": 209,
   "Name": "Nha Khoa Bình Tâm",
   "address": "787 Lê Trọng Tấn, phường Bình Hưng Hòa, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.8158991,
   "Latitude": 106.6017891
 },
 {
   "STT": 210,
   "Name": "Nha Khoa Bình Tây",
   "address": "62 Nguyễn Thị Nhỏ, phường 14, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7530029,
   "Latitude": 106.6508552
 },
 {
   "STT": 211,
   "Name": "Nha Khoa Bình Thành",
   "address": "171 Đường Liên Khu 4-5, phường Bình Hưng Hòa B, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.7984598,
   "Latitude": 106.591685
 },
 {
   "STT": 212,
   "Name": "Nha Khoa Bình Thới",
   "address": "216 Lạc Long Quân, phường 10, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.7622634,
   "Latitude": 106.6426134
 },
 {
   "STT": 213,
   "Name": "Nha Khoa Bình Tiên",
   "address": "303A Bình Tiên, phường 8, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.746517,
   "Latitude": 106.642518
 },
 {
   "STT": 214,
   "Name": "Nha Khoa Bình Trị",
   "address": "761 Tỉnh Lộ 10, phường Bình Trị Đông B, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.7577443,
   "Latitude": 106.6098663
 },
 {
   "STT": 215,
   "Name": "Nha Khoa Bình Đăng",
   "address": "75 Quốc Lộ 50, phường 5, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.7389957,
   "Latitude": 106.6563805
 },
 {
   "STT": 216,
   "Name": "Nha Khoa Bình Đông",
   "address": "306 Bến Bình Đông, phường 15, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.736731,
   "Latitude": 106.640087
 },
 {
   "STT": 217,
   "Name": "Nha Khoa Bội Châu",
   "address": "931 Hồng Bàng, phường 9, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.7543949,
   "Latitude": 106.6395119
 },
 {
   "STT": 218,
   "Name": "Nha Khoa Bốn Xã",
   "address": "9 Lê Văn Quới, phường Bình Trị Đông, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.7745683,
   "Latitude": 106.6188147
 },
 {
   "STT": 219,
   "Name": "Nha Khoa Bác sĩ Nguyễn Ngọc Hoa",
   "address": "61 Tản Đà, phường 10, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7519002,
   "Latitude": 106.6642625
 },
 {
   "STT": 220,
   "Name": "Nha Khoa Bác sĩ Nguyễn Thị Khen",
   "address": "A35 Cống Quỳnh, phường Nguyễn Cư Trinh, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.766577,
   "Latitude": 106.687828
 },
 {
   "STT": 221,
   "Name": "Nha Khoa Bác sĩ Nguyễn Thị Kim Sa",
   "address": "945 Nguyễn Trãi, phường 14, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7526301,
   "Latitude": 106.6549531
 },
 {
   "STT": 222,
   "Name": "Nha Khoa Bác sĩ Trương Văn Ngọc",
   "address": "90G Võ Thị Sáu, phường Tân Định, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.788558,
   "Latitude": 106.692449
 },
 {
   "STT": 223,
   "Name": "Nha Khoa Bác sĩ Ánh Hòa",
   "address": "24 Lê Khôi, phường Phú Thạnh, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.779038,
   "Latitude": 106.627959
 },
 {
   "STT": 224,
   "Name": "Nha Khoa Bác sĩ Hoàng Trung Hiếu",
   "address": "142 Bình Quới, phường 27, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.81825,
   "Latitude": 106.723139
 },
 {
   "STT": 225,
   "Name": "Nha Khoa Bác sĩ Nguyễn Bá Hiền",
   "address": "205 Bà Hạt, phường 9, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7661855,
   "Latitude": 106.6700468
 },
 {
   "STT": 226,
   "Name": "Nha Khoa Bác sĩ Nguyễn Hữu Nhân",
   "address": "529/83 Huỳnh Văn Bánh, phường 14, Phú Nhuận, Hồ Chí Minh",
   "Longtitude": 10.790791,
   "Latitude": 106.668198
 },
 {
   "STT": 227,
   "Name": "Nha Khoa Bác sĩ Nguyễn Thị Cẩm Bình",
   "address": "59 Trần Huy Liệu, phường 12, Phú Nhuận, Hồ Chí Minh",
   "Longtitude": 10.791902,
   "Latitude": 106.677903
 },
 {
   "STT": 228,
   "Name": "Nha Khoa Bác sĩ Nguyễn Thị Mai Trinh",
   "address": "118A Hoàng Hoa Thám, phường 7, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.807942,
   "Latitude": 106.688925
 },
 {
   "STT": 229,
   "Name": "Nha Khoa Bác sĩ Nguyễn Văn Hải",
   "address": "489 Nguyễn Kiệm, phường 9, Phú Nhuận, Hồ Chí Minh",
   "Longtitude": 10.802901,
   "Latitude": 106.678933
 },
 {
   "STT": 230,
   "Name": "Nha Khoa Bác sĩ Tạ Thị Hoàng Oanh",
   "address": "496E Âu Cơ, phường 10, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.7820289,
   "Latitude": 106.6430402
 },
 {
   "STT": 231,
   "Name": "Nha Khoa Bác sĩ Toàn",
   "address": "21/10 Nguyễn Thiện Thuật, phường 2, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.7663367,
   "Latitude": 106.6814176
 },
 {
   "STT": 232,
   "Name": "Nha Khoa Bác sĩ Đỗ Thu Hằng",
   "address": "372/26 Lý Thường Kiệt, phường 8, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.780275,
   "Latitude": 106.653127
 },
 {
   "STT": 233,
   "Name": "Nha Khoa Bửu Ngôn",
   "address": "504 Nguyễn Đình Chiểu, phường 4, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.7716745,
   "Latitude": 106.6834782
 },
 {
   "STT": 234,
   "Name": "Nha Khoa Calmette",
   "address": "36 Lê Thị Hồng Gấm, phường Nguyễn Thái Bình, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.770061,
   "Latitude": 106.698673
 },
 {
   "STT": 235,
   "Name": "Nha Khoa Cẩm Tú",
   "address": "4B Trần Hưng Đạo, phường Phạm Ngũ Lão, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.769579,
   "Latitude": 106.69644
 },
 {
   "STT": 236,
   "Name": "Nha Khoa Cẩm Tú Cơ Sở 1",
   "address": "4B Trần Hưng Đạo, phường Nguyễn Thái Bình, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.770789,
   "Latitude": 106.6989104
 },
 {
   "STT": 237,
   "Name": "Nha Khoa Cẩm Tú Cơ Sở 2",
   "address": "62/3A Quang Trung, phường 8, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8443919,
   "Latitude": 106.640477
 },
 {
   "STT": 238,
   "Name": "Nha Khoa Cẩm Tú Cơ Sở 3",
   "address": "74/9 Nguyễn Ảnh Thủ, phường Trung Mỹ Tây, Quận 12, Hồ Chí Minh",
   "Longtitude": 10.8634105,
   "Latitude": 106.6126799
 },
 {
   "STT": 239,
   "Name": "Nha Khoa Cao Hà",
   "address": "Block A, Lầu 3, Căn 3.52 - 3.51 Tòa nhà Charmington La Pointe, 181 Cao Thắng, phường 12, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7748428,
   "Latitude": 106.6745907
 },
 {
   "STT": 240,
   "Name": "Nha Khoa Cao Thắng",
   "address": "15A Cao Thắng, phường 2, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.7688642,
   "Latitude": 106.6832129
 },
 {
   "STT": 241,
   "Name": "Nha Khoa Cát Tiên Cs1",
   "address": "27K Bình Phú, phường 13, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.7393452,
   "Latitude": 106.6291755
 },
 {
   "STT": 242,
   "Name": "Nha Khoa Cát Tiên Cs2",
   "address": "244 Đường Số 1, phường Bình Trị Đông B, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.7485791,
   "Latitude": 106.6133083
 },
 {
   "STT": 243,
   "Name": "Nha Khoa Cát Trắng",
   "address": "762 - 764 Điện Biên Phủ, phường 10, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7683474,
   "Latitude": 106.6747218
 },
 {
   "STT": 244,
   "Name": "Nha Khoa Cát Tường",
   "address": "16-18 Độc Lập, phường Tân Thành, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.791851,
   "Latitude": 106.636473
 },
 {
   "STT": 245,
   "Name": "Nha Khoa Cát Tường",
   "address": "741 Hồng Bàng, phường 6, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.7536937,
   "Latitude": 106.6460353
 },
 {
   "STT": 246,
   "Name": "Nha Khoa Cây Trâm",
   "address": "218B Lê Văn Thọ, phường 11, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8437816,
   "Latitude": 106.6571196
 },
 {
   "STT": 247,
   "Name": "Nha Khoa Chánh Hưng",
   "address": "357 Phạm Hùng, xã Bình Hưng, huyện Bình Chánh, Hồ Chí Minh",
   "Longtitude": 10.7376661,
   "Latitude": 106.6705214
 },
 {
   "STT": 248,
   "Name": "Nha Khoa Chánh Tâm",
   "address": "128C Phạm Văn Hai, phường 3, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.796291,
   "Latitude": 106.6611889
 },
 {
   "STT": 249,
   "Name": "Nha Khoa Châu",
   "address": "309 Nhật Tảo, phường 8, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7638594,
   "Latitude": 106.6648166
 },
 {
   "STT": 250,
   "Name": "Nha Khoa Châu Á",
   "address": "116 Lý Thường Kiệt, phường 7, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7616096,
   "Latitude": 106.6608519
 },
 {
   "STT": 251,
   "Name": "Nha Khoa Châu Âu",
   "address": "39 Lý Thái Tổ, phường 1, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7667617,
   "Latitude": 106.6768633
 },
 {
   "STT": 252,
   "Name": "Nha Khoa Châu Đức",
   "address": "178 Nguyễn Tư Giản, phường 12, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8314225,
   "Latitude": 106.6410888
 },
 {
   "STT": 253,
   "Name": "Nha Khoa Chí Công",
   "address": "11G Vũ Huy Tấn, phường 3, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.7957475,
   "Latitude": 106.6953885
 },
 {
   "STT": 254,
   "Name": "Nha Khoa Chợ Lớn",
   "address": "19 Châu Văn Liêm, phường 14, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7514033,
   "Latitude": 106.6587929
 },
 {
   "STT": 255,
   "Name": "Nha Khoa Chợ Đệm",
   "address": "A14/4 Nguyễn Hữu Trí, thị trấn Tân Túc, huyện Bình Chánh, Hồ Chí Minh",
   "Longtitude": 10.6962476,
   "Latitude": 106.5931824
 },
 {
   "STT": 256,
   "Name": "Nha Khoa Cơ Đốc Khang Thịnh",
   "address": "619 Lũy Bán Bích, phường Phú Thạnh, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.77937,
   "Latitude": 106.635202
 },
 {
   "STT": 257,
   "Name": "Nha Khoa Cống Quỳnh",
   "address": "256 Cống Quỳnh, phường Phạm Ngũ Lão, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.7674295,
   "Latitude": 106.6868917
 },
 {
   "STT": 258,
   "Name": "Nha Khoa Công Tâm",
   "address": "6 Đường Số 5, phường 10, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.7858063,
   "Latitude": 106.646263
 },
 {
   "STT": 259,
   "Name": "Nha Khoa Cộng Đồng",
   "address": "167 Nguyễn Xí, phường 26, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.8140824,
   "Latitude": 106.7080949
 },
 {
   "STT": 260,
   "Name": "Nha Khoa Củ Chi",
   "address": "848 Khu phố 8 Quốc Lộ 22, thị trấn Củ Chi, huyện Củ Chi, Hồ Chí Minh",
   "Longtitude": 10.97117,
   "Latitude": 106.482386
 },
 {
   "STT": 261,
   "Name": "Nha Khoa Cung Vĩnh Phát",
   "address": "399A Cách Mạng Tháng 8, phường 13, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.780827,
   "Latitude": 106.675832
 },
 {
   "STT": 262,
   "Name": "Nha Khoa D.Casta",
   "address": "94 Trần Quang Diệu, phường 14, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.788928,
   "Latitude": 106.677839
 },
 {
   "STT": 263,
   "Name": "Nha Khoa Dễ Thương",
   "address": "36 Xóm Chiếu, phường 13, Quận 4, Hồ Chí Minh",
   "Longtitude": 10.7594999,
   "Latitude": 106.7114454
 },
 {
   "STT": 264,
   "Name": "Nha Khoa Delta",
   "address": "266 Huỳnh Văn Bánh, phường 11, Phú Nhuận, Hồ Chí Minh",
   "Longtitude": 10.7918299,
   "Latitude": 106.674565
 },
 {
   "STT": 265,
   "Name": "Nha Khoa Dent Art",
   "address": "27 Trần Xuân Hòa, phường 7, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7533097,
   "Latitude": 106.6670498
 },
 {
   "STT": 266,
   "Name": "Nha Khoa Denta",
   "address": "179A Lê Văn Sỹ, phường 14, Phú Nhuận, Hồ Chí Minh",
   "Longtitude": 10.7921798,
   "Latitude": 106.6708315
 },
 {
   "STT": 267,
   "Name": "Nha Khoa Dental Clinic",
   "address": "151 Hồ Văn Huê, phường 9, Phú Nhuận, Hồ Chí Minh",
   "Longtitude": 10.805737,
   "Latitude": 106.6782341
 },
 {
   "STT": 268,
   "Name": "Nha Khoa Dental Clinic Bác sĩ Nguyễn Phú Thọ",
   "address": "285/66 Cách Mạng Tháng 8, phường 12, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7812589,
   "Latitude": 106.6751714
 },
 {
   "STT": 269,
   "Name": "Nha Khoa Diamond",
   "address": "15 Trần Trọng Cung, phường Tân Thuận Đông, Quận 7, Hồ Chí Minh",
   "Longtitude": 10.7435607,
   "Latitude": 106.7303497
 },
 {
   "STT": 270,
   "Name": "Nha Khoa Diệp Nguyên",
   "address": "141 Trần Huy Liệu, phường 8, Phú Nhuận, Hồ Chí Minh",
   "Longtitude": 10.795844,
   "Latitude": 106.677604
 },
 {
   "STT": 271,
   "Name": "Nha Khoa Diệu Hiền",
   "address": "2/4B Thống Nhất, phường 16, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.839274,
   "Latitude": 106.665511
 },
 {
   "STT": 272,
   "Name": "Nha Khoa Digital",
   "address": "414 Phạm Thái Bường, phường Tân Phong, Quận 7, Hồ Chí Minh",
   "Longtitude": 10.7274531,
   "Latitude": 106.7077703
 },
 {
   "STT": 273,
   "Name": "Nha Khoa Dr. Hùng",
   "address": "244A Cống Quỳnh, phường Phạm Ngũ Lão, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.7670693,
   "Latitude": 106.6877405
 },
 {
   "STT": 274,
   "Name": "Nha Khoa Dr. Trần",
   "address": "B17/28 Quốc Lộ 50, xã Bình Hưng, huyện Bình Chánh, Hồ Chí Minh",
   "Longtitude": 10.7400598,
   "Latitude": 106.6562766
 },
 {
   "STT": 275,
   "Name": "Nha Khoa Dr.Vương",
   "address": "108 Đặng Văn Bi, phường Bình Thọ, Thủ Đức, Hồ Chí Minh",
   "Longtitude": 10.844567,
   "Latitude": 106.762154
 },
 {
   "STT": 276,
   "Name": "Nha Khoa Dung Hồ",
   "address": "44 Đinh Tiên Hoàng, phường Đa Kao, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.789633,
   "Latitude": 106.698606
 },
 {
   "STT": 277,
   "Name": "Nha Khoa Dương Nguyệt Trinh",
   "address": "55 Ba Tháng Hai, phường 11, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7754236,
   "Latitude": 106.6800967
 },
 {
   "STT": 278,
   "Name": "Nha Khoa Duy Tân",
   "address": "20 Tân Vĩnh, phường 4, Quận 4, Hồ Chí Minh",
   "Longtitude": 10.756843,
   "Latitude": 106.7018499
 },
 {
   "STT": 279,
   "Name": "Nha Khoa Duyên",
   "address": "20 đường nội khu Hưng Gia 2, phường Tân Phong, Quận 7, Hồ Chí Minh",
   "Longtitude": 10.7334164,
   "Latitude": 106.7077911
 },
 {
   "STT": 280,
   "Name": "Nha Khoa Duyên Việt",
   "address": "163 Trần Quý, phường 4, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.758647,
   "Latitude": 106.6547779
 },
 {
   "STT": 281,
   "Name": "Nha Khoa Duyên Việt",
   "address": "512 Lê Văn Sỹ, phường 14, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.7890633,
   "Latitude": 106.6756741
 },
 {
   "STT": 282,
   "Name": "Nha Khoa Eden Dental Clinic",
   "address": "171 Nguyễn Thị Thập, phường Tân Phú, Quận 7, Hồ Chí Minh",
   "Longtitude": 10.7379167,
   "Latitude": 106.719467
 },
 {
   "STT": 283,
   "Name": "Nha Khoa Family",
   "address": "1073 Nguyễn Trãi, phường 14, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7530864,
   "Latitude": 106.6508948
 },
 {
   "STT": 284,
   "Name": "Nha Khoa Family",
   "address": "C5/13B2 Phạm Hùng, xã Bình Hưng, huyện Bình Chánh, Hồ Chí Minh",
   "Longtitude": 10.729919,
   "Latitude": 106.6772326
 },
 {
   "STT": 285,
   "Name": "Nha Khoa Family",
   "address": "148 Trưng Nữ Vương, phường 4, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8225906,
   "Latitude": 106.6880687
 },
 {
   "STT": 286,
   "Name": "Nha Khoa Gia Hoàng",
   "address": "616 Hậu Giang, phường 12, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.74816,
   "Latitude": 106.634613
 },
 {
   "STT": 287,
   "Name": "Nha Khoa Gia Phú",
   "address": "160 Gia Phú, phường 1, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.7469049,
   "Latitude": 106.652572
 },
 {
   "STT": 288,
   "Name": "Nha Khoa Gia Phước",
   "address": "83 Ngô Thị Thu Minh, phường 2, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.7966712,
   "Latitude": 106.662817
 },
 {
   "STT": 289,
   "Name": "Nha Khoa Gia Đình",
   "address": "140 Ký Con, phường Nguyễn Thái Bình, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.7679445,
   "Latitude": 106.6978347
 },
 {
   "STT": 290,
   "Name": "Nha Khoa Gia Đình",
   "address": "175 Nguyễn Văn Luông, phường 10, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.7409389,
   "Latitude": 106.633515
 },
 {
   "STT": 291,
   "Name": "Nha Khoa Gia Đình",
   "address": "420 Nguyễn Văn Luông, phường 12, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.7516502,
   "Latitude": 106.6350888
 },
 {
   "STT": 292,
   "Name": "Nha Khoa Gia Định",
   "address": "123A Đinh Tiên Hoàng, phường 3, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.799024,
   "Latitude": 106.696167
 },
 {
   "STT": 293,
   "Name": "Nha Khoa Gia Đình",
   "address": "9 Võ Trường Toản, phường 2, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.801959,
   "Latitude": 106.700071
 },
 {
   "STT": 294,
   "Name": "Nha Khoa Gia Đình",
   "address": "428 Trần Hưng Đạo, phường 11, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7521789,
   "Latitude": 106.6594451
 },
 {
   "STT": 295,
   "Name": "Nha Khoa Gia Đình Năm Châu Cs Ngô Quyền",
   "address": "255 Ngô Quyền, phường 6, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7654333,
   "Latitude": 106.6640481
 },
 {
   "STT": 296,
   "Name": "Nha Khoa Gia Định Sg",
   "address": "40 Lê Quang Định, phường 14, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.8041182,
   "Latitude": 106.6983155
 },
 {
   "STT": 297,
   "Name": "Nha Khoa Global Care",
   "address": "123 Hồ Văn Huê, phường 9, Phú Nhuận, Hồ Chí Minh",
   "Longtitude": 10.8045738,
   "Latitude": 106.6778093
 },
 {
   "STT": 298,
   "Name": "Nha Khoa Gò Dầu",
   "address": "398 Tân Sơn Nhì, phường Tân Sơn Nhì, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.7951485,
   "Latitude": 106.6293577
 },
 {
   "STT": 299,
   "Name": "Nha Khoa Gò Dưa",
   "address": "95 Gò Dưa, phường Tam Bình, Thủ Đức, Hồ Chí Minh",
   "Longtitude": 10.869617,
   "Latitude": 106.73728
 },
 {
   "STT": 300,
   "Name": "Nha Khoa Gò Mây",
   "address": "826 Lê Trọng Tấn, phường Bình Hưng Hòa, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.8161404,
   "Latitude": 106.6017233
 },
 {
   "STT": 301,
   "Name": "Nha Khoa Gò Vấp",
   "address": "154 Nguyễn Văn Nghi, phường 5, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.823948,
   "Latitude": 106.687219
 },
 {
   "STT": 302,
   "Name": "Nha Khoa Grand Dentistry",
   "address": "32-34 Ngô Đức Kế, phường Bến Thành, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.773548,
   "Latitude": 106.705312
 },
 {
   "STT": 303,
   "Name": "Nha Khoa Hà Bảo",
   "address": "132 Nơ Trang Long, phường 11, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.8141909,
   "Latitude": 106.700055
 },
 {
   "STT": 304,
   "Name": "Nha Khoa Hà Minh",
   "address": "10 Lâm Hoành, phường An Lạc, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.7363991,
   "Latitude": 106.6149962
 },
 {
   "STT": 305,
   "Name": "Nha Khoa Hà Đông 2",
   "address": "115 Phạm Văn Chiêu, phường 14, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8497964,
   "Latitude": 106.6515328
 },
 {
   "STT": 306,
   "Name": "Nha Khoa Hải Nguyên",
   "address": "293 - 295 Hàn Hải Nguyên, phường 2, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.758219,
   "Latitude": 106.644458
 },
 {
   "STT": 307,
   "Name": "Nha Khoa Hải Nha 3",
   "address": "36A Đường Số 13, phường Linh Xuân, Thủ Đức, Hồ Chí Minh",
   "Longtitude": 10.8824967,
   "Latitude": 106.762449
 },
 {
   "STT": 308,
   "Name": "Nha Khoa Hải Thủy",
   "address": "473 Nguyễn Trãi, phường 7, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7550037,
   "Latitude": 106.6724387
 },
 {
   "STT": 309,
   "Name": "Nha Khoa Hải Tiên",
   "address": "366 Trần Hưng Đạo, phường 2, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7565141,
   "Latitude": 106.6847654
 },
 {
   "STT": 310,
   "Name": "Nha Khoa Hải Đăng",
   "address": "1 Đường Số 41, phường 6, Quận 4, Hồ Chí Minh",
   "Longtitude": 10.7580909,
   "Latitude": 106.6999578
 },
 {
   "STT": 311,
   "Name": "Nha Khoa Hàn Châu",
   "address": "004 Lô B Phan Văn Trị, phường 2, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7557258,
   "Latitude": 106.6783424
 },
 {
   "STT": 312,
   "Name": "Nha Khoa Hàn Lâm",
   "address": "202 Trần Quý, phường 6, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.7586169,
   "Latitude": 106.653081
 },
 {
   "STT": 313,
   "Name": "Nha Khoa Hàn Mai",
   "address": "93/3 Nơ Trang Long, phường 11, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.81736,
   "Latitude": 106.698547
 },
 {
   "STT": 314,
   "Name": "Nha Khoa Hàn Việt",
   "address": "70 Tân Mỹ, phường Tân Phú, Quận 7, Hồ Chí Minh",
   "Longtitude": 10.7363181,
   "Latitude": 106.7181258
 },
 {
   "STT": 315,
   "Name": "Nha Khoa Hana",
   "address": "151 Lương Định Của, phường Bình An, Quận 2, Hồ Chí Minh",
   "Longtitude": 10.7891843,
   "Latitude": 106.7371983
 },
 {
   "STT": 316,
   "Name": "Nha Khoa Hằng Minh",
   "address": "145 Đường D1, phường 25, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.8036081,
   "Latitude": 106.721451
 },
 {
   "STT": 317,
   "Name": "Nha Khoa Hàng Xanh",
   "address": "23 Đinh Bộ Lĩnh, phường 24, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.8042504,
   "Latitude": 106.7093345
 },
 {
   "STT": 318,
   "Name": "Nha Khoa Hanh Lan",
   "address": "6 Phạm Đình Toái, phường 6, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.7763449,
   "Latitude": 106.686175
 },
 {
   "STT": 319,
   "Name": "Nha Khoa Hạnh Nguyên",
   "address": "481 - 493 Lý Thái Tổ, phường 9, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7679129,
   "Latitude": 106.6679334
 },
 {
   "STT": 320,
   "Name": "Nha Khoa Hạnh Phúc",
   "address": "521 Lê Quang Định, phường 1, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8181555,
   "Latitude": 106.6892572
 },
 {
   "STT": 321,
   "Name": "Nha Khoa Hậu",
   "address": "C9/31 Trịnh Như Khuê, xã Bình Chánh, huyện Bình Chánh, Hồ Chí Minh",
   "Longtitude": 10.6656062,
   "Latitude": 106.5721106
 },
 {
   "STT": 322,
   "Name": "Nha Khoa Hậu Giang",
   "address": "558 Hậu Giang, phường 12, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.74849,
   "Latitude": 106.635994
 },
 {
   "STT": 323,
   "Name": "Nha Khoa Hb",
   "address": "204 Xô Viết Nghệ Tĩnh, phường 21, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.798219,
   "Latitude": 106.711132
 },
 {
   "STT": 324,
   "Name": "Nha Khoa Hiền Phúc",
   "address": "801 Xô Viết Nghệ Tĩnh, phường 26, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.8143385,
   "Latitude": 106.7160767
 },
 {
   "STT": 325,
   "Name": "Nha Khoa Hiển Vinh",
   "address": "44 Nguyễn Huy Tự, phường Đa Kao, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.7927451,
   "Latitude": 106.6964129
 },
 {
   "STT": 326,
   "Name": "Nha Khoa Hiền Đức",
   "address": "16 Trần Quang Khải, phường Tân Định, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.791907,
   "Latitude": 106.688061
 },
 {
   "STT": 327,
   "Name": "Nha Khoa Hiệp Nghĩa",
   "address": "4 Công Trường Hòa Bình, phường 19, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.7921143,
   "Latitude": 106.7089617
 },
 {
   "STT": 328,
   "Name": "Nha Khoa Hiệp Thành",
   "address": "1045 Nguyễn Ảnh Thủ, phường Tân Chánh Hiệp, Quận 12, Hồ Chí Minh",
   "Longtitude": 10.8700695,
   "Latitude": 106.6182804
 },
 {
   "STT": 329,
   "Name": "Nha Khoa Hiếu",
   "address": "215 Lý Chiêu Hoàng, phường 10, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.73909,
   "Latitude": 106.6247004
 },
 {
   "STT": 330,
   "Name": "Phòng khám Chấn Thương Chỉnh Hình Bác sĩ Đinh Ngọc Minh",
   "address": "144 Đường 10, phường 13, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.7543426,
   "Latitude": 106.6270129
 },
 {
   "STT": 331,
   "Name": "Phòng khám Chấn Thương Chỉnh Hình 175",
   "address": "11 Phạm Huy Thông, phường 17, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.833558,
   "Latitude": 106.681451
 },
 {
   "STT": 332,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình & Cột Sống Bác sĩ Phan Vương Huy Đổng",
   "address": "108/4 Quốc lộ 22, Khu phố 1, phường Tân Thới Nhất, Quận 12, Hồ Chí Minh",
   "Longtitude": 10.8436352,
   "Latitude": 106.614884
 },
 {
   "STT": 333,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình & Khoa Nhi Bác sĩ Huỳnh Mạnh Nhi",
   "address": "565/95 Nguyễn Trãi, phường 7, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.754207,
   "Latitude": 106.6676608
 },
 {
   "STT": 334,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình & Phẫu Thuật Thẩm Mỹ Bác sĩ Lê Văn Tư",
   "address": "256/60 Phan Huy Ích, phường 12, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8379952,
   "Latitude": 106.6366911
 },
 {
   "STT": 335,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình Bác sĩ Bình",
   "address": "42 Đông Hồ, phường 8, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.783636,
   "Latitude": 106.652529
 },
 {
   "STT": 336,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình Bác sĩ Cao Thanh Trúc",
   "address": "214/19/17 Nguyễn Hữu Cảnh, phường Tân Định, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.7826262,
   "Latitude": 106.7066538
 },
 {
   "STT": 337,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình Bác sĩ Cao Thỉ",
   "address": "1/18 A, cư xá Nam Hải, Phạm Hùng, phường 4, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.7373141,
   "Latitude": 106.6716169
 },
 {
   "STT": 338,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình Bác sĩ Lê Gia Ánh Thỳ",
   "address": "277 Hòa Hảo, phường 4, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7621011,
   "Latitude": 106.6697586
 },
 {
   "STT": 339,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình Bác sĩ Lê Sỹ Tuấn",
   "address": "767/11A Trần Hưng Đạo, phường 1, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7553921,
   "Latitude": 106.6822923
 },
 {
   "STT": 340,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình Bác sĩ Nguyễn Tấn Toàn",
   "address": "61 Hùng Vương, phường 4, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7618033,
   "Latitude": 106.6779355
 },
 {
   "STT": 341,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình Bác sĩ Nguyễn Thế Hanh",
   "address": "11 Nguyễn Bá Tòng, phường Tân Thành, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.793595,
   "Latitude": 106.636761
 },
 {
   "STT": 342,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình Bác sĩ Nguyễn Tiến Linh",
   "address": "48/9 Quang Trung, phường 10, Gò Vấp, Hồ Chí Minh ",
   "Longtitude": 10.8265829,
   "Latitude": 106.6792524
 },
 {
   "STT": 343,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình Bác sĩ Nguyễn Tôn Ngọc Huỳnh",
   "address": "134/1 Thành Thái, phường 12, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7690393,
   "Latitude": 106.6657766
 },
 {
   "STT": 344,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình Bác sĩ Nguyễn Trọng Tín",
   "address": "48 đường 817A Tạ Quang Bửu, phường 5, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.7351725,
   "Latitude": 106.6683526
 },
 {
   "STT": 345,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình Bác sĩ Nguyễn Văn Mỹ Anh",
   "address": "6F Cư xá Bùi Minh Trực, phường 5, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.7371657,
   "Latitude": 106.6578568
 },
 {
   "STT": 346,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình Bác sĩ Nguyễn Văn Tiến Lưu",
   "address": "56B Trần Phú, phường 4, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7623843,
   "Latitude": 106.6785851
 },
 {
   "STT": 347,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình Bác sĩ Tiêu Chí Viễn",
   "address": "C03 Chung Cư Bình Thới, Lãnh Bình Thăng, phường 8, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.760605,
   "Latitude": 106.649142
 },
 {
   "STT": 348,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình Bác sĩ Trịnh Công Bình",
   "address": "217 Lê Đại Hành, phường 13, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.76353,
   "Latitude": 106.655829
 },
 {
   "STT": 349,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình Bác sĩ Trịnh Xuân Lê",
   "address": "561 Bà Hạt, phường 8, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7651257,
   "Latitude": 106.6654032
 },
 {
   "STT": 350,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình Bác sĩ Võ Văn Mẫn",
   "address": "30 Hùng Vương, phường 1, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7619277,
   "Latitude": 106.6762666
 },
 {
   "STT": 351,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình Bác sĩ Vũ Viết Chính",
   "address": "340C/33 Hoàng Văn Thụ, phường 4, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.7977025,
   "Latitude": 106.6580575
 },
 {
   "STT": 352,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình Bác sĩ Đặng Thị Bé Thu",
   "address": "117/5 An Bình, phường 6, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7514955,
   "Latitude": 106.6723354
 },
 {
   "STT": 353,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình Bác sĩ Đỗ Phước Hùng",
   "address": "112/43 An Bình, phường 5, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7533624,
   "Latitude": 106.6722408
 },
 {
   "STT": 354,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình Việt Úc",
   "address": "819 Trần Hưng Đạo, phường 1, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7549753,
   "Latitude": 106.6807594
 },
 {
   "STT": 355,
   "Name": "Phòng khám Chuyên khoa Cơ Xương Khớp & Chấn Thương Chỉnh Hình Bác sĩ Mai Trọng Tường",
   "address": "558 Lê Hồng Phong, phường 10, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7685731,
   "Latitude": 106.6741875
 },
 {
   "STT": 356,
   "Name": "Phòng khám Chuyên khoa Cơ Xương Khớp Bác sĩ Lê Minh Trí",
   "address": "175 Tân Phước, phường 6, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7619578,
   "Latitude": 106.6643661
 },
 {
   "STT": 357,
   "Name": "Phòng khám Chuyên khoa Cơ Xương Khớp Bác sĩ Lê Thành Phương",
   "address": "108 Nghĩa Thục, phường 5, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7526232,
   "Latitude": 106.6741473
 },
 {
   "STT": 358,
   "Name": "Phòng khám Chuyên khoa Cơ Xương Khớp Cột Sống Bác sĩ Nguyễn Ngọc Tuấn",
   "address": "109/24/24 Dương Bá Trạc, phường 1, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.7480524,
   "Latitude": 106.6896131
 },
 {
   "STT": 359,
   "Name": "Phòng khám Chuyên khoa Ngoại Chỉnh Hình  Bác sĩ Trần Đức Vinh",
   "address": "86 Huỳnh Mẫn Đạt, phường 2, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7556263,
   "Latitude": 106.6766064
 },
 {
   "STT": 360,
   "Name": "Phòng khám Chuyên khoa Nội Xương Khớp  Bác sĩ Hoàng Mạnh Cường",
   "address": "11TER Trần Phú, phường 4, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7637987,
   "Latitude": 106.680065
 },
 {
   "STT": 361,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Bác sĩ Tống Xuân Vũ",
   "address": "296 Ngô Quyền, phường 8, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7654368,
   "Latitude": 106.6643049
 },
 {
   "STT": 362,
   "Name": "Phòng khám Chuyên khoa Sọ Não Cột Sống Bác sĩ Phạm Anh Tuấn & Chuyên khoa Chấn Thương Chỉnh Hình Bác sĩ Trang Mạnh Khôi",
   "address": "45 Ngô Quyền, phường 10, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7521398,
   "Latitude": 106.6666229
 },
 {
   "STT": 363,
   "Name": "Phòng khám Chuyên khoa Tạo Hình & Thẩm Mỹ Thanh Tuyền",
   "address": "247 Đường 3 Tháng 2, phường 10, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7706881,
   "Latitude": 106.6727134
 },
 {
   "STT": 364,
   "Name": "Phòng khám Chuyên khoa Xương Khớp Bác sĩ Phan Dư Lê Thắng",
   "address": "142 Đặng Nguyên Cẩn, phường 13, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.7547631,
   "Latitude": 106.631826
 },
 {
   "STT": 365,
   "Name": "Phòng khám Nhi Đồng & Chấn Thương Chỉnh Hình Bác sĩ Liên Bác sĩ Khoan",
   "address": "57/6 Nguyễn Văn Bứa, Ấp 1, xã Xuân Thới Sơn, huyện Hóc Môn, Hồ Chí Minh",
   "Longtitude": 10.8725839,
   "Latitude": 106.5385641
 },
 {
   "STT": 366,
   "Name": "Phòng khám Phục Hồi Chức Năng Phúc An",
   "address": "552A Tân Kỳ Tân Quý, phường Bình Hưng Hòa, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.7946829,
   "Latitude": 106.6102106
 },
 {
   "STT": 367,
   "Name": "Phòng khám Thẩm Mỹ Bác sĩ Võ Thị Thu Tâm",
   "address": "R08 Cư xá Vĩnh Hội, phường 8, Quận 4, Hồ Chí Minh",
   "Longtitude": 10.760614,
   "Latitude": 106.701815
 },
 {
   "STT": 368,
   "Name": "Phòng khám Vật Lý Trị Liệu",
   "address": "150 Hồng bàng, phường 12, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7548992,
   "Latitude": 106.6596077
 },
 {
   "STT": 369,
   "Name": "Phòng khám Vật Lý Trị Liệu Bác sĩ Lưu Thị Ngọc Diệp",
   "address": "1015 Nguyễn Trãi, phường 14, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7526351,
   "Latitude": 106.6524414
 },
 {
   "STT": 370,
   "Name": "Phòng khám Đa Khoa Liên Tâm",
   "address": "67 Liên Tỉnh 5, phường 5, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.73611,
   "Latitude": 106.657748
 },
 {
   "STT": 371,
   "Name": "Phòng khám Điều Trị Đau Và Phục Hồi Chức Năng Bác sĩ Thái Thị Hoa",
   "address": "73 Cao Triều Phát, phường Tân Phong, Quận 7, Hồ Chí Minh",
   "Longtitude": 10.7318842,
   "Latitude": 106.7075111
 },
 {
   "STT": 372,
   "Name": "Phòng Khám Chuyên khoa Chấn Thương Chỉnh Hình Bác sĩ Lê Quang Tuấn",
   "address": "25 Đường 3052 Phạm Thế Hiển, phường 7, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.70745,
   "Latitude": 106.621793
 },
 {
   "STT": 373,
   "Name": "Hoàng Trọng Mộng Chuyên khoa Tai Mũi Họng",
   "address": "5 Lưu Xuân Tín, phường 10, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7510879,
   "Latitude": 106.6596325
 },
 {
   "STT": 374,
   "Name": "Huỳnh Khắc Cường Chuyên khoa Tai Mũi Họng",
   "address": "56/10 Nguyễn Thông, phường 9, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.781746,
   "Latitude": 106.678914
 },
 {
   "STT": 375,
   "Name": "Nguyễn Thành Lợi Chuyên khoa Tai Mũi Họng",
   "address": "382/2 Nguyễn Thị Minh Khai, phường 5, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.7721936,
   "Latitude": 106.6876936
 },
 {
   "STT": 376,
   "Name": "Phòng khám Bác sĩ Nguyễn Hữu Hiện",
   "address": "210 Nguyễn Sơn, phường Phú Thọ Hòa, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.7827559,
   "Latitude": 106.627303
 },
 {
   "STT": 377,
   "Name": "Phòng khám 55 Thăng Long",
   "address": "55 Thăng Long, phường 4, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.803219,
   "Latitude": 106.660607
 },
 {
   "STT": 378,
   "Name": "Giải Phẩu Thẩm Mỹ Minh Uyên",
   "address": "404/2 Nguyễn Duy Dương, phường 9, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7554981,
   "Latitude": 106.672027
 },
 {
   "STT": 379,
   "Name": "Phòng khám Chuyên khoa Gan Mật Bác sĩ Trương Bá Trung",
   "address": "26A Nguyễn Thái Sơn, phường 3, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8213762,
   "Latitude": 106.6859401
 },
 {
   "STT": 380,
   "Name": "Phòng khám Chuyên khoa Gan Mật Bác sĩ Lý Thị Mỹ Dung",
   "address": "3 Lê Đại Hành, phường 4, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.758629,
   "Latitude": 106.659148
 },
 {
   "STT": 381,
   "Name": "Phòng khám Chuyên khoa Gan Mật Bác sĩ Đinh Dạ Lý Hương",
   "address": "18 Tăng Bạt Hổ, phường 12, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7581085,
   "Latitude": 106.6623989
 },
 {
   "STT": 382,
   "Name": "Phòng khám Chuyên khoa Nội Tiêu Hóa  Bác sĩ Nguyễn Ngọc Kha",
   "address": "948 Nguyễn Kiệm, phường 3, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8243525,
   "Latitude": 106.6792516
 },
 {
   "STT": 383,
   "Name": "Phòng khám Chuyên khoa Nội Tiêu Hóa  Gan Mật  Bác sĩ Nguyễn Y Anh",
   "address": "265 Nguyễn Thiện Thuật, phường 1, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.76943,
   "Latitude": 106.6779338
 },
 {
   "STT": 384,
   "Name": "Phòng khám Chuyên khoa Nội Tiêu Hóa Tâm Nhất",
   "address": "69-71 Phạm Hữu Chí, phường 12, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7557034,
   "Latitude": 106.6588471
 },
 {
   "STT": 385,
   "Name": "Phòng khám Nội Tiêu Hóa Bác sĩ Nguyễn Bá Sơn",
   "address": "586 Phan Văn Trị, phường 7, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8302904,
   "Latitude": 106.6794121
 },
 {
   "STT": 386,
   "Name": "Phòng khám Nội Tiêu Hóa Gan Mật Bác sĩ Phương  Thận Nội Tiết Bác sĩ Kim Chi",
   "address": "357A/25 Nguyễn Trọng Tuyển, phường 1, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.797195,
   "Latitude": 106.666023
 },
 {
   "STT": 387,
   "Name": "Phòng khám Tiêu Hóa & Gan Mật Bác sĩ Hữu Đức",
   "address": "101 Trần Quang Diệu, phường 14, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.789398,
   "Latitude": 106.677795
 },
 {
   "STT": 388,
   "Name": "Phòng khám Tiêu Hóa Bác sĩ Trần Ngọc Bảo",
   "address": "164 Ngô Quyền, phường 5 , Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7627918,
   "Latitude": 106.664895
 },
 {
   "STT": 389,
   "Name": "Phòng khám Tiêu Hóa  Nội Soi",
   "address": "91 Phạm Hữu Chí, phường 12, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7555946,
   "Latitude": 106.6579456
 },
 {
   "STT": 390,
   "Name": "Phòng khám Chuyên khoa Nội Thần Kinh  Bác sĩ Trần Lê Thanh Tâm",
   "address": "235 Tôn Đản, phường 15, Quận 4, Hồ Chí Minh",
   "Longtitude": 10.756657,
   "Latitude": 106.7063469
 },
 {
   "STT": 391,
   "Name": "Phòng khám Chuyên khoa Thận & Tiết Niệu Bác sĩ Trần Văn Vũ",
   "address": "8 Trần Nhân Tôn, phường 2, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7620945,
   "Latitude": 106.6748837
 },
 {
   "STT": 392,
   "Name": "Phòng khám & Siêu Âm Sản Khoa Bác sĩ Phạm Minh Khôi Nguyên",
   "address": "25/8 Lê Văn Thọ, phường 8, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.838151,
   "Latitude": 106.656957
 },
 {
   "STT": 393,
   "Name": "Phòng khám & Spa Ánh Nhật Minh",
   "address": "287/71 Cách mạng Tháng 8, phường 12, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7772791,
   "Latitude": 106.6791176
 },
 {
   "STT": 394,
   "Name": "Phòng khám Nha Sĩ Tường",
   "address": "32/1 Nguyễn Huy Lượng, phường 14, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.806174,
   "Latitude": 106.696409
 },
 {
   "STT": 395,
   "Name": "Phòng khám Bác sĩ An",
   "address": "983 Tạ Quang Bửu, phường 6, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.7341214,
   "Latitude": 106.6549229
 },
 {
   "STT": 396,
   "Name": "Phòng khám Bác sĩ An",
   "address": "96 Nguyễn Xiển, phường Long Thạnh Mỹ, Quận 9, Hồ Chí Minh",
   "Longtitude": 10.8524329,
   "Latitude": 106.8333266
 },
 {
   "STT": 397,
   "Name": "Phòng khám Bác sĩ Bình",
   "address": "19 Đường 1, phường Hiệp Bình Chánh, Thủ Đức, Hồ Chí Minh",
   "Longtitude": 10.8592295,
   "Latitude": 106.7065835
 },
 {
   "STT": 398,
   "Name": "Phòng khám Bác sĩ Cao Minh",
   "address": "649/26 Điện Biên Phủ, hẻm cư xá Tân Cảng, phường 25, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.7992139,
   "Latitude": 106.719502
 },
 {
   "STT": 399,
   "Name": "Phòng khám Bác sĩ Lý Thị Kiểng",
   "address": "25/23 Bà Chí, phường 9, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.7439759,
   "Latitude": 106.634974
 },
 {
   "STT": 400,
   "Name": "Phòng khám Bác sĩ Mỹ Hồng",
   "address": "963 Nguyễn Duy Trinh, phường Phú Hữu, Quận 9, Hồ Chí Minh",
   "Longtitude": 10.7916162,
   "Latitude": 106.80184
 },
 {
   "STT": 401,
   "Name": "Phòng khám Bác sĩ Nghệ",
   "address": "39 Đường số 6, phường Bình Chiểu, Thủ Đức, Hồ Chí Minh",
   "Longtitude": 10.876105,
   "Latitude": 106.7234254
 },
 {
   "STT": 402,
   "Name": "Phòng khám Bác sĩ Nguyễn Hữu Văn",
   "address": "31 Đường Số 138, phường Tân Phú, Quận 9, Hồ Chí Minh",
   "Longtitude": 10.8656037,
   "Latitude": 106.807623
 },
 {
   "STT": 403,
   "Name": "Phòng khám Bác sĩ Nguyễn Quang Vinh",
   "address": "31A Nguyễn Văn Lạc, phường 21, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.794001,
   "Latitude": 106.709684
 },
 {
   "STT": 404,
   "Name": "Phòng khám Bác sĩ Phước",
   "address": "104 Gò Dưa, phường Tam Bình, Thủ Đức, Hồ Chí Minh",
   "Longtitude": 10.871439,
   "Latitude": 106.744035
 },
 {
   "STT": 405,
   "Name": "Phòng khám Bác sĩ Tô Duy Ngân",
   "address": "61/34 Đường số 48, phường Hiệp Bình Chánh, Thủ Đức, Hồ Chí Minh",
   "Longtitude": 10.8348339,
   "Latitude": 106.7257511
 },
 {
   "STT": 406,
   "Name": "Phòng khám Bác sĩ Trần Công Hương",
   "address": "50 Đại Lộ 2, phường Phước Bình, Quận 9, Hồ Chí Minh",
   "Longtitude": 10.8177749,
   "Latitude": 106.772187
 },
 {
   "STT": 407,
   "Name": "Phòng khám Bác sĩ Trần Công Luận",
   "address": "8 Trần Minh Quyền, phường 11, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.769813,
   "Latitude": 106.6756472
 },
 {
   "STT": 408,
   "Name": "Phòng khám 139",
   "address": "139 Thành Thái, phường 14, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7748255,
   "Latitude": 106.664143
 },
 {
   "STT": 409,
   "Name": "Phòng khám 18 Phạm Hữu Chí",
   "address": "18 Phạm Hữu Chí, 12, Quận 5, Thành Phố Hồ Chí Minh",
   "Longtitude": 10.755871,
   "Latitude": 106.65804
 },
 {
   "STT": 410,
   "Name": "Phòng khám 22",
   "address": "22 Tôn Thất Hiệp, phường 13, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.765577,
   "Latitude": 106.653989
 },
 {
   "STT": 411,
   "Name": "Phòng khám 31 Lương Thế Vinh",
   "address": "31 Lương Thế Vinh, phường Tân Thới Hòa, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.763923,
   "Latitude": 106.630988
 },
 {
   "STT": 412,
   "Name": "Phòng khám 39",
   "address": "39 Bàu Cát 1, phường 14, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.794917,
   "Latitude": 106.641918
 },
 {
   "STT": 413,
   "Name": "Phòng khám 47",
   "address": "18 Phạm Hữu Chí, phường 12, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7558394,
   "Latitude": 106.6580183
 },
 {
   "STT": 414,
   "Name": "Phòng khám 59 Phó Cơ Điều",
   "address": "59 Phó Cơ Điều, phường 12, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7571668,
   "Latitude": 106.6570175
 },
 {
   "STT": 415,
   "Name": "Phòng khám 5d Chuyên Sản Phụ Khoa & Hiếm Muộn",
   "address": "412 Cao Thắng, phường 12, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7750319,
   "Latitude": 106.6727793
 },
 {
   "STT": 416,
   "Name": "Phòng khám 77",
   "address": "15 Tân Khai, phường 4, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.758519,
   "Latitude": 106.658147
 },
 {
   "STT": 417,
   "Name": "Phòng khám 93 Khuông Việt",
   "address": "93 Khuông Việt, phường Phú Trung, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.776842,
   "Latitude": 106.640242
 },
 {
   "STT": 418,
   "Name": "Phòng khám An Phước",
   "address": "556 Lê Văn Lương, phường Tân Phong, Quận 7, Hồ Chí Minh",
   "Longtitude": 10.7327948,
   "Latitude": 106.7010068
 },
 {
   "STT": 419,
   "Name": "Phòng khám Anh Khoa",
   "address": "3A69/1 Tỉnh Lộ 10, xã Phạm Văn Hai, huyện Bình Chánh, Hồ Chí Minh",
   "Longtitude": 10.789044,
   "Latitude": 106.5157211
 },
 {
   "STT": 420,
   "Name": "Phòng khám Âu Châu",
   "address": "508 Ngô Gia Tự, phường 9, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.758436,
   "Latitude": 106.6678515
 },
 {
   "STT": 421,
   "Name": "Phòng khám Bác sĩ Chu Thị Bá",
   "address": "9 Đường số 6 Cư Xá Đô Thành, phường 4, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.7732593,
   "Latitude": 106.6825664
 },
 {
   "STT": 422,
   "Name": "Phòng khám Bác sĩ Chu Trọng Hiệp",
   "address": "31/783 Tạ Quang Bửu, phường 4, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.7373099,
   "Latitude": 106.6747951
 },
 {
   "STT": 423,
   "Name": "Phòng khám Bác sĩ Gia Đình Hoàn Mỹ Lexington",
   "address": "Tầng trệt Khối C, Tòa nhà Lexington Residence, 67 Mai Chí Thọ, phường An Phú, Quận 2, Hồ Chí Minh",
   "Longtitude": 10.7984364,
   "Latitude": 106.7517804
 },
 {
   "STT": 424,
   "Name": "Phòng khám Bác sĩ Gia Đình Hoàn Mỹ Masteri",
   "address": "Tầng trệt T5 A01-02 Tòa nhà Masteri Thảo Điền, 159 Xa Lộ Hà Nội, phường Thảo Điền, Quận 2, Hồ Chí Minh",
   "Longtitude": 10.8029275,
   "Latitude": 106.742494
 },
 {
   "STT": 425,
   "Name": "Phòng khám Bác sĩ Gia Đình Minh Đăng",
   "address": "25 lô A Cư Xá Phú Lâm D, phường 10, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.7384243,
   "Latitude": 106.6312516
 },
 {
   "STT": 426,
   "Name": "Phòng khám Bác sĩ Gia Đình Sài Gòn",
   "address": "181 Nguyễn Sơn, phường Phú Thạnh, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.7825849,
   "Latitude": 106.6267809
 },
 {
   "STT": 427,
   "Name": "Phòng khám Bác sĩ Làn Da Bác sĩ Bùi Mạnh Hà",
   "address": "192 Đường 3 Tháng 2, phường 12, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.774711,
   "Latitude": 106.6788516
 },
 {
   "STT": 428,
   "Name": "Phòng khám Bác sĩ Nguyễn Bá Khoa",
   "address": "107 Châu văn Liêm, phường 14, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7533405,
   "Latitude": 106.6583831
 },
 {
   "STT": 429,
   "Name": "Phòng khám Bác sĩ Nguyễn Hữu Tín",
   "address": "649 Nguyễn Văn Quá, phường Đông Hưng Thuận, Quận 12, Hồ Chí Minh",
   "Longtitude": 10.8511645,
   "Latitude": 106.637214
 },
 {
   "STT": 430,
   "Name": "Phòng khám Bác sĩ Nguyễn Văn Liêm",
   "address": "52 Tô Ký, phường Đông Hưng Thuận, Quận 12, Hồ Chí Minh",
   "Longtitude": 10.8494439,
   "Latitude": 106.632421
 },
 {
   "STT": 431,
   "Name": "Phòng khám Bác sĩ Đặng Thị Hà",
   "address": "92 Nguyễn Thiện Thuật, phường 2, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.7674474,
   "Latitude": 106.6807818
 },
 {
   "STT": 432,
   "Name": "Phòng khám Bảo Sanh Mỹ Anh",
   "address": "578 đường số 7, phường Tân Tạo, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.7503752,
   "Latitude": 106.595544
 },
 {
   "STT": 433,
   "Name": "Phòng khám Bệnh Bác sĩ Lý Hồng Vân",
   "address": "196 Lê Văn Việt, phường Tăng Nhơn Phú B, Quận 9, Hồ Chí Minh",
   "Longtitude": 10.8443429,
   "Latitude": 106.7850276
 },
 {
   "STT": 434,
   "Name": "Phòng khám Bệnh Bác sĩ Nguyễn Anh Cường",
   "address": "948 Tân Kỳ Tân Quý, phường Bình Hưng Hòa, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.7877131,
   "Latitude": 106.5985182
 },
 {
   "STT": 435,
   "Name": "Phòng khám Bệnh Bác sĩ Vũ Thanh Tùng & Bác sĩ Nguyễn Thị Hồng",
   "address": "30 Mai Văn Vĩnh, phường Tân Quy, Quận 7, Hồ Chí Minh",
   "Longtitude": 10.7411768,
   "Latitude": 106.7137185
 },
 {
   "STT": 436,
   "Name": "Phòng khám Bệnh 609",
   "address": "609 Trường Chinh, phường Tân Sơn Nhì, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.8034398,
   "Latitude": 106.6359448
 },
 {
   "STT": 437,
   "Name": "Phòng khám Bệnh Bác sĩ Võ Thị Hồng Liên",
   "address": "1096 Kha Vạn Cân, phường Linh Chiểu, Thủ Đức, Hồ Chí Minh",
   "Longtitude": 10.8578353,
   "Latitude": 106.7581179
 },
 {
   "STT": 438,
   "Name": "Phòng khám Bệnh Mạch Máu Bác sĩ Trần Văn Bình",
   "address": "17 Võ Thị Sáu, phường Đa Kao, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.791062,
   "Latitude": 106.695319
 },
 {
   "STT": 439,
   "Name": "Phòng khám Bệnh Ngoài Giờ 57c Tô Ngọc Vân",
   "address": "57C Tô Ngọc Vân, phường Thạnh Xuân, Quận 12, Hồ Chí Minh",
   "Longtitude": 10.8571224,
   "Latitude": 106.6707232
 },
 {
   "STT": 440,
   "Name": "Phòng khám Bệnh Ngoài Giờ Bác sĩ Dương Quang Thảo",
   "address": "52/6B Lý Thường Kiệt, thị trấn Hóc Môn, huyện Hóc Môn, Hồ Chí Minh",
   "Longtitude": 10.8851237,
   "Latitude": 106.5879034
 },
 {
   "STT": 441,
   "Name": "Phòng khám Bệnh Ngoài Giờ Bác sĩ Mã Thanh Phong",
   "address": "220B Phan Văn Khỏe, phường 2, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.7479739,
   "Latitude": 106.648372
 },
 {
   "STT": 442,
   "Name": "Phòng khám Bệnh Ngoài Giờ Bác sĩ Nguyễn Bá Nhuận",
   "address": "243/9 Tô Hiến Thành, phường 13, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7784915,
   "Latitude": 106.6677229
 },
 {
   "STT": 443,
   "Name": "Phòng khám Bệnh Ngoài Giờ Bác sĩ Nguyễn Minh Trí",
   "address": "110/45/4 Bà Hom, phường 13, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.7542127,
   "Latitude": 106.6333947
 },
 {
   "STT": 444,
   "Name": "Phòng khám Bệnh Ngoài Giờ Bác sĩ Nguyễn Thành Công",
   "address": "10 Đường Số 8, phường Phước Bình, Quận 9, Hồ Chí Minh",
   "Longtitude": 10.816522,
   "Latitude": 106.77403
 },
 {
   "STT": 445,
   "Name": "Phòng khám Bệnh Ngoài Giờ Bác sĩ Thúy Hằng",
   "address": "305 Bến Chương Dương, phường Cầu Kho, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.7576903,
   "Latitude": 106.6904974
 },
 {
   "STT": 446,
   "Name": "Phòng khám Bệnh Ngoài Giờ Bác sĩ Trần Thao Lược",
   "address": "208 Đình Phong Phú, phường Tăng Nhơn Phú B, Quận 9, Hồ Chí Minh",
   "Longtitude": 10.836333,
   "Latitude": 106.778139
 },
 {
   "STT": 447,
   "Name": "Phòng khám Bệnh Nhân Đạo",
   "address": "134 Cách Mạng Tháng Tám, phường 10, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.778885,
   "Latitude": 106.680396
 },
 {
   "STT": 448,
   "Name": "Phòng khám Bệnh Phổi Bác sĩ Trần Nhật Quang",
   "address": "25A Hoàng Văn Hòe, phường Tân Quý, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.7947036,
   "Latitude": 106.6195884
 },
 {
   "STT": 449,
   "Name": "Phòng khám Bệnh Trĩ Lương Y Đỗ Quý Thành",
   "address": "77 Cô Bắc, phường Cô Giang, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.764531,
   "Latitude": 106.694578
 },
 {
   "STT": 450,
   "Name": "Phòng khám Bệnh Y Học Cổ Truyền Nhân Ái Việt Trung",
   "address": "292 Nguyễn Tất Thành, phường 13, Quận 4, Hồ Chí Minh",
   "Longtitude": 10.761638,
   "Latitude": 106.7095439
 },
 {
   "STT": 451,
   "Name": "Phòng khám Bệnh Y Học Cổ Truyền Trường Sinh",
   "address": "30/3 Ấp Nam Lân, Phan Văn Hớn, xã Bà Điểm, huyện Hóc Môn, Hồ Chí Minh",
   "Longtitude": 10.8402683,
   "Latitude": 106.5990952
 },
 {
   "STT": 452,
   "Name": "Phòng khám Bệnh Đông Y Đa Khoa Quận 5",
   "address": "186A Lương Nhữ Học, phường 11, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7536653,
   "Latitude": 106.6603013
 },
 {
   "STT": 453,
   "Name": "Phòng khám Béo Phì Abc",
   "address": "151/1 Nguyễn Kim, phường 7, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7642094,
   "Latitude": 106.6614425
 },
 {
   "STT": 454,
   "Name": "Phòng khám Bệnh Ngoài Giờ Bác sĩ Hòa",
   "address": "31 Đường số 15, phường Bình Chiểu, Thủ Đức, Hồ Chí Minh",
   "Longtitude": 10.8769902,
   "Latitude": 106.7444958
 },
 {
   "STT": 455,
   "Name": "Phòng khám Bác sĩ Nguyễn Thị Mỹ Hạnh",
   "address": "239 Ngô Quyền, phường 6, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7651423,
   "Latitude": 106.6641084
 },
 {
   "STT": 456,
   "Name": "Phòng khám Bác sĩ Phan Vũ Bảo",
   "address": "163 Hàn Hải Nguyên, phường 2, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.75804,
   "Latitude": 106.6469539
 },
 {
   "STT": 457,
   "Name": "Phòng khám Bác sĩ Phượng Chuyên khoa Mắt - Bác sĩ Phúc Chuyên khoa Nội Nhi",
   "address": "79 Nguyễn Hồng Đào, phường 14, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.796056,
   "Latitude": 106.643241
 },
 {
   "STT": 458,
   "Name": "Phòng khám Bác sĩ Tuyết Mai",
   "address": "1274 Nguyễn Duy Trinh, phường Long Trường, Quận 9, Hồ Chí Minh",
   "Longtitude": 10.8040367,
   "Latitude": 106.8151979
 },
 {
   "STT": 459,
   "Name": "Phòng khám Bác sĩ Việt Khoa Ngoại Tổng Hợp  Bác sĩ Hải Khoa Sản",
   "address": "59 Bàu Cát, phường 14, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.7930657,
   "Latitude": 106.641282
 },
 {
   "STT": 460,
   "Name": "Phòng khám Bác sĩ Võ Duy Điền",
   "address": "25 Đường 339, phường Phước Long B, Quận 9, Hồ Chí Minh",
   "Longtitude": 10.8229663,
   "Latitude": 106.772436
 },
 {
   "STT": 461,
   "Name": "Phòng khám Bác sĩ Đặng Dung Thức",
   "address": "361 Minh Phụng, phường 2, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.758063,
   "Latitude": 106.643686
 },
 {
   "STT": 462,
   "Name": "Phòng khám Bác sĩ Ánh Mai",
   "address": "14/24 Ngô Tất Tố, phường 19, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.7929793,
   "Latitude": 106.7105391
 },
 {
   "STT": 463,
   "Name": "Phòng khám Bác sĩ Bích Hiền",
   "address": "171A Trương Phước Phan, phường Bình Trị Đông, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.7631537,
   "Latitude": 106.6131968
 },
 {
   "STT": 464,
   "Name": "Phòng khám Bác sĩ Duy",
   "address": "B69/1 Mỹ Hòa 2, xã Xuân Thới Đông, huyện Hóc Môn, Hồ Chí Minh",
   "Longtitude": 10.8606646,
   "Latitude": 106.6032062
 },
 {
   "STT": 465,
   "Name": "Phòng khám Bác sĩ Hoàng Văn Thịnh",
   "address": "65A Trần Quang Cơ, phường Phú Thạnh, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.777626,
   "Latitude": 106.625349
 },
 {
   "STT": 466,
   "Name": "Phòng khám Bác sĩ Huỳnh Ngọc Hớn",
   "address": "12 Mạc Tiên Tích, phường 11, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7551795,
   "Latitude": 106.6658981
 },
 {
   "STT": 467,
   "Name": "Phòng khám Bác sĩ Lê Cao Phương Duy",
   "address": "606/12 Ba Tháng Hai, phường 14, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7778211,
   "Latitude": 106.6810741
 },
 {
   "STT": 468,
   "Name": "Phòng khám Bác sĩ Lê Hồng Quang",
   "address": "417/26/4 Quang Trung, phường 10, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8311175,
   "Latitude": 106.6687415
 },
 {
   "STT": 469,
   "Name": "Phòng khám Bác sĩ Lê Phúc",
   "address": "8 Tôn Thất Hiệp, phường 13, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.765703,
   "Latitude": 106.654185
 },
 {
   "STT": 470,
   "Name": "Phòng khám Bác sĩ Lê Quốc Bảo",
   "address": "3 Phan Chu Trinh, phường 2, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.80256,
   "Latitude": 106.701691
 },
 {
   "STT": 471,
   "Name": "Phòng khám Bác sĩ Lê Thị Tuyết Nhung",
   "address": "482/31 Lê Quang Định, phường 11, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.8099229,
   "Latitude": 106.6933822
 },
 {
   "STT": 472,
   "Name": "Phòng khám Bác sĩ Lương Văn Sinh",
   "address": "193A Vườn Lài, phường Phú Thọ Hòa, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.7879539,
   "Latitude": 106.630289
 },
 {
   "STT": 473,
   "Name": "Phòng khám Bác sĩ Mai Lan",
   "address": "57 Nguyễn Súy, phường Tân Quý, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.7915616,
   "Latitude": 106.6223852
 },
 {
   "STT": 474,
   "Name": "Phòng khám Bác sĩ Minh Châu",
   "address": "66 Kha Vạn Cân, phường Hiệp Bình Chánh, Thủ Đức, Hồ Chí Minh",
   "Longtitude": 10.8268266,
   "Latitude": 106.7138247
 },
 {
   "STT": 475,
   "Name": "Phòng khám Bác sĩ Nguyễn Hữu Phúc",
   "address": "74/4 Nguyễn Ảnh Thủ, xã Trung Chánh, huyện Hóc Môn, Hồ Chí Minh",
   "Longtitude": 10.865062,
   "Latitude": 106.613621
 },
 {
   "STT": 476,
   "Name": "Phòng khám Bác sĩ Nguyễn Thị Út",
   "address": "828/16 Trường Chinh, phường 15, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.8254237,
   "Latitude": 106.627357
 },
 {
   "STT": 477,
   "Name": "Phòng khám Bác sĩ Nguyễn Văn Khoa",
   "address": "105 Phù Đổng Thiên Vương, phường 11, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7549582,
   "Latitude": 106.6623147
 },
 {
   "STT": 478,
   "Name": "Phòng khám Bác sĩ Nguyễn Văn Tuấn",
   "address": "48 Lưu Nhân Chú, phường 5, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.790763,
   "Latitude": 106.6630491
 },
 {
   "STT": 479,
   "Name": "Phòng khám Bác sĩ Nguyễn Xuân Nguyên",
   "address": "85 Nguyễn Thông, phường 9, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.7805319,
   "Latitude": 106.679944
 },
 {
   "STT": 480,
   "Name": "Phòng khám Bác sĩ Phạm Hồng Quảng",
   "address": "82 Lý Nam Đế, phường 7, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.7603358,
   "Latitude": 106.6599147
 },
 {
   "STT": 481,
   "Name": "Phòng khám Bác sĩ Thiện",
   "address": "322 Long Phước, phường Long Phước, Quận 9, Hồ Chí Minh",
   "Longtitude": 10.8087667,
   "Latitude": 106.8594172
 },
 {
   "STT": 482,
   "Name": "Phòng khám Bác sĩ Thu Hà",
   "address": "417 Minh Phụng, phường 10, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.760438,
   "Latitude": 106.644114
 },
 {
   "STT": 483,
   "Name": "Phòng khám Bác sĩ Tuấn Bác sĩ Bông",
   "address": "58 Trần Hưng Đạo, phường 7, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7536307,
   "Latitude": 106.6796662
 },
 {
   "STT": 484,
   "Name": "Phòng khám Bác sĩ Vành Khuyên",
   "address": "32/1 Ấp Nam Lân, Phan Văn Hớn, xã Bà Điểm, huyện Hóc Môn, Hồ Chí Minh",
   "Longtitude": 10.836445,
   "Latitude": 106.604221
 },
 {
   "STT": 485,
   "Name": "Phòng khám Bác sĩ Võ Quang Phúc",
   "address": "160/1/26 Xô Viết Nghệ Tĩnh, phường 21, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.79635,
   "Latitude": 106.710441
 },
 {
   "STT": 486,
   "Name": "Phòng khám Bác sĩ Võ Văn Dũng",
   "address": "26/1 Nguyễn Ảnh Thủ, xã Bà Điểm, huyện Hóc Môn, Hồ Chí Minh",
   "Longtitude": 10.8538171,
   "Latitude": 106.6055891
 },
 {
   "STT": 487,
   "Name": "Phòng khám Bác sĩ Vũ Thị Nhung",
   "address": "224 Gò Dưa, phường Tam Bình, Thủ Đức, Hồ Chí Minh",
   "Longtitude": 10.8706413,
   "Latitude": 106.7404272
 },
 {
   "STT": 488,
   "Name": "Phòng khám Bác sĩ Vũ Đức Thịnh",
   "address": "520A/14 Thành Thái, phường 12, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7684962,
   "Latitude": 106.6669306
 },
 {
   "STT": 489,
   "Name": "Phòng khám Bác sĩ Đinh Quang Châu",
   "address": "356D Minh Phụng, phường 9, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.759186,
   "Latitude": 106.644318
 },
 {
   "STT": 490,
   "Name": "Phòng khám Bác sĩ Nguyễn Thị Lệ",
   "address": "32 Đường số 4, phường An Lạc A, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.7503265,
   "Latitude": 106.6206546
 },
 {
   "STT": 491,
   "Name": "Phòng khám Chăm Sóc Sức Khỏe Bác sĩ Võ Thị Thanh",
   "address": "209 Nguyễn Thượng Hiền, phường 6, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.8058489,
   "Latitude": 106.685881
 },
 {
   "STT": 492,
   "Name": "Phòng khám Chẩn Trị Y Học Cổ Truyền  Bác sĩ Bùi Anh Dũng",
   "address": "Lô C, 006 Mạc Thiên Tích, phường 11, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7550539,
   "Latitude": 106.6648543
 },
 {
   "STT": 493,
   "Name": "Phòng khám Chẩn Trị Y Học Cổ Truyền  Bác sĩ Đinh Thị Thanh Nhàn",
   "address": "6 đường 27, phường Bình Trị Đông B, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.7551171,
   "Latitude": 106.6148793
 },
 {
   "STT": 494,
   "Name": "Phòng khám Chẩn Trị Y Học Cổ Truyền  Lương Y Trương Tuyết Nhung",
   "address": "50-52 Mã Lò, phường Bình Trị Đông A, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.7593772,
   "Latitude": 106.6056924
 },
 {
   "STT": 495,
   "Name": "Phòng khám Chẩn Trị Y Học Cổ Truyền Thành Tín",
   "address": "54C Trương Vĩnh Ký, phường Tân Thành, Tân Phú, Hồ Chí Minh",
   "Longtitude": 10.793681,
   "Latitude": 106.635372
 },
 {
   "STT": 496,
   "Name": "Phòng khám Chẩn Trị Đông Y An Triệu Đường",
   "address": "384/31/6 Lý Thái Tổ, phường 10, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7678496,
   "Latitude": 106.6752366
 },
 {
   "STT": 497,
   "Name": "Phòng khám Chẩn Đoán Y Khoa Khang Đức",
   "address": "484 Đường 3 Tháng 2, phường 14, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7672906,
   "Latitude": 106.666077
 },
 {
   "STT": 498,
   "Name": "Phòng khám Chữ Thập Đỏ Quận 8",
   "address": "25 Đinh Hòa, phường 11, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.747513,
   "Latitude": 106.659766
 },
 {
   "STT": 499,
   "Name": "Phòng khám Chuyên khoa Ths. Bác sĩ Tô Thanh Long",
   "address": "2F Lý Thường Kiệt, phường 12, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.757014,
   "Latitude": 106.6621747
 },
 {
   "STT": 500,
   "Name": "Phòng khám Chuyên khoa 490",
   "address": "490 Vĩnh Viễn, phường 8, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7632806,
   "Latitude": 106.6650303
 },
 {
   "STT": 501,
   "Name": "Phòng khám Chuyên khoa Bệnh Phổi & Lao Bác sĩ Phạm Duy Tín",
   "address": "280 Trần Hưng Đạo B, phường 11, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7523392,
   "Latitude": 106.6632049
 },
 {
   "STT": 502,
   "Name": "Phòng khám Chuyên khoa Bướu Cổ & Lồng Ngực Bác sĩ Hồ Nam",
   "address": "320 Lãnh Binh Thăng, phường 11, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.7647359,
   "Latitude": 106.647988
 },
 {
   "STT": 503,
   "Name": "Phòng khám Chuyên khoa Bướu Cổ Bác sĩ Lê Hữu Tâm",
   "address": "60A Lê Đại Hành, phường 7, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.7592434,
   "Latitude": 106.6591066
 },
 {
   "STT": 504,
   "Name": "Phòng khám Chuyên khoa Bướu Cổ Tiểu Đường Bác sĩ Lê Tấn Đức",
   "address": "9 Lê Đại Hành, phường 4, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.7585586,
   "Latitude": 106.6591193
 },
 {
   "STT": 505,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình",
   "address": "131 Lạc Long Quân, phường 3, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.7620605,
   "Latitude": 106.6413972
 },
 {
   "STT": 506,
   "Name": "Phòng khám Chuyên khoa Chẩn Trị Y Học Dân Tộc Bác sĩ Tuyết",
   "address": "(Lô A) 111 Chợ Quán, phường 1, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.752822,
   "Latitude": 106.682729
 },
 {
   "STT": 507,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh Bác sĩ Nguyễn Thị Thanh Hoa",
   "address": "113 Đường Số 2, phường Tăng Nhơn Phú B, Quận 9, Hồ Chí Minh",
   "Longtitude": 10.8415263,
   "Latitude": 106.7849706
 },
 {
   "STT": 508,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh Bác sĩ Nguyễn Trung Cường",
   "address": "258 Nguyễn Thiện Thuật, phường 3, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.7696429,
   "Latitude": 106.6781273
 },
 {
   "STT": 509,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh Bác sĩ Đỗ Văn Liêm & Chuyên khoa Ung Bướu Bác sĩ Phan Thế Sung",
   "address": "26 Nơ Trang Long, phường 14, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.8050964,
   "Latitude": 106.694995
 },
 {
   "STT": 510,
   "Name": "Phòng khám Chuyên khoa Cơ Xương Khớp & Chấn Thương Chỉnh Hình Bác sĩ Mai Trọng Tường",
   "address": "558 Lê Hồng Phong, phường 10, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7685731,
   "Latitude": 106.6741875
 },
 {
   "STT": 511,
   "Name": "Phòng khám Chuyên khoa Cơ Xương Khớp & Lão Khoa Bác sĩ Phan Vĩnh Khang",
   "address": "Hẻm 172, 17P/1 Nguyễn Thị Tần, phường 2, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.7454795,
   "Latitude": 106.6864799
 },
 {
   "STT": 512,
   "Name": "Phòng khám Chuyên khoa Cơ Xương Khớp & Ung Bướu Bác sĩ Hiến",
   "address": "126 Nguyễn Thị Thập, phường Bình Thuận, Quận 7, Hồ Chí Minh",
   "Longtitude": 10.7382917,
   "Latitude": 106.7163834
 },
 {
   "STT": 513,
   "Name": "Phòng khám Chuyên khoa Cơ Xương Khớp Bác sĩ Âu Dương Huy",
   "address": "164 Nhật Tảo, phường 8, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.764612,
   "Latitude": 106.6671217
 },
 {
   "STT": 514,
   "Name": "Phòng khám Chuyên khoa Cơ Xương Khớp Bác sĩ Hà Văn Hội",
   "address": " 139 Nguyễn Tiểu La, phường 5, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.762701,
   "Latitude": 106.6664463
 },
 {
   "STT": 515,
   "Name": "Phòng khám Chuyên khoa Cơ Xương Khớp Bác sĩ Hồ Phạm Thục Lan",
   "address": "LL2 Ba Vì, Cư xá Bắc Hải, phường 15, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7810279,
   "Latitude": 106.665019
 },
 {
   "STT": 516,
   "Name": "Phòng khám Chuyên khoa Cơ Xương Khớp Bác sĩ Linh",
   "address": "50/9 Quang Trung, phường 10, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8265829,
   "Latitude": 106.6792524
 },
 {
   "STT": 517,
   "Name": "Phòng khám Chuyên khoa Cơ Xương Khớp Bác sĩ Nguyễn Văn An",
   "address": "1096 Cách Mạng Tháng Tám, phường 4, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.7920111,
   "Latitude": 106.6550425
 },
 {
   "STT": 518,
   "Name": "Phòng khám Chuyên khoa Cơ Xương Khớp Bác sĩ Phạm Thái Hữu",
   "address": " 157 Nguyễn Kiệm, phường 3, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8131093,
   "Latitude": 106.6786774
 },
 {
   "STT": 519,
   "Name": "Phòng khám Chuyên khoa Cơ Xương Khớp Bác sĩ Quốc",
   "address": "113/87/46 An Dương Vương, phường An Lạc, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.7234443,
   "Latitude": 106.6198296
 },
 {
   "STT": 520,
   "Name": "Phòng khám Chuyên khoa Cơ Xương Khớp Bác sĩ Tăng Hà Nam Anh",
   "address": "24/2 Đoàn Thị Điểm, phường 1, Phú Nhuận, Hồ Chí Minh",
   "Longtitude": 10.7991758,
   "Latitude": 106.6810254
 },
 {
   "STT": 521,
   "Name": "Phòng khám Chuyên khoa Cơ Xương Khớp Bác sĩ Trần Công Tường",
   "address": "167/4 Sư Vạn Hạnh, phường 3, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.761205,
   "Latitude": 106.673425
 },
 {
   "STT": 522,
   "Name": "Phòng khám Chuyên khoa Cơ Xương Khớp Bác sĩ Trịnh Minh Tú",
   "address": "611/9A Điện Biên Phủ, phường 1, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.7692913,
   "Latitude": 106.6769053
 },
 {
   "STT": 523,
   "Name": "Phòng khám Chuyên khoa Cơ Xương Khớp Bác sĩ Vương Hữu Định",
   "address": " 162 Nguyễn Thị Nhỏ, phường 15, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.7748736,
   "Latitude": 106.6531553
 },
 {
   "STT": 524,
   "Name": "Phòng khám Chuyên khoa Cơ Xương Khớp Cột Sống Bác sĩ Nguyễn Ngọc Tuấn",
   "address": "109/24/24 Dương Bá Trạc, phường 1, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.7480524,
   "Latitude": 106.6896131
 },
 {
   "STT": 525,
   "Name": "Phòng khám Chuyên khoa Cơ Xương Khớp Bác sĩ Trương Tấn Trung",
   "address": "73 Huỳnh Thị Phụng, phường 4, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.740771,
   "Latitude": 106.6723433
 },
 {
   "STT": 526,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Hoàng Đức Hiệp",
   "address": "213 Âu Cơ, phường 5, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.773623,
   "Latitude": 106.648673
 },
 {
   "STT": 527,
   "Name": "Phòng khám Chuyên khoa Da & Laser Da 1",
   "address": "601 Xô Viết Nghệ Tĩnh, phường 26, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.810624,
   "Latitude": 106.7131412
 },
 {
   "STT": 528,
   "Name": "Phòng khám Chuyên khoa Da Liễu",
   "address": "170/5 Tuệ Tĩnh, phường 12, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.761215,
   "Latitude": 106.653559
 },
 {
   "STT": 529,
   "Name": "Phòng khám Chuyên khoa Da Liễu",
   "address": "945 Cách Mạng Tháng Tám, phường 7, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.789726,
   "Latitude": 106.6585209
 },
 {
   "STT": 530,
   "Name": "Phòng khám Chuyên khoa Da Liễu",
   "address": "286/8 Minh Phụng, phường 2, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.756381,
   "Latitude": 106.644019
 },
 {
   "STT": 531,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Huỳnh Mai",
   "address": "156 Cách Mạng Tháng Tám, phường 10, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.7788679,
   "Latitude": 106.679929
 },
 {
   "STT": 532,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Bùi Văn Đức",
   "address": "201 Nguyễn Biểu, phường 2, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7571004,
   "Latitude": 106.6831581
 },
 {
   "STT": 533,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Huỳnh Bá Đường Long",
   "address": "210 Tôn Đản, phường 8, Quận 4, Hồ Chí Minh",
   "Longtitude": 10.757823,
   "Latitude": 106.706387
 },
 {
   "STT": 534,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Lê Minh Thọ",
   "address": "351 Võ Văn Tần, phường 5, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.7710162,
   "Latitude": 106.6853643
 },
 {
   "STT": 535,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Mai Khánh Phong",
   "address": "82 Liên Tỉnh 5, phường 6, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.7361554,
   "Latitude": 106.6567027
 },
 {
   "STT": 536,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Nguyễn Kiến Châu",
   "address": "16 Bà Hom, phường 13, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.75485,
   "Latitude": 106.6278619
 },
 {
   "STT": 537,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Nguyễn Tất Thắng",
   "address": " 1057 Nguyễn Trãi, phường 14, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7529939,
   "Latitude": 106.6511834
 },
 {
   "STT": 538,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Nguyễn Viết Các",
   "address": "761 Nguyễn Kiệm, phường 3, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8185658,
   "Latitude": 106.6786617
 },
 {
   "STT": 539,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Bùi Thị Cúc",
   "address": "517/107 Nguyễn Tri Phương, phường 8, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7540791,
   "Latitude": 106.6692966
 },
 {
   "STT": 540,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Cẩm Trúc",
   "address": " 50 Phan Văn Hân, phường 19, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.791804,
   "Latitude": 106.707061
 },
 {
   "STT": 541,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Châu Văn Trở",
   "address": "142 Ngô Quyền, phường 5, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7624057,
   "Latitude": 106.6648972
 },
 {
   "STT": 542,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Châu Văn Trở",
   "address": " 574 Sư Vạn Hạnh, phường 12, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7701498,
   "Latitude": 106.6704731
 },
 {
   "STT": 543,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Chung Kim Hùng",
   "address": " 159 Bình Thới, phường 11, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.765613,
   "Latitude": 106.6472579
 },
 {
   "STT": 544,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Dương Thanh Hòa",
   "address": "201A Cô Giang, phường Cô Giang, Quận 1, Hồ Chí Minh",
   "Longtitude": 10.761246,
   "Latitude": 106.692913
 },
 {
   "STT": 545,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Dương Thị Minh Nguyệt",
   "address": "10 Phạm Đình Toái, phường 6, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.776183,
   "Latitude": 106.686038
 },
 {
   "STT": 546,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Hà Văn Phước",
   "address": "157/6 Đường 3 Tháng 2, phường 11, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7745087,
   "Latitude": 106.6792037
 },
 {
   "STT": 547,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Hoàng Thiện Quang",
   "address": "210 Tân Khai, phường 4, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.757873,
   "Latitude": 106.654013
 },
 {
   "STT": 548,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Hoàng Văn Minh",
   "address": "515 Hồng Bàng, phường 14, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7533463,
   "Latitude": 106.6531062
 },
 {
   "STT": 549,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Huỳnh Huy Hoàng",
   "address": "286/8 Minh Phụng, phường 2, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.756381,
   "Latitude": 106.644019
 },
 {
   "STT": 550,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Huỳnh Kim Phượng",
   "address": " 01 Đường số 15, phường 4, Quận 4, Hồ Chí Minh",
   "Longtitude": 10.7569828,
   "Latitude": 106.7039823
 },
 {
   "STT": 551,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Lê Hữu Bách",
   "address": "520 Tỉnh Lộ 8, xã Tân An Hội, huyện Củ Chi, Hồ Chí Minh",
   "Longtitude": 10.9820883,
   "Latitude": 106.5103991
 },
 {
   "STT": 552,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Lê Phương Mai",
   "address": " 1258 Trường Sa, phường 14, Phú Nhuận, Hồ Chí Minh",
   "Longtitude": 10.7909504,
   "Latitude": 106.6663185
 },
 {
   "STT": 553,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Lê Quốc Trung",
   "address": " 159A Đồng Đen, phường 11, Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.789911,
   "Latitude": 106.643253
 },
 {
   "STT": 554,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Lê Vũ Chí Phước",
   "address": "563/7B10 Bà Hạt, phường 8, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7671789,
   "Latitude": 106.6738165
 },
 {
   "STT": 555,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Lý Hữu Đức",
   "address": "175/4 Nguyễn Thiện Thuật, phường 1, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.7682391,
   "Latitude": 106.6790368
 },
 {
   "STT": 556,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Mai Chí Phương",
   "address": "21 Trần Phú, phường 4, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7629619,
   "Latitude": 106.6795508
 },
 {
   "STT": 557,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Nghiêm Linh Cát",
   "address": "285/125/19 Cách Mạng Tháng Tám, phường 12, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7799479,
   "Latitude": 106.672667
 },
 {
   "STT": 558,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Ngô Duy Đăng Khoa",
   "address": " 229 Nhật Tảo, phường 8, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7641939,
   "Latitude": 106.6659924
 },
 {
   "STT": 559,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Ngô Minh Vinh",
   "address": "419 Cách Mạng Tháng 8, phường 13, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7805605,
   "Latitude": 106.6750788
 },
 {
   "STT": 560,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Ngô Quốc Hưng",
   "address": " 446 Gia Phú, phường 3, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.7433041,
   "Latitude": 106.6462082
 },
 {
   "STT": 561,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Nguyễn Ánh Tuyết",
   "address": "253B Lê Văn Lương, phường Tân Quy, Quận 7, Hồ Chí Minh",
   "Longtitude": 10.7447841,
   "Latitude": 106.7044132
 },
 {
   "STT": 562,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Nguyễn Kim Thoa",
   "address": "81A Duy Tân, phường 15, Phú Nhuận, Hồ Chí Minh",
   "Longtitude": 10.795168,
   "Latitude": 106.6798699
 },
 {
   "STT": 563,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Nguyễn Thành Hy",
   "address": "800 Tỉnh Lộ 10, khu phố 1, phường Bình Trị Đông A, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.7580948,
   "Latitude": 106.6074925
 },
 {
   "STT": 564,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Nguyễn Thị Thùy Dung",
   "address": "21 Đường D4, khu dân cư Him Lam, phường Tân Hưng, Quận 7, Hồ Chí Minh",
   "Longtitude": 10.7450961,
   "Latitude": 106.6973865
 },
 {
   "STT": 565,
   "Name": "Phòng khám Chuyên khoa Da Liễu  Bác sĩ Nguyễn Thị Đan Thanh",
   "address": " 159/14 Hoàng Văn Thụ, phường 8, Phú Nhuận, Hồ Chí Minh",
   "Longtitude": 10.798591,
   "Latitude": 106.674988
 },
 {
   "STT": 566,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu  BS. Nguyễn Thị Đào",
   "address": " 134/48 Thành Thái, phường 12, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7689272,
   "Latitude": 106.6666533
 },
 {
   "STT": 567,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Phạm Thị Kim Anh",
   "address": " 437 Huỳnh Văn Bánh, phường 13, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.7917816,
   "Latitude": 106.6712986
 },
 {
   "STT": 568,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Phạm Thụy An",
   "address": "376 Nguyễn Tri Phương, phường 4, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.76425,
   "Latitude": 106.66807
 },
 {
   "STT": 569,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Phạm Thúy Ngà",
   "address": "42 Hoàng Văn Thụ, phường 9, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.799438,
   "Latitude": 106.67885
 },
 {
   "STT": 570,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Phạm Xuân Khoa",
   "address": "66/2 Nguyễn Văn Trỗi, phường 8, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.795213,
   "Latitude": 106.676935
 },
 {
   "STT": 571,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Phan Văn Phương",
   "address": "140 Bàu Cát 3, phường 14, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.7921852,
   "Latitude": 106.6440312
 },
 {
   "STT": 572,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Sơn",
   "address": "386 Bà Hạt, phường 8 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7667845,
   "Latitude": 106.6691362
 },
 {
   "STT": 573,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Trần Ngọc Ánh",
   "address": " 04 Hoa Cúc, phường 7, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.7989276,
   "Latitude": 106.6880135
 },
 {
   "STT": 574,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Trần Ngọc Ánh",
   "address": " 1C1 Phan Xích Long, phường 7, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.798117,
   "Latitude": 106.685173
 },
 {
   "STT": 575,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Trần Thị Bích Vân",
   "address": " 468 Lê Văn Việt, phường Tăng Nhơn Phú A, Quận 9, thành phố Hồ Chí Minh",
   "Longtitude": 10.8462882,
   "Latitude": 106.7984787
 },
 {
   "STT": 576,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Trần Thị Hoài Hương",
   "address": " 380/5 Nơ Trang Long, phường 13, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8252714,
   "Latitude": 106.7066752
 },
 {
   "STT": 577,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Trần Thị Thanh Mai",
   "address": "133 Hoa Lan, phường 2, quận Phú Nhuận, thành phố Hồ Chí Minh\n",
   "Longtitude": 10.797107,
   "Latitude": 106.6884899
 },
 {
   "STT": 578,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Trần Thiện Thuần",
   "address": " 495/3 Tô Hiến Thành, phường 14, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7735925,
   "Latitude": 106.661473
 },
 {
   "STT": 579,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Trần Văn Ngà",
   "address": " 12 Lý Tự Trọng, phường Bến Nghé, Quận 1, thành phố Hồ Chí Minh",
   "Longtitude": 10.782129,
   "Latitude": 106.704526
 },
 {
   "STT": 580,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Trần Văn Tính",
   "address": " 12 Quốc lộ 1A, phường Tân Thới Hiệp, Quận 12, thành phố Hồ Chí Minh",
   "Longtitude": 10.8670166,
   "Latitude": 106.642136
 },
 {
   "STT": 581,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Trịnh Xuân Thủy & Chuyên Khoa Nội Tổng Quát - BS. Trịnh Xuân Khánh",
   "address": " 63 Nguyễn Thông, phường 9, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.7818461,
   "Latitude": 106.679192
 },
 {
   "STT": 582,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Trương Lê Anh Tuấn",
   "address": " 19 Mai Văn Vĩnh, phường Tân Quy, Quận 7, thành phố Hồ Chí Minh",
   "Longtitude": 10.7417381,
   "Latitude": 106.713695
 },
 {
   "STT": 583,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Trương Luân",
   "address": " 278/16 Tô Hiến Thành, phường 15, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.782378,
   "Latitude": 106.667694
 },
 {
   "STT": 584,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Võ Thị Bạch Sương",
   "address": " 123 Hòa Hưng, phường 12, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.778431,
   "Latitude": 106.6726763
 },
 {
   "STT": 585,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Võ Thị Thanh Vân",
   "address": " 97 - 99 Đường số 10, phường 13, Quận 6, thành phố Hồ Chí Minh",
   "Longtitude": 10.7537312,
   "Latitude": 106.6269682
 },
 {
   "STT": 586,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Vũ Ngọc Thu",
   "address": " 295 Nguyễn Thượng Hiền, phường 4, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.7712248,
   "Latitude": 106.6857784
 },
 {
   "STT": 587,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Vũ Quang Bình",
   "address": " 453/127 Lê Văn Sỹ, phường 12, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.787841,
   "Latitude": 106.672313
 },
 {
   "STT": 588,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Đặng Hoàng Anh",
   "address": " 664/109 Nguyễn Đình Chiểu, phường 3, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.770126,
   "Latitude": 106.6783954
 },
 {
   "STT": 589,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - BS. Đỗ Xuân Thắng",
   "address": "72 Sương Nguyệt Ánh, phường Bến Thành, Quận 1, thành phố Hồ Chí Minh",
   "Longtitude": 10.7724696,
   "Latitude": 106.6893428
 },
 {
   "STT": 590,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - TS.BS Lê Thái Vân Thanh",
   "address": "14 - 16 Hòa Bình, phường 5, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.7629739,
   "Latitude": 106.650084
 },
 {
   "STT": 591,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu O2SKIN",
   "address": "343/5F Tô Hiến Thành, phường 12, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.77634,
   "Latitude": 106.6653402
 },
 {
   "STT": 592,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu Phan Hoàng Mỹ",
   "address": " 82 đường số 23, phường 14, Quận 6, thành phố Hồ Chí Minh",
   "Longtitude": 10.7424579,
   "Latitude": 106.6253989
 },
 {
   "STT": 593,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu Sắc Việt - BS. Võ Quang Đỉnh",
   "address": " 352 Lạc Long Quân, phường 5, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.768629,
   "Latitude": 106.643952
 },
 {
   "STT": 594,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu Táo Đỏ",
   "address": " 30/1A Ngô Thời Nhiệm, phường 7, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.777619,
   "Latitude": 106.686545
 },
 {
   "STT": 595,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu Trần Thịnh",
   "address": " 980 Trần Hưng Đạo, phường 7, Quận 5, thành phố Hồ Chí Minh",
   "Longtitude": 10.7531307,
   "Latitude": 106.6723938
 },
 {
   "STT": 596,
   "Name": "Phòng Khám Chuyên Khoa Da Sài Gòn Đẹp",
   "address": " Tầng 1, 254 Trần Hưng Đạo, phường Cầu Kho, Quận 1, thành phố Hồ Chí Minh",
   "Longtitude": 10.76115,
   "Latitude": 106.688807
 },
 {
   "STT": 597,
   "Name": "Phòng Khám Chuyên Khoa Da Skinic",
   "address": " 366 Cao Thắng, phường 12, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7751072,
   "Latitude": 106.6737056
 },
 {
   "STT": 598,
   "Name": "Phòng Khám Chuyên Khoa Da Stamford",
   "address": " 99 Sương Nguyệt Ánh, phường Bến Thành, Quận 1, thành phố Hồ Chí Minh",
   "Longtitude": 10.7709325,
   "Latitude": 106.6882539
 },
 {
   "STT": 599,
   "Name": "Phòng Khám Chuyên Khoa Dinh Dưỡng Nhi - BS. Đặng Thị Phương Lan",
   "address": " 66/46/31F Đường 3 Tháng 2, phường 14, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7654739,
   "Latitude": 106.6629684
 },
 {
   "STT": 600,
   "Name": "Phòng Khám Chuyên Khoa Gan - Mật - BS. Lý Thị Mỹ Dung",
   "address": "3 Lê Đại Hành, phường 4 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.758629,
   "Latitude": 106.659148
 },
 {
   "STT": 601,
   "Name": "Phòng Khám Chuyên Khoa Hô Hấp & Chuyên Khoa Nhi - BS. Lê Thị Ngọc Bích",
   "address": " 70 Hải Thượng Lãn Ông, phường 10, Quận 5, thành phố Hồ Chí Minh",
   "Longtitude": 10.7511889,
   "Latitude": 106.6627489
 },
 {
   "STT": 602,
   "Name": "Phòng Khám Chuyên Khoa Hô Hấp Nhi - BS. Đỗ Châu Việt",
   "address": " 399/8 Nguyễn Đình Chiểu, phường 5, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.7709899,
   "Latitude": 106.6835793
 },
 {
   "STT": 603,
   "Name": "Phòng Khám Chuyên Khoa Hòa Hưng",
   "address": " 130B Cách Mạng Tháng Tám, phường 11, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.779886,
   "Latitude": 106.673742
 },
 {
   "STT": 604,
   "Name": "Phòng Khám Chuyên Khoa Hồng Phúc Gia Định",
   "address": " 143 Bạch Đằng, phường 15, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8028549,
   "Latitude": 106.708605
 },
 {
   "STT": 605,
   "Name": "Phòng Khám Chuyên Khoa Lao & Bệnh Phổi - BS. Lê Bá Quang",
   "address": " 100B Phú Thọ, phường 2, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.757665,
   "Latitude": 106.641889
 },
 {
   "STT": 606,
   "Name": "Phòng Khám Chuyên Khoa Lao & Bệnh Phổi - BS. Lê Tấn Phong",
   "address": " 418/31 Hồng Bàng, phường 16, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.754426,
   "Latitude": 106.649247
 },
 {
   "STT": 607,
   "Name": "Phòng Khám Chuyên Khoa Lao & Bệnh Phổi - BS. Lê Tiến Dũng",
   "address": "291 Hàn Hải Nguyên, phường 2, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.758215,
   "Latitude": 106.64449
 },
 {
   "STT": 608,
   "Name": "Phòng Khám Chuyên Khoa Lao & Bệnh Phổi - BS. Ngô Thanh Bình",
   "address": " 189 Phó Cơ Điều, phường 6, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.761455,
   "Latitude": 106.656895
 },
 {
   "STT": 609,
   "Name": "Phòng Khám Chuyên Khoa Lao & Bệnh Phổi - BS. Nguyễn Hải Công",
   "address": "768 Nguyễn Kiệm, phường 3, Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 610,
   "Name": "Phòng Khám Chuyên Khoa Lao & Bệnh Phổi - BS. Nguyễn Như Vinh",
   "address": " 28/7 Nguyễn Cảnh Chân, phường Cầu Kho, Quận 1, thành phố Hồ Chí Minh",
   "Longtitude": 10.7603082,
   "Latitude": 106.686599
 },
 {
   "STT": 611,
   "Name": "Phòng Khám Chuyên Khoa Lao & Bệnh Phổi - BS. Nguyễn Quang Hiển\n",
   "address": "4F Đường Số 7, Cư Xá Bình Thới, phường 8, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.763276,
   "Latitude": 106.6479333
 },
 {
   "STT": 612,
   "Name": "Phòng Khám Chuyên Khoa Lao & Bệnh Phổi - BS. Nguyễn Văn Tẩn",
   "address": " Lô A (Chung Cư Hùng Vương), 5 Đặng Thái Thân, phường 11, Quận 5, thành phố Hồ Chí Minh",
   "Longtitude": 10.7540578,
   "Latitude": 106.6648965
 },
 {
   "STT": 613,
   "Name": "Phòng Khám Chuyên Khoa Lao & Bệnh Phổi - BS. Nguyễn Đình Duy",
   "address": "335/7 Điện Biên Phủ, phường 4, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.7751224,
   "Latitude": 106.6832348
 },
 {
   "STT": 614,
   "Name": "Phòng Khám Chuyên Khoa Lao & Bệnh Phổi - BS. Quang Văn Trí",
   "address": " 106 - 108 Hùng Vương, phường 9, Quận 5, thành phố Hồ Chí Minh",
   "Longtitude": 10.757499,
   "Latitude": 106.6685101
 },
 {
   "STT": 615,
   "Name": "Phòng Khám Chuyên Khoa Lao & Phổi - BS. Nguyễn Sơn Lam",
   "address": "26/27 Tân Sơn Nhì, phường Tân Sơn Nhì, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.796388,
   "Latitude": 106.630744
 },
 {
   "STT": 616,
   "Name": "Phòng Khám Chuyên Khoa Lao - BS Diệp",
   "address": "149/71/12 Bành Văn Trân, phường 7, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.790056,
   "Latitude": 106.653959
 },
 {
   "STT": 617,
   "Name": "Phòng Khám Chuyên Khoa Lao - BS Phúc",
   "address": "1103A Tự Lập, phường 4, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.792607,
   "Latitude": 106.655951
 },
 {
   "STT": 618,
   "Name": "Phòng Khám Chuyên Khoa Lao - BS. Lê Phước Hải",
   "address": "\n43 Âu Cơ, phường 14, Quận 11, thành phố Hồ Chí Minh\n",
   "Longtitude": 10.770022,
   "Latitude": 106.651073
 },
 {
   "STT": 619,
   "Name": "Phòng Khám Chuyên Khoa Lao - BS. Nguyễn Xuân Quang",
   "address": " TK24/9 Nguyễn Cảnh Chân, phường Cầu Kho, Quận 1, thành phố Hồ Chí Minh",
   "Longtitude": 10.760468,
   "Latitude": 106.686986
 },
 {
   "STT": 620,
   "Name": "Phòng Khám Chuyên Khoa Lao - BS. Trương Quang Hiển",
   "address": "28 Đường số 7 - CX Bình Thới, phường 8, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.7607722,
   "Latitude": 106.6478906
 },
 {
   "STT": 621,
   "Name": "Phòng Khám Chuyên Khoa Lao -Tâm Thần - Da Liễu",
   "address": " 389/5 Tỉnh Lộ 10, phường An Lạc A, quận Bình Tân, thành phố Hồ Chí Minh",
   "Longtitude": 10.7567662,
   "Latitude": 106.6223142
 },
 {
   "STT": 622,
   "Name": "Phòng Khám Chuyên Khoa Lao Phổi - BS. Nguyễn Đặng Tiệp",
   "address": " 8 Lê Hoàng Phái, phường 17, Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8397321,
   "Latitude": 106.6755703
 },
 {
   "STT": 623,
   "Name": "Phòng Khám Chuyên Khoa Lao Phổi - BS. Phạm Hồng Cách",
   "address": " 23 Phan Văn Trị, phường 5, Quận 5, thành phố Hồ Chí Minh",
   "Longtitude": 10.823729,
   "Latitude": 106.69094
 },
 {
   "STT": 624,
   "Name": "Phòng Khám Chuyên Khoa Lao Phổi - BS. Trương Nhuận Xương",
   "address": "73 Cao Thắng, phường 3, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.7716141,
   "Latitude": 106.6799887
 },
 {
   "STT": 625,
   "Name": "Phòng Khám Chuyên Khoa Lao Phổi - BS. Đỗ Ánh Hà",
   "address": "\n1A8 Nguyễn Thái Sơn, phường 3, Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8158832,
   "Latitude": 106.6797352
 },
 {
   "STT": 626,
   "Name": "Phòng Khám Chuyên Khoa Mắt",
   "address": "96 Phan Văn Hân, phường 17, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.793513,
   "Latitude": 106.706847
 },
 {
   "STT": 627,
   "Name": "Phòng Khám Chuyên Khoa Mắt",
   "address": " 11 Lê Lộ, phường Phú Thạnh, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.7834645,
   "Latitude": 106.6234836
 },
 {
   "STT": 628,
   "Name": "Phòng Khám Chuyên Khoa Mắt",
   "address": " 64 Nguyễn Hồng Đào, phường 14, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.7959509,
   "Latitude": 106.6429204
 },
 {
   "STT": 629,
   "Name": "Phòng Khám Chuyên Khoa Mắt & Nhi - BS. Đặng Trung Hiếu",
   "address": " 200 Tân Quý, phường Tân Quý, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.7914687,
   "Latitude": 106.6206356
 },
 {
   "STT": 630,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS Bùi Việt Hoàng",
   "address": "46/2Bis Trần Quang Diệu, phường 14, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.7881313,
   "Latitude": 106.6782608
 },
 {
   "STT": 631,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS Chính & BS Phượng",
   "address": "340C/33-35 Hoàng Văn Thụ, phường 4, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.7977025,
   "Latitude": 106.6580575
 },
 {
   "STT": 632,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS Diệp Hữu Thắng",
   "address": " 211 Lê Thánh Tôn, phường Bến Thành, Quận 1, thành phố Hồ Chí Minh",
   "Longtitude": 10.7724027,
   "Latitude": 106.6968289
 },
 {
   "STT": 633,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS Ngọc Linh",
   "address": "6 Đường số 4, phường An Lạc A, quận Bình Tân, thành phố Hồ Chí Minh",
   "Longtitude": 10.7487115,
   "Latitude": 106.6204456
 },
 {
   "STT": 634,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS Nguyễn Hữu Chức",
   "address": "79 Hòa Hưng, phường 12, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7795058,
   "Latitude": 106.6751933
 },
 {
   "STT": 635,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS Nguyễn Thế Hồ",
   "address": "\n417 Sư Vạn Hạnh, phường 12, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7723769,
   "Latitude": 106.6692358
 },
 {
   "STT": 636,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS Nguyễn Thị Ngọc Liên",
   "address": "30/1G Ngô Thời Nhiệm, phường 7, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.777619,
   "Latitude": 106.686545
 },
 {
   "STT": 637,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS Nguyễn Trí Dũng & BS Nguyễn Ngọc Châu Trang",
   "address": "202 Nguyễn Đình Chiểu, phường 6, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.778928,
   "Latitude": 106.69017
 },
 {
   "STT": 638,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS Phạm Văn Hoàng",
   "address": "202 Nguyễn Đình Chiểu, phường 6, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.778928,
   "Latitude": 106.69017
 },
 {
   "STT": 639,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS Thanh Trúc",
   "address": "93/2 Phạm Văn Hai, phường 3, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.79229,
   "Latitude": 106.662906
 },
 {
   "STT": 640,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS Trang Thanh Nghiệp",
   "address": "523 Hồng Bàng, phường 14 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7533584,
   "Latitude": 106.6527327
 },
 {
   "STT": 641,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS Đoàn Hồng Dung",
   "address": "45 Nguyễn Lâm, phường 6, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7622962,
   "Latitude": 106.6627339
 },
 {
   "STT": 642,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Bùi Trung Dũng",
   "address": "298 Tùng Thiện Vương, phường 13 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.74614,
   "Latitude": 106.659359
 },
 {
   "STT": 643,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Huỳnh Nghiệp - BS. Giao My",
   "address": "C005 Lê Hồng Phong, phường 2 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7557186,
   "Latitude": 106.6785533
 },
 {
   "STT": 644,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Huỳnh Tấn Cảnh",
   "address": "405A Xô Viết Nghệ Tĩnh, phường 24, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8053329,
   "Latitude": 106.71132
 },
 {
   "STT": 645,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Huỳnh Thị Bích Liễu",
   "address": "535 An Dương Vương, phường 8 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7575984,
   "Latitude": 106.674688
 },
 {
   "STT": 646,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Lê Hồng Hà",
   "address": "78 Hồ Văn Huê, phường 9, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.8012823,
   "Latitude": 106.6759351
 },
 {
   "STT": 647,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Lê Quang Thụy",
   "address": "401/17/2 Cây Trâm, phường 8, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.843834,
   "Latitude": 106.648679
 },
 {
   "STT": 648,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Lê Thanh Hải",
   "address": "114 Gia Phú, phường 1, Quận 6, thành phố Hồ Chí Minh",
   "Longtitude": 10.747277,
   "Latitude": 106.653438
 },
 {
   "STT": 649,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Lương Ngọc Tuấn",
   "address": "638A Nguyễn Trãi, phường 8 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7540678,
   "Latitude": 106.6666965
 },
 {
   "STT": 650,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Lương Thu Hà",
   "address": "843/22 Nguyễn Kiệm, phường 3, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8118824,
   "Latitude": 106.6786079
 },
 {
   "STT": 651,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Mai Ngọc Quế",
   "address": "630 Nguyễn Chí Thanh, phường 4 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.758123,
   "Latitude": 106.65871
 },
 {
   "STT": 652,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Mai Đăng Tâm",
   "address": "220/133 Trần Quang Diệu, phường 14 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.790096,
   "Latitude": 106.6801739
 },
 {
   "STT": 653,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Ngô Thanh Tùng",
   "address": "171 Tôn Thất Hiệp, phường 12 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7624099,
   "Latitude": 106.652374
 },
 {
   "STT": 654,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Ngô Thị Kim Loan",
   "address": "45 Đặng Dung, phường Tân Định , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7926062,
   "Latitude": 106.690415
 },
 {
   "STT": 655,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Nguyễn Công Kiệt",
   "address": "805 Nguyễn Trãi, phường 14 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7527987,
   "Latitude": 106.6582555
 },
 {
   "STT": 656,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Nguyễn Hoàng Cẩn",
   "address": "156/5 Đặng Văn Ngữ, phường 14, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.792294,
   "Latitude": 106.667691
 },
 {
   "STT": 657,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Nguyễn Liên Sơn",
   "address": "753/7 Tỉnh lộ 10, phường Bình Trị Đông B , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7580974,
   "Latitude": 106.6172997
 },
 {
   "STT": 658,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Nguyễn Long Vượng",
   "address": "2788 Phạm Thế Hiển, phường 7 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.718408,
   "Latitude": 106.632977
 },
 {
   "STT": 659,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Nguyễn Ngọc Anh",
   "address": "125/13 Lê Đức Thọ, phường 17, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.842609,
   "Latitude": 106.674639
 },
 {
   "STT": 660,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Nguyễn Phú Thiện",
   "address": "772 Hồng Bàng, phường 1 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.754921,
   "Latitude": 106.6385449
 },
 {
   "STT": 661,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Nguyễn Quang Huy",
   "address": "280 Lê văn Sỹ, phường 13 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.786878,
   "Latitude": 106.680036
 },
 {
   "STT": 662,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Nguyễn Quốc Toản",
   "address": "394/6 Minh Phụng, phường 9 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.759826,
   "Latitude": 106.644557
 },
 {
   "STT": 663,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Nguyễn Sĩ Hùng",
   "address": "163/14/7 Tô Hiến Thành, phường 13 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7789256,
   "Latitude": 106.6682711
 },
 {
   "STT": 664,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Nguyễn Thị Tú Uyên",
   "address": "5 Trương Vĩnh Ký, phường Tân Thành, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.79317,
   "Latitude": 106.637366
 },
 {
   "STT": 665,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Nguyễn Thị Tuyết Minh",
   "address": "543B Minh Phụng, phường 10 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.763168,
   "Latitude": 106.6445899
 },
 {
   "STT": 666,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Nguyễn Thị Uyên Duyên",
   "address": "179/11 Hoà Bình, phường Hiệp Tân, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.791457,
   "Latitude": 106.6293762
 },
 {
   "STT": 667,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Nguyễn Thị Điệp",
   "address": "763 Tỉnh Lộ 7, Xã Phước Thạnh , huyện Củ Chi , thành phố Hồ Chí Minh",
   "Longtitude": 11.0573698,
   "Latitude": 106.4727133
 },
 {
   "STT": 668,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Nguyễn Trần Thúy Hằng",
   "address": "47A Đặng Văn Ngữ, phường 14, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.7933247,
   "Latitude": 106.6688088
 },
 {
   "STT": 669,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Nguyễn Văn Hảo",
   "address": "20 đường 3A Cư Xá Bình Thới, phường 8 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7625441,
   "Latitude": 106.6490997
 },
 {
   "STT": 670,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Nguyễn Xuân Vũ",
   "address": "11 Nguyễn Văn Đừng, phường 6 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7509715,
   "Latitude": 106.6710881
 },
 {
   "STT": 671,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Phạm Huy Tảo & Chuyên Khoa Nội Nhi - BS. Vũ Thị Thanh Huyền",
   "address": "319.A10 Lý Thường Kiệt, phường 15 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7775688,
   "Latitude": 106.6555811
 },
 {
   "STT": 672,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Thanh",
   "address": "11/37 Nguyễn Thái Sơn, phường 3, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8206412,
   "Latitude": 106.6835277
 },
 {
   "STT": 673,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Thịnh",
   "address": "27/4C Đường Hiệp Thành 13, phường Hiệp Thành , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.88457,
   "Latitude": 106.636242
 },
 {
   "STT": 674,
   "Name": "Phòng Khám Chuyên Khoa Mắt - Bs. Trần Anh Tuấn",
   "address": "172 Lương Nhữ Học, phường 11 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7534234,
   "Latitude": 106.6602117
 },
 {
   "STT": 675,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Trần Duy Kiên",
   "address": "20 Hoàng Dư Khương, phường 12 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7762297,
   "Latitude": 106.6720604
 },
 {
   "STT": 676,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Trần Huy Hoàng",
   "address": "49 Lê Văn Sỹ, phường 13, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.7905448,
   "Latitude": 106.6731613
 },
 {
   "STT": 677,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Trần Kế Tổ",
   "address": "37B/1 Nguyễn Thông, phường 7 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7760877,
   "Latitude": 106.6866892
 },
 {
   "STT": 678,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Trần Phan Việt Nga",
   "address": "189B/A26 Cống Quỳnh, phường Nguyễn Cư Trinh , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.765806,
   "Latitude": 106.685576
 },
 {
   "STT": 679,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Trần Văn Tây",
   "address": "185 Thái Phiên, phường 9 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.759994,
   "Latitude": 106.646101
 },
 {
   "STT": 680,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Trần Đình Tùng",
   "address": "23 Trần Xuân Hoàn, phường 7 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7532368,
   "Latitude": 106.667059
 },
 {
   "STT": 681,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Trịnh Bạch Tuyết",
   "address": "43D/2 Hồ Văn Huê, phường 9, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.799811,
   "Latitude": 106.675157
 },
 {
   "STT": 682,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Võ Nguyên Uyên Thảo",
   "address": "164D Võ Thị Sáu, phường 6 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.786431,
   "Latitude": 106.690358
 },
 {
   "STT": 683,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Võ Quang Minh",
   "address": "38/11 Hoàng Văn Thụ, phường 9, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.799673,
   "Latitude": 106.678869
 },
 {
   "STT": 684,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Võ Thị Hoàng Lan",
   "address": "175/4 Nguyễn Thiện Thuật,phường 1 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7682391,
   "Latitude": 106.6790368
 },
 {
   "STT": 685,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Võ Văn Nghiêm",
   "address": "990 Trần Hưng Đạo, phường 7 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.753124,
   "Latitude": 106.6721844
 },
 {
   "STT": 686,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Vũ Thị Việt Thu",
   "address": "1243 Phan Văn Trị, phường 10, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8330997,
   "Latitude": 106.6711074
 },
 {
   "STT": 687,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Định Thị Bích Thanh",
   "address": "209 Điện Biên Phủ, phường 6 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.784878,
   "Latitude": 106.692465
 },
 {
   "STT": 688,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Đoàn Trọng Hậu",
   "address": "24/2 Phạm Ngọc Thạch, phường 3, Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.78456,
   "Latitude": 106.693205
 },
 {
   "STT": 689,
   "Name": "Phòng Khám Chuyên Khoa Mắt - TS.BS. Trần Thị Phương Thu & TS.BS. Võ Đức Dũng",
   "address": "521A Điện Biên Phủ, phường 3 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7710571,
   "Latitude": 106.6774222
 },
 {
   "STT": 690,
   "Name": "Phòng Khám Chuyên Khoa Mắt -BS. Nguyễn Thị Kim Dung",
   "address": "115/61/36 Lò Siêu, phường 8, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.759585,
   "Latitude": 106.650051
 },
 {
   "STT": 691,
   "Name": "Phòng Khám Chuyên Khoa Mắt American Eye Center",
   "address": "105 Tôn Dật Tiên, Tầng 5, Crescent Plaza (Phú Mỹ Hưng),phường Tân Phú , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7273525,
   "Latitude": 106.7199267
 },
 {
   "STT": 692,
   "Name": "Phòng Khám Chuyên Khoa Mắt BS. Dương Quang Huỳnh Nga",
   "address": "44 Tản Đà, phường 10 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7518491,
   "Latitude": 106.6646627
 },
 {
   "STT": 693,
   "Name": "Phòng Khám Chuyên Khoa Mắt BS. Nguyễn Thị Diễm Uyên",
   "address": "16 Bàn Cờ, phường 3 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7696784,
   "Latitude": 106.6807044
 },
 {
   "STT": 694,
   "Name": "Phòng Khám Chuyên Khoa Mắt - BS. Đoàn Quốc Việt",
   "address": "130 Cao Lỗ, phường 4 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7398303,
   "Latitude": 106.67727
 },
 {
   "STT": 695,
   "Name": "Phòng Khám Chuyên Khoa Ngoại & Chấn Thương Chỉnh Hình - BS. Lương Đình Lâm",
   "address": "495/8/21 Tô Hiến Thành, phường 14 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7728225,
   "Latitude": 106.6617972
 },
 {
   "STT": 696,
   "Name": "Phòng Khám Chuyên Khoa Ngoại (Tiết Niệu) - BS. Nguyễn Thành Như",
   "address": "07 Nguyễn Thông, phường 6 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7763299,
   "Latitude": 106.6864943
 },
 {
   "STT": 697,
   "Name": "Phòng Khám Chuyên Khoa Ngoại - BS. Lê Thể Đăng",
   "address": "186 Mai Xuân Thưởng, phường 2 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7488705,
   "Latitude": 106.6478662
 },
 {
   "STT": 698,
   "Name": "Phòng Khám Chuyên Khoa Ngoại - BS. Ngô Văn Độ",
   "address": "27 Đường Số 30, phường 6, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8433281,
   "Latitude": 106.6777757
 },
 {
   "STT": 699,
   "Name": "Phòng Khám Chuyên Khoa Ngoại - BS. Phạm Văn Tịnh",
   "address": "29 Trương Văn Hải, phường Tăng Nhơn Phú B , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8423477,
   "Latitude": 106.7788991
 },
 {
   "STT": 700,
   "Name": "Phòng Khám Chuyên Khoa Ngoại BS Phạm Văn Kiên - Phụ Khoa BS Huỳnh Thị Thu Thủy",
   "address": "55/30 Nguyễn Chế Nghĩa, phường 13 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.743914,
   "Latitude": 106.653931
 },
 {
   "STT": 701,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Chấn Thương Chỉnh Hình - BS. Lê Văn Thọ",
   "address": "268-269 Bãi Sậy, phường 8 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7473353,
   "Latitude": 106.6456876
 },
 {
   "STT": 702,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Chỉnh Hình - BS. Trần Đức Vinh",
   "address": "86 Huỳnh Mẫn Đạt, phường 2 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7556263,
   "Latitude": 106.6766064
 },
 {
   "STT": 703,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Nhi - BS. Lê Tấn Sơn & Nội Tổng Quát - BS. Nguyễn Thanh Thúy",
   "address": "93 Tôn Thất Hiệp, phường 13 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764113,
   "Latitude": 106.653212
 },
 {
   "STT": 704,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Thần Kinh - BS. Lê Điền Nhi",
   "address": "22/78 Cư xá Lữ Gia, phường 15 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.774087,
   "Latitude": 106.655522
 },
 {
   "STT": 705,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Thần Kinh - BS. Nguyễn Minh Quan",
   "address": "53/40/1 Vườn Lài, phường Phú Thọ Hòa, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.786689,
   "Latitude": 106.633239
 },
 {
   "STT": 706,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Thần Kinh - BS. Trần Quang Vinh & Siêu Âm Tổng Quát - BS. Lâm Thị Lệ Hằng",
   "address": "783 Trần Xuân Soạn, Chung cư Hoàng Anh Gia Lai 2, phường Tân Hưng , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7516757,
   "Latitude": 106.6992509
 },
 {
   "STT": 707,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Thần Kinh - BS. Võ Xuân Sơn",
   "address": "500/19 Nguyễn Chí Thanh,phường 7 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7590542,
   "Latitude": 106.6620246
 },
 {
   "STT": 708,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Thận Niệu & Nam Khoa - BS. Đoàn Trí Dũng",
   "address": "220/109 Lê Văn Sỹ, phường 14 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.788302,
   "Latitude": 106.681284
 },
 {
   "STT": 709,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Thận Niệu - BS. Trần Lê Linh Phương",
   "address": "109 Đỗ Ngọc Thạnh, phường 15 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7566517,
   "Latitude": 106.6559002
 },
 {
   "STT": 710,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tiết Niệu - BS. Trần Ngọc Sinh",
   "address": "79 Lê Đại Hành, phường 6 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.760496,
   "Latitude": 106.658097
 },
 {
   "STT": 711,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tim Mạch - BS. Châu Phú Thi",
   "address": "46 Tân Hương, phường Tân Quý, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.7913685,
   "Latitude": 106.6284506
 },
 {
   "STT": 712,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tổng Hợp - BS Huỳnh Thị Linh Thu",
   "address": "3/3A Nguyễn Văn Thủ, phường Đa Kao , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7924243,
   "Latitude": 106.7014773
 },
 {
   "STT": 713,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tổng Hợp - BS Nguyễn Thanh An",
   "address": "457 Điện Biên Phủ, phường 3 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7720778,
   "Latitude": 106.6785117
 },
 {
   "STT": 714,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tổng Hợp - BS. Lê Quang Đình",
   "address": "3 Xóm Vôi, phường 14 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7517738,
   "Latitude": 106.6515818
 },
 {
   "STT": 715,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tổng Hợp - BS. Lương Trung Hiếu",
   "address": "113 Xóm Củi, phường 11 , Quận 8, thành phố Hồ Chí Minh",
   "Longtitude": 10.7459662,
   "Latitude": 106.6610253
 },
 {
   "STT": 716,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tổng Hợp - BS. Phạm Quang Tuyến",
   "address": "126/14A Phan Văn Trị, phường 12, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.809871,
   "Latitude": 106.696749
 },
 {
   "STT": 717,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tổng Hợp - BS. Phan Anh Tuấn",
   "address": "18 Đặng Chất, phường 3 , Quận 8, thành phố Hồ Chí Minh",
   "Longtitude": 10.7474207,
   "Latitude": 106.6847356
 },
 {
   "STT": 718,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tổng Hợp - BS. Phan Hòa",
   "address": "16 Nguyễn Bá Học, phường 4, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.758044,
   "Latitude": 106.656439
 },
 {
   "STT": 719,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tổng Hợp - BS. Trương Đình Khải",
   "address": "37A Xóm Đất, phường 8 , Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.758518,
   "Latitude": 106.6483499
 },
 {
   "STT": 720,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tổng Hợp - ThS.BS. Phạm Đình Duy",
   "address": "8 Nguyễn Lâm, phường 6 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7619273,
   "Latitude": 106.6633918
 },
 {
   "STT": 721,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tổng Quát & Tim Mạch - PGS. TS. Võ Thành Nhân",
   "address": "61 Hàn Hải Nguyên, phường 2 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7571,
   "Latitude": 106.645338
 },
 {
   "STT": 722,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tổng Quát - BS. Chương",
   "address": "43 Nam Cao, phường Tân Phú ,Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8599457,
   "Latitude": 106.7997675
 },
 {
   "STT": 723,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tổng Quát - BS. Hồ Ngọc Điệp",
   "address": "144 Phan Văn Trị, phường 12, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.810614,
   "Latitude": 106.695974
 },
 {
   "STT": 724,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tổng Quát - BS. Hồ Tiêm",
   "address": "12/8 Trương Định, phường 6 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.776528,
   "Latitude": 106.691415
 },
 {
   "STT": 725,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tổng Quát - BS. Nguyễn Hữu Thiện",
   "address": "683 Kinh Dương Vương, phường An Lạc, quận Bình Tân, thành phố Hồ Chí Minh",
   "Longtitude": 10.7360744,
   "Latitude": 106.6140316
 },
 {
   "STT": 726,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tổng Quát - BS. Nguyễn Thanh Nguyện",
   "address": "24 Đỗ Quang Đẩu, phường Phạm Ngũ Lão , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.766639,
   "Latitude": 106.691247
 },
 {
   "STT": 727,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tổng Quát - BS. Nguyễn Đình Xương",
   "address": "95/2D Phạm Văn Hai, phường 3, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.79229,
   "Latitude": 106.662906
 },
 {
   "STT": 728,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tổng Quát - BS. Phạm Văn Tấn",
   "address": "216B Trần Hưng Đạo, phường 11 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7525903,
   "Latitude": 106.6660366
 },
 {
   "STT": 729,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tổng Quát - BS. Quách Thanh Hưng",
   "address": "219 Lê Đại Hành, phường 13 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.763558,
   "Latitude": 106.65581
 },
 {
   "STT": 730,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tổng Quát - BS. Trịnh Quốc Minh",
   "address": "1428 Ba Tháng Hai, phường 16 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7549582,
   "Latitude": 106.6434853
 },
 {
   "STT": 731,
   "Name": "Phòng Khám Chuyên Khoa Ngoại Tổng Quát - BS. Đoàn Công Tiến",
   "address": "41 Ngụy Như Kon Tum, phường Hiệp Tân, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.775034,
   "Latitude": 106.6320805
 },
 {
   "STT": 732,
   "Name": "Phòng Khám Chuyên Khoa Nhi",
   "address": "231 Phan Văn Hưng, phường 17, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.7962866,
   "Latitude": 106.7065103
 },
 {
   "STT": 733,
   "Name": "Phòng Khám Chuyên Khoa Nhi",
   "address": "27/4 Ấp 3, Quốc Lộ 1A, Xã Tân Quý Tây , Huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.6792011,
   "Latitude": 106.5865874
 },
 {
   "STT": 734,
   "Name": "Phòng Khám Chuyên Khoa Nhi",
   "address": "94 Đường số 1, phường An Lạc A, quận Bình Tân, thành phố Hồ Chí Minh",
   "Longtitude": 10.7485482,
   "Latitude": 106.6173677
 },
 {
   "STT": 735,
   "Name": "Phòng Khám Chuyên Khoa Nhi",
   "address": "1227 Tỉnh Lộ 10, phường Tân Tạo, quận Bình Tân, thành phố Hồ Chí Minh",
   "Longtitude": 10.7556616,
   "Latitude": 106.5939441
 },
 {
   "STT": 736,
   "Name": "Phòng Khám Chuyên Khoa Nhi",
   "address": "27 Phú Lộc, phường 6, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.7877094,
   "Latitude": 106.6610984
 },
 {
   "STT": 737,
   "Name": "Phòng Khám Chuyên Khoa Nhi",
   "address": "133 Hàn Hải Nguyên, phường 2 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.757991,
   "Latitude": 106.647745
 },
 {
   "STT": 738,
   "Name": "Phòng Khám Chuyên Khoa Nhi",
   "address": "24 - 26 Lạc Long Quân, phường 3, Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7590182,
   "Latitude": 106.6410006
 },
 {
   "STT": 739,
   "Name": "Phòng Khám Chuyên Khoa Nhi",
   "address": "533B Cách Mạng Tháng Tám, phường 15, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7829529,
   "Latitude": 106.671896
 },
 {
   "STT": 740,
   "Name": "Phòng Khám Chuyên Khoa Nhi & Dinh Dưỡng - BS Nguyễn Thị Hoa",
   "address": "165/1B Văn Thân, phường 8 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.741494,
   "Latitude": 106.637223
 },
 {
   "STT": 741,
   "Name": "Phòng Khám Chuyên Khoa Nhi & Dinh Dưỡng - BS. Nguyễn Thị Ngọc Hương",
   "address": "25 Trần Quang Diệu, phường 14 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7898992,
   "Latitude": 106.6780736
 },
 {
   "STT": 742,
   "Name": "Phòng Khám Chuyên Khoa Nhi & Hô Hấp - BS. Mai Đình Quý & BS. Mai Thanh Thuỷ",
   "address": "021K Chung Cư Ngô Gia Tự, Sư Vạn Hạnh, phường 2 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7624208,
   "Latitude": 106.6733813
 },
 {
   "STT": 743,
   "Name": "Phòng Khám Chuyên Khoa Nhi & Nội Tổng Quát - Bs.Trần Thị Bích Hằng & BS. Phan Duy Quang",
   "address": "18A đường số 10, phường Trường Thọ , quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8514192,
   "Latitude": 106.7641083
 },
 {
   "STT": 744,
   "Name": "Phòng Khám Chuyên Khoa Nhi - Bệnh Viện Hùng Vương",
   "address": "249-251 Hồng Bàng, phường 11 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.754952,
   "Latitude": 106.6618082
 },
 {
   "STT": 745,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Bùi Xuân Vũ",
   "address": "50 Hậu Giang, phường 2 , Quận 6, thành phố Hồ Chí Minh",
   "Longtitude": 10.7497199,
   "Latitude": 106.6475864
 },
 {
   "STT": 746,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Cao Hoài Nhân",
   "address": "429 Nguyễn Văn Luông, phường 12 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.752103,
   "Latitude": 106.634732
 },
 {
   "STT": 747,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Cao Thị Phi Oanh",
   "address": "154 Nguyễn Duy Trinh, phường Bình Trưng Tây , Quận 2 , thành phố Hồ Chí Minh",
   "Longtitude": 10.78788,
   "Latitude": 106.7598
 },
 {
   "STT": 748,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Dương Tấn Hải",
   "address": "2003 Phạm Thế Hiển, phường 6, Quận 8, thành phố Hồ Chí Minh",
   "Longtitude": 10.73521,
   "Latitude": 106.647049
 },
 {
   "STT": 749,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Hoàng Ngọc Dung",
   "address": "82 Bắc Hải, phường 6, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.785876,
   "Latitude": 106.6635723
 },
 {
   "STT": 750,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Huân",
   "address": "23 đường số 18 khu Him Lam,phường Tân Hưng , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7391363,
   "Latitude": 106.6988332
 },
 {
   "STT": 751,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Ksor Thuôi",
   "address": "911 Phạm Văn Bạch, phường 12, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.82391,
   "Latitude": 106.639021
 },
 {
   "STT": 752,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Lê Thị Khánh Vân",
   "address": "138 Bis Lê Lai, phường Bến Thành, Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7693331,
   "Latitude": 106.6918869
 },
 {
   "STT": 753,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Lê Thị Kim",
   "address": "28 Nam Quốc Cang, phường Phạm Ngũ Lão , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.76826,
   "Latitude": 106.687412
 },
 {
   "STT": 754,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Lê Văn Lợi",
   "address": "181 Nguyễn Thiện Thuật, phường 1 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7684939,
   "Latitude": 106.6791369
 },
 {
   "STT": 755,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Minh",
   "address": "\n356 Lê Văn Lương, phường Tân Hưng, Quận 7, thành phố Hồ Chí Minh",
   "Longtitude": 10.7430482,
   "Latitude": 106.6969285
 },
 {
   "STT": 756,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Nguyễn Anh Tuấn",
   "address": "242A Bà Hom, phường 13 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.755824,
   "Latitude": 106.626391
 },
 {
   "STT": 757,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Nguyễn Hữu Nhân",
   "address": "628/62 Hậu Giang, phường 12 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.749745,
   "Latitude": 106.634278
 },
 {
   "STT": 758,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Nguyễn Quang Diệu",
   "address": "017 CC2 Bàu Cát, Đồng Đen, phường 14 , Tân Bình , thành phố Hồ Chí Minh",
   "Longtitude": 10.7918224,
   "Latitude": 106.6439749
 },
 {
   "STT": 759,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Nguyễn Quang Khánh",
   "address": "197 Nguyễn Thị Nhỏ, phường 9 , Tân Bình , thành phố Hồ Chí Minh",
   "Longtitude": 10.7729735,
   "Latitude": 106.6528945
 },
 {
   "STT": 760,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Nguyễn Sĩ Bảo",
   "address": "43 Út Tịch, phường 4 , Tân Bình , thành phố Hồ Chí Minh",
   "Longtitude": 10.7939669,
   "Latitude": 106.6592783
 },
 {
   "STT": 761,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Nguyễn Thanh Hải",
   "address": "240A Minh Phụng, phường 6 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7536881,
   "Latitude": 106.643281
 },
 {
   "STT": 762,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Nguyễn Thị Thủy",
   "address": "149 Phạm Phú Thứ, phường 11 , Tân Bình , thành phố Hồ Chí Minh",
   "Longtitude": 10.787964,
   "Latitude": 106.647109
 },
 {
   "STT": 763,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Nguyễn Tiến Thạch",
   "address": "549 Gia Phú, phường 3 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7425903,
   "Latitude": 106.6454289
 },
 {
   "STT": 764,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Nguyễn Văn Đôn",
   "address": "60 Nguyễn Trường Tộ, phường 12 , Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7651171,
   "Latitude": 106.7047563
 },
 {
   "STT": 765,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Phạm Minh Triết",
   "address": "625 Hậu Giang, phường 11 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.747619,
   "Latitude": 106.633943
 },
 {
   "STT": 766,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Phạm Thị Ngọc Tuyết",
   "address": "212 Nguyễn Trãi, phường 3 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7568706,
   "Latitude": 106.6770361
 },
 {
   "STT": 767,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Phạm Tiến Lợi",
   "address": "178 Lê Văn Thọ, phường 11 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.842218,
   "Latitude": 106.657353
 },
 {
   "STT": 768,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Phan Đăng Nghị",
   "address": "29/51 Lê Đức Thọ, phường 7 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.831454,
   "Latitude": 106.6822959
 },
 {
   "STT": 769,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Thân Đức Dũng",
   "address": "31E Cư Xá Phú Lâm D, phường 10, Quận 6, thành phố Hồ Chí Minh",
   "Longtitude": 10.7399297,
   "Latitude": 106.6326585
 },
 {
   "STT": 770,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Trần Thu Vân",
   "address": "150 Nguyễn Sỹ Sách, phường 15 , Tân Bình , thành phố Hồ Chí Minh",
   "Longtitude": 10.8210033,
   "Latitude": 106.637853
 },
 {
   "STT": 771,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Trần Văn Hải",
   "address": "A15 Bà Hom, phường 13 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.754866,
   "Latitude": 106.629447
 },
 {
   "STT": 772,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Tuấn & BS Trang",
   "address": "229/14 Quốc lộ 13, phường 26, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8131851,
   "Latitude": 106.7087216
 },
 {
   "STT": 773,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS Vũ Tuấn Ngọc",
   "address": "44 Bến Cát, phường 7, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.786053,
   "Latitude": 106.656306
 },
 {
   "STT": 774,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Bạch Tuyết",
   "address": "180 Tây Hòa, phường Phước Long A , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8261574,
   "Latitude": 106.7643969
 },
 {
   "STT": 775,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Bùi Quang Vinh",
   "address": "1030 Âu Cơ, phường 14 , quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.7830224,
   "Latitude": 106.6423243
 },
 {
   "STT": 776,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Bùi Quang Vinh",
   "address": "489/16 Huỳnh Văn Bánh, phường 13, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.790068,
   "Latitude": 106.6711332
 },
 {
   "STT": 777,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Bùi Văn Đỡ",
   "address": "160 Dương Bá Trạc, phường 2 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.747382,
   "Latitude": 106.688646
 },
 {
   "STT": 778,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Bùi Xuân Vĩnh",
   "address": "151E Hai Bà Trưng, phường 6 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.783292,
   "Latitude": 106.697023
 },
 {
   "STT": 779,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Chung Thị Mộng Thúy",
   "address": "\n8C Cư xá Đồng Tiến, phường 14, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.770813,
   "Latitude": 106.665046
 },
 {
   "STT": 780,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Công Hòa",
   "address": "28 Đường Số 51, phường 14, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8464303,
   "Latitude": 106.6424685
 },
 {
   "STT": 781,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Diệp Quế Trinh",
   "address": "91/3a Huỳnh Thiện Lộc, phường Hòa Thạnh, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.779983,
   "Latitude": 106.638998
 },
 {
   "STT": 782,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Dư Minh Trí",
   "address": "45 Nguyễn Công Trứ, phường 19, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.791262,
   "Latitude": 106.709885
 },
 {
   "STT": 783,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Dương Công Minh",
   "address": "176/27 Trần Huy Liệu, phường 15, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.7987249,
   "Latitude": 106.6785359
 },
 {
   "STT": 784,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Dương Lệ Nguyệt",
   "address": "114 Lô G Chung Cư Nguyễn Thiện Thuật, phường 1 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7684307,
   "Latitude": 106.6765914
 },
 {
   "STT": 785,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Hạnh",
   "address": "606 Phạm Văn Chí, phường 8 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.740877,
   "Latitude": 106.639519
 },
 {
   "STT": 786,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Hồ Tấn Thanh Bình",
   "address": "33/7C Phạm Văn Chiêu, phường 14, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.849875,
   "Latitude": 106.651239
 },
 {
   "STT": 787,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Hồ Thụy Kim Nguyên",
   "address": "78 Nguyễn Ngọc Lộc, phường 14 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7677991,
   "Latitude": 106.6630969
 },
 {
   "STT": 788,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Hoàng Lê Phúc",
   "address": "375 Cách Mạng Tháng 8, phường 13 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.780275,
   "Latitude": 106.676077
 },
 {
   "STT": 789,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Hoàng Nguyên Lộc",
   "address": "32 Hiệp Thành 5, phường Hiệp Thành , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8798414,
   "Latitude": 106.6370988
 },
 {
   "STT": 790,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Hoàng Thị Thanh Thủy",
   "address": "439B2 Phan Văn Trị, phường 5, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.823011,
   "Latitude": 106.692319
 },
 {
   "STT": 791,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Hứa Thị Mỹ Trang",
   "address": "399/5 Nguyễn Đình Chiểu, phường 5 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7711903,
   "Latitude": 106.6836221
 },
 {
   "STT": 792,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Huỳnh Mai & BS. Ngọc Lễ",
   "address": "A6/18 Quốc Lộ 50, Xã Bình Hưng, huyện Bình Chánh, thành phố Hồ Chí Minh",
   "Longtitude": 10.7298,
   "Latitude": 106.6562
 },
 {
   "STT": 793,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Huỳnh Thị Ngọc Thu",
   "address": "40 Tăng Bạt Hổ, phường 12 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7580178,
   "Latitude": 106.6619527
 },
 {
   "STT": 794,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Huỳnh Thị Thanh Dung",
   "address": "804 Hồng Bàng, phường 1 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7550449,
   "Latitude": 106.637677
 },
 {
   "STT": 795,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Huỳnh Trọng Dân",
   "address": "198 Phó Cơ Điều, phường 6 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.760954,
   "Latitude": 106.657247
 },
 {
   "STT": 796,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Huỳnh Tuấn Khanh",
   "address": "21 Nguyễn Văn Lạc, phường 21, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.794589,
   "Latitude": 106.7098186
 },
 {
   "STT": 797,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Khôi",
   "address": "223 Lê Văn Lương, Xã Phước Kiển, huyện Nhà Bè , thành phố Hồ Chí Minh",
   "Longtitude": 10.6894369,
   "Latitude": 106.702485
 },
 {
   "STT": 798,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Lại Văn Tiến",
   "address": "Lầu 1, 34 Nguyễn Duy Dương, phường 8 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7561922,
   "Latitude": 106.6718709
 },
 {
   "STT": 799,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Lê Hữu Khánh",
   "address": "437 An Dương Vương, phường 11, Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7447689,
   "Latitude": 106.6237419
 },
 {
   "STT": 800,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Lê Huy Hùng",
   "address": "613 Lê Hồng Phong, phường 10 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7693618,
   "Latitude": 106.6736164
 },
 {
   "STT": 801,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Lê Minh Quý",
   "address": "13 đường số 1A, phường An Lạc, quận Bình Tân, thành phố Hồ Chí Minh",
   "Longtitude": 10.737903,
   "Latitude": 106.6202212
 },
 {
   "STT": 802,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Lê Phan Kim Thoa",
   "address": "470 Lê Quang Định, phường 11, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8136843,
   "Latitude": 106.6898739
 },
 {
   "STT": 803,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Lê Tấn Bảo",
   "address": "143/82 Gò Dầu, phường Tân Quý, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.7939419,
   "Latitude": 106.6207163
 },
 {
   "STT": 804,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Lê Thị Ngọc Nga",
   "address": "18/18A Phan Văn Trị, phường 2 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7582056,
   "Latitude": 106.6838306
 },
 {
   "STT": 805,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Lê Thị Huyền Châu",
   "address": "453 Hồ Ngọc Lãm, phường An Lạc, quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7250108,
   "Latitude": 106.6091388
 },
 {
   "STT": 806,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Lê Thị Kim Quý",
   "address": "433/13 Hai Bà Trưng, phường 8 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.790751,
   "Latitude": 106.687802
 },
 {
   "STT": 807,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Lê Thị Minh Đức",
   "address": "159 Trần Huy Liệu, phường 8, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.7965129,
   "Latitude": 106.677588
 },
 {
   "STT": 808,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Lê Thị Phan Oanh",
   "address": "440/55 Nguyễn Kiệm, phường 3, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.802192,
   "Latitude": 106.680092
 },
 {
   "STT": 809,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Lê Thị Thu Mai",
   "address": "213/5 Nguyễn Thiện Thuật, phường 1 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7677716,
   "Latitude": 106.6796781
 },
 {
   "STT": 810,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Lê Văn Trường",
   "address": "138 Lê Hoàng Phái, phường 17, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8422718,
   "Latitude": 106.6754617
 },
 {
   "STT": 811,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Lê Văn Tùng",
   "address": "2 Đường số 7, Cư Xá Bình Thới,phường 8 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7633247,
   "Latitude": 106.6479402
 },
 {
   "STT": 812,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Lương Anh Tuấn",
   "address": "62 Thích Quảng Đức, phường 5, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.805004,
   "Latitude": 106.682908
 },
 {
   "STT": 813,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Lý Kiều Diễm",
   "address": "16 Đường 4, Cư Xá Bình Thới,phường 8 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7618054,
   "Latitude": 106.6487497
 },
 {
   "STT": 814,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Lý Tố Khanh",
   "address": "150 Tên Lửa, phường Bình Trị Đông , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7470409,
   "Latitude": 106.6121005
 },
 {
   "STT": 815,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Mã Tú Thanh",
   "address": "B4/8 Quốc lộ 50, Xã Bình Hưng, huyện Bình Chánh, thành phố Hồ Chí Minh",
   "Longtitude": 10.7298,
   "Latitude": 106.6562
 },
 {
   "STT": 816,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Mai Sỹ Phụng",
   "address": "B10/207C Ấp 2, Xã Tân Nhựt, huyện Bình Chánh, thành phố Hồ Chí Minh",
   "Longtitude": 10.6884812,
   "Latitude": 106.603389
 },
 {
   "STT": 817,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Mai Vân Bôn",
   "address": "376 Hàn Hải Nguyên, phường 1 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.758698,
   "Latitude": 106.64128
 },
 {
   "STT": 818,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Mỹ Dung",
   "address": "57 Dương Quảng Hàm, phường 5, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8273205,
   "Latitude": 106.6909897
 },
 {
   "STT": 819,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Ngô Minh Xuân",
   "address": "41 An Điềm, phường 10 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7515243,
   "Latitude": 106.6659234
 },
 {
   "STT": 820,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Ngô Ngọc Minh Thư",
   "address": "419 Hòa Hảo, phường 5 , Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7612966,
   "Latitude": 106.6667289
 },
 {
   "STT": 821,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyên",
   "address": "259 Nguyễn Sơn, phường Phú Thạnh, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.78316,
   "Latitude": 106.624972
 },
 {
   "STT": 822,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Bá Nghiên",
   "address": "65 Vạn Kiếp, phường 3, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.800207,
   "Latitude": 106.693619
 },
 {
   "STT": 823,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Diệu Vinh",
   "address": "67 Miếu Bình Đông, phường Bình Hưng Hòa A, quận Bình Tân, thành phố Hồ Chí Minh",
   "Longtitude": 10.778119,
   "Latitude": 106.617213
 },
 {
   "STT": 824,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Duy Tiên",
   "address": "88 Phùng Văn Cung, phường 7, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.801731,
   "Latitude": 106.686199
 },
 {
   "STT": 825,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Duy Toàn",
   "address": "759 Nguyễn Kiệm, phường 3 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8185654,
   "Latitude": 106.6786616
 },
 {
   "STT": 826,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Hoàng Mai Anh",
   "address": "1872 Phạm Thế Hiển, phường 6 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.736876,
   "Latitude": 106.649074
 },
 {
   "STT": 827,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Hoàng Nhật Mai",
   "address": "20 đường số 1, khu Tái Định Cư, cảng Phú Định, phường 16 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7102493,
   "Latitude": 106.6139568
 },
 {
   "STT": 828,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Hoàng Phong",
   "address": "38M Nguyễn Ảnh Thủ, phường Trung Mỹ Tây , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8654809,
   "Latitude": 106.6141767
 },
 {
   "STT": 829,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Hồng Nga",
   "address": "507 Thống Nhất, phường 16 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.846516,
   "Latitude": 106.664993
 },
 {
   "STT": 830,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Hồng Nhân",
   "address": "1158 Nguyễn Văn Quá, phường Tân Thới Hiệp , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8547424,
   "Latitude": 106.6388095
 },
 {
   "STT": 831,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Hồng Tài",
   "address": "199 Nguyễn Súy, phường Tân Quý, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.7954831,
   "Latitude": 106.6226157
 },
 {
   "STT": 832,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Khánh Vân",
   "address": "600/9D Nguyễn Trãi, phường 7 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.754346,
   "Latitude": 106.6668546
 },
 {
   "STT": 833,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Kiến Mậu",
   "address": "79/104 Trần Văn Đang, phường 9, Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.785064,
   "Latitude": 106.67587
 },
 {
   "STT": 834,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Mạnh Hùng",
   "address": "334 Nguyễn Thị Tú, phường Bình Hưng Hòa B , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.8151021,
   "Latitude": 106.5855348
 },
 {
   "STT": 835,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Minh Tân",
   "address": "10 Trần Minh Quyền, phường 11 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.769855,
   "Latitude": 106.6755652
 },
 {
   "STT": 836,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Minh Tiến",
   "address": "22/24 Cư Xá Lữ Gia, phường 15 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7733712,
   "Latitude": 106.655821
 },
 {
   "STT": 837,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Ngọc Cường",
   "address": "43H/2 Nguyễn Ảnh Thủ, phường Hiệp Thành , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8761763,
   "Latitude": 106.6485538
 },
 {
   "STT": 838,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Ngọc Sáng",
   "address": "21M Nguyễn Ảnh Thủ, phường Trung Mỹ Tây , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8666294,
   "Latitude": 106.6150242
 },
 {
   "STT": 839,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Phúc Nghị",
   "address": "98 Đỗ Tấn Phong, phường 9 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.7995608,
   "Latitude": 106.6775119
 },
 {
   "STT": 840,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Quang Thiện",
   "address": "384 Phạm Văn Chí, phường 4 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7431996,
   "Latitude": 106.6437173
 },
 {
   "STT": 841,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Quốc Hải",
   "address": "86 Minh Phụng, phường 5 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.749342,
   "Latitude": 106.642668
 },
 {
   "STT": 842,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thái Sơn",
   "address": "63 Trần Mai Ninh, phường 12, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.792828,
   "Latitude": 106.64775
 },
 {
   "STT": 843,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thanh Giảng",
   "address": "84 Đề Thám, phường Cầu Ông Lãnh , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7642209,
   "Latitude": 106.695555
 },
 {
   "STT": 844,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thanh Hùng",
   "address": "1Z Văn Cao, phường Phú Thạnh, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.782685,
   "Latitude": 106.620262
 },
 {
   "STT": 845,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thanh Khiết",
   "address": "18 Đường số 11 (Số cũ 19I), Khu A, Khu phố 1, phường Tân Phú ,Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8308361,
   "Latitude": 106.7114992
 },
 {
   "STT": 846,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thanh Lâm",
   "address": "6 Trương Vĩnh Ký, phường Tân Thành, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.794665,
   "Latitude": 106.629459
 },
 {
   "STT": 847,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thanh Liêm",
   "address": "154 Cây Keo, phường Hiệp Tân, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.772376,
   "Latitude": 106.627556
 },
 {
   "STT": 848,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thanh Thiện",
   "address": "43/31 Dạ Nam, phường 2 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.748393,
   "Latitude": 106.686171
 },
 {
   "STT": 849,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thanh Trúc",
   "address": "29/8 Lê Hoàng Phái, phường 17, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8397321,
   "Latitude": 106.6755703
 },
 {
   "STT": 850,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thành Đô",
   "address": "150/1C Nguyễn Ảnh Thủ, Xã Trung Chánh , huyện Hóc Môn , thành phố Hồ Chí Minh",
   "Longtitude": 10.8582673,
   "Latitude": 106.6091247
 },
 {
   "STT": 851,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thị An Khánh",
   "address": "163/15/16 Tô Hiến Thành, phường 13 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7800996,
   "Latitude": 106.6697014
 },
 {
   "STT": 852,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thị Diệu Huyền",
   "address": "62 Lý Chính Thắng, phường 8 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.790186,
   "Latitude": 106.6861629
 },
 {
   "STT": 853,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thị Hà",
   "address": "11 Phú Mỹ, phường 22 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.7871901,
   "Latitude": 106.7139649
 },
 {
   "STT": 854,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thị Hồng Cẩm",
   "address": "322 Kinh Dương Vương, phường An Lạc A , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.743064,
   "Latitude": 106.6203564
 },
 {
   "STT": 855,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thị Ngọc Lan",
   "address": "120/6 Phạm Ngũ Lão, phường 7, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.825405,
   "Latitude": 106.681042
 },
 {
   "STT": 856,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thị Ngọc Yến",
   "address": "601/4A Cách mạng tháng 8,phường 15 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7841609,
   "Latitude": 106.668753
 },
 {
   "STT": 857,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thị Ngụ",
   "address": "10 Cư Xá 675 Nguyễn Kiệm,phường 9 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.8100776,
   "Latitude": 106.6780001
 },
 {
   "STT": 858,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thị Quế Anh",
   "address": "96 Nguyễn Thị Nhỏ,, phường 15 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.772536,
   "Latitude": 106.653065
 },
 {
   "STT": 859,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thị Thanh",
   "address": "515 Quang Trung, phường 10 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8347921,
   "Latitude": 106.6640758
 },
 {
   "STT": 860,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thị Thanh Xuân",
   "address": "611/95 Điện Biên Phủ, phường 1 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7687332,
   "Latitude": 106.6769354
 },
 {
   "STT": 861,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thị Thu Hà",
   "address": "94 Đường Số 1, khu phố 3,phường Bình Trị Đông B , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.748554,
   "Latitude": 106.6199209
 },
 {
   "STT": 862,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thị Thu Hậu",
   "address": "9A Trần Quy Cáp, phường 11, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.815927,
   "Latitude": 106.696988
 },
 {
   "STT": 863,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thị Út",
   "address": "20J Cư xá Ngân Hàng, Trần Xuân Soạn, phường Tân Thuận Tây ,Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.750478,
   "Latitude": 106.717686
 },
 {
   "STT": 864,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Thu Tịnh",
   "address": "1549 Tỉnh lộ 10, khu phố 4,phường Tân Tạo A , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7578737,
   "Latitude": 106.5818549
 },
 {
   "STT": 865,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Trọng Lân",
   "address": "52 Thuận Kiều, phường 4 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.758903,
   "Latitude": 106.658814
 },
 {
   "STT": 866,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Trọng Linh",
   "address": "463 Huỳnh Văn Bánh, phường 14, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.79184,
   "Latitude": 106.6704
 },
 {
   "STT": 867,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Tuấn Khiêm",
   "address": "97 Trần Quốc Tuấn, phường 1, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.821121,
   "Latitude": 106.692578
 },
 {
   "STT": 868,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Văn Hiền",
   "address": "207 Đường 19, phường Bình Trị Đông B , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7532318,
   "Latitude": 106.6115515
 },
 {
   "STT": 869,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Đình Hải",
   "address": "376 Lê Quang Định, phường 11, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.80949,
   "Latitude": 106.6931
 },
 {
   "STT": 870,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Đình Huấn",
   "address": "23 Nguyễn Văn Đậu, phường 5, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.8049782,
   "Latitude": 106.6869718
 },
 {
   "STT": 871,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Nguyễn Đức Tuấn",
   "address": "371 Nơ Trang Long, phường 13, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8197686,
   "Latitude": 106.703135
 },
 {
   "STT": 872,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Như Phượng",
   "address": "22/15 Trường Chinh, Tổ 37, Khu phố 3, phường Tân Thới Nhất , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.80095,
   "Latitude": 106.63794
 },
 {
   "STT": 873,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Phạm Bích Chi",
   "address": "373/175 Lý Thường Kiệt, phường 9 , Tân Bình , thành phố Hồ Chí Minh",
   "Longtitude": 10.780629,
   "Latitude": 106.6549408
 },
 {
   "STT": 874,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Phạm Diệp Thùy Dương",
   "address": "163 Nguyễn Văn Trỗi, phường 11, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.7956385,
   "Latitude": 106.6751935
 },
 {
   "STT": 875,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Phạm Hoàng Minh Khôi",
   "address": "88/31 Đường số 17, Khu phố 4,phường Tân Thuận Tây , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7475394,
   "Latitude": 106.7188251
 },
 {
   "STT": 876,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Phạm Lê An",
   "address": "411 Âu Cơ, phường Phú Trung, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.777568,
   "Latitude": 106.645775
 },
 {
   "STT": 877,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Phạm Mai Đằng",
   "address": "100A Thích Quảng Đức, phường 5, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.806465,
   "Latitude": 106.681011
 },
 {
   "STT": 878,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Phạm Mỹ Nga",
   "address": "661 Thống Nhất, phường 13, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.853347,
   "Latitude": 106.666422
 },
 {
   "STT": 879,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Phạm Ngọc Thạch",
   "address": "1543 Tỉnh Lộ 10, phường Tân Tạo A , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7578313,
   "Latitude": 106.5820686
 },
 {
   "STT": 880,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Phạm Thị Lệ Hoa",
   "address": "597 Bà Hạt, phường 8 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7650013,
   "Latitude": 106.6648967
 },
 {
   "STT": 881,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Phạm Thị Thanh Tâm",
   "address": "689 Hưng Phú, phường 9 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7459998,
   "Latitude": 106.6680051
 },
 {
   "STT": 882,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Phạm Thị Thủy",
   "address": "511/18 Huỳnh Văn Bánh, phường 14 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.791592,
   "Latitude": 106.6681854
 },
 {
   "STT": 883,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Phạm Thị Thùy Trang",
   "address": "75 Nghĩa Phát, phường 6, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.786594,
   "Latitude": 106.658638
 },
 {
   "STT": 884,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Phạm Văn Ánh",
   "address": "520A Hà Huy Giáp, phường Thạnh Lộc , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.87571,
   "Latitude": 106.6770496
 },
 {
   "STT": 885,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Phạm Văn Chiến",
   "address": "137A Ba Vân, phường 14 , Tân Bình , thành phố Hồ Chí Minh",
   "Longtitude": 10.7959332,
   "Latitude": 106.6397003
 },
 {
   "STT": 886,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Phạm Văn Tung & Sản Phụ Khoa - Siêu Âm - BS. Nguyễn Thị Hồng Loan",
   "address": "65 Tân Kỳ Tân Quý, phường Tân Sơn Nhì, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.7893449,
   "Latitude": 106.6267516
 },
 {
   "STT": 887,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Phan Hữu Nguyệt Diễm",
   "address": "49B Đường Số 16, phường 4 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7413059,
   "Latitude": 106.6729814
 },
 {
   "STT": 888,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Phan Thanh Tâm",
   "address": "39 Phan Văn Năm, phường Phú Thạnh, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.780074,
   "Latitude": 106.629444
 },
 {
   "STT": 889,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Phan Thị Minh Tâm",
   "address": "27 Phú Lộc, phường 6 , Tân Bình , thành phố Hồ Chí Minh",
   "Longtitude": 10.7877094,
   "Latitude": 106.6610984
 },
 {
   "STT": 890,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Phan Tuấn Anh",
   "address": "6 Phan Đình Phùng, phường Hiệp Phú , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8442662,
   "Latitude": 106.7743624
 },
 {
   "STT": 891,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Quách Thanh Hậu",
   "address": "147 Hòa Bình, phường Hiệp Tân, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.7669508,
   "Latitude": 106.6259449
 },
 {
   "STT": 892,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Thái Quang Tùng",
   "address": "700 Hương Lộ 2, phường Bình Trị Đông A , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7664409,
   "Latitude": 106.6059621
 },
 {
   "STT": 893,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Thảo",
   "address": "88 TL 15, phường Thạnh Lộc ,Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 11.0213653,
   "Latitude": 106.5638747
 },
 {
   "STT": 894,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Thọ Đức & BS. Thanh Huyền",
   "address": "146 Nguyễn Văn Đậu, phường 7, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.809495,
   "Latitude": 106.689515
 },
 {
   "STT": 895,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Thu Hà",
   "address": "214/15 Nguyễn Oanh, phường 17, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.835469,
   "Latitude": 106.677077
 },
 {
   "STT": 896,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Tiêu Châu Thy",
   "address": "487/39 Huỳnh Tấn Phát, KP1, phường Tân Thuận Đông , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.75566,
   "Latitude": 106.7222267
 },
 {
   "STT": 897,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Tiêu Ngọc Trân",
   "address": "20/3 Núi Thành, phường 13, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.799787,
   "Latitude": 106.642335
 },
 {
   "STT": 898,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Tố Hảo",
   "address": "39 Lê Lợi, Thị trấn Hóc Môn, huyện Hóc Môn , thành phố Hồ Chí Minh",
   "Longtitude": 10.8900509,
   "Latitude": 106.5928427
 },
 {
   "STT": 899,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Tô Thành Quý",
   "address": "401 Phú Thọ Hòa, phường Phú Thọ Hòa, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.7863307,
   "Latitude": 106.6276567
 },
 {
   "STT": 900,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Trần Cảnh Tri",
   "address": "22 Lạc Long Quân, phường 3 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.759107,
   "Latitude": 106.641066
 },
 {
   "STT": 901,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Trần Diễm Trang",
   "address": "368 Nguyễn Văn Nghi, phường 7, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.82638,
   "Latitude": 106.684483
 },
 {
   "STT": 902,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Trần Hoàng Út",
   "address": "431 Tỉnh Lộ 10, phường An Lạc A, quận Bình Tân, thành phố Hồ Chí Minh",
   "Longtitude": 10.7569097,
   "Latitude": 106.6201469
 },
 {
   "STT": 903,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Trần Hùng Dũng",
   "address": "508/2 Nguyễn Đình Chiểu, phường 4 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7738121,
   "Latitude": 106.685404
 },
 {
   "STT": 904,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Trần Minh Lâm",
   "address": "950/7 Nguyễn Kiệm, phường 3, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8196601,
   "Latitude": 106.6786804
 },
 {
   "STT": 905,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Trần Ngọc Đường",
   "address": "186/B15 Bình Thới, phường 14 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.767348,
   "Latitude": 106.647739
 },
 {
   "STT": 906,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Trần Nguyên Khôi",
   "address": "23 Bạch Đằng, phường 15 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.802704,
   "Latitude": 106.710799
 },
 {
   "STT": 907,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Trần Thị Hoài Thu",
   "address": "20/1 Kỳ Đồng, phường 9 , Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.782624,
   "Latitude": 106.681797
 },
 {
   "STT": 908,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Trần Thị Kim Ngân",
   "address": "61 Khiếu Năng Tĩnh, phường An Lạc A , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7529332,
   "Latitude": 106.6212774
 },
 {
   "STT": 909,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Trần Thị Kim Vân",
   "address": "230 Cây Trâm, phường 9, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8439138,
   "Latitude": 106.6497423
 },
 {
   "STT": 910,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Trần Thị Mộng Hiệp",
   "address": "3 Đường Số 5A, Cư Xá Bình Thới,phường 8 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.762914,
   "Latitude": 106.6490064
 },
 {
   "STT": 911,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Trần Thị Thúy",
   "address": "359A Lê Quang Định, phường 5, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8123239,
   "Latitude": 106.687347
 },
 {
   "STT": 912,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Trần Thị Việt",
   "address": "7 Phan Chu Trinh, phường 2 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.802705,
   "Latitude": 106.701655
 },
 {
   "STT": 913,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Trần Trúc Bình",
   "address": "570 Phạm Văn Chí, phường 8 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.74147,
   "Latitude": 106.6403799
 },
 {
   "STT": 914,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Trần Văn Cường",
   "address": "102 Nguyễn Ảnh Thủ, phường Hiệp Thành , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8775495,
   "Latitude": 106.6391018
 },
 {
   "STT": 915,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Trần Đắc Nguyên Anh",
   "address": "53 Nguyễn Sơn Hà, phường 5 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7700923,
   "Latitude": 106.6848268
 },
 {
   "STT": 916,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Trang Giang Sang",
   "address": "120/22 Tôn Thất Hiệp, phường 13, Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764866,
   "Latitude": 106.6523689
 },
 {
   "STT": 917,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Trịnh Thị Minh Châu",
   "address": "575/20 Điện Biên Phủ, phường 1 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7694655,
   "Latitude": 106.6764619
 },
 {
   "STT": 918,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Trương Anh Mậu",
   "address": "94/3 Hà Huy Giáp, phường Thạnh Xuân , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8852895,
   "Latitude": 106.6816109
 },
 {
   "STT": 919,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Trương Hữu Khanh",
   "address": "93/5 Ấp Vạn Hạnh, Xã Tân Xuân , huyện Hóc Môn , thành phố Hồ Chí Minh",
   "Longtitude": 10.8648269,
   "Latitude": 106.6135373
 },
 {
   "STT": 920,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Trương Thị Thúy Mai",
   "address": "399/10 Thống Nhất, phường 11, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8407876,
   "Latitude": 106.6649669
 },
 {
   "STT": 921,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Từ Thị Hoàng Phượng",
   "address": "65 Nhật Tảo, phường 4 , Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.764919,
   "Latitude": 106.668887
 },
 {
   "STT": 922,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Võ Công Đồng",
   "address": "140/2 Cư Xá Bình Thới, phường 1, Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7631283,
   "Latitude": 106.6495178
 },
 {
   "STT": 923,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Võ Hoàng Khoa",
   "address": "21/2 Vườn Lài, phường Phú Thọ Hòa, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.786702,
   "Latitude": 106.635114
 },
 {
   "STT": 924,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Võ Thị Vân",
   "address": "250B Bãi Sậy, phường 4 , Quận 6, thành phố Hồ Chí Minh",
   "Longtitude": 10.747463400000001,
   "Latitude": 106.6462033
 },
 {
   "STT": 925,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Võ Đức Trí",
   "address": "103 Cây Keo, phường Hiệp Tân, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.771732,
   "Latitude": 106.628615
 },
 {
   "STT": 926,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Vũ Hữu Hùng",
   "address": "402 Nguyễn Thái Sơn, phường 5, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.829014,
   "Latitude": 106.6921909
 },
 {
   "STT": 927,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Vũ Huy Trụ",
   "address": "1063 Nguyễn Trãi, phường 14 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7530304,
   "Latitude": 106.6510739
 },
 {
   "STT": 928,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Vũ Ngọc Lương",
   "address": "814/15 Sư Vạn Hạnh, phường 12 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7726101,
   "Latitude": 106.6683399
 },
 {
   "STT": 929,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Vũ Thị Dân",
   "address": "164 Hưng Phú, phường 8 , Quận 8, thành phố Hồ Chí Minh",
   "Longtitude": 10.7502886,
   "Latitude": 106.6792196
 },
 {
   "STT": 930,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Vũ Thiên Ân & Chuyên Khoa Nội Tổng Quát - BS. Vũ Đình Tuân",
   "address": "89/14 Thái Phiên, phường 2 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.757499,
   "Latitude": 106.645276
 },
 {
   "STT": 931,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Vũ Thùy Dương",
   "address": "160 Độc Lập, phường Tân Thành, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.792854,
   "Latitude": 106.63045
 },
 {
   "STT": 932,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Vương Thị Yến",
   "address": "106/78/B Nguyễn Kiệm, phường 3, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8128883,
   "Latitude": 106.6786654
 },
 {
   "STT": 933,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Yến",
   "address": "56 Huỳnh Đình Hai, phường 14, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8042285,
   "Latitude": 106.698721
 },
 {
   "STT": 934,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Đặng Ngọc Dũng",
   "address": "313F Trịnh Đình Trọng, phường Hòa Thạnh, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.776379,
   "Latitude": 106.634581
 },
 {
   "STT": 935,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Đặng Phi Yến",
   "address": "130 Phan Văn Trị, phường 12, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.809772,
   "Latitude": 106.696401
 },
 {
   "STT": 936,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Đặng Thế Dân",
   "address": "68 Hoàng Hoa Thám, phường 7, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.806879,
   "Latitude": 106.6905515
 },
 {
   "STT": 937,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Đặng Thị Kim Trinh",
   "address": "339/5 Lê Văn Sỹ, phường 13 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7876,
   "Latitude": 106.67735
 },
 {
   "STT": 938,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Đặng Văn Quý",
   "address": "415 Đất Mới, phường Bình Trị Đông A , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7736878,
   "Latitude": 106.6090917
 },
 {
   "STT": 939,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Đăng Vinh Quốc",
   "address": "184C Lê Văn Sỹ, phường 10 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.793452,
   "Latitude": 106.670291
 },
 {
   "STT": 940,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Đặng Xuân Vinh",
   "address": "123 Đường Số 11, phường 11 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8430579,
   "Latitude": 106.6604739
 },
 {
   "STT": 941,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Đào Trung Hiếu",
   "address": "8 Nguyễn Trọng Tuyển, phường 15 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.7972629,
   "Latitude": 106.6807539
 },
 {
   "STT": 942,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Đinh Anh Tuấn",
   "address": "29/61/4 Lê Đức Thọ, phường 7, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8306567,
   "Latitude": 106.6822574
 },
 {
   "STT": 943,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Đinh Tấn Phương",
   "address": "Tổ 8, khu phố 2, Thị trấn Củ Chi ,huyện Củ Chi , thành phố Hồ Chí Minh",
   "Longtitude": 10.9725812,
   "Latitude": 106.496089
 },
 {
   "STT": 944,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Đinh Việt Hưng",
   "address": "43 Đường Số 33A, phường Bình Trị Đông B , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7539419,
   "Latitude": 106.6081738
 },
 {
   "STT": 945,
   "Name": "Phòng Khám Chuyên Khoa Nhi - BS. Đoàn Thị Kim Ngân",
   "address": "610 Minh Phụng, phường 9 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.763702,
   "Latitude": 106.645095
 },
 {
   "STT": 946,
   "Name": "Phòng Khám Chuyên Khoa Nhi - ThS. BS. Nguyễn Bảo Tường & Nội Tổng Quát - BS. Lê Thị Đan Thùy",
   "address": "347/165 Lê Đại Hành, phường 13, Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.765622,
   "Latitude": 106.65342
 },
 {
   "STT": 947,
   "Name": "Phòng Khám Chuyên Khoa Nhi - ThS.BS Nguyễn Thành Đạt",
   "address": "155 Hồng Lạc, phường 10, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.7860706,
   "Latitude": 106.6491106
 },
 {
   "STT": 948,
   "Name": "Phòng Khám Chuyên Khoa Nhi BS. Phan Ngọc Duy Cần",
   "address": "192A Hòa Bình, phường Hiệp Tân, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.7859461,
   "Latitude": 106.6230172
 },
 {
   "STT": 949,
   "Name": "Phòng Khám Chuyên Khoa Nhi Minh Oanh - BS. Võ Thị Kim Oanh & BS. Dương Thái Quang Minh",
   "address": "30 Miếu Gò Xoài, phường Bình Hưng Hòa A , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.779178,
   "Latitude": 106.610203
 },
 {
   "STT": 950,
   "Name": "Phòng Khám Chuyên Khoa Nhi Đồng - BS Vưu Thanh Tùng",
   "address": "F1/8M2, Xã Vĩnh Lộc A, huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8238657,
   "Latitude": 106.5642998
 },
 {
   "STT": 951,
   "Name": "Phòng Khám Chuyên Khoa Niệu - BS. Nguyễn Văn Hiệp",
   "address": "7 Trương Định, phường 6 , Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.776086,
   "Latitude": 106.691295
 },
 {
   "STT": 952,
   "Name": "Phòng Khám Chuyên Khoa Niệu - BS. Võ Trọng Thanh Phong",
   "address": "399 Tạ Quang Bửu, phường 2 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.74412,
   "Latitude": 106.685107
 },
 {
   "STT": 953,
   "Name": "Phòng Khám Chuyên Khoa Niệu Nam Khoa - BS. Lê Sĩ Hùng",
   "address": "285/64 Cách Mạng Tháng Tám,phường 12 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.779722,
   "Latitude": 106.6732169
 },
 {
   "STT": 954,
   "Name": "Phòng Khám Chuyên Khoa Nội & Da Liễu - BS. Phạm Anh Tuấn & BS. Trần Thị Thanh Thủy",
   "address": "136 Võ Thị Sáu, phường 8 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.78763,
   "Latitude": 106.691532
 },
 {
   "STT": 955,
   "Name": "Phòng Khám Chuyên Khoa Nội & Lao Phổi - BS. Nguyễn Đức Bằng",
   "address": "201 Chung Cư Ngô Quyền, Ngô Quyền, phường 9 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7590579,
   "Latitude": 106.6658625
 },
 {
   "STT": 956,
   "Name": "Phòng Khám Chuyên Khoa Nội & Ngoại - BS. Phùng Tấn Cường",
   "address": "152/16 Thành Thái, phường 13 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7685468,
   "Latitude": 106.666907
 },
 {
   "STT": 957,
   "Name": "Phòng Khám Chuyên Khoa Nội & Nhi Khoa - BS. Lê Nguyệt Minh",
   "address": "11 Nguyễn Thị Tần, phường 2 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7484526,
   "Latitude": 106.6851533
 },
 {
   "STT": 958,
   "Name": "Phòng Khám Chuyên Khoa Nội - BS Phạm Khắc Hiệu",
   "address": "150 Lê Hồng Phong, phường 3 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7581623,
   "Latitude": 106.6780349
 },
 {
   "STT": 959,
   "Name": "Phòng Khám Chuyên Khoa Nội - BS. Huỳnh Minh Tuấn",
   "address": "84 đường số 10, phường 13 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7531616,
   "Latitude": 106.6274304
 },
 {
   "STT": 960,
   "Name": "Phòng Khám Chuyên Khoa Nội - BS. Huỳnh Quang Đại",
   "address": "234C Nguyễn Văn Luông, phường 11 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7448059,
   "Latitude": 106.6349773
 },
 {
   "STT": 961,
   "Name": "Phòng Khám Chuyên Khoa Nội - BS. Nguyễn Vĩnh Thống",
   "address": "27 Phú Lâm, phường 9 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.753334,
   "Latitude": 106.640528
 },
 {
   "STT": 962,
   "Name": "Phòng Khám Chuyên Khoa Nội - BS. Trương Mỹ Phương",
   "address": "549B Hậu Giang, phường 11 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.747382,
   "Latitude": 106.635502
 },
 {
   "STT": 963,
   "Name": "Phòng Khám Chuyên Khoa Nội - BS. Đặng Văn Thoại",
   "address": "221/18 Phan Văn Khỏe, phường 5, Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.747768,
   "Latitude": 106.645463
 },
 {
   "STT": 964,
   "Name": "Phòng Khám Chuyên Khoa Nội - BS. Đinh Công Trứ",
   "address": "1220 Lê Đức Thọ, phường 13 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8527039,
   "Latitude": 106.6601942
 },
 {
   "STT": 965,
   "Name": "Phòng Khám Chuyên Khoa Nội - BS. Đỗ Thị Cẩm Hà",
   "address": "151A Lũy Bán Bích, phường Tân Thới Hòa, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.7666323,
   "Latitude": 106.6318631
 },
 {
   "STT": 966,
   "Name": "Phòng Khám Chuyên Khoa Nội - Nhi - BS. Nguyễn Hữu Nghĩa & BS. Vương Thị Yến Thu",
   "address": "18/1 Trường Chinh, phường Tân Hưng Thuận , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8301993,
   "Latitude": 106.623702
 },
 {
   "STT": 967,
   "Name": "Phòng Khám Chuyên Khoa Nội - Tim Mạch",
   "address": "59 Hàn Hải Nguyên, phường 16 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.757836,
   "Latitude": 106.649922
 },
 {
   "STT": 968,
   "Name": "Phòng Khám Chuyên Khoa Nội Bướu Cổ - BS. Nguyễn Quang Trung",
   "address": "78 Đường 100 Bình Thới, phường 14 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7691185,
   "Latitude": 106.6500315
 },
 {
   "STT": 969,
   "Name": "Phòng Khám Chuyên Khoa Nội Cơ Xương Khớp - BS. Hồ Mai Hương",
   "address": "100F Hùng Vương, phường 9 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.758886,
   "Latitude": 106.6703042
 },
 {
   "STT": 970,
   "Name": "Phòng Khám Chuyên Khoa Nội Cơ Xương Khớp - BS. Riết Thân",
   "address": "549/19 Xô Viết Nghệ Tĩnh,phường 26 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.809509,
   "Latitude": 106.711488
 },
 {
   "STT": 971,
   "Name": "Phòng Khám Chuyên Khoa Nội Hô Hấp - BS. Trần Minh Trí",
   "address": "411/6 Lê Đại Hành, phường 11 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.767486,
   "Latitude": 106.652826
 },
 {
   "STT": 972,
   "Name": "Phòng Khám Chuyên Khoa Nội Hồng Phúc - BS. Lưu Tuấn Anh",
   "address": "92 Bến Phú Lâm, phường 9 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7520659,
   "Latitude": 106.641021
 },
 {
   "STT": 973,
   "Name": "Phòng Khám Chuyên Khoa Nội Nhi - BS. Bạch Văn Cam",
   "address": "163 Nguyễn Văn Cừ, phường 2 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7582244,
   "Latitude": 106.6841825
 },
 {
   "STT": 974,
   "Name": "Phòng Khám Chuyên Khoa Nội Nhi - BS. Bành Tấn Phong",
   "address": "210 Phan Huy Ích, phường 12, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8403592,
   "Latitude": 106.637659
 },
 {
   "STT": 975,
   "Name": "Phòng Khám Chuyên Khoa Nội Nhi - BS. Chế Thanh Đoan",
   "address": "21 Tôn Thất Hiệp, phường 13 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.765192,
   "Latitude": 106.653994
 },
 {
   "STT": 976,
   "Name": "Phòng Khám Chuyên Khoa Nội Nhi - BS. Hồ Thị Bích Thủy",
   "address": "130/14C Lâm Văn Bền, phường Tân Thuận Tây , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7511961,
   "Latitude": 106.7157894
 },
 {
   "STT": 977,
   "Name": "Phòng Khám Chuyên Khoa Nội Nhi - BS. Kim Xuyến & BS. Hiếu Trung",
   "address": "1/19 Quang Trung, Thị trấn Hóc Môn , huyện Hóc Môn , thành phố Hồ Chí Minh",
   "Longtitude": 10.8883792,
   "Latitude": 106.5966777
 },
 {
   "STT": 978,
   "Name": "Phòng Khám Chuyên Khoa Nội Nhi - BS. Lê Hồng Thiện",
   "address": "216 Huỳnh Tấn Phát, phường Tân Thuận Tây , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7544847,
   "Latitude": 106.7273926
 },
 {
   "STT": 979,
   "Name": "Phòng Khám Chuyên Khoa Nội Nhi - BS. Lê Thị Thanh Tuyền",
   "address": "19 Đường số 65, phường Tân Kiểng , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.745379,
   "Latitude": 106.705123
 },
 {
   "STT": 980,
   "Name": "Phòng Khám Chuyên Khoa Nội Nhi - BS. Nguyễn Huy Luân & Chuyên Khoa Ngoại Tổng Quát - BS. Phan Chung Thùy Lynh",
   "address": "587 Nguyễn Thị Thập, phường Tân Phong , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7397952,
   "Latitude": 106.7038741
 },
 {
   "STT": 981,
   "Name": "Phòng Khám Chuyên Khoa Nội Nhi - BS. Nguyễn Khắc Dũng",
   "address": "68 Bàu Cát 3, phường 14, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.7928257,
   "Latitude": 106.6426458
 },
 {
   "STT": 982,
   "Name": "Phòng Khám Chuyên Khoa Nội Nhi - BS. Nguyễn Thị Oanh Tuyết",
   "address": "55A An Phú Đông 12, Tổ 38, Khu phố 5, phường An Phú Đông ,Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8542602,
   "Latitude": 106.704072
 },
 {
   "STT": 983,
   "Name": "Phòng Khám Chuyên Khoa Nội Nhi - BS. Nguyễn Thị Thu Thảo",
   "address": "64 Trần Quang Khải, phường Tân Định , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7924067,
   "Latitude": 106.6938811
 },
 {
   "STT": 984,
   "Name": "Phòng Khám Chuyên Khoa Nội Nhi - BS. Nguyễn Đức Long",
   "address": "529/2 Kha Vạn Cân, phường Linh Đông, quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8410804,
   "Latitude": 106.7462591
 },
 {
   "STT": 985,
   "Name": "Phòng Khám Chuyên Khoa Nội Nhi - BS. Trần Anh",
   "address": "137 Tuệ Tĩnh, phường 12 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.761303,
   "Latitude": 106.654219
 },
 {
   "STT": 986,
   "Name": "Phòng Khám Chuyên Khoa Nội Nhi - BS. Vũ Hùng Khanh",
   "address": "463 Lê Đức Thọ, phường 16 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.847051,
   "Latitude": 106.66761
 },
 {
   "STT": 987,
   "Name": "Phòng Khám Chuyên Khoa Nội Nhi BS. Nguyễn Duy Toàn",
   "address": "81 Nguyễn Kiệm, phường 3, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8174971,
   "Latitude": 106.678649
 },
 {
   "STT": 988,
   "Name": "Phòng Khám Chuyên Khoa Nội Nhiễm",
   "address": "12 Tuệ Tĩnh, phường 13 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764192,
   "Latitude": 106.653734
 },
 {
   "STT": 989,
   "Name": "Phòng khám chuyên khoa nội nhiễm- BS Trần Thị Hồng Châu",
   "address": "47/5 Tân Hóa, phường 14 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.755882,
   "Latitude": 106.636568
 },
 {
   "STT": 990,
   "Name": "Phòng Khám Chuyên Khoa Nội Phổi Và Đông Y - BS Phạm Như Cẩm & BS Hòa",
   "address": "159 Nguyễn Kiệm, phường 3 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8240591,
   "Latitude": 106.6790968
 },
 {
   "STT": 991,
   "Name": "Phòng Khám Chuyên Khoa Nội Soi Tai Mũi Họng - BS. Nguyễn Văn Hùng",
   "address": "315/20A Lê Văn Sỹ, phường 13 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.786749,
   "Latitude": 106.67786
 },
 {
   "STT": 992,
   "Name": "Phòng Khám Chuyên Khoa Nội Tâm Thần - BS. Nguyễn Đăng Khoa",
   "address": "25 Trần Xuân Hòa, phường 7 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7532733,
   "Latitude": 106.6670544
 },
 {
   "STT": 993,
   "Name": "Phòng Khám Chuyên Khoa Nội Thần Kinh",
   "address": "163/62 Thành Thái, phường 14 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7719601,
   "Latitude": 106.6655517
 },
 {
   "STT": 994,
   "Name": "Phòng Khám Chuyên Khoa Nội Thần Kinh - BS Ngô Văn Phương",
   "address": "543/69 Nguyễn Đình Chiểu, phường 2 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7675387,
   "Latitude": 106.6818101
 },
 {
   "STT": 995,
   "Name": "Phòng Khám Chuyên Khoa Nội Thần Kinh - BS. Bùi Huy Hảo",
   "address": "216/18 Lạc Long Quân, phường 10 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.761259,
   "Latitude": 106.642474
 },
 {
   "STT": 996,
   "Name": "Phòng Khám Chuyên Khoa Nội Thần Kinh - BS. Bùi Thị Quỳnh Châu",
   "address": "9 Tống Văn Trân, phường 5 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.770212,
   "Latitude": 106.643585
 },
 {
   "STT": 997,
   "Name": "Phòng Khám Chuyên Khoa Nội Thần Kinh - BS. Lê Anh Tuấn",
   "address": "133 Đường số 45, phường 5 ,Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7602998,
   "Latitude": 106.6985824
 },
 {
   "STT": 998,
   "Name": "Phòng Khám Chuyên Khoa Nội Thần Kinh - BS. Lê Nguyễn Nhựt Tín",
   "address": "1/6 Cư Xá Lữ Gia, phường 15 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.773304,
   "Latitude": 106.6551434
 },
 {
   "STT": 999,
   "Name": "Phòng Khám Chuyên Khoa Nội Thần Kinh - BS. Nguyễn Anh Tài",
   "address": "355/18 Sư Vạn Hạnh, phường 12 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7729171,
   "Latitude": 106.6692343
 },
 {
   "STT": 1000,
   "Name": "Phòng Khám Chuyên Khoa Nội Thần Kinh - BS. Nguyễn Hoàng Thông",
   "address": "41 Hồ Tùng Mậu, phường Bến Nghé , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.771373,
   "Latitude": 106.703989
 },
 {
   "STT": 1001,
   "Name": "Phòng Khám Chuyên Khoa Nội Thần Kinh - BS. Nguyễn Hữu Công",
   "address": "78/C4 Phạm Ngũ Lão, phường 3, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8213363,
   "Latitude": 106.6828452
 },
 {
   "STT": 1002,
   "Name": "Phòng Khám Chuyên Khoa Nội Thần Kinh - BS. Nguyễn Huy Thắng",
   "address": "77-79 Tôn Thất Hiệp, phường 13 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764229,
   "Latitude": 106.65345
 },
 {
   "STT": 1003,
   "Name": "Phòng Khám Chuyên Khoa Nội Thần Kinh - BS. Nguyễn Minh Châu",
   "address": "297/24 Lý Thường Kiệt, phường 15 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.772983,
   "Latitude": 106.656786
 },
 {
   "STT": 1004,
   "Name": "Phòng Khám Chuyên Khoa Nội Thần Kinh - BS. Nguyễn Thanh Tùng",
   "address": "17A Đường số 8, phường Linh Trung, quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8551116,
   "Latitude": 106.7687833
 },
 {
   "STT": 1005,
   "Name": "Phòng Khám Chuyên Khoa Nội Thần Kinh - BS. Phan Mỹ Hạnh",
   "address": "40/9 Trần Thị Nghĩ, phường 7 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8271035,
   "Latitude": 106.6805144
 },
 {
   "STT": 1006,
   "Name": "Phòng Khám Chuyên Khoa Nội Thần Kinh - BS. Thành Quang",
   "address": "186 Linh Trung, phường Linh Trung , quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8630363,
   "Latitude": 106.7613334
 },
 {
   "STT": 1007,
   "Name": "Phòng Khám Chuyên Khoa Nội Thần Kinh - BS. Trần Công Thắng",
   "address": "173/4 Thành Thái, phường 14 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7681902,
   "Latitude": 106.6668809
 },
 {
   "STT": 1008,
   "Name": "Phòng Khám Chuyên Khoa Nội Thần Kinh - BS. Trương Lê Tuấn Anh",
   "address": "76 đường số 4, phường Tân Kiểng, Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7444614,
   "Latitude": 106.7146098
 },
 {
   "STT": 1009,
   "Name": "Phòng Khám Chuyên Khoa Nội Thần Kinh - BS. Trương Văn Luyện",
   "address": "13 Đặng Minh Khiêm, phường 4 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.758063,
   "Latitude": 106.658005
 },
 {
   "STT": 1010,
   "Name": "Phòng Khám Chuyên Khoa Nội Thần Kinh - BS. Trịnh Thị Ngọc Trinh",
   "address": "75A Đường số 2, Khu phố 6,phường Hiệp Bình Phước , Thủ Đức , thành phố Hồ Chí Minh",
   "Longtitude": 10.8383109,
   "Latitude": 106.7179823
 },
 {
   "STT": 1011,
   "Name": "Phòng Khám Chuyên Khoa Nội Tiết - BS Nguyễn Bích Đào",
   "address": "215F Nguyễn Trãi, phường Nguyễn Cư Trinh , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.765265,
   "Latitude": 106.688336
 },
 {
   "STT": 1012,
   "Name": "Phòng Khám Chuyên Khoa Nội Tiết - BS. Hồ Đắc Phương",
   "address": "135 Đào Duy Từ, phường 5 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.760036,
   "Latitude": 106.665747
 },
 {
   "STT": 1013,
   "Name": "Phòng Khám Chuyên Khoa Nội Tiết - BS. Mai Yến Linh",
   "address": "71 Đường số 13, Khu phố 2,phường Linh Chiểu , Thủ Đức , thành phố Hồ Chí Minh",
   "Longtitude": 10.8473265,
   "Latitude": 106.7653005
 },
 {
   "STT": 1014,
   "Name": "Phòng Khám Chuyên Khoa Nội Tiết - BS. Ngô Thế Phi",
   "address": "92 Tam Hà, phường Tam Phú, quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8621349,
   "Latitude": 106.7465144
 },
 {
   "STT": 1015,
   "Name": "Phòng Khám Chuyên Khoa Nội Tiết - BS. Nguyễn Thị Lệ Thủy",
   "address": "92 Trần Khắc Chân, phường Tân Định , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7946342,
   "Latitude": 106.690973
 },
 {
   "STT": 1016,
   "Name": "Phòng Khám Chuyên Khoa Nội Tiết - BS. Trần Văn Hai",
   "address": "1114A Ba Tháng Hai, phường 12 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7606708,
   "Latitude": 106.6537891
 },
 {
   "STT": 1017,
   "Name": "Phòng Khám Chuyên Khoa Nội Tiết Tim Mạch - BS Lê Tuyết Hoa",
   "address": "109/9 Lê Quốc Hưng, phường 12 ,Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764853,
   "Latitude": 106.704627
 },
 {
   "STT": 1018,
   "Name": "Phòng Khám Chuyên Khoa Nội Tiêu Hóa - BS. Nguyễn Ngọc Kha",
   "address": "948 Nguyễn Kiệm, phường 3 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8243525,
   "Latitude": 106.6792516
 },
 {
   "STT": 1019,
   "Name": "Phòng Khám Chuyên Khoa Nội Tiêu Hóa - Gan Mật - BS Nguyễn Y Anh",
   "address": "265 Nguyễn Thiện Thuật, phường 1 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.76943,
   "Latitude": 106.6779338
 },
 {
   "STT": 1020,
   "Name": "Phòng Khám Chuyên Khoa Nội Tiêu Hóa Tâm Nhất",
   "address": "69-71 Phạm Hữu Chí, phường 12 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7557034,
   "Latitude": 106.6588471
 },
 {
   "STT": 1021,
   "Name": "Phòng Khám Chuyên Khoa Nội Tim Mạch - BS. Cao Thanh Tâm",
   "address": "194 Đất Thánh, phường 6, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.7828732,
   "Latitude": 106.6601938
 },
 {
   "STT": 1022,
   "Name": "Phòng Khám Chuyên Khoa Nội Tim Mạch - BS. Lê Thị Thủy Tùng & Nội Nhi - BS. Đông Quốc Chí",
   "address": "215 Xóm Đất, phường 9 , Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.758775,
   "Latitude": 106.645291
 },
 {
   "STT": 1023,
   "Name": "Phòng Khám Chuyên Khoa Nội Tim Mạch - BS. Lương Hiếu Trung",
   "address": "148/25 Tôn Thất Hiệp, phường 13, Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.763744,
   "Latitude": 106.652018
 },
 {
   "STT": 1024,
   "Name": "Phòng Khám Chuyên Khoa Nội Tim Mạch - BS. Ngô Minh Hùng",
   "address": "1017/2 Hồng Bàng, phường 12 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7542101,
   "Latitude": 106.6359657
 },
 {
   "STT": 1025,
   "Name": "Phòng Khám Chuyên Khoa Nội Tim Mạch - BS. Nguyễn Kim Chung",
   "address": "793/51/41 Trần Xuân Soạn, phường Tân Hưng , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7475856,
   "Latitude": 106.700068
 },
 {
   "STT": 1026,
   "Name": "Phòng Khám Chuyên Khoa Nội Tim Mạch - BS. Nguyễn Thị Lệ Trang",
   "address": "7/19 Thành Thái, phường 14 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7709422,
   "Latitude": 106.6654249
 },
 {
   "STT": 1027,
   "Name": "Phòng Khám Chuyên Khoa Nội Tim Mạch - BS. Tôn Thất Minh",
   "address": "37 Đường D1, Khu đô thị mới Him Lam, phường Tân Hưng , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.744957,
   "Latitude": 106.6994736
 },
 {
   "STT": 1028,
   "Name": "Phòng Khám Chuyên Khoa Nội Tim Mạch - BS. Đặng Thị Thúy Anh",
   "address": "415 Sư Vạn Hạnh, phường 12 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7723376,
   "Latitude": 106.6692851
 },
 {
   "STT": 1029,
   "Name": "Phòng Khám Chuyên Khoa Nội Tim Mạch. Siêu Âm Tim - BS. Nguyễn Thượng Nghĩa",
   "address": "400A Lê Văn Sỹ, phường 14, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.788759,
   "Latitude": 106.676595
 },
 {
   "STT": 1030,
   "Name": "Phòng Khám Chuyên Khoa Nội Tim Mạch - BS. Trang",
   "address": "18 Đông Hồ, phường 4 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.739832,
   "Latitude": 106.6709599
 },
 {
   "STT": 1031,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "56A/48 Lạc Long Quân, phường 3, Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.761118,
   "Latitude": 106.637933
 },
 {
   "STT": 1032,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "67 Đường Số 3 - CX Lữ Gia,phường 15 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7731617,
   "Latitude": 106.6554397
 },
 {
   "STT": 1033,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "69 đường Phú Thọ, phường 1 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.782916,
   "Latitude": 106.634435
 },
 {
   "STT": 1034,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "71-73 Lý Thường Kiệt, phường 7 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7622128,
   "Latitude": 106.6604347
 },
 {
   "STT": 1035,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "20 Đường số 10, phường Linh Tây, quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8508132,
   "Latitude": 106.7633744
 },
 {
   "STT": 1036,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "14 Lê Quý Đôn, phường Bình Thọ, quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8419951,
   "Latitude": 106.765926
 },
 {
   "STT": 1037,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "45 Ngô Tất Tố, phường 21 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.793149,
   "Latitude": 106.71054
 },
 {
   "STT": 1038,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "120 Nơ Trang Long, phường 14, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.809523,
   "Latitude": 106.695212
 },
 {
   "STT": 1039,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "84 Điện Biên Phủ, phường 17, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.7951419,
   "Latitude": 106.7025816
 },
 {
   "STT": 1040,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "109G Lò Siêu, phường 16 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7562525,
   "Latitude": 106.6485212
 },
 {
   "STT": 1041,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "118/51 Bạch Đằng, phường 24, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.803835,
   "Latitude": 106.707602
 },
 {
   "STT": 1042,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "443 Nơ Trang Long, phường 13, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8211574,
   "Latitude": 106.7053312
 },
 {
   "STT": 1043,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "77 Tôn Thất Hiệp, phường 13 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764229,
   "Latitude": 106.65345
 },
 {
   "STT": 1044,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "341/3 Lạc Long Quân, phường 5 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.767591,
   "Latitude": 106.642559
 },
 {
   "STT": 1045,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "41 Trần Quý, phường 4 , Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.759357,
   "Latitude": 106.657861
 },
 {
   "STT": 1046,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "8/67 Tân Hóa, phường 1 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7569009,
   "Latitude": 106.6384379
 },
 {
   "STT": 1047,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "84A Đỗ Xuân Hợp, phường Phước Long A , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8315798,
   "Latitude": 106.7675852
 },
 {
   "STT": 1048,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "96 Trần Nguyên Hãn, phường 13 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.745646,
   "Latitude": 106.658416
 },
 {
   "STT": 1049,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "292 Hòa Hảo, phường 4 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.76225,
   "Latitude": 106.669534
 },
 {
   "STT": 1050,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "28 Thành Thái, phường 12 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7687303,
   "Latitude": 106.6668076
 },
 {
   "STT": 1051,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "104/16 Thành Thái, phường 12 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7685468,
   "Latitude": 106.666907
 },
 {
   "STT": 1052,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "293 Hòa Hảo, phường 4 , Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.761979,
   "Latitude": 106.6694909
 },
 {
   "STT": 1053,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "242 Nguyễn Chí Thanh, phường 3, Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7601554,
   "Latitude": 106.669603
 },
 {
   "STT": 1054,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "117/3 Thành Thái, phường 14 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7688878,
   "Latitude": 106.6660988
 },
 {
   "STT": 1055,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "9/4 Hồ Thị Kỷ, phường 1 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7635359,
   "Latitude": 106.677629
 },
 {
   "STT": 1056,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "314 Hòa Hảo, phường 4 , Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7620158,
   "Latitude": 106.6691581
 },
 {
   "STT": 1057,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "299 Hòa Hảo, phường 4 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.761992,
   "Latitude": 106.669387
 },
 {
   "STT": 1058,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "190 Nguyễn Duy Dương, phường 3 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.761759,
   "Latitude": 106.670876
 },
 {
   "STT": 1059,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "200 Nguyễn Duy Dương, phường 3 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.761933,
   "Latitude": 106.670848
 },
 {
   "STT": 1060,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp",
   "address": "24 Trần Nhân Tôn, phường 2 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7623731,
   "Latitude": 106.674896
 },
 {
   "STT": 1061,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lê Tấn Phát",
   "address": "300A Lê Quang Định, phường 11, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8180045,
   "Latitude": 106.6954544
 },
 {
   "STT": 1062,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Văn Minh",
   "address": "1618 Tỉnh Lộ 10, phường Tân Tạo, quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.758402,
   "Latitude": 106.5804721
 },
 {
   "STT": 1063,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp & Sản Phụ Khoa",
   "address": "316-318 Tùng Thiện Vương,phường 13 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7455888,
   "Latitude": 106.6583344
 },
 {
   "STT": 1064,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp & Siêu Âm - BS. Dương Quang Minh & BS. Nguyễn Thị Lộc",
   "address": "107 Lê Lâm, phường Phú Thạnh, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.780529,
   "Latitude": 106.625175
 },
 {
   "STT": 1065,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp & Tai Mũi Họng - BS. Lưu Hồng Ân",
   "address": "34 (số cũ A33/2) Đường số 39, Khu phố 6, phường Bình Thuận ,Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.744043,
   "Latitude": 106.716227
 },
 {
   "STT": 1066,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp & Tim Mạch - BS. Lâm Thanh Phong",
   "address": "43B Nguyễn Duy Trinh, phường Long Trường , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7986358,
   "Latitude": 106.8237374
 },
 {
   "STT": 1067,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp & Tim Mạch - BS. Lê Thế Cường",
   "address": "795 Thống Nhất, phường 13 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8550609,
   "Latitude": 106.6655413
 },
 {
   "STT": 1068,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp & Ung Bướu - BS. Huỳnh Ngọc Phương Thảo",
   "address": "8A Tân Hóa, phường 1 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.755749,
   "Latitude": 106.637286
 },
 {
   "STT": 1069,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Trần Xuân Trường",
   "address": "28/3C Trường Chinh, phường Tân Thới Nhất , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.839058,
   "Latitude": 106.617158
 },
 {
   "STT": 1070,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Đình Huề",
   "address": " 79 Nguyễn Kiệm, phường 3, Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8173997,
   "Latitude": 106.67864
 },
 {
   "STT": 1071,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - Bác Sĩ Bảng",
   "address": "268 Mai Xuân Thưởng, phường 2 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7511173,
   "Latitude": 106.6477367
 },
 {
   "STT": 1072,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Bành Văn Sơn",
   "address": "943/2 Quang Trung, phường 14, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8454334,
   "Latitude": 106.6383256
 },
 {
   "STT": 1073,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Cao Tấn Phước",
   "address": "B113 Nguyễn Văn Quá, phường Đông Hưng Thuận , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8420845,
   "Latitude": 106.6305882
 },
 {
   "STT": 1074,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Hoàng Việt",
   "address": "9 Lê Văn Linh, phường 13 , Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.76414,
   "Latitude": 106.707239
 },
 {
   "STT": 1075,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Huỳnh Huệ Đình",
   "address": "356T Lê Quang Sung, phường 6 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7518499,
   "Latitude": 106.643072
 },
 {
   "STT": 1076,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Huỳnh Văn Thiên",
   "address": "56 lô O Cư Xá Vĩnh Hội, phường 9, Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7616455,
   "Latitude": 106.7004408
 },
 {
   "STT": 1077,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Khánh",
   "address": "780 Lạc Long Quân, phường 9, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.787179,
   "Latitude": 106.649099
 },
 {
   "STT": 1078,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Lại Thị Công Thành",
   "address": "217B Bãi Sậy, phường 4 , Quận 6, thành phố Hồ Chí Minh",
   "Longtitude": 10.7470883,
   "Latitude": 106.6444867
 },
 {
   "STT": 1079,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Lâm Sanh Hùng",
   "address": "209/13E Tôn Thất Thuyết, phường 3 , Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.754021,
   "Latitude": 106.6993449
 },
 {
   "STT": 1080,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Lê Ngọc Thạch",
   "address": "76 Bis Văn Thân, phường 8 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7418878,
   "Latitude": 106.6372845
 },
 {
   "STT": 1081,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Lê Thượng Vũ",
   "address": "80/23 Trần Quang Diệu, phường 14 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.789675,
   "Latitude": 106.678673
 },
 {
   "STT": 1082,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Lê Đình Lộc",
   "address": "311 Hậu Giang, phường 5 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.749433,
   "Latitude": 106.641504
 },
 {
   "STT": 1083,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Mai Hoàng Hùng",
   "address": "83 Hậu Giang, phường 5 , Quận 6, thành phố Hồ Chí Minh",
   "Longtitude": 10.749376,
   "Latitude": 106.647069
 },
 {
   "STT": 1084,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Mỹ Trang",
   "address": "321 Đoàn Văn Bơ, phường 13 ,Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.761329,
   "Latitude": 106.707194
 },
 {
   "STT": 1085,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Ngô Minh Đức",
   "address": "70 Nguyễn Thiện Thuật, phường 2 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.767248,
   "Latitude": 106.6810456
 },
 {
   "STT": 1086,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Ngô Thanh Thúy",
   "address": "71/1 Cô Bắc, phường Cô Giang ,Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764603,
   "Latitude": 106.694733
 },
 {
   "STT": 1087,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Ngô Văn Tiến",
   "address": "416 Tôn Đản, phường 4 , Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7537201,
   "Latitude": 106.7052483
 },
 {
   "STT": 1088,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Nguyễn Anh Thư",
   "address": "110 Đề Thám, phường Cầu Ông Lãnh , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764629,
   "Latitude": 106.695273
 },
 {
   "STT": 1089,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Nguyễn Học Triết",
   "address": "213/5 Nguyễn Thiện Thuật,phường 1 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7677716,
   "Latitude": 106.6796781
 },
 {
   "STT": 1090,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Nguyễn Minh Tuấn",
   "address": "523 Hậu Giang, phường 11 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7481286,
   "Latitude": 106.6358676
 },
 {
   "STT": 1091,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Nguyễn Nẫm",
   "address": "TK 29/1 Nguyễn Cảnh Chân,phường Cầu Kho , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7606036,
   "Latitude": 106.6870414
 },
 {
   "STT": 1092,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Nguyễn Thanh Trung",
   "address": "53 Hùng Vương, phường 4 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7629463,
   "Latitude": 106.6776058
 },
 {
   "STT": 1093,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Nguyễn Tự",
   "address": "120 Bình Tiên, phường 3 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.742452,
   "Latitude": 106.643115
 },
 {
   "STT": 1094,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Nguyễn Văn Dũng",
   "address": "36 Nguyễn Duy Trinh, phường Bình Trưng Tây , Quận 2 , thành phố Hồ Chí Minh",
   "Longtitude": 10.787644,
   "Latitude": 106.764359
 },
 {
   "STT": 1095,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Nguyễn Văn Minh",
   "address": "34 Đường Số 11A, phường Bình Trị Đông B, quận Bình Tân, thành phố Hồ Chí Minh",
   "Longtitude": 10.7469024,
   "Latitude": 106.6136624
 },
 {
   "STT": 1096,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Nhàn & BS Cầu",
   "address": "116 Phạm Văn Chí, phường 4 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7458791,
   "Latitude": 106.6483665
 },
 {
   "STT": 1097,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Quảng",
   "address": "186 Nguyễn Duy Trinh, phường Bình Trưng Tây , Quận 2 , thành phố Hồ Chí Minh",
   "Longtitude": 10.786684,
   "Latitude": 106.7640533
 },
 {
   "STT": 1098,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Thanh Liêm",
   "address": "67/11 Huỳnh Tấn Phát, Thị trấn Nhà Bè , huyện Nhà Bè , thành phố Hồ Chí Minh",
   "Longtitude": 10.695668,
   "Latitude": 106.740064
 },
 {
   "STT": 1099,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Thông",
   "address": "124 Bình Tiên, phường 3 , Quận 6, thành phố Hồ Chí Minh",
   "Longtitude": 10.742642,
   "Latitude": 106.6434167
 },
 {
   "STT": 1100,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Trần Thị Bạch Vân",
   "address": "126 Sương Nguyệt Ánh, phường Bến Thành , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7712317,
   "Latitude": 106.6881186
 },
 {
   "STT": 1101,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Trần Thị Thủy Trinh",
   "address": "286/2 Bình Tiên, phường 4 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.745412,
   "Latitude": 106.642708
 },
 {
   "STT": 1102,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Vĩnh Khang",
   "address": "30/2 Huỳnh Tấn Phát, Thị trấn Nhà Bè , huyện Nhà Bè , thành phố Hồ Chí Minh",
   "Longtitude": 10.698121,
   "Latitude": 106.738786
 },
 {
   "STT": 1103,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Võ Quang Huy",
   "address": "186 Bãi Sậy, phường 4 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7476143,
   "Latitude": 106.6474432
 },
 {
   "STT": 1104,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Võ Thị Thiên Hương",
   "address": "31A-31B Trần Bình Trọng, phường 1 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7531798,
   "Latitude": 106.6819564
 },
 {
   "STT": 1105,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Xuân & BS Thảo",
   "address": "93/4 Nguyễn Văn Luông, phường 10 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.741311,
   "Latitude": 106.632749
 },
 {
   "STT": 1106,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Điểm",
   "address": "592A Nguyễn Duy Trinh, phường Bình Trưng Đông , Quận 2 , thành phố Hồ Chí Minh",
   "Longtitude": 10.78204,
   "Latitude": 106.7794935
 },
 {
   "STT": 1107,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Đỗ Ngọc Thi",
   "address": "136/63 Hậu Giang, phường 6 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.750218,
   "Latitude": 106.644826
 },
 {
   "STT": 1108,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS Đỗ Văn Khoa",
   "address": "14/8 Bình Đông, phường 15 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.734373,
   "Latitude": 106.636268
 },
 {
   "STT": 1109,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lê Thị Cẩm Thơ & BS. Trần Văn Hòa",
   "address": "91 Phan Huy Thực, phường Tân Kiểng , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7473715,
   "Latitude": 106.7072944
 },
 {
   "STT": 1110,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Bùi Kim Dung & Chuyên Khoa Nội Thần Kinh - BS. Hồ Nguyễn Yến Phi",
   "address": "chung cư Hùng Vương, 26 Tản Đà,phường 11 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7532004,
   "Latitude": 106.6644372
 },
 {
   "STT": 1111,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Bùi Nhuận Quý",
   "address": "37 Nhiêu Tứ, phường 7 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.8007429,
   "Latitude": 106.6891873
 },
 {
   "STT": 1112,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Bùi Quang Đi",
   "address": "6C Phan Bội Châu, phường 2, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.802562,
   "Latitude": 106.700997
 },
 {
   "STT": 1113,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Bùi Thị Mai Trinh",
   "address": "36 Lê Văn Lương, phường Tân Hưng , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7373239,
   "Latitude": 106.702209
 },
 {
   "STT": 1114,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Bùi Trọng Hưng",
   "address": "741 Phạm Văn Bạch, phường 12, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8314777,
   "Latitude": 106.6427778
 },
 {
   "STT": 1115,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Cao Thị Tâm",
   "address": "646 Hưng Phú, phường 10 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7462579,
   "Latitude": 106.668347
 },
 {
   "STT": 1116,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Châu Ngọc Hoa",
   "address": "719 Bùi Đình Túy, phường 12, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.809388,
   "Latitude": 106.706861
 },
 {
   "STT": 1117,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Chung Bá Ngọc",
   "address": "15/2A Nơ Trang Long, phường 7, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.806459,
   "Latitude": 106.694837
 },
 {
   "STT": 1118,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Chung Kế Văn",
   "address": "568 Hậu Giang, phường 12 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.748436,
   "Latitude": 106.635767
 },
 {
   "STT": 1119,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Diễm & BS. Di",
   "address": "48B Nguyễn Huy Lượng, phường 14 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8054969,
   "Latitude": 106.6952828
 },
 {
   "STT": 1120,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Diệp Trọng Khải",
   "address": "21/5 Nguyễn Ảnh Thủ, Xã Bà Điểm , huyện Hóc Môn , thành phố Hồ Chí Minh",
   "Longtitude": 10.8460179,
   "Latitude": 106.5999565
 },
 {
   "STT": 1121,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Dư Văn Thành",
   "address": "95 Trần văn Kiểu, phường 10 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.735449,
   "Latitude": 106.6266658
 },
 {
   "STT": 1122,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Dương Quang Thảo",
   "address": "52/6B Khu phố 3, Thị trấn Hóc Môn , huyện Hóc Môn , thành phố Hồ Chí Minh",
   "Longtitude": 10.8883459,
   "Latitude": 106.5900712
 },
 {
   "STT": 1123,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Hà Thanh Hải",
   "address": "568A Nguyễn Chí Thanh, phường 7 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7582866,
   "Latitude": 106.6606169
 },
 {
   "STT": 1124,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Hảo",
   "address": "3 Tôn Thất Thiệp, phường Hiệp Phú , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8423963,
   "Latitude": 106.7731985
 },
 {
   "STT": 1125,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Hồ Thị Đoan Trinh",
   "address": "320/7 Huỳnh Văn Bánh, phường 11 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.791838,
   "Latitude": 106.672703
 },
 {
   "STT": 1126,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Hoàng Thanh Hiền",
   "address": "98B Tân Khai, phường 4 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.75823,
   "Latitude": 106.655679
 },
 {
   "STT": 1127,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Hoàng Tiến Mỹ",
   "address": "333 Nguyễn Thiện Thuật, phường 1 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7700561,
   "Latitude": 106.6772137
 },
 {
   "STT": 1128,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Hoàng Văn Long",
   "address": "55 Nguyễn Quyền, phường 11 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.748699,
   "Latitude": 106.665255
 },
 {
   "STT": 1129,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Huệ",
   "address": "9 Đường Số 9, phường Long Thạnh Mỹ , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8470564,
   "Latitude": 106.8165567
 },
 {
   "STT": 1130,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Huỳnh Phước Hải",
   "address": "22 Cô Giang, phường Cầu Ông Lãnh , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764139,
   "Latitude": 106.69593
 },
 {
   "STT": 1131,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Huỳnh Quang Khánh",
   "address": "300/7 Khuông Việt, phường Phú Trung, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.779381,
   "Latitude": 106.640962
 },
 {
   "STT": 1132,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Huỳnh Thị Kiểu",
   "address": "565 Huỳnh Tấn Phát, KP1, phường Tân Thuận Đông , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7416923,
   "Latitude": 106.7298373
 },
 {
   "STT": 1133,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Huỳnh Văn Tiến",
   "address": "158 Đường Số 5, phường Bình Trị Đông B, quận Bình Tân, thành phố Hồ Chí Minh",
   "Longtitude": 10.7497726,
   "Latitude": 106.6117708
 },
 {
   "STT": 1134,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Khang",
   "address": "146/37/19 Vũ Tùng, phường 1, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.801653,
   "Latitude": 106.702685
 },
 {
   "STT": 1135,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Khoa & Nha Khoa - BS. Vinh",
   "address": "126 Nguyễn Đình Chính, phường 15 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.795562,
   "Latitude": 106.678143
 },
 {
   "STT": 1136,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Kiều Minh Chứ",
   "address": "2355/1B Tổ 13, Khu phố 1,phường An Phú Đông , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.851754,
   "Latitude": 106.7244215
 },
 {
   "STT": 1137,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Kiều Phước Thọ",
   "address": "134/7 Nguyễn Ảnh Thủ, phường Trung Mỹ Tây , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8633947,
   "Latitude": 106.6127389
 },
 {
   "STT": 1138,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Kim Yến",
   "address": "160/1/4 Xô Viết Nghệ Tĩnh,phường 21 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.797369,
   "Latitude": 106.711066
 },
 {
   "STT": 1139,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. La Thị Thiên Hương",
   "address": "90/59 Vũ Tùng, phường 2 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.801359,
   "Latitude": 106.7013039
 },
 {
   "STT": 1140,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lâm Nguyễn Nhã Trúc",
   "address": "195 Lũy Bán Bích, phường Hiệp Tân, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.769237,
   "Latitude": 106.63146
 },
 {
   "STT": 1141,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lâm Thanh Vân",
   "address": "18 Đường số 15, phường Tân Kiểng , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.745103,
   "Latitude": 106.7055431
 },
 {
   "STT": 1142,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lâm Tuấn Kiệt",
   "address": "99 Phan Đình Phùng, phường 17, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.794226,
   "Latitude": 106.683716
 },
 {
   "STT": 1143,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lê Hoàng Oanh",
   "address": "129/52N Bến Vân Đồn, phường 6 ,Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.762726,
   "Latitude": 106.699537
 },
 {
   "STT": 1144,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lê Kim Sang",
   "address": "35 Cây Keo, phường Hiệp Tân, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.771175,
   "Latitude": 106.630305
 },
 {
   "STT": 1145,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lê Mai Anh",
   "address": "596 Hưng Phú, phường 9 , Quận 8, thành phố Hồ Chí Minh",
   "Longtitude": 10.746543,
   "Latitude": 106.669719
 },
 {
   "STT": 1146,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lê Ngọc Ánh",
   "address": "182/47 Lê Văn Sỹ, phường 10, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.794427,
   "Latitude": 106.670879
 },
 {
   "STT": 1147,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lê Như Vương Anh",
   "address": "262 Lê Văn Thọ, phường 6 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.844777,
   "Latitude": 106.657251
 },
 {
   "STT": 1148,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lê Quang Luận",
   "address": "124 Lê Văn Thọ, phường 11 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.847165,
   "Latitude": 106.656746
 },
 {
   "STT": 1149,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lê Quốc Tuấn",
   "address": "458 Phạm Văn Bạch, phường 12, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8263013,
   "Latitude": 106.6405596
 },
 {
   "STT": 1150,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lê Thanh Phuông",
   "address": "1 Lương Văn Can, phường 15 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.736055,
   "Latitude": 106.639235
 },
 {
   "STT": 1151,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lê Thị Diệu Hồng",
   "address": "86/20 Trần Hưng Đạo, phường 7 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7533834,
   "Latitude": 106.6701669
 },
 {
   "STT": 1152,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lê Thị Đến",
   "address": "907/25 Lò Gốm, phường 5 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.750168,
   "Latitude": 106.640017
 },
 {
   "STT": 1153,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lê Thiện Anh Tuấn",
   "address": "94/1024C Dương Quảng Hàm,phường 6 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.837141,
   "Latitude": 106.683447
 },
 {
   "STT": 1154,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lê Trần Trúc Mai Loan",
   "address": "502/5 Hưng Phú, phường 9 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.747116,
   "Latitude": 106.671768
 },
 {
   "STT": 1155,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lê Văn Hùng",
   "address": "640 Quốc Lộ 22, Tổ 4, Khu Phố 2,Thị trấn Củ Chi , huyện Củ Chi , thành phố Hồ Chí Minh",
   "Longtitude": 10.972192,
   "Latitude": 106.4965434
 },
 {
   "STT": 1156,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lê Đình Phú",
   "address": "26 Đường số 4, phường Phước Bình , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.818932,
   "Latitude": 106.772492
 },
 {
   "STT": 1157,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lư Kim Phượng",
   "address": "3/5/3 Thành Thái, phường 14 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7692833,
   "Latitude": 106.6661127
 },
 {
   "STT": 1158,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lư Văn Hiếu",
   "address": "49 Đường Số 6 - KDC Cầu Xáng,Xã Phạm Văn Hai, huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.7936117,
   "Latitude": 106.5919842
 },
 {
   "STT": 1159,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lương Hoàng Liêm",
   "address": "645/8 Đường Số 671, phường Tân Phú , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8507294,
   "Latitude": 106.8118165
 },
 {
   "STT": 1160,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lương Quốc Việt",
   "address": "20/4B Lê Văn Khương, Tổ 7, Khu phố 1, phường Thới An , Quận 12, thành phố Hồ Chí Minh",
   "Longtitude": 10.873811,
   "Latitude": 106.648756
 },
 {
   "STT": 1161,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lưu Trúc Viên",
   "address": "407 Tỉnh Lộ 15, Tổ 89, Ấp Phú Bình, Xã Phú Hòa Đông , huyện Củ Chi , thành phố Hồ Chí Minh",
   "Longtitude": 10.9870066,
   "Latitude": 106.5751013
 },
 {
   "STT": 1162,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lưu Vĩnh Đạt",
   "address": "176 Hoàng Diệu, phường 9 , Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7629045,
   "Latitude": 106.7028973
 },
 {
   "STT": 1163,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lý Hồng Ân",
   "address": "1196 Huỳnh Tấn Phát, phường Tân Phú , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7191829,
   "Latitude": 106.7366463
 },
 {
   "STT": 1164,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Lý Thị Thu Vân",
   "address": "128/1 Bùi Hữu Nghĩa, phường 7 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7542205,
   "Latitude": 106.6749244
 },
 {
   "STT": 1165,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Mã Minh Hương",
   "address": "153 Đường số 19, phường Bình Trị Đông B , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7532304,
   "Latitude": 106.6132539
 },
 {
   "STT": 1166,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Mai Hồ Duy",
   "address": "B69/1 Nguyễn Thị Sóc, Xã Xuân Thới Đông , huyện Hóc Môn , thành phố Hồ Chí Minh",
   "Longtitude": 10.8606646,
   "Latitude": 106.6032062
 },
 {
   "STT": 1167,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Mai Sĩ Xuân",
   "address": "32/30 Lê Thị Hồng, phường 17, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8336992,
   "Latitude": 106.6777849
 },
 {
   "STT": 1168,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nghiên",
   "address": "39/3F Phan Văn Đối, Xã Bà Điểm , huyện Hóc Môn , thành phố Hồ Chí Minh",
   "Longtitude": 10.835736,
   "Latitude": 106.5973766
 },
 {
   "STT": 1169,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Ngô Nguyễn Kim Hường",
   "address": "436 Đường 3 Tháng 2, phường 12, Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7703554,
   "Latitude": 106.6683515
 },
 {
   "STT": 1170,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Anh Minh",
   "address": "270/15 Tân Thới Hiệp 02, Khu phố 3, phường Tân Thới Hiệp , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8590424,
   "Latitude": 106.6333375
 },
 {
   "STT": 1171,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Chí Thành",
   "address": "152/26 Thành Thái, phường 10 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7686285,
   "Latitude": 106.6668261
 },
 {
   "STT": 1172,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Chí Thiện",
   "address": "175A Phạm Văn Chí, phường 3 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.745103,
   "Latitude": 106.647627
 },
 {
   "STT": 1173,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Dũng Tuấn",
   "address": "8/83 Lê Đức Thọ, phường 17 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8345073,
   "Latitude": 106.6815912
 },
 {
   "STT": 1174,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Hoàn Vũ",
   "address": "410 Huỳnh Văn Bánh, phường 14, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.791592,
   "Latitude": 106.6681854
 },
 {
   "STT": 1175,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Hoàng Thanh Phương",
   "address": "16 Trần Thanh Mại, phường Tân Tạo, quận Bình Tân, thành phố Hồ Chí Minh",
   "Longtitude": 10.755733,
   "Latitude": 106.5890264
 },
 {
   "STT": 1176,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Hữu Phúc",
   "address": "14 Đặng Dung, phường Tân Định ,Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.792829,
   "Latitude": 106.691045
 },
 {
   "STT": 1177,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Hữu Thiện",
   "address": "117 Trung Mỹ Tây, phường Trung Mỹ Tây , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8519352,
   "Latitude": 106.6122901
 },
 {
   "STT": 1178,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Hữu Thưởng",
   "address": "22/394 Lê Đức Thọ, phường 15, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.849598,
   "Latitude": 106.665533
 },
 {
   "STT": 1179,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Lê Cường",
   "address": "34 Đường số 4 Khu Bình Đăng,phường 6 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7332688,
   "Latitude": 106.6547196
 },
 {
   "STT": 1180,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Mạnh Khang",
   "address": "342 Lê Quang Sung, phường 6 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.751871,
   "Latitude": 106.643968
 },
 {
   "STT": 1181,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Mão",
   "address": "57 Tân Sơn Nhì, phường Tân Sơn Nhì, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.802388,
   "Latitude": 106.635004
 },
 {
   "STT": 1182,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Ngọc Chín",
   "address": "1113/54D Huỳnh Tấn Phát, phường Phú Thuận, Quận 7, thành phố Hồ Chí Minh",
   "Longtitude": 10.7148489,
   "Latitude": 106.737159
 },
 {
   "STT": 1183,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Ngọc Thạch",
   "address": "11/7 Nguyễn Thái Sơn, phường 3, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8157929,
   "Latitude": 106.6799585
 },
 {
   "STT": 1184,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Tấn Dũng",
   "address": "576 Phạm Thế Hiển, phường 4, Quận 8, thành phố Hồ Chí Minh",
   "Longtitude": 10.746069,
   "Latitude": 106.67726
 },
 {
   "STT": 1185,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Tế Kha",
   "address": "95 Hoa Lan, phường 2 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.7972547,
   "Latitude": 106.6894443
 },
 {
   "STT": 1186,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Thành Danh",
   "address": "2 Bùi Thị He, Thị trấn Củ Chi , huyện Củ Chi , thành phố Hồ Chí Minh",
   "Longtitude": 10.971043,
   "Latitude": 106.4826275
 },
 {
   "STT": 1187,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Thành Dũng",
   "address": "B163A Xóm Chiếu, phường 16 ,Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7565374,
   "Latitude": 106.70979
 },
 {
   "STT": 1188,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Thanh Hiền",
   "address": "21 Khu A Trường Sơn, Cư Xá Bắc Hải, phường 15 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.783132,
   "Latitude": 106.664189
 },
 {
   "STT": 1189,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Thành Hưng",
   "address": "46 Lê Đại Hành, phường 7 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7590432,
   "Latitude": 106.6591857
 },
 {
   "STT": 1190,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Thành Trí",
   "address": "217 Hàn Hải Nguyên, phường 2 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.758098,
   "Latitude": 106.646044
 },
 {
   "STT": 1191,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Thế Gia",
   "address": "285 Nguyễn Văn Lượng, phường 10 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8385589,
   "Latitude": 106.6658145
 },
 {
   "STT": 1192,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Thế Hùng",
   "address": "317 Lê Đại Hành, phường 13 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.765109,
   "Latitude": 106.654713
 },
 {
   "STT": 1193,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Thế Thành",
   "address": "46 Đinh Tiên Hoàng, phường 1, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.795119,
   "Latitude": 106.6962549
 },
 {
   "STT": 1194,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Thị Hậu",
   "address": "73 Liên Tỉnh 5, phường 5 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.736312,
   "Latitude": 106.656725
 },
 {
   "STT": 1195,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Thị Hồng Cúc",
   "address": "57 Đường số 81, Khu phố 1, phường Tân Quy, Quận 7, thành phố Hồ Chí Minh",
   "Longtitude": 10.740241,
   "Latitude": 106.7050568
 },
 {
   "STT": 1196,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Thị Huệ",
   "address": "A13/1 Ấp 1, Quốc Lộ 50, Xã Bình Hưng , Huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.6463956,
   "Latitude": 106.6565717
 },
 {
   "STT": 1197,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Thị Mùi",
   "address": "28 Đường số 18, phường 8 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8387381,
   "Latitude": 106.6562551
 },
 {
   "STT": 1198,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Thị Ngọc Linh",
   "address": "67 Hoàng Hoa Thám, phường 6, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.806205,
   "Latitude": 106.6901627
 },
 {
   "STT": 1199,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Thị Thu Nga",
   "address": "128 Mã Lò, khu phố 2, phường Bình Trị Đông , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7621761,
   "Latitude": 106.6047924
 },
 {
   "STT": 1200,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Thị Thu Thảo",
   "address": "382 Hồ Ngọc Lãm, phường An Lạc, quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7231349,
   "Latitude": 106.6111242
 },
 {
   "STT": 1201,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Thị Thu Thảo",
   "address": "204/2 Nơ Trang Long, phường 12, quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.816993,
   "Latitude": 106.700073
 },
 {
   "STT": 1202,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Thị Thu Vân",
   "address": "608 Trần Hưng Đạo, phường 2 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7552902,
   "Latitude": 106.6786295
 },
 {
   "STT": 1203,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Thị Đố",
   "address": "87 Huỳnh Văn Hai, phường 14, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8049449,
   "Latitude": 106.6976655
 },
 {
   "STT": 1204,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Thượng Khanh",
   "address": "26 Phan Bội Châu, phường 14, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8040323,
   "Latitude": 106.7001821
 },
 {
   "STT": 1205,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Trận",
   "address": "82/18 Đinh Tiên Hoàng, phường 1, quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.795895,
   "Latitude": 106.696669
 },
 {
   "STT": 1206,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Tri Lộc",
   "address": "253A Hà Đặc, phường Trung Mỹ Tây , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8571717,
   "Latitude": 106.6160167
 },
 {
   "STT": 1207,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Trí Trung",
   "address": "322 Đường số 7, phường Tân Tạo, quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7519089,
   "Latitude": 106.6036862
 },
 {
   "STT": 1208,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Trung Vinh",
   "address": "321 Lê Đại Hành, phường 13 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7651869,
   "Latitude": 106.6545469
 },
 {
   "STT": 1209,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Trường Kỳ",
   "address": "13/2A Bông Sao, phường 5 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.740773,
   "Latitude": 106.661501
 },
 {
   "STT": 1210,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Tuấn Cường",
   "address": "20 Đường Số 5, phường Tân Phú , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7362776,
   "Latitude": 106.7196472
 },
 {
   "STT": 1211,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Tuấn Nghị",
   "address": "313/16 Nguyễn Trãi, phường 7 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7539038,
   "Latitude": 106.6687002
 },
 {
   "STT": 1212,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Văn Hưng",
   "address": "118 Liên Tỉnh 5, phường 6 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7383659,
   "Latitude": 106.656054
 },
 {
   "STT": 1213,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Văn Khuê",
   "address": "370 Nguyễn Văn Luông, phường 12 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7508022,
   "Latitude": 106.6352917
 },
 {
   "STT": 1214,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Văn Lơ",
   "address": "30 Nguyễn Văn Đừng, phường 6 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7517408,
   "Latitude": 106.6712832
 },
 {
   "STT": 1215,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Văn Nhôm",
   "address": "160 Cây Keo, phường Hiệp Tân, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.77244,
   "Latitude": 106.627401
 },
 {
   "STT": 1216,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Viết Trung",
   "address": "51 Nguyễn Đức Thuận, phường 13, Tân Bình , thành phố Hồ Chí Minh",
   "Longtitude": 10.8056477,
   "Latitude": 106.6412283
 },
 {
   "STT": 1217,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Xuân Ninh",
   "address": "134 Lê Sát, phường Tân Quý, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.7935976,
   "Latitude": 106.6190582
 },
 {
   "STT": 1218,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Đình Khen",
   "address": "A21 Tô Ký, phường Trung Mỹ Tây, Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.856544,
   "Latitude": 106.6143944
 },
 {
   "STT": 1219,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Đình Nam",
   "address": "79 Bạch Đằng, phường 15 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.80277,
   "Latitude": 106.709711
 },
 {
   "STT": 1220,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Đô",
   "address": "C88A Nguyễn Văn Quá, phường Đông Hưng Thuận , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8420845,
   "Latitude": 106.6305882
 },
 {
   "STT": 1221,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nhan Tô Tài",
   "address": "368/8 Tân Thới Hiệp 21, Khu phố 3, phường Tân Thới Hiệp , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8307416,
   "Latitude": 106.6056595
 },
 {
   "STT": 1222,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Ninh Thị Tuyết",
   "address": "319 Phú Thọ Hòa, phường Phú Thọ Hòa, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.78764,
   "Latitude": 106.6267666
 },
 {
   "STT": 1223,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Phạm Hoàng Minh Nhựt",
   "address": "1/120 Nguyễn Văn Quá, phường Đông Hưng Thuận , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8538737,
   "Latitude": 106.6385194
 },
 {
   "STT": 1224,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Phạm Hoàng Tuấn",
   "address": "198 Lý Thường Kiệt, Thị trấn Hóc Môn , huyện Hóc Môn , thành phố Hồ Chí Minh",
   "Longtitude": 10.88742,
   "Latitude": 106.5917
 },
 {
   "STT": 1225,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Phạm Ngọc Lai",
   "address": "71 Nguyễn Thượng Hiền, phường 5, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.813929,
   "Latitude": 106.6852399
 },
 {
   "STT": 1226,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Phạm Quốc Dũng",
   "address": "94 Lê Thị Bạch Cát, phường 13 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764424,
   "Latitude": 106.652086
 },
 {
   "STT": 1227,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Phạm Thị Phương Thanh",
   "address": "115 Phan Văn Hân, phường 17, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.792889,
   "Latitude": 106.706937
 },
 {
   "STT": 1228,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Phạm Thị Thu Hà",
   "address": "766/7 Cách Mạng Tháng Tám,phường 5, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.787584,
   "Latitude": 106.664512
 },
 {
   "STT": 1229,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Phạm Tiến Ngọc",
   "address": "98 Đường số 3, phường 9 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8472522,
   "Latitude": 106.6514486
 },
 {
   "STT": 1230,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Phạm Văn Sơn",
   "address": "296 Nguyễn Thái Sơn, phường 4, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8221338,
   "Latitude": 106.6850751
 },
 {
   "STT": 1231,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Phạm Xuân Hường & BS. Lê Hải Vân",
   "address": "861/64 Trần Xuân Soạn, phường Tân Hưng , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.750235,
   "Latitude": 106.698903
 },
 {
   "STT": 1232,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Phạm Đình Phú",
   "address": "790/38 Nguyễn Kiệm, phường 13, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.811559,
   "Latitude": 106.679509
 },
 {
   "STT": 1233,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Phạm Đình Thảo",
   "address": "809 Lê Đức Thọ, phường 16 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.849648,
   "Latitude": 106.663786
 },
 {
   "STT": 1234,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Phan Phi Yến",
   "address": "31A Bình Phú, phường 10 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7404195,
   "Latitude": 106.6292685
 },
 {
   "STT": 1235,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Phan Thị Hồng Hà",
   "address": "134A Xóm Đất, phường 8 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.758922,
   "Latitude": 106.646309
 },
 {
   "STT": 1236,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Phan Thị Lộc",
   "address": "A17 Quốc lộ 22, phường Trung Mỹ Tây , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.856544,
   "Latitude": 106.6143944
 },
 {
   "STT": 1237,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Phan Văn Duyệt",
   "address": "110 Tôn Thất Hiệp, phường 13 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764308,
   "Latitude": 106.653042
 },
 {
   "STT": 1238,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Phan Đăng Lộc",
   "address": "90 Trần Hữu Trang, phường 10, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.792959,
   "Latitude": 106.671944
 },
 {
   "STT": 1239,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Phú",
   "address": "1341 Tỉnh Lộ 10, phường Tân Tạo A , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7566529,
   "Latitude": 106.5897169
 },
 {
   "STT": 1240,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Phùng Hoàng Đạo",
   "address": "45 Đường Số 5, phường 9, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8483048,
   "Latitude": 106.6700522
 },
 {
   "STT": 1241,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Phùng Thế Truyền",
   "address": "C26 Tổ 14 Khu phố 1, phường Đông Hưng Thuận , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8452824,
   "Latitude": 106.629938
 },
 {
   "STT": 1242,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Tân Văn Chiều",
   "address": "266A Nguyễn Ảnh Thủ, phường Hiệp Thành, Quận 12, thành phố Hồ Chí Minh",
   "Longtitude": 10.8797898,
   "Latitude": 106.6358135
 },
 {
   "STT": 1243,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Thái Hoàng Hải",
   "address": "\n24 Nơ Trang Long, phường 14, Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8050964,
   "Latitude": 106.694995
 },
 {
   "STT": 1244,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Thành",
   "address": "61 Vườn Lài, phường An Phú Đông, Quận 12, thành phố Hồ Chí Minh",
   "Longtitude": 10.8556976,
   "Latitude": 106.6926259
 },
 {
   "STT": 1245,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Thuận & Sản Phụ Khoa - BS. Hạnh",
   "address": "60A HT05, Tổ 27, Khu phố 3, phường Hiệp Thành, Quận 12, thành phố Hồ Chí Minh",
   "Longtitude": 10.8847152,
   "Latitude": 106.6362702
 },
 {
   "STT": 1246,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Tôn Thanh Trà",
   "address": "101 Nguyễn Kiệm, phường 3 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8129802,
   "Latitude": 106.6788056
 },
 {
   "STT": 1247,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Trần Công Hiệp",
   "address": "39 Phú Mỹ, phường 22, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.7897152,
   "Latitude": 106.7140384
 },
 {
   "STT": 1248,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Trần Cự Hiên",
   "address": "217 Dương Tử Giang, phường 4 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.758517,
   "Latitude": 106.654263
 },
 {
   "STT": 1249,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Trần Kiết Diều",
   "address": "682 Nguyễn Trãi, phường 11, Quận 5, thành phố Hồ Chí Minh",
   "Longtitude": 10.7532519,
   "Latitude": 106.6619572
 },
 {
   "STT": 1250,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Trần Ngọc Phiên",
   "address": "61 Gò Xoài, phường Bình Hưng Hòa A, quận Bình Tân, thành phố Hồ Chí Minh",
   "Longtitude": 10.77956,
   "Latitude": 106.602853
 },
 {
   "STT": 1251,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Trần Nhị Vinh",
   "address": "102 Phú Thọ, phường 2, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.757786,
   "Latitude": 106.641887
 },
 {
   "STT": 1252,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Trần Quốc Sơn",
   "address": "912 Nguyễn Duy Trinh, phường Phú Hữu, Quận 9, thành phố Hồ Chí Minh",
   "Longtitude": 10.7915874,
   "Latitude": 106.798219
 },
 {
   "STT": 1253,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Trần Thanh Tuấn",
   "address": "1929A Phạm Thế Hiển, phường 6, Quận 8, thành phố Hồ Chí Minh",
   "Longtitude": 10.735676,
   "Latitude": 106.648339
 },
 {
   "STT": 1254,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Trần Thanh Đức",
   "address": "135/1 Nguyễn Hữu Cảnh, phường 22 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.7956483,
   "Latitude": 106.7174862
 },
 {
   "STT": 1255,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Trần Thị Hồng",
   "address": "26 Bác Ái, phường Bình Thọ, quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.847047,
   "Latitude": 106.766817
 },
 {
   "STT": 1256,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Trần Văn Hiệp",
   "address": "B1/13 Quốc lộ 1, Ấp 2, Xã Bình Chánh, Huyện Bình Chánh, thành phố Hồ Chí Minh",
   "Longtitude": 10.6630417,
   "Latitude": 106.5672462
 },
 {
   "STT": 1257,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Trần Văn Hùng",
   "address": "98/2 Huỳnh Tấn Phát, phường Tân Thuận Tây, Quận 7, thành phố Hồ Chí Minh",
   "Longtitude": 10.7552109,
   "Latitude": 106.7238689
 },
 {
   "STT": 1258,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Trần Văn Ngọc",
   "address": "\n2 Tuy Lý Vương, phường 13, Quận 8, thành phố Hồ Chí Minh",
   "Longtitude": 10.7444455,
   "Latitude": 106.6558686
 },
 {
   "STT": 1259,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Trần Vĩnh Khanh",
   "address": "563 Quang Trung, phường 11 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8344624,
   "Latitude": 106.6640463
 },
 {
   "STT": 1260,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Trần Đình Huấn",
   "address": "281 Phạm Văn Chiêu, phường 14, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.850572,
   "Latitude": 106.6536662
 },
 {
   "STT": 1261,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Trang Thành Lợi",
   "address": "386 Nguyễn Kiệm, phường 3 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.800104,
   "Latitude": 106.680077
 },
 {
   "STT": 1262,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Trịnh Thế Phong",
   "address": "37 Nguyễn Đình Chính, phường 15, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.796189,
   "Latitude": 106.680193
 },
 {
   "STT": 1263,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Trịnh Thị Kim Dung",
   "address": "529 Tân Chánh Hiệp 13, Tổ 42A, Khu phố 2, phường Tân Chánh Hiệp , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8457457,
   "Latitude": 106.6322541
 },
 {
   "STT": 1264,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Trịnh Xuân An",
   "address": "143/36/8 Liên khu 5-6, phường Bình Hưng Hòa B , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7897311,
   "Latitude": 106.5889626
 },
 {
   "STT": 1265,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Trương Dũng Chinh",
   "address": "20 Lương Văn Can, phường 15 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.735078,
   "Latitude": 106.63947
 },
 {
   "STT": 1266,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Trương Ngọc Tiến",
   "address": "2/1 Thạnh Xuân 24, Tổ 24, Khu phố 2, phường Thạnh Xuân , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 1267,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Trương Tuấn Nhựt",
   "address": "42 Đông Hồ, phường 8 , quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.783636,
   "Latitude": 106.652529
 },
 {
   "STT": 1268,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Võ Hiếu Cương",
   "address": "76 Dã Tượng, phường 10 , Quận 8, thành phố Hồ Chí Minh",
   "Longtitude": 10.745193,
   "Latitude": 106.664604
 },
 {
   "STT": 1269,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Võ Thị Minh Trung",
   "address": "542B Lê Văn Khương, phường Thới An , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8851244,
   "Latitude": 106.6480678
 },
 {
   "STT": 1270,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Võ Thị Xuân Mai",
   "address": "3413A Phạm Thế Hiển, phường 7 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.709148,
   "Latitude": 106.624165
 },
 {
   "STT": 1271,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Võ Văn Hùng",
   "address": "26A Hưng Phú, phường 8 , Quận 8, thành phố Hồ Chí Minh",
   "Longtitude": 10.750892,
   "Latitude": 106.681855
 },
 {
   "STT": 1272,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Võ Văn Kim",
   "address": "B7/19 Trịnh Như Khuê, Xã Bình Chánh, huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.6656062,
   "Latitude": 106.5721106
 },
 {
   "STT": 1273,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Võ Văn Tâm",
   "address": "1322 Quang Trung, phường 14, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8371004,
   "Latitude": 106.6559987
 },
 {
   "STT": 1274,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Vũ Duy Toàn",
   "address": "Tổ 5 Ấp An Nghĩa, Xã An Thới Đông , huyện Cần Giờ , thành phố Hồ Chí Minh",
   "Longtitude": 10.5848635,
   "Latitude": 106.8254864
 },
 {
   "STT": 1275,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Vũ Ngọc Hải",
   "address": "12 Đường số 17, phường Tân Thuận Tây , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7475098,
   "Latitude": 106.7193363
 },
 {
   "STT": 1276,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Vũ Trí Thức",
   "address": "170 Rạch Bà Lớn, Ấp 5, Xã Phong Phú , Huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.7232853,
   "Latitude": 106.6445186
 },
 {
   "STT": 1277,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Đặng Bửu Thất",
   "address": "9/24 Âu Dương Lân, phường 3 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.748151,
   "Latitude": 106.682459
 },
 {
   "STT": 1278,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Đặng Nhật Quang",
   "address": "TK 18/3 Nguyễn Cảnh Chân,phường Cầu Kho , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7606013,
   "Latitude": 106.6870395
 },
 {
   "STT": 1279,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Đặng Thanh Huy",
   "address": "467 Hồ Ngọc Lãm, phường An Lạc, quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.72534,
   "Latitude": 106.6091
 },
 {
   "STT": 1280,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Đặng Văn Chép",
   "address": "7/9 Ấp 6, Xã Đông Thạnh , huyện Hóc Môn , thành phố Hồ Chí Minh",
   "Longtitude": 10.8920805,
   "Latitude": 106.625788
 },
 {
   "STT": 1281,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Đào Thị Mỹ Liên",
   "address": "8 Đường số 9, phường Tam Bình, quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8668783,
   "Latitude": 106.7371631
 },
 {
   "STT": 1282,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Đổ Hiền Thanh",
   "address": "18 Trần Văn Chẩm, Tổ 1, Ấp Hậu,Xã Tân Thông Hội , huyện Củ Chi, thành phố Hồ Chí Minh",
   "Longtitude": 10.9604536,
   "Latitude": 106.5110893
 },
 {
   "STT": 1283,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Đỗ Quốc Hùng",
   "address": "347 Nguyễn Duy Dương, phường 4 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.765385,
   "Latitude": 106.669851
 },
 {
   "STT": 1284,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Đỗ Thành Tài",
   "address": "76/30C Phan Tây Hồ, phường 7, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.800714,
   "Latitude": 106.687531
 },
 {
   "STT": 1285,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Đỗ Xuân Cảnh",
   "address": "9 Đường số 6, phường 11 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7452468,
   "Latitude": 106.6327625
 },
 {
   "STT": 1286,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Đỗ Xuân Hải",
   "address": "342 Vườn Lài, phường Phú Thọ Hòa, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.788626,
   "Latitude": 106.623068
 },
 {
   "STT": 1287,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Đoàn Huỳnh Anh",
   "address": "E9/16 Nguyễn Hữu Trí, Thị trấn Tân Túc, huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.696357,
   "Latitude": 106.5933293
 },
 {
   "STT": 1288,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Đoàn Tiến Mỹ",
   "address": "434 Tỉnh Lộ 10, khu phố 15,phường Bình Trị Đông , quận Bình Tân , thành phố Hồ Chí Minh",
   "Longtitude": 10.7572145,
   "Latitude": 106.6207121
 },
 {
   "STT": 1289,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Đồng Nữ Kim Hoàng",
   "address": "27/3 Trần Hữu Trang, phường 11, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.793529,
   "Latitude": 106.674726
 },
 {
   "STT": 1290,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - Cột Sống",
   "address": "197 Xô Viết Nghệ Tĩnh, phường 17, quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.799593,
   "Latitude": 106.710981
 },
 {
   "STT": 1291,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - Khoa Chấn Thương Chỉnh Hình & Cơ Xương Khớp - BS. Vũ Huyền Trinh",
   "address": "586 Hưng Phú, phường 9 , Quận 8, thành phố Hồ Chí Minh",
   "Longtitude": 10.746606,
   "Latitude": 106.669893
 },
 {
   "STT": 1292,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp Ái Vy",
   "address": "160 Lê Hoàng Phái, phường 17, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8425835,
   "Latitude": 106.675459
 },
 {
   "STT": 1293,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp Gan Tâm Đức",
   "address": "258 Vườn Lài, phường Phú Thọ Hòa, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.787993,
   "Latitude": 106.626184
 },
 {
   "STT": 1294,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp Hồng Ân",
   "address": "21 Trương Vĩnh Ký, phường Tân Thành, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.7932404,
   "Latitude": 106.6367969
 },
 {
   "STT": 1295,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Hợp - BS. Nguyễn Đỗ Ngọc",
   "address": "41 Đường số 2 , Khu phố 6,phường Bình Chiểu , Thủ Đức , thành phố Hồ Chí Minh",
   "Longtitude": 10.874435,
   "Latitude": 106.7222747
 },
 {
   "STT": 1296,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát",
   "address": "353 Bà Hạt, phường 4 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.765966,
   "Latitude": 106.669208
 },
 {
   "STT": 1297,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát & Ung Bướu - BS. Quách Vĩnh Phúc & Nhi Khoa - BS. Nguyễn Thùy Trang",
   "address": "161/3A3 Trường Chinh, phường Tân Hưng Thuận , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8356482,
   "Latitude": 106.6200941
 },
 {
   "STT": 1298,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Chung Hoài Nữ",
   "address": "323/11 Minh Phụng, phường 2 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.756433,
   "Latitude": 106.643112
 },
 {
   "STT": 1299,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát & Chuyên Khoa Nhi - BS. Nguyễn Trọng Hiếu",
   "address": "24 Ký Hòa, phường 11 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7545758,
   "Latitude": 106.6618805
 },
 {
   "STT": 1300,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát & Chuyên Khoa Tiêu Hóa Gan Mật - BS. Nguyễn Hữu Thịnh",
   "address": "24 Thuận Kiều, phường 12 , Quận 5 , Hồ Chí Min",
   "Longtitude": 10.755431,
   "Latitude": 106.6585902
 },
 {
   "STT": 1301,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát & Da Liễu - BS. Chung Minh Hùng",
   "address": "145 Bình Thới, phường 11 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7656481,
   "Latitude": 106.6476045
 },
 {
   "STT": 1302,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát & Da Liễu - BS. Lê Ngọc Diệp",
   "address": "133/3/37 Tô Hiến Thành, phường 13 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.779408,
   "Latitude": 106.668807
 },
 {
   "STT": 1303,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát & Lão Khoa - BS. Lê Tấn Lợi",
   "address": "382 Nguyễn Công Trứ, phường Cầu Ông Lãnh , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.765697,
   "Latitude": 106.697717
 },
 {
   "STT": 1304,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát & Nhi Khoa - BS. Lê Thị Gấm",
   "address": "920 Nguyễn Thị Định, phường Thạnh Mỹ Lợi , Quận 2 , thành phố Hồ Chí Minh",
   "Longtitude": 10.77768,
   "Latitude": 106.7644
 },
 {
   "STT": 1305,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát & Nhi Khoa - BS. Nguyễn Hữu Chí",
   "address": "351 Tôn Đản, phường 15 , Quận 4, thành phố Hồ Chí Minh",
   "Longtitude": 10.7540739,
   "Latitude": 106.705617
 },
 {
   "STT": 1306,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát & Nhi Khoa - BS. Nguyễn Kinh Doanh",
   "address": "154/55 Lê Quốc Hưng, phường 12, Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7627089,
   "Latitude": 106.705059
 },
 {
   "STT": 1307,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát & Nhi Khoa - BS. Nguyễn Văn Kỳ",
   "address": "59 Tân Lập 1, phường Hiệp Phú ,Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8480897,
   "Latitude": 106.7778153
 },
 {
   "STT": 1308,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát & Nhiễm Bệnh Nhiệt Đới - BS. Nguyễn Anh Vũ",
   "address": "556 Lê Văn Quới, phường Bình Hưng Hòa , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.776406,
   "Latitude": 106.6033957
 },
 {
   "STT": 1309,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát & Sản Phụ Khoa - BS. Nguyễn Thị Kim Oanh & BS. Nguyễn Việt Hường",
   "address": "445 Lý Thái Tổ, phường 9 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7678967,
   "Latitude": 106.6686511
 },
 {
   "STT": 1310,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát & Siêu Âm 768 Lê Đức Thọ",
   "address": "768 Lê Đức Thọ, phường 15 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8477685,
   "Latitude": 106.6684162
 },
 {
   "STT": 1311,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát & Tai Mũi Họng - BS. Phan Như Tuấn",
   "address": "29 Đường số 1, phường Bình Thuận , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.741282,
   "Latitude": 106.7260801
 },
 {
   "STT": 1312,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát & Tim Mạch - BS. Châu Văn Vinh",
   "address": "48 Lê Đức Thọ, phường 7 , Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8327766,
   "Latitude": 106.6825419
 },
 {
   "STT": 1313,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát & Tim Mạch - BS. Trương Minh Châu",
   "address": "194/4 Nguyễn Thị Đặng, phường Tân Thới Hiệp , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8571267,
   "Latitude": 106.6393314
 },
 {
   "STT": 1314,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Võ Thị Thu Hà",
   "address": "117 Văn Cao, phường Phú Thọ Hòa, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.788781,
   "Latitude": 106.622693
 },
 {
   "STT": 1315,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Đặng Ngọc Thành",
   "address": "2B Nguyễn Thiện Thuật, phường 2, Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7663714,
   "Latitude": 106.6821697
 },
 {
   "STT": 1316,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - Bác Sĩ Vương Hiền",
   "address": "135 Đặng Văn Ngữ, phường 14, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.79195,
   "Latitude": 106.667827
 },
 {
   "STT": 1317,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS Bùi Thị Mai Liên",
   "address": "115/22 Phạm Đình Hổ, phường 6 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.747371,
   "Latitude": 106.6496617
 },
 {
   "STT": 1318,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS Hà Quang Sự",
   "address": "149 Xóm Đất, phường 8 , Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.758679,
   "Latitude": 106.646083
 },
 {
   "STT": 1319,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS Hồ Đắc Duy",
   "address": "443/30 Lê Văn Sỹ, phường 12 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.788893,
   "Latitude": 106.674255
 },
 {
   "STT": 1320,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS Lê Nữ Hòa Hiệp",
   "address": "399/30 Nguyễn Đình Chiểu,phường 5 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7707391,
   "Latitude": 106.6838535
 },
 {
   "STT": 1321,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS Nguyễn Phú Tĩnh",
   "address": "327/38 Trần Bình Trọng, phường 4 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7624181,
   "Latitude": 106.6783887
 },
 {
   "STT": 1322,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS Nguyễn Thanh Sử",
   "address": "284 Đỗ Ngọc Thạnh, phường 4 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7578289,
   "Latitude": 106.656109
 },
 {
   "STT": 1323,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Quốc Hùng",
   "address": "9/6 Đường số 15, phường Tân Kiểng , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7450087,
   "Latitude": 106.7054093
 },
 {
   "STT": 1324,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Âu Thanh Tùng",
   "address": "20 Kỳ Đồng, phường 9 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.782461,
   "Latitude": 106.681851
 },
 {
   "STT": 1325,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Bùi Hữu Vinh & BS. Hoàng Văn Kỳ",
   "address": "54/1A Phạm Văn Chiêu, phường 8, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.844828,
   "Latitude": 106.642747
 },
 {
   "STT": 1326,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Bùi Mạnh Kính",
   "address": "59 Lê Văn Sỹ, phường 13 , Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.785649,
   "Latitude": 106.676723
 },
 {
   "STT": 1327,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Bùi Sơn Tùng",
   "address": "15 Đường số 12A, phường 6 ,Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.759834,
   "Latitude": 106.699418
 },
 {
   "STT": 1328,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Bùi Thị Vân Hương",
   "address": "53B Huỳnh Khương Ninh, phường Đa Kao , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.789596,
   "Latitude": 106.695982
 },
 {
   "STT": 1329,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Bùi Văn Cải",
   "address": "139 Phạm Văn Chí, phường 3 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.745411,
   "Latitude": 106.648048
 },
 {
   "STT": 1330,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Bùi Xuân Thái",
   "address": "1A 222/1 Vĩnh Lộc, Xã Phạm Văn Hai, huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.7722114,
   "Latitude": 106.5518355
 },
 {
   "STT": 1331,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Cảnh",
   "address": "11/10D1 Phạm Văn Chiêu, phường 14 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.85176,
   "Latitude": 106.6557629
 },
 {
   "STT": 1332,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Cao Hoài Tuấn Anh",
   "address": "A2/38A Quốc lộ 50, Xã Phong Phú, Huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.6995307,
   "Latitude": 106.6468148
 },
 {
   "STT": 1333,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Cao Thị Huỳnh Anh & BS. Hà Thanh Bình",
   "address": "587/2 Nhật Tảo, phường 7 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7650422,
   "Latitude": 106.6691161
 },
 {
   "STT": 1334,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Cao Văn Hội",
   "address": "2724 Phạm Thế Hiển, phường 7 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.720871,
   "Latitude": 106.6342967
 },
 {
   "STT": 1335,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Châu Hoàng Minh",
   "address": "C4/127/3 Ấp 3, Xã Bình Lợi , Huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.7535236,
   "Latitude": 106.4815716
 },
 {
   "STT": 1336,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Chu Thị Thanh Phương",
   "address": "2/10 Cao Thắng, phường 5 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.769215,
   "Latitude": 106.6841584
 },
 {
   "STT": 1337,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Cù Xuân Thi",
   "address": "125 Ngô Quyền, phường Hiệp Phú, Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8436917,
   "Latitude": 106.7774537
 },
 {
   "STT": 1338,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Cung Hồng Dũng",
   "address": "55 An Điềm, phường 10 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7514865,
   "Latitude": 106.6655496
 },
 {
   "STT": 1339,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Diệp Thị Thanh Bình",
   "address": "254/33 Cách Mạng Tháng Tám,phường 6 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7756424,
   "Latitude": 106.6847891
 },
 {
   "STT": 1340,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Dương Lệ Chi",
   "address": "143 Tuệ Tĩnh, phường 12 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.761213,
   "Latitude": 106.654141
 },
 {
   "STT": 1341,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Dương Tấn Chương",
   "address": "706 Lê Trọng Tấn, phường Bình Hưng Hòa, quận Bình Tân, thành phố Hồ Chí Minh",
   "Longtitude": 10.8091949,
   "Latitude": 106.6151707
 },
 {
   "STT": 1342,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Dương Thanh Trắc",
   "address": "216 Tân Khai, phường 4 , Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.757856,
   "Latitude": 106.653902
 },
 {
   "STT": 1343,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Dương Văn Hải & BS. Nguyễn Thị Lệ Nga",
   "address": "456 Nguyễn Đình Chiểu, phường 4, Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7725121,
   "Latitude": 106.6842591
 },
 {
   "STT": 1344,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Hà Thị Thiện",
   "address": "162 Đường số 11, phường Linh Xuân , quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8874961,
   "Latitude": 106.7683446
 },
 {
   "STT": 1345,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Hà Thu Thủy",
   "address": "406 Lê Văn Lương, Khu Phố 1,phường Tân Hưng , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7240971,
   "Latitude": 106.71222
 },
 {
   "STT": 1346,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Hà Văn Hải",
   "address": "100/21/28 Đinh Tiên Hoàng,phường 1 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.796644,
   "Latitude": 106.6969
 },
 {
   "STT": 1347,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Hà Vinh Khánh",
   "address": "14 Nguyễn Trãi, phường 3 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7587372,
   "Latitude": 106.6815124
 },
 {
   "STT": 1348,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Hải",
   "address": "1565 Huỳnh Tấn Phát, phường Phú Mỹ , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7018404,
   "Latitude": 106.7381763
 },
 {
   "STT": 1349,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Hồ Hữu Lộc",
   "address": "334C Hồng Bàng, phường 15 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7534785,
   "Latitude": 106.6516238
 },
 {
   "STT": 1350,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Hồ Hữu Triều",
   "address": "59C Bình Thới,, phường 14 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.767558,
   "Latitude": 106.650184
 },
 {
   "STT": 1351,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Hồ Huy Tuấn",
   "address": "113 Hùng Vương, phường 4 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7628276,
   "Latitude": 106.6778476
 },
 {
   "STT": 1352,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Hồ Tấn Phát",
   "address": "167 Võ Văn Tần, phường 5 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7739123,
   "Latitude": 106.6879785
 },
 {
   "STT": 1353,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Hồ Thị Liên Hoa",
   "address": "20 Đường 3 Tháng 2, phường 12 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7776375,
   "Latitude": 106.6809899
 },
 {
   "STT": 1354,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Hồ Văn Minh",
   "address": "67 Xóm Củi, phường 11 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7469238,
   "Latitude": 106.6606543
 },
 {
   "STT": 1355,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Hoàng Hồng Kiêm",
   "address": "B12A/6 Ấp 2, Xã Vĩnh Lộc B, huyện Bình Chánh, thành phố Hồ Chí Minh",
   "Longtitude": 10.79352,
   "Latitude": 106.5791
 },
 {
   "STT": 1356,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Hoàng Lan",
   "address": "270C Võ Thị Sáu, phường 7 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.780359,
   "Latitude": 106.6844825
 },
 {
   "STT": 1357,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Hoàng Văn Hưng",
   "address": "211 Phan Đăng Lưu, phường 1, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.79975,
   "Latitude": 106.681
 },
 {
   "STT": 1358,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Hoàng Đức Long",
   "address": "432 Huỳnh Tấn Phát, phường Bình Thuận , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7448831,
   "Latitude": 106.7292457
 },
 {
   "STT": 1359,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Hồng Tuấn An",
   "address": "150/9 Trần Tuấn Khải, phường 5 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7503674,
   "Latitude": 106.674342
 },
 {
   "STT": 1360,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Hùng",
   "address": "8 Nguyễn Công Trứ, phường 19, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.790538,
   "Latitude": 106.71002
 },
 {
   "STT": 1361,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Hương",
   "address": "234A Thành Công, phường Tân Thành, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.7944618,
   "Latitude": 106.6294597
 },
 {
   "STT": 1362,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Huỳnh Lê Phương",
   "address": "4 lô T Cư xá Vĩnh Hội, phường 6 ,Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7616455,
   "Latitude": 106.7004408
 },
 {
   "STT": 1363,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Huỳnh Minh Sang",
   "address": "257B Phan Văn Khỏe, phường 5 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.747282,
   "Latitude": 106.642281
 },
 {
   "STT": 1364,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Huỳnh Như Cừ",
   "address": "27 Bis Tôn Thất Tùng, phường Phạm Ngũ Lão , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7705568,
   "Latitude": 106.6877834
 },
 {
   "STT": 1365,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Huỳnh Phan Phúc Linh",
   "address": "175 Trần Quý, phường 4 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.75847,
   "Latitude": 106.654166
 },
 {
   "STT": 1366,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Huỳnh Thanh Long",
   "address": "22/40 Cư Xá Lữ Gia, phường 15 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.773658,
   "Latitude": 106.654738
 },
 {
   "STT": 1367,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Huỳnh Thị Mỹ Dung",
   "address": "451 Tỉnh Lộ 10, phường An Lạc, quận Bình Tân, thành phố Hồ Chí Minh",
   "Longtitude": 10.7571282,
   "Latitude": 106.6197111
 },
 {
   "STT": 1368,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Huỳnh Thị Tố Khanh",
   "address": "55 Đường 1011 Phạm Thế Hiển,phường 5 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7423209,
   "Latitude": 106.6657501
 },
 {
   "STT": 1369,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Huỳnh Thông Minh",
   "address": "A10/4 Khu phố 1, Thị trấn Tân Túc, huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.686546,
   "Latitude": 106.593148
 },
 {
   "STT": 1370,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Huỳnh Đình Đại",
   "address": "784 Nguyễn Chí Thanh, phường 4, Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.757279,
   "Latitude": 106.654883
 },
 {
   "STT": 1371,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Khổng Minh Sơn",
   "address": "57 Thạch Thị Thanh, phường Tân Định , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.790457,
   "Latitude": 106.692098
 },
 {
   "STT": 1372,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Khưu Minh Lợi",
   "address": "112 Đường Số 24B, phường Bình Trị Đông B , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7553781,
   "Latitude": 106.6111355
 },
 {
   "STT": 1373,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lại Phước Hòa",
   "address": "A12/26A khu phố 1, Thị trấn Tân Túc, huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.6844901,
   "Latitude": 106.5731392
 },
 {
   "STT": 1374,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lâm Thanh Bình",
   "address": "5 Trần Quang Khải, phường Tân Định , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7925143,
   "Latitude": 106.6956374
 },
 {
   "STT": 1375,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lâm Văn Hoàng",
   "address": "398 Cách Mạng Tháng Tám,phường 11 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7833679,
   "Latitude": 106.6717621
 },
 {
   "STT": 1376,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Anh Tú",
   "address": "009 Chung cư A2 Phan Xích Long,phường 7 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.7994135,
   "Latitude": 106.6866129
 },
 {
   "STT": 1377,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Công Tấn",
   "address": "86/3K Đường Tân Thới Hiệp 7,Khu phố 3, phường Tân Thới Hiệp ,Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.86551,
   "Latitude": 106.642211
 },
 {
   "STT": 1378,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Công Thành",
   "address": "B6/26 Quốc lộ 1A, Xã Bình Chánh, Huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.6630417,
   "Latitude": 106.5672462
 },
 {
   "STT": 1379,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Hoàng Ninh",
   "address": "313/12 Nuyễn Trãi, phường 12 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7536104,
   "Latitude": 106.6605672
 },
 {
   "STT": 1380,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Hồng Minh",
   "address": "183/2 Đường Số 9, phường 16, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8494393,
   "Latitude": 106.667243
 },
 {
   "STT": 1381,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Hữu Chí",
   "address": "24 Đặng Tất, phường Tân Định ,Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.792982,
   "Latitude": 106.688449
 },
 {
   "STT": 1382,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Minh Tuấn",
   "address": "275 Lạc Long Quân, phường 3 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.765093,
   "Latitude": 106.64213
 },
 {
   "STT": 1383,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Nguyễn Thùy Khanh",
   "address": "157 Đường 22, phường 11 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.742998,
   "Latitude": 106.6261063
 },
 {
   "STT": 1384,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Thanh Bình",
   "address": "151 Lê Thị Hồng Gấm, phường Cầu Ông Lãnh , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.767428,
   "Latitude": 106.696187
 },
 {
   "STT": 1385,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Thanh Sơn",
   "address": "290 Long Phước, phường Long Phước , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.810254,
   "Latitude": 106.8593788
 },
 {
   "STT": 1386,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Thị Kiến Trúc",
   "address": "11 Đường Số 7, phường 17, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.846029,
   "Latitude": 106.676999
 },
 {
   "STT": 1387,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Thị Kiều Oanh",
   "address": "E9/18 Khu phố 5, Thị trấn Tân Túc, Huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.6979001,
   "Latitude": 106.5944539
 },
 {
   "STT": 1388,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Thị Lan",
   "address": "12 Đường số 2, Khu phố 2, phường Tân Quy , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.743786,
   "Latitude": 106.7056292
 },
 {
   "STT": 1389,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Thị Liên",
   "address": "468/9 Nguyễn Văn Luông, phường 12 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.752243,
   "Latitude": 106.6357529
 },
 {
   "STT": 1390,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Thị Ngọc Linh",
   "address": "80B Bà Hom, phường 13 , Quận 6, thành phố Hồ Chí Minh",
   "Longtitude": 10.755181,
   "Latitude": 106.629496
 },
 {
   "STT": 1391,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Thị Thu Ba",
   "address": "31 Lô K, Cư xá Ngân Hàng, Khu phố 5, phường Tân Thuận Tây ,Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.750921,
   "Latitude": 106.717006
 },
 {
   "STT": 1392,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Thị Thủy",
   "address": "44A Thống Nhất, phường Tân Thành, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.791333,
   "Latitude": 106.631323
 },
 {
   "STT": 1393,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Thị Tuyết Phượng",
   "address": "91 Tân Vĩnh, phường 6 , Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.759624,
   "Latitude": 106.70013
 },
 {
   "STT": 1394,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Văn Châu",
   "address": "514/37 Lê Đức Thọ, phường 17, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.840142,
   "Latitude": 106.677475
 },
 {
   "STT": 1395,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Văn Phụng",
   "address": "B42 Nguyễn Thần Hiến, phường 18 , Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.756019,
   "Latitude": 106.716987
 },
 {
   "STT": 1396,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Văn Tuyến",
   "address": "5 Trần Chánh Chiếu, phường 14 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7519998,
   "Latitude": 106.653091
 },
 {
   "STT": 1397,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Đình Thông",
   "address": " 30 Phan Văn Hớn, phường Tân Thới Nhất , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8292885,
   "Latitude": 106.6143944
 },
 {
   "STT": 1398,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lưu Hoàng Hải",
   "address": "81 Đường 24, phường 11 , Quận 6, thành phố Hồ Chí Minh",
   "Longtitude": 10.7425124,
   "Latitude": 106.627543
 },
 {
   "STT": 1399,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lưu Hùng Vũ",
   "address": "193 Bà Hạt, phường 9 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7662152,
   "Latitude": 106.6702696
 },
 {
   "STT": 1400,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lưu Văn Ái",
   "address": "787 Lũy Bán Bích, phường Hòa Thạnh, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.7780282,
   "Latitude": 106.6349844
 },
 {
   "STT": 1401,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lý Quốc Thịnh",
   "address": "156A Đường 45, phường Tân Quy, Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.74252,
   "Latitude": 106.7119964
 },
 {
   "STT": 1402,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lý Thị Chi",
   "address": "467 Nguyễn Văn Tăng, phường Long Thạnh Mỹ , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8425732,
   "Latitude": 106.8282256
 },
 {
   "STT": 1403,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lý Vĩnh Minh",
   "address": "106 Lê Đại Hành, phường 7 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7601715,
   "Latitude": 106.6584968
 },
 {
   "STT": 1404,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Mạch Triều Hà",
   "address": "7 Học Lạc, phường 14 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7514629,
   "Latitude": 106.6543025
 },
 {
   "STT": 1405,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Mai Phước Vĩnh",
   "address": "242/97/11 Nguyễn Thiện Thuật,phường 3 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7710079,
   "Latitude": 106.6792992
 },
 {
   "STT": 1406,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Minh Đỉnh",
   "address": "30 Đường Số 28, phường Bình Trị Đông B , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.74504,
   "Latitude": 106.6123362
 },
 {
   "STT": 1407,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Mỹ Dung",
   "address": "382C Hưng Phú, phường 9 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.747551,
   "Latitude": 106.674357
 },
 {
   "STT": 1408,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Ngô Dương Huấn",
   "address": "529 Nguyễn Trãi, phường 7 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7542702,
   "Latitude": 106.6688469
 },
 {
   "STT": 1409,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Ngô Hùng Trí",
   "address": "141 Tân Hương, phường Tân Quý, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.7896461,
   "Latitude": 106.6244633
 },
 {
   "STT": 1410,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Ngô Thị Diệu Minh",
   "address": "89/7 Đoàn Văn Bơ, phường 12 ,Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764252,
   "Latitude": 106.703098
 },
 {
   "STT": 1411,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Ngô Thị Ngọc Nhung",
   "address": "300 Lạc Long Quân, phường 5 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.767027,
   "Latitude": 106.642852
 },
 {
   "STT": 1412,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Ngô Tú Loan",
   "address": "280 Khánh Hội, phường 5 , Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.760063,
   "Latitude": 106.698431
 },
 {
   "STT": 1413,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Ngọc Ánh",
   "address": "03 Lô A Chung Cư 1Ha, Đường H, phường An Phú , Quận 2 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7676187,
   "Latitude": 106.6785951
 },
 {
   "STT": 1414,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Bạch Nhân",
   "address": "67-69 Đường số 6, phường Bình Trị Đông B , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.749547,
   "Latitude": 106.618372
 },
 {
   "STT": 1415,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Bích Hà",
   "address": "525/15 Sư Vạn Hạnh, phường 12 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7717955,
   "Latitude": 106.6699903
 },
 {
   "STT": 1416,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Chí Đức",
   "address": "117 Đường Số 11, phường Phước Bình , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.813328,
   "Latitude": 106.7707658
 },
 {
   "STT": 1417,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Dũng Chí",
   "address": "98 Cao Văn Lầu, phường 2 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7496398,
   "Latitude": 106.6488819
 },
 {
   "STT": 1418,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Duy Hùng",
   "address": "387/18 Hàn Hải Nguyên, phường 2 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.757977,
   "Latitude": 106.642272
 },
 {
   "STT": 1419,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Duy Phúc",
   "address": "200B Hậu Giang, phường 6 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.749974,
   "Latitude": 106.64266
 },
 {
   "STT": 1420,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Hoài Nam",
   "address": "106B/90 Lạc Long Quân, phường 3 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.759623,
   "Latitude": 106.640079
 },
 {
   "STT": 1421,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Hồng Nam",
   "address": "810 Tỉnh lộ 10, phường Bình Trị Đông A , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7580312,
   "Latitude": 106.6088612
 },
 {
   "STT": 1422,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Hồng Thu",
   "address": "202 Nguyễn Cư Trinh, phường Nguyễn Cư Trinh , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7631858,
   "Latitude": 106.6878506
 },
 {
   "STT": 1423,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Hùng Minh",
   "address": "129 đường số 6, phường Bình Hưng Hòa , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.8043133,
   "Latitude": 106.6021018
 },
 {
   "STT": 1424,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Hữu Hiển",
   "address": "487/7 Huỳnh Tấn Phát, phường Phú Mỹ , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7176997,
   "Latitude": 106.7367246
 },
 {
   "STT": 1425,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Hữu Hoàng",
   "address": "575/3 Điện Biên Phủ, phường 1 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.769448,
   "Latitude": 106.6769605
 },
 {
   "STT": 1426,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Hữu Thơ",
   "address": "61/11 Lê Văn Lương, phường Tân Kiểng , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7491228,
   "Latitude": 106.7101958
 },
 {
   "STT": 1427,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Kỳ Chân",
   "address": "305 Hai Bà Trưng, phường 8 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.789261,
   "Latitude": 106.690001
 },
 {
   "STT": 1428,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Mạnh Hùng",
   "address": "70/1 Đường số 37, phường Tân Quy , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7443909,
   "Latitude": 106.7168559
 },
 {
   "STT": 1429,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Minh Tuấn",
   "address": "V6/50 Khu Định cư An Phú Tây,Xã An Phú Tây, huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.6839415,
   "Latitude": 106.6143376
 },
 {
   "STT": 1430,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Năng Xuyến",
   "address": "78/H4 Cộng Hòa, phường 4, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.801197,
   "Latitude": 106.65906
 },
 {
   "STT": 1431,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Ngọc Chúc",
   "address": "50 Nguyễn Thiện Thuật, phường 2, Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7670425,
   "Latitude": 106.6813126
 },
 {
   "STT": 1432,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Ngọc Khôi",
   "address": "21 Đào Tấn, phường 5 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7506638,
   "Latitude": 106.6753471
 },
 {
   "STT": 1433,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Ngọc Thắng",
   "address": "\n450 Nhật Tảo, phường 7, Quận 10, thành phố Hồ Chí Minh\n",
   "Longtitude": 10.7632918,
   "Latitude": 106.6613881
 },
 {
   "STT": 1434,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Phi Hùng",
   "address": "658/1 Cách Mạng Tháng Tám, phường 11, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.78665,
   "Latitude": 106.665497
 },
 {
   "STT": 1435,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Quang Dũng",
   "address": "19A Phạm Thế Hiển, phường 4 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.742834,
   "Latitude": 106.670585
 },
 {
   "STT": 1436,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Quang Khiên",
   "address": "29 Đường số 2, phường 6 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.737636,
   "Latitude": 106.6674583
 },
 {
   "STT": 1437,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Quốc Tuần",
   "address": "137/3 Bùi Hữu Nghĩa, phường 7 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7542704,
   "Latitude": 106.6748324
 },
 {
   "STT": 1438,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Quốc Việt",
   "address": "447 Nguyễn Chí Thanh, phường 15 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7563313,
   "Latitude": 106.6520601
 },
 {
   "STT": 1439,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Tân Phú",
   "address": "567 Kha Vạn Cân, phường Linh Đông , quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8466001,
   "Latitude": 106.7510763
 },
 {
   "STT": 1440,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Tấn Tài",
   "address": "102/6C Đường Tân Thới Hiệp 21,phường Tân Thới Hiệp , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8637171,
   "Latitude": 106.6393807
 },
 {
   "STT": 1441,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thái Phương",
   "address": "299 Trần Bình Trọng, phường 4 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7615432,
   "Latitude": 106.6793117
 },
 {
   "STT": 1442,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thanh Liêm",
   "address": "103 Bình Quới, phường 27 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.817678,
   "Latitude": 106.72129
 },
 {
   "STT": 1443,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thanh Sơn",
   "address": "35 Đường Số 144, phường Tân Phú , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8672689,
   "Latitude": 106.805861
 },
 {
   "STT": 1444,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thanh Sung",
   "address": "C6/23A Ấp 3, Xã Hưng Long, huyện Bình Chánh, thành phố Hồ Chí Minh",
   "Longtitude": 10.6629492,
   "Latitude": 106.6206998
 },
 {
   "STT": 1445,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thanh Tuấn",
   "address": "30A Lô D Cư xá Thanh Đa,phường 27 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8169965,
   "Latitude": 106.7230251
 },
 {
   "STT": 1446,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thế Hân",
   "address": "12A Lê Trọng Tấn, phường Tây Thạnh, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.80607,
   "Latitude": 106.630671
 },
 {
   "STT": 1447,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thị Ánh Hồng",
   "address": "488/15 Đường 3 Tháng 2, phường 14 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7654745,
   "Latitude": 106.6629695
 },
 {
   "STT": 1448,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thị Ánh Nguyệt",
   "address": "45 Nguyễn Văn Cừ, phường 1 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.755395,
   "Latitude": 106.6856453
 },
 {
   "STT": 1449,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thị Ánh Vân",
   "address": "88 Trần Bình Trọng, phường 1 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7548472,
   "Latitude": 106.6819396
 },
 {
   "STT": 1450,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thị Bích Liên",
   "address": "359 Cách Mạng Tháng Tám, phường 12, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7812564,
   "Latitude": 106.6752162
 },
 {
   "STT": 1451,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thị Dân An",
   "address": "C5/6 Ấp 3, Xã Tân Kiên, huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8181488,
   "Latitude": 106.5708708
 },
 {
   "STT": 1452,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thị Diệu Hiền",
   "address": "611/62 Điện Biên Phủ, phường 1 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7680074,
   "Latitude": 106.6778818
 },
 {
   "STT": 1453,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thị Kim Tùng",
   "address": "41 Âu Cơ, phường 14 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.769986,
   "Latitude": 106.651093
 },
 {
   "STT": 1454,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thị Ngọc Thúy",
   "address": "111A Đường Số 138, phường Tân Phú , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8627102,
   "Latitude": 106.8073223
 },
 {
   "STT": 1455,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thị Như Hà",
   "address": "148 Lạc Long Quân, phường 3, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.758896,
   "Latitude": 106.638601
 },
 {
   "STT": 1456,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thị Phương",
   "address": "A1/2B Ấp 1, Xã Vĩnh Lộc A, huyện Bình Chánh, thành phố Hồ Chí Minh",
   "Longtitude": 10.7939388,
   "Latitude": 106.5787705
 },
 {
   "STT": 1457,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thị Thu Thảo",
   "address": "458 Đoàn Văn Bơ, phường 14, Quận 4, thành phố Hồ Chí Minh",
   "Longtitude": 10.756729,
   "Latitude": 106.715298
 },
 {
   "STT": 1458,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thị Thu Vân",
   "address": "107 Lê Hồng Phong, phường 2, Quận 5, thành phố Hồ Chí Minh",
   "Longtitude": 10.7568123,
   "Latitude": 106.6781751
 },
 {
   "STT": 1459,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thị Tường Vy",
   "address": "249 Đường Liên khu 4-5, phường Bình Hưng Hòa , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7984152,
   "Latitude": 106.5949498
 },
 {
   "STT": 1460,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thị Tuyết",
   "address": "606/139 Đường 3 Tháng 2, phường 14, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7654721,
   "Latitude": 106.6629651
 },
 {
   "STT": 1461,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thiên Bình",
   "address": "120 Đường số 19, phường Bình Trị Đông B , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7532725,
   "Latitude": 106.6161533
 },
 {
   "STT": 1462,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thu Hương",
   "address": "4 Lê Ngô Cát, phường 7 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.776655,
   "Latitude": 106.685491
 },
 {
   "STT": 1463,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Tiến Dũng",
   "address": "80 Nguyễn Văn Tăng, phường Long Thạnh Mỹ, Quận 9, thành phố Hồ Chí Minh",
   "Longtitude": 10.847161,
   "Latitude": 106.8158316
 },
 {
   "STT": 1464,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Trọng Hoàng",
   "address": "13A1 Lê Thánh Tôn, phường Bến Nghé, Quận 1, thành phố Hồ Chí Minh",
   "Longtitude": 10.7814485,
   "Latitude": 106.7056668
 },
 {
   "STT": 1465,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Trung Thành",
   "address": "651/6 Điện Biên Phủ, phường 1 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7683729,
   "Latitude": 106.6753477
 },
 {
   "STT": 1466,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Trung Tín",
   "address": "161/136B Bùi Hữu Nghĩa, phường 7 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7547876,
   "Latitude": 106.6732432
 },
 {
   "STT": 1467,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Trường Sơn",
   "address": "Lô F chung cư Nguyễn Trãi, 014 Mặc Thiên Tích, phường 8 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7550995,
   "Latitude": 106.6672182
 },
 {
   "STT": 1468,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Tường Trình",
   "address": "100 Võ Trứ, phường 9, Quận 8, thành phố Hồ Chí Minh",
   "Longtitude": 10.746526,
   "Latitude": 106.672608
 },
 {
   "STT": 1469,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Văn Dũng",
   "address": "331/95 Nguyễn Thượng Hiền, phường 4, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.7752238,
   "Latitude": 106.6833234
 },
 {
   "STT": 1470,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Văn Khôi",
   "address": "59/9 Trường Sơn, phường 15, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7855554,
   "Latitude": 106.6663317
 },
 {
   "STT": 1471,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Văn Lơ",
   "address": "59 Bùi Thị Xuân, phường Bến Thành , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7708327,
   "Latitude": 106.6896174
 },
 {
   "STT": 1472,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Văn Phú",
   "address": "728/13/7 Trần Hưng Đạo, phường 2 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7551905,
   "Latitude": 106.6778778
 },
 {
   "STT": 1473,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Vạn Phước",
   "address": "39A Phú Thọ, phường 1 , Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.757407,
   "Latitude": 106.64165
 },
 {
   "STT": 1474,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Văn Tấn & Sản Phụ Khoa - Siêu Âm - BS. Phan Kim Chi",
   "address": "114 Huỳnh Tấn Phát, phường Tân Thuận Tây , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.753431,
   "Latitude": 106.726514
 },
 {
   "STT": 1475,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - Bs. Nguyễn Văn Đạo",
   "address": "789 Tỉnh Lộ 10, phường Bình Trị Đông B, quận Bình Tân, thành phố Hồ Chí Minh",
   "Longtitude": 10.7578384,
   "Latitude": 106.6091964
 },
 {
   "STT": 1476,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Viết Thịnh",
   "address": "410-412 Hai Bà Trưng, phường Tân Định, Quận 1, thành phố Hồ Chí Minh",
   "Longtitude": 10.7910588,
   "Latitude": 106.688054
 },
 {
   "STT": 1477,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Xuân Bích Huyên",
   "address": "42 Dạ Nam, phường 2 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.749299,
   "Latitude": 106.685571
 },
 {
   "STT": 1478,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Xuân Huân",
   "address": "93 Vườn Chuối, phường 4, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.7737183,
   "Latitude": 106.6831915
 },
 {
   "STT": 1479,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Xuân Hùng",
   "address": "226 Sư Vạn Hạnh, phường 2, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.761297,
   "Latitude": 106.673078
 },
 {
   "STT": 1480,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Xuân Phúc",
   "address": "D4/92 Ấp 4, Xã Phong Phú, huyện Bình Chánh, thành phố Hồ Chí Minh",
   "Longtitude": 10.6535608,
   "Latitude": 106.6953841
 },
 {
   "STT": 1481,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Xuân Quốc",
   "address": "2/2 Đỗ Xuân Hợp, phường Phước Bình, Quận 9, thành phố Hồ Chí Minh",
   "Longtitude": 10.8220541,
   "Latitude": 106.7704956
 },
 {
   "STT": 1482,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Xuân Quốc",
   "address": "84 Đường Số 2, phường Phước Bình, Quận 9, thành phố Hồ Chí Minh",
   "Longtitude": 10.8196239,
   "Latitude": 106.770968
 },
 {
   "STT": 1483,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Xuân Trình",
   "address": "32/91 Ông Ích Khiêm, phường 14, Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.767685,
   "Latitude": 106.646312
 },
 {
   "STT": 1484,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Xuân Vinh",
   "address": "1085 Huỳnh Tấn Phát, Khu phố 3, phường Phú Thuận, Quận 7, thành phố Hồ Chí Minh",
   "Longtitude": 10.7240867,
   "Latitude": 106.734858
 },
 {
   "STT": 1485,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Đăng Khoa",
   "address": "\n126 Nguyễn Đình Chính, phường 15, Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.795562,
   "Latitude": 106.678143
 },
 {
   "STT": 1486,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Đăng Tâm",
   "address": "74 Đỗ Ngọc Thạnh, phường 14, Quận 5, thành phố Hồ Chí Minh",
   "Longtitude": 10.7524911,
   "Latitude": 106.656224
 },
 {
   "STT": 1487,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Đỗ Thứ",
   "address": "14 Hồ Văn Tư, Khu phố 1, phường Trường Thọ, Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8447171,
   "Latitude": 106.7562578
 },
 {
   "STT": 1488,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Phạm Nguyễn Thành Thái",
   "address": "104 Thành Thái, phường 12, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7741524,
   "Latitude": 106.6648167
 },
 {
   "STT": 1489,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Phạm Thị Hồng Mai",
   "address": "341/80B Lạc Long Quân, phường 5, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.768533,
   "Latitude": 106.641597
 },
 {
   "STT": 1490,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Phạm Thị Thúy Liễu",
   "address": "Q6 Cư xá Vĩnh Hội - Nguyễn Hữu, phường 6, Quận 4, thành phố Hồ Chí Minh",
   "Longtitude": 10.7616455,
   "Latitude": 106.7004408
 },
 {
   "STT": 1491,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Phạm Thị Thủy Tiên",
   "address": "60 Huỳnh Thiện Lộc, phường Hòa Thạnh, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.778346,
   "Latitude": 106.636159
 },
 {
   "STT": 1492,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Phạm Trọng Lễ",
   "address": "71D Lạc Long Quân, phường 5, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.770261,
   "Latitude": 106.645736
 },
 {
   "STT": 1493,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Phạm Văn Thái",
   "address": "1503 Nguyễn Cửu Phú, Xã Tân Kiên, huyện Bình Chánh, thành phố Hồ Chí Minh",
   "Longtitude": 10.7165741,
   "Latitude": 106.5813709
 },
 {
   "STT": 1494,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Phạm Đình Hưng",
   "address": "E8/1A Vĩnh Lộc, Xã Vĩnh Lộc B, huyện Bình Chánh, thành phố Hồ Chí Minh",
   "Longtitude": 10.7722114,
   "Latitude": 106.5518355
 },
 {
   "STT": 1495,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Phan Quốc Vinh",
   "address": "284/3 Lê Văn Sỹ, phường 10, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.787276,
   "Latitude": 106.6799829
 },
 {
   "STT": 1496,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Phan Trung Hiếu",
   "address": "116B Tây Thạnh, phường Tây Thạnh, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.8135154,
   "Latitude": 106.6227488
 },
 {
   "STT": 1497,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Phan Đức Thiện",
   "address": "15 Nguyễn Huy Lượng, phường 11, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8057528,
   "Latitude": 106.6969621
 },
 {
   "STT": 1498,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Phó Trí Dũng",
   "address": "23 Yersin, phường Cầu Ông Lãnh ,Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.765632,
   "Latitude": 106.698165
 },
 {
   "STT": 1499,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Phùng Anh Đức",
   "address": "\n144 Bình Thới, phường 14, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.7670117,
   "Latitude": 106.6490346
 },
 {
   "STT": 1500,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Phương",
   "address": "621/49 Nguyễn Ảnh Thủ, phường Hiệp Thành, Quận 12, thành phố Hồ Chí Minh",
   "Longtitude": 10.8770352,
   "Latitude": 106.6423391
 },
 {
   "STT": 1501,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Quách Tuấn Kiệt",
   "address": "136 Nguyễn Chí Thanh, phường 3, Quận 5, thành phố Hồ Chí Minh",
   "Longtitude": 10.760687,
   "Latitude": 106.671837
 },
 {
   "STT": 1502,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Quản Thu Hương",
   "address": "87/42 Đinh Tiên Hoàng, phường 3, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.796959,
   "Latitude": 106.694496
 },
 {
   "STT": 1503,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Tân",
   "address": "51 - 53 Hoàng Hoa Thám, phường 6, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8058669,
   "Latitude": 106.690464
 },
 {
   "STT": 1504,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Tăng Thiện Quốc",
   "address": "C9/31A Ấp 3, Xã Vĩnh Lộc B, huyện Bình Chánh, thành phố Hồ Chí Minh",
   "Longtitude": 10.7939733,
   "Latitude": 106.5792914
 },
 {
   "STT": 1505,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Tăng Trần Khôi",
   "address": "258 Huỳnh Văn Bánh, phường 11, Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.7917788,
   "Latitude": 106.6748613
 },
 {
   "STT": 1506,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Thái Minh Sâm",
   "address": "530 Nguyễn Chí Thanh, phường 7, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7585112,
   "Latitude": 106.6613837
 },
 {
   "STT": 1507,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Thái Thị Hồng Ánh",
   "address": "725/2 Đường 3 Tháng 2, phường 6, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7657877,
   "Latitude": 106.6644714
 },
 {
   "STT": 1508,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Thái Thời Giai",
   "address": "833/8A Trần Hưng Đạo, phường 1, Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7543598,
   "Latitude": 106.6794608
 },
 {
   "STT": 1509,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Thái Tuấn Nguyên",
   "address": "B02 Cư xá Nguyễn Đình Chiểu, phường 4, Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.80461,
   "Latitude": 106.6797
 },
 {
   "STT": 1510,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Thanh",
   "address": "30 Đường số 1, phường Tân Kiểng, Quận 7, thành phố Hồ Chí Minh",
   "Longtitude": 10.7485546,
   "Latitude": 106.7083122
 },
 {
   "STT": 1511,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Tô Văn Hai",
   "address": "3/67 Cư Xá Lữ Gia, phường 15, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.7731617,
   "Latitude": 106.6554397
 },
 {
   "STT": 1512,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Tôn Thất Bửu & BS. Lương Thúy Yến",
   "address": "233/102D Nguyễn Trãi, phường 2, Quận 5, thành phố Hồ Chí Minh",
   "Longtitude": 10.7563322,
   "Latitude": 106.6772413
 },
 {
   "STT": 1513,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Anh Huy",
   "address": "553B1 Hậu Giang, phường 12, Quận 6, thành phố Hồ Chí Minh",
   "Longtitude": 10.7479694,
   "Latitude": 106.6352699
 },
 {
   "STT": 1514,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Chấn Thanh",
   "address": "201 Tạ Uyên, phường 4, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.758332,
   "Latitude": 106.653313
 },
 {
   "STT": 1515,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Hoàng Minh Châu",
   "address": "945 Lê Đức Thọ, phường 16, Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8495586,
   "Latitude": 106.6653768
 },
 {
   "STT": 1516,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Hoàng Tuấn",
   "address": "155 Bis Trần Quang Khải, phường Tân Định, Quận 1, thành phố Hồ Chí Minh",
   "Longtitude": 10.7920322,
   "Latitude": 106.6921942
 },
 {
   "STT": 1517,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Hồng Đào",
   "address": "205 Tân Kỳ Tân Quý, phường Tân Sơn Nhì, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.8030246,
   "Latitude": 106.6302162
 },
 {
   "STT": 1518,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Hữu Nghĩa",
   "address": "3 đường 24A, phường 11 , Quận 6, thành phố Hồ Chí Minh",
   "Longtitude": 10.7422773,
   "Latitude": 106.6232038
 },
 {
   "STT": 1519,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Mậu Kim",
   "address": "390A Hoàng Diệu, phường 5, Quận 4, thành phố Hồ Chí Minh",
   "Longtitude": 10.7599485,
   "Latitude": 106.6986432
 },
 {
   "STT": 1520,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Minh Giang",
   "address": "465 Đỗ Xuân Hợp, phường Phước Long B, Quận 9, thành phố Hồ Chí Minh",
   "Longtitude": 10.8147076,
   "Latitude": 106.7794935
 },
 {
   "STT": 1521,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Minh Kiệt",
   "address": "34A Văn Cao, phường Phú Thọ Hòa, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.784743,
   "Latitude": 106.6216029
 },
 {
   "STT": 1522,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Minh Đông",
   "address": "105 Tân Quý, phường Tân Quý, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.7910525,
   "Latitude": 106.6207914
 },
 {
   "STT": 1523,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Minh Đúng",
   "address": "856 Hương Lộ 2, phường Bình Trị Đông A , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7672905,
   "Latitude": 106.5984378
 },
 {
   "STT": 1524,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Ngọc Dũng",
   "address": "183 Lý Thái Tổ, phường 9, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.767601,
   "Latitude": 106.6734218
 },
 {
   "STT": 1525,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Ngọc Hữu Đức",
   "address": "29 Trần Hữu Đức, phường 1, Quận 5, thành phố Hồ Chí Minh",
   "Longtitude": 10.7530301,
   "Latitude": 106.6820301
 },
 {
   "STT": 1526,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Nguyễn Thu Thủy",
   "address": "242/1 Tôn Thất Thuyết, phường 3, Quận 4, thành phố Hồ Chí Minh",
   "Longtitude": 10.752926,
   "Latitude": 106.699744
 },
 {
   "STT": 1527,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Quang Nam",
   "address": "8 Phan Phú Tiên, phường 10, Quận 5, thành phố Hồ Chí Minh",
   "Longtitude": 10.7512972,
   "Latitude": 106.6657788
 },
 {
   "STT": 1528,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Quốc Hùng",
   "address": "1730 Phạm Thế Hiển, phường 6, Quận 8, thành phố Hồ Chí Minh",
   "Longtitude": 10.738368,
   "Latitude": 106.6518777
 },
 {
   "STT": 1529,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Quyết Tiến",
   "address": "179 Minh Phụng, phường 9, Quận 6, thành phố Hồ Chí Minh",
   "Longtitude": 10.7529075,
   "Latitude": 106.642824
 },
 {
   "STT": 1530,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Thị Tuyến",
   "address": "82 Hiệp Bình, phường Hiệp Bình Chánh, quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8407697,
   "Latitude": 106.7316371
 },
 {
   "STT": 1531,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Thúy Đào",
   "address": "302 Trần Hưng Đạo, phường 11, Quận 5, thành phố Hồ Chí Minh",
   "Longtitude": 10.7523027,
   "Latitude": 106.6626996
 },
 {
   "STT": 1532,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Trường Thành",
   "address": "130 Lương Nhữ Học, phường 11, Quận 5, thành phố Hồ Chí Minh",
   "Longtitude": 10.752382,
   "Latitude": 106.6601455
 },
 {
   "STT": 1533,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Văn Cang",
   "address": "13 (số cũ A19/1), Đường số 41, Khu phố 6, phường Bình Thuận, Quận 7, thành phố Hồ Chí Minh",
   "Longtitude": 10.743318,
   "Latitude": 106.716672
 },
 {
   "STT": 1534,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Văn Dũng",
   "address": "32 Nguyễn Văn Đừng, phường 6, Quận 5, thành phố Hồ Chí Minh",
   "Longtitude": 10.751771,
   "Latitude": 106.6712817
 },
 {
   "STT": 1535,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Văn Lộc",
   "address": "700 Đoàn Văn Bơ, phường 16, Quận 4, thành phố Hồ Chí Minh",
   "Longtitude": 10.7566941,
   "Latitude": 106.7148871
 },
 {
   "STT": 1536,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Văn Thạch",
   "address": "827 Hưng Phú, phường 10, Quận 8, thành phố Hồ Chí Minh",
   "Longtitude": 10.7455719,
   "Latitude": 106.6648979
 },
 {
   "STT": 1537,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trần Đình Phong",
   "address": "223 Lê Văn Sỹ, phường 13, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.786908,
   "Latitude": 106.679177
 },
 {
   "STT": 1538,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trịnh Anh Dũng",
   "address": "8 Trần Quang Khải, phường Tân Định, Quận 1, thành phố Hồ Chí Minh",
   "Longtitude": 10.791763,
   "Latitude": 106.688034
 },
 {
   "STT": 1539,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trịnh Công Hoan",
   "address": "468 Lạc Long Quân, phường 5, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.77052,
   "Latitude": 106.645299
 },
 {
   "STT": 1540,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trịnh Nhựt Toản",
   "address": "30/19 Nguyễn Đình Chi, phường 9, Quận 6, thành phố Hồ Chí Minh",
   "Longtitude": 10.7505344,
   "Latitude": 106.6419104
 },
 {
   "STT": 1541,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trịnh Đức Khương",
   "address": "59/320 Bông Sao, phường 5, Quận 8, thành phố Hồ Chí Minh",
   "Longtitude": 10.7348036,
   "Latitude": 106.6625211
 },
 {
   "STT": 1542,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trung",
   "address": "480 Quốc lộ 13, phường Hiệp Bình Phước, quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8501078,
   "Latitude": 106.7190778
 },
 {
   "STT": 1543,
   "Name": "hòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trương Công Nguyễn Bửu Phiệt",
   "address": "\n766B/21 Lạc Long Quân, phường 9, Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.778741,
   "Latitude": 106.649974
 },
 {
   "STT": 1544,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trương Nguyễn Duy Linh",
   "address": "D3/65 Ấp 4, Xã Phong Phú, huyện Bình Chánh, thành phố Hồ Chí Minh",
   "Longtitude": 10.659057,
   "Latitude": 106.6884199
 },
 {
   "STT": 1545,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trương Quang Hoành",
   "address": "45A Trần Quốc Toản, phường 8, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.7874192,
   "Latitude": 106.688301
 },
 {
   "STT": 1546,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trương Thị Mai Hương",
   "address": "138A Trần Phú, phường 4, Quận 5, thành phố Hồ Chí Minh",
   "Longtitude": 10.7610711,
   "Latitude": 106.6774979
 },
 {
   "STT": 1547,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Trương Đoàn Chí Trung",
   "address": "99A Hòa Hưng, phường 12 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7789735,
   "Latitude": 106.6740953
 },
 {
   "STT": 1548,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Ung Mạc Hy Minh",
   "address": "10/2 Đường số 5 Cư xá Bình Thới, phường 6, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.763022,
   "Latitude": 106.648792
 },
 {
   "STT": 1549,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Văng Công Chỉnh",
   "address": "214 Hà Tôn Quyền, phường 6 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.759274,
   "Latitude": 106.652806
 },
 {
   "STT": 1550,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Vi Kiến Cương",
   "address": "27D1 Âu Cơ, phường 14 , Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.770949,
   "Latitude": 106.649643
 },
 {
   "STT": 1551,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Vĩnh",
   "address": "246 Phạm Đăng Giảng, phường Bình Hưng Hòa, quận Bình Tân, thành phố Hồ Chí Minh",
   "Longtitude": 10.8123539,
   "Latitude": 106.6012074
 },
 {
   "STT": 1552,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Võ Công Khanh",
   "address": "109G Lò Siêu, phường 16, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.7562525,
   "Latitude": 106.6485212
 },
 {
   "STT": 1553,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Võ Dũng",
   "address": "24 Xóm Chiếu, phường 13, Quận 4, thành phố Hồ Chí Minh",
   "Longtitude": 10.7598509,
   "Latitude": 106.7114575
 },
 {
   "STT": 1554,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Võ Quang Đình Nam",
   "address": "835/67 Trần Hưng Đạo, phường 1, Quận 5, thành phố Hồ Chí Minh",
   "Longtitude": 10.7533241,
   "Latitude": 106.670429
 },
 {
   "STT": 1555,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Võ Tấn Kiệt",
   "address": "242A Gò Dầu, phường Tân Quý, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.7959144,
   "Latitude": 106.6186047
 },
 {
   "STT": 1556,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Võ Tấn Long & Chuyên Khoa Da Liễu - BS. Phạm Thị Tiếng",
   "address": "226 Bình Tiên, phường 4, Quận 6, thành phố Hồ Chí Minh",
   "Longtitude": 10.7446731,
   "Latitude": 106.6431257
 },
 {
   "STT": 1557,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Võ Thanh Phong",
   "address": "413 Hoàng Hữu Nam, phường Long Bình, Quận 9, thành phố Hồ Chí Minh",
   "Longtitude": 10.8740206,
   "Latitude": 106.8149668
 },
 {
   "STT": 1558,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Võ Văn Thuận",
   "address": "895 Lạc Long Quân, phường 11, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.785274,
   "Latitude": 106.650835
 },
 {
   "STT": 1559,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Vũ Bảo Ngọc",
   "address": " 198 - 200 Nguyễn Tất Thành, phường 13, Quận 4, thành phố Hồ Chí Minh",
   "Longtitude": 10.7624288,
   "Latitude": 106.708345
 },
 {
   "STT": 1560,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Vũ Mạnh Cường",
   "address": "009 Lô A Chung cư Tôn Thất Thuyết, phường 4, Quận 4, thành phố Hồ Chí Minh",
   "Longtitude": 10.7543788,
   "Latitude": 106.6919534
 },
 {
   "STT": 1561,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Vũ Quang Việt",
   "address": "248/8 Pasteur, phường 8, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.7862408,
   "Latitude": 106.6887649
 },
 {
   "STT": 1562,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Vũ Quý Đôn",
   "address": "9 Đường số 1, Khu phố 2, phường Phú Mỹ, Quận 7, thành phố Hồ Chí Minh",
   "Longtitude": 10.7164053,
   "Latitude": 106.733573
 },
 {
   "STT": 1563,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Vũ Thị Kim Thanh",
   "address": "60 Lê Đại Hành, phường 7, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.7592434,
   "Latitude": 106.6591066
 },
 {
   "STT": 1564,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Vũ Thị Mai",
   "address": "5M-45/3 Ấp 5, Xã Phạm Văn Hai, huyện Bình Chánh, thành phố Hồ Chí Minh",
   "Longtitude": 10.7761435,
   "Latitude": 106.542813
 },
 {
   "STT": 1565,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Vũ Tuấn Khanh",
   "address": "44/1 Tân Hải, phường 13, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.8006012,
   "Latitude": 106.6397134
 },
 {
   "STT": 1566,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Vũ Văn Mạnh",
   "address": "130/44 Lê Quốc Hưng, phường 12, Quận 4, thành phố Hồ Chí Minh",
   "Longtitude": 10.76309,
   "Latitude": 106.704594
 },
 {
   "STT": 1567,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Vũ Văn Đăng",
   "address": "185D Trần Hưng Đạo, phường Cô Giang, Quận 1, thành phố Hồ Chí Minh",
   "Longtitude": 10.76578,
   "Latitude": 106.69417
 },
 {
   "STT": 1568,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Vương Anh Đào",
   "address": "300 Điện Biên Phủ, phường 7, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.776986,
   "Latitude": 106.683764
 },
 {
   "STT": 1569,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Vương Kim Chi",
   "address": "53B Trần Đình Xu, phường Cầu Kho, Quận 1, thành phố Hồ Chí Minh",
   "Longtitude": 10.7607224,
   "Latitude": 106.6909381
 },
 {
   "STT": 1570,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Đặng Mỹ Hương",
   "address": "635/3 Nguyễn Đình Chiểu, phường 2, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.7666303,
   "Latitude": 106.6791431
 },
 {
   "STT": 1571,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Đặng Quang Vinh",
   "address": "606/18 Đường 3 Tháng 2, phường 14, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7654745,
   "Latitude": 106.6629694
 },
 {
   "STT": 1572,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Đào Duy Khanh",
   "address": "1395 Ba Tháng Hai, phường 16, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.7573515,
   "Latitude": 106.6481758
 },
 {
   "STT": 1573,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Đinh Quang Thiện",
   "address": "142 Phùng Văn Cung, phường 4, Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.805668,
   "Latitude": 106.6809289
 },
 {
   "STT": 1574,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Đinh Văn Định",
   "address": "Kios 07 Khu D chợ Hưng Long, Xã Hưng Long, huyện Bình Chánh, thành phố Hồ Chí Minh",
   "Longtitude": 10.6579417,
   "Latitude": 106.6081706
 },
 {
   "STT": 1575,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Đinh Xuân Bơ",
   "address": "186C Đường số 8, Khu phố 3, phường Linh Xuân, Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8775058,
   "Latitude": 106.7767123
 },
 {
   "STT": 1576,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Đỗ Thị Diễm Thúy",
   "address": "16/33 Nguyễn Thiện Thuật, phường 2, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.7672508,
   "Latitude": 106.6821822
 },
 {
   "STT": 1577,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Đỗ Thị Đủ",
   "address": "Số 1 Đường số 14 , Khu phố 6,phường Bình Chiểu , Thủ Đức , thành phố Hồ Chí Minh",
   "Longtitude": 10.8824494,
   "Latitude": 106.7296859
 },
 {
   "STT": 1578,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Đỗ Trọng Hải",
   "address": "112 Nguyễn Lâm, phường 6, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7651443,
   "Latitude": 106.6630841
 },
 {
   "STT": 1579,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Đỗ Vũ Phương",
   "address": "167 Cô Giang, phường Cô Giang, Quận 1, thành phố Hồ Chí Minh",
   "Longtitude": 10.761903,
   "Latitude": 106.693948
 },
 {
   "STT": 1580,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Đoàn Lê Minh Hạnh",
   "address": "35 Đường Số 11, KDC Khang Điền, phường Phước Long B, Quận 9, thành phố Hồ Chí Minh",
   "Longtitude": 10.8202219,
   "Latitude": 106.7788125
 },
 {
   "STT": 1581,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Đông",
   "address": "27B Vĩnh Hội, phường 4, Quận 4, thành phố Hồ Chí Minh",
   "Longtitude": 10.756198,
   "Latitude": 106.702972
 },
 {
   "STT": 1582,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - Bs.Lê Hùng",
   "address": "69 Đường số 8, phường Trường Thọ, quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8444915,
   "Latitude": 106.7566663
 },
 {
   "STT": 1583,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - Bs.Lê Thanh Sơn",
   "address": "118 Hồ Văn Tư, phường Trường Thọ, quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8467232,
   "Latitude": 106.7555855
 },
 {
   "STT": 1584,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - CK Da Liễu - BS Trần Quang Bính",
   "address": "79 Trần Văn Đang, phường 9, Quận 3, thành phố Hồ Chí Minh",
   "Longtitude": 10.78349,
   "Latitude": 106.676277
 },
 {
   "STT": 1585,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - Tim Mạch & Chuyên Khoa Ngoại Chấn Thương Chỉnh Hình 139",
   "address": "\n139 Thành Thái, phường 14, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7748255,
   "Latitude": 106.664143
 },
 {
   "STT": 1586,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - Tim Mạch - BS. Hà Công Quỳnh",
   "address": "398 Hàn Hải Nguyên, phường 1, Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.758694,
   "Latitude": 106.64088
 },
 {
   "STT": 1587,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - Tim Mạch - BS. Võ Ánh Thái Thuận & Chuyên Khoa Nhi - BS. Vương Ngọc Thiên Thanh",
   "address": "300/7 Trịnh Đình Trọng, phường Hòa Thạnh, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.776695,
   "Latitude": 106.637964
 },
 {
   "STT": 1588,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát Bình Điền",
   "address": "E9/16 Nguyễn Hữu Trí, Thị trấn Tân Túc, huyện Bình Chánh, thành phố Hồ Chí Minh",
   "Longtitude": 10.696357,
   "Latitude": 106.5933293
 },
 {
   "STT": 1589,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát Bs Long - Hiếu",
   "address": "E7/33H Đường Trần Đại Nghĩa, Xã Lê Minh Xuân, huyện Bình Chánh, thành phố Hồ Chí Minh",
   "Longtitude": 10.7418662,
   "Latitude": 106.542358
 },
 {
   "STT": 1590,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát Hùng Thịnh",
   "address": "68A Trương Phước Phan, phường Bình Trị Đông, quận Bình Tân, thành phố Hồ Chí Minh",
   "Longtitude": 10.7703452,
   "Latitude": 106.6137496
 },
 {
   "STT": 1591,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát Phương Nam",
   "address": "408 Rừng Sác, Xã Bình Khánh, huyện Cần Giờ, thành phố Hồ Chí Minh\n",
   "Longtitude": 10.635576,
   "Latitude": 106.7925
 },
 {
   "STT": 1592,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát Thái Hòa",
   "address": "1503 Nguyễn Cửu Phú, Xã Tân Kiên, huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.7165741,
   "Latitude": 106.5813709
 },
 {
   "STT": 1593,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát Và Siêu Âm - BS. Thanh Liêm",
   "address": "103 Bình Quới, phường 27, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.817678,
   "Latitude": 106.72129
 },
 {
   "STT": 1594,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát Và Thận",
   "address": "488/15 Ba Tháng Hai, phường 14, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7654745,
   "Latitude": 106.6629695
 },
 {
   "STT": 1595,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Lê Đình Sơn",
   "address": "55 Đường số 5, phường Linh Chiểu, quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8524464,
   "Latitude": 106.7697218
 },
 {
   "STT": 1596,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Liên Nhựt",
   "address": "835/30 Trần Hưng Đạo, phường 1, Quận 5, thành phố Hồ Chí Minh",
   "Longtitude": 10.7543417,
   "Latitude": 106.680392
 },
 {
   "STT": 1597,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Ngọc Quang",
   "address": "60 Dân Chủ, phường Bình Thọ, quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.849242,
   "Latitude": 106.7672
 },
 {
   "STT": 1598,
   "Name": "Phòng Khám Chuyên Khoa Nội Xương Khớp - BS.Hoàng Mạnh Cường",
   "address": "11TER Trần Phú, phường 4, Quận 5, thành phố Hồ Chí Minh",
   "Longtitude": 10.7637987,
   "Latitude": 106.680065
 },
 {
   "STT": 1599,
   "Name": "Phòng Khám Chuyên Khoa Nội Đan Viên",
   "address": "1244A Huỳnh Tấn Phát, phường Tân Phú, Quận 7, thành phố Hồ Chí Minh",
   "Longtitude": 10.7178072,
   "Latitude": 106.7367375
 },
 {
   "STT": 1600,
   "Name": "Phòng Khám Chuyên Khoa Nội Nhi - BS. Phạm Thị Minh Rạng",
   "address": "123 Trịnh Quang Nghị, phường 7, Quận 8, thành phố Hồ Chí Minh",
   "Longtitude": 10.707405,
   "Latitude": 106.6205361
 },
 {
   "STT": 1601,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thiên Hương",
   "address": "96 Trần Nguyên Hãn, phường 13, Quận 8, thành phố Hồ Chí Minh",
   "Longtitude": 10.745646,
   "Latitude": 106.658416
 },
 {
   "STT": 1602,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Phạm Ngọc Giao",
   "address": "70 Trần Nguyên Hãn, phường 13, Quận 8, thành phố Hồ Chí Minh",
   "Longtitude": 10.746071,
   "Latitude": 106.658226
 },
 {
   "STT": 1603,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Cao Ngọc Nga",
   "address": "282 Đoàn Văn Bơ, phường 10, Quận 4, thành phố Hồ Chí Minh",
   "Longtitude": 10.761784,
   "Latitude": 106.706162
 },
 {
   "STT": 1604,
   "Name": "Phòng Khám Chuyên Khoa Nội Tổng Quát - BS. Nguyễn Thị Dung",
   "address": "473 Đoàn Văn Bơ, phường 13 ,Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7587188,
   "Latitude": 106.7115958
 },
 {
   "STT": 1605,
   "Name": "Phòng Khám Chuyên Khoa Phẫu Thuật Thẩm Mỹ",
   "address": "181/29 Ba Tháng Hai, phường 11, Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7654743,
   "Latitude": 106.662969
 },
 {
   "STT": 1606,
   "Name": "Phòng Khám Chuyên Khoa Phẫu Thuật Thẩm Mỹ - BS. Bùi Xuân Trường",
   "address": "47 Lương Hữu Khánh, phường Phạm Ngũ Lão , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7692213,
   "Latitude": 106.6867393
 },
 {
   "STT": 1607,
   "Name": "Phòng Khám Chuyên Khoa Phẫu Thuật Thẩm Mỹ - BS. Lê Nguyễn Diên Minh",
   "address": "141 Trần Huy Liệu, phường 8, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.795844,
   "Latitude": 106.677604
 },
 {
   "STT": 1608,
   "Name": "Phòng Khám Chuyên Khoa Phẫu Thuật Thẩm Mỹ - BS. Tống Xuân Vũ",
   "address": "296 Ngô Quyền, phường 8 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7654368,
   "Latitude": 106.6643049
 },
 {
   "STT": 1609,
   "Name": "Phòng Khám Chuyên Khoa Phẫu Thuật Thẩm Mỹ BS. Phan Quốc Vinh",
   "address": "787 Lê Hồng Phong (nối dài),phường 12 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7726266,
   "Latitude": 106.6720859
 },
 {
   "STT": 1610,
   "Name": "Phòng Khám Chuyên Khoa Phổi & Chuyên Khoa Tim - BS. Trương Thanh Thiết",
   "address": "322/15 An Dương Vương, phường 4 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.760265,
   "Latitude": 106.6793205
 },
 {
   "STT": 1611,
   "Name": "Phòng Khám Chuyên Khoa Phổi - BS. Hồng Hạnh",
   "address": "210 Phan Văn Trị, phường 12, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.814323,
   "Latitude": 106.695392
 },
 {
   "STT": 1612,
   "Name": "Phòng Khám Chuyên Khoa Phụ Sản - BS Nguyễn Thị Ngọc Điểm",
   "address": "181 Bình Phú, phường 11 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7446023,
   "Latitude": 106.630595
 },
 {
   "STT": 1613,
   "Name": "Phòng Khám Chuyên Khoa Quỳnh Trâm",
   "address": "116 Lê Văn Thịnh, phường Bình Trưng Tây , Quận 2 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7836543,
   "Latitude": 106.7690294
 },
 {
   "STT": 1614,
   "Name": "Phòng Khám Chuyên Khoa Răng Châu Âu",
   "address": "17A Lê Văn Miến, phường Thảo Điền , Quận 2 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8050318,
   "Latitude": 106.7323753
 },
 {
   "STT": 1615,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt",
   "address": "544 Minh Phụng, phường 10 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7627693,
   "Latitude": 106.6448343
 },
 {
   "STT": 1616,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt",
   "address": "53 Lãnh Binh Thăng, phường 12 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.762243,
   "Latitude": 106.6552142
 },
 {
   "STT": 1617,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt",
   "address": "18 Đường số 26, phường Bình Thọ, quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8337775,
   "Latitude": 106.7292448
 },
 {
   "STT": 1618,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt",
   "address": "254 Phan Văn Hân, phường 17, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.7945762,
   "Latitude": 106.7022317
 },
 {
   "STT": 1619,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt",
   "address": "823 Lũy Bán Bích, phường Tân Thành, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.787747,
   "Latitude": 106.6368344
 },
 {
   "STT": 1620,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt",
   "address": "319 Lê Đại Hành, phường 13 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.765207,
   "Latitude": 106.654633
 },
 {
   "STT": 1621,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt",
   "address": "77 Xóm Đất, phường 8 , Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.758507,
   "Latitude": 106.647574
 },
 {
   "STT": 1622,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt",
   "address": "21A Xóm Đất, phường 8 , Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.758441,
   "Latitude": 106.649241
 },
 {
   "STT": 1623,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt",
   "address": "93/3 Nơ Trang Long, phường 11, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.81736,
   "Latitude": 106.698547
 },
 {
   "STT": 1624,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt",
   "address": "168 Phó Cơ Điều, phường 6 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.760344,
   "Latitude": 106.6572489
 },
 {
   "STT": 1625,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt",
   "address": "69 Liên Tỉnh 5, phường 5 , Quận 8, thành phố Hồ Chí Minh",
   "Longtitude": 10.7361554,
   "Latitude": 106.6567027
 },
 {
   "STT": 1626,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt",
   "address": "18 Đường Số 1, Khu nhà Hiệp Ân,phường 5 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7370366,
   "Latitude": 106.6664873
 },
 {
   "STT": 1627,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt & Nội Tổng Quát",
   "address": "48-50 Bùi Hữu Nghĩa, phường 5 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7530065,
   "Latitude": 106.6751107
 },
 {
   "STT": 1628,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt & Nội Tổng Quát - BS Nghĩa BS Hoa",
   "address": "120F Trần Bình Trọng, phường 2 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7577143,
   "Latitude": 106.6809087
 },
 {
   "STT": 1629,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Bác Sĩ Võ Đắc Tuyến",
   "address": "449/112B Sư Vạn Hạnh, phường 12 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7721451,
   "Latitude": 106.6696547
 },
 {
   "STT": 1630,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS Hoàng Lê Bích Thuận",
   "address": "1/1 Trần Nhật Duật, phường Tân Định , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.792136,
   "Latitude": 106.690046
 },
 {
   "STT": 1631,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS Lâm Phi Long",
   "address": "159A Nguyễn Thị Định, phường An Phú , Quận 2 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7897465,
   "Latitude": 106.7546156
 },
 {
   "STT": 1632,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS Ngọc Cẩm",
   "address": "167/6 Nguyễn Biểu, phường 2 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7571219,
   "Latitude": 106.6834849
 },
 {
   "STT": 1633,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS Nguyễn Thị Kính",
   "address": "2A Trần Quang Diệu, phường 13 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.787417,
   "Latitude": 106.678145
 },
 {
   "STT": 1634,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS Phạm Thị Tuyết",
   "address": "560 Phạm Văn Chí, phường 8 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.741623,
   "Latitude": 106.640608
 },
 {
   "STT": 1635,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS Quách Kim Nguyên",
   "address": "60 Bàn Cờ, phường 3 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.770424,
   "Latitude": 106.6797974
 },
 {
   "STT": 1636,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS Thu",
   "address": "195 Khánh Hội, phường 3 , Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7574729,
   "Latitude": 106.699932
 },
 {
   "STT": 1637,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Bạch Ngọc",
   "address": "143/4 An Bình, phường 6 , Quận 5, thành phố Hồ Chí Minh",
   "Longtitude": 10.7506695,
   "Latitude": 106.6696593
 },
 {
   "STT": 1638,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Bạch Thị Minh Thu",
   "address": "611/1H Điện Biên Phủ, phường 1 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7694696,
   "Latitude": 106.6761603
 },
 {
   "STT": 1639,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Bích Ngọc",
   "address": "8 Lô B7 Cư xá 304 Điện Biên Phủ,phường 25 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8015835,
   "Latitude": 106.7121796
 },
 {
   "STT": 1640,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Bùi Mỹ Sơn",
   "address": "52/17 Cư xá Lữ Gia, phường 15 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.772445,
   "Latitude": 106.654287
 },
 {
   "STT": 1641,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Bùi Quế Dương",
   "address": "55/17 Trần Đình Xu, phường Cầu Kho , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.760271,
   "Latitude": 106.6904296
 },
 {
   "STT": 1642,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Bùi Thị Kim Liên",
   "address": "329/5 Điện Biên Phủ, phường 4 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7752831,
   "Latitude": 106.6831531
 },
 {
   "STT": 1643,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Bùi Thị Loan Chi",
   "address": "126 Đường DC11, phường Sơn Kỳ, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.8093018,
   "Latitude": 106.6094792
 },
 {
   "STT": 1644,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Bùi Thị Xuân Phương",
   "address": "572 Nguyễn Trãi, phường 8 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7550584,
   "Latitude": 106.6681117
 },
 {
   "STT": 1645,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Cao Minh Nhựt",
   "address": "A4/38B Ấp 1, Xã Vĩnh Lộc B, huyện Bình Chánh, thành phố Hồ Chí Minh",
   "Longtitude": 10.7513089,
   "Latitude": 106.5871589
 },
 {
   "STT": 1646,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Dương Thị Hoài Xuân",
   "address": "6B Phan Kế Bính, phường Đa Kao, Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.790118,
   "Latitude": 106.699714
 },
 {
   "STT": 1647,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Hà Thị Điều",
   "address": "634/9 Phạm Văn Chí, phường 8 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.741365,
   "Latitude": 106.6384829
 },
 {
   "STT": 1648,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Hoàng Anh",
   "address": "796 Hưng Phú, phường 10 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.745608,
   "Latitude": 106.664944
 },
 {
   "STT": 1649,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Hoàng Tử Hùng",
   "address": "349/129 Lê Đại Hành, phường 15, Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.765785,
   "Latitude": 106.6534549
 },
 {
   "STT": 1650,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Huỳnh Anh Khương",
   "address": "38 Nguyễn Văn Đừng, phường 6 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.751857,
   "Latitude": 106.6712894
 },
 {
   "STT": 1651,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Huỳnh Hữu Thục Hiền",
   "address": "453/1 Nguyễn Đình Chiểu, phường 5 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7704114,
   "Latitude": 106.6827966
 },
 {
   "STT": 1652,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Huỳnh Thế Hòa",
   "address": "67 Bàn Cờ, phường 3 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7703711,
   "Latitude": 106.6795531
 },
 {
   "STT": 1653,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Huỳnh Thị Hoa",
   "address": "568 Điện Biên Phủ, phường 11 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7716111,
   "Latitude": 106.6775488
 },
 {
   "STT": 1654,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Huỳnh Thị Hồng",
   "address": "162 Nguyễn Phúc Nguyên, phường 9 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7805813,
   "Latitude": 106.6793592
 },
 {
   "STT": 1655,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Huỳnh Văn Tùng",
   "address": "123/15 Bông Sao, phường 5 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.741261,
   "Latitude": 106.661548
 },
 {
   "STT": 1656,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Khanh",
   "address": "643 Phan Văn Trị, phường 7 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8268806,
   "Latitude": 106.68838
 },
 {
   "STT": 1657,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Kiều Chinh",
   "address": "120 Nguyễn Duy Dương, phường 9 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7598096,
   "Latitude": 106.6714224
 },
 {
   "STT": 1658,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Kim Hoa",
   "address": "2874B Phạm Thế Hiển, phường 7 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.715384,
   "Latitude": 106.631176
 },
 {
   "STT": 1659,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Lâm Thị Yến Hương",
   "address": "351/16 Lê Đại Hành, phường 11 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.765868,
   "Latitude": 106.651121
 },
 {
   "STT": 1660,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Lê Huỳnh Thiên Ân",
   "address": "71/1 Hoàng Hoa Thám, phường 6, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.806257,
   "Latitude": 106.690081
 },
 {
   "STT": 1661,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Lê Quốc Dũng",
   "address": "137/18 Thoại Ngọc Hầu, phường Phú Thạnh, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.780203,
   "Latitude": 106.634921
 },
 {
   "STT": 1662,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Lê Thành Kiệt",
   "address": "357 Xô Viết Nghệ Tĩnh, phường 24, quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.804149,
   "Latitude": 106.711316
 },
 {
   "STT": 1663,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Lê Thị Cẩm Trúc",
   "address": "443 Đỗ Xuân Hợp, phường Phước Long B , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.81834,
   "Latitude": 106.7732
 },
 {
   "STT": 1664,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Lê Thị Hồng Nga",
   "address": "266 Hoà Hảo, phường 4 , Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7623339,
   "Latitude": 106.669922
 },
 {
   "STT": 1665,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Lê Thu Hương",
   "address": "71B Nguyễn Khắc Nhu, phường Cô Giang , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.763696,
   "Latitude": 106.692847
 },
 {
   "STT": 1666,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Lê Trung Chánh",
   "address": "531 Trần Hưng Đạo, phường Cầu Kho , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.757459,
   "Latitude": 106.6864679
 },
 {
   "STT": 1667,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Lê Văn Trang",
   "address": "110 Lê Thị Hồng, phường 17 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8356359,
   "Latitude": 106.6797869
 },
 {
   "STT": 1668,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Liên",
   "address": "362/7 Nguyễn Đình Chiểu, phường 4 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7741252,
   "Latitude": 106.6855133
 },
 {
   "STT": 1669,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Mai Nguyệt Thu Hương",
   "address": "243 Đặng Nguyên Cẩn, phường 14, Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.75707,
   "Latitude": 106.634537
 },
 {
   "STT": 1670,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Mai Thanh Quang",
   "address": "979 Quốc Lộ 22, Tổ 1, Ấp Bàu Tre, Xã Tân An Hội , huyện Củ Chi, thành phố Hồ Chí Minh",
   "Longtitude": 10.9840731,
   "Latitude": 106.4716425
 },
 {
   "STT": 1671,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Ngô Phùng Thanh",
   "address": "384/1 Lê Văn Sỹ, phường 14 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.788204,
   "Latitude": 106.677773
 },
 {
   "STT": 1672,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Ngô Quang Hào",
   "address": "59/6 Huỳnh Tấn Phát, Thị trấn Nhà Bè , huyện Nhà Bè , thành phố Hồ Chí Minh",
   "Longtitude": 10.700807,
   "Latitude": 106.738144
 },
 {
   "STT": 1673,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn An Phương Nguyên",
   "address": "13 Đường Số 18, phường Phước Bình , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.813602,
   "Latitude": 106.771231
 },
 {
   "STT": 1674,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Công Chánh",
   "address": "492/1 Minh Phụng, phường 9 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.761927,
   "Latitude": 106.645251
 },
 {
   "STT": 1675,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Duy Đăng",
   "address": "306 Trần Hưng Đạo, phường Nguyễn Cư Trinh , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.757426,
   "Latitude": 106.68566
 },
 {
   "STT": 1676,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Hiếu Hạnh",
   "address": "137 Phó Cơ Điều, phường 6 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.760315,
   "Latitude": 106.656937
 },
 {
   "STT": 1677,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Hoàng Long",
   "address": "441/5B Nguyễn Đình Chiểu,phường 3 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7705703,
   "Latitude": 106.6830698
 },
 {
   "STT": 1678,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Hùng Vỹ",
   "address": "Nhà Nghỉ Công Đoàn Thanh Đa, Lô V, phường 27 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.818553,
   "Latitude": 106.718595
 },
 {
   "STT": 1679,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Hữu Tiến",
   "address": "25/16 Cống Quỳnh, phường Nguyễn Cư Trinh , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.765896,
   "Latitude": 106.688813
 },
 {
   "STT": 1680,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Mỹ Hương",
   "address": "132 Đường Số 8, phường 11, Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8420172,
   "Latitude": 106.662323
 },
 {
   "STT": 1681,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Phúc Diên Thảo",
   "address": "81 Cao Triều Phát, phường Tân Phong , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7318473,
   "Latitude": 106.7072612
 },
 {
   "STT": 1682,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Quốc Khánh",
   "address": "C5/13B2 Ấp 4A, Xã Bình Hưng, huyện Bình Chánh, thành phố Hồ Chí Minh",
   "Longtitude": 10.7357646,
   "Latitude": 106.6744549
 },
 {
   "STT": 1683,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Quốc Khánh",
   "address": "28/41 Cư Xá Lữ Gia, phường 15 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7733403,
   "Latitude": 106.6559668
 },
 {
   "STT": 1684,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Thị Hồng Châu",
   "address": "8 Nguyễn Phúc Nguyên, phường 9, Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7783423,
   "Latitude": 106.6814231
 },
 {
   "STT": 1685,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Thị Kim Nguyên",
   "address": "361 Quốc lộ 1A, phường An Phú Đông , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8596614,
   "Latitude": 106.7057733
 },
 {
   "STT": 1686,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Thị Lan Chi",
   "address": "C13 Tô Ký, phường Đông Hưng Thuận , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8433839,
   "Latitude": 106.630604
 },
 {
   "STT": 1687,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Thị Lệ Thủy",
   "address": "95 Liên Tỉnh 5, phường 5 , Quận 8, thành phố Hồ Chí Minh",
   "Longtitude": 10.738591,
   "Latitude": 106.656448
 },
 {
   "STT": 1688,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Thị Mỹ An",
   "address": "318C Nguyễn Thiện Thuật,phường 3 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7704131,
   "Latitude": 106.6772584
 },
 {
   "STT": 1689,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Thị Song Hà",
   "address": "D7/25 Nguyễn Thị Tú, Ấp 4, Xã Vĩnh Lộc B , huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8163162,
   "Latitude": 106.6002359
 },
 {
   "STT": 1690,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Thị Thanh Trúc",
   "address": "90B/11A Lý Thường Kiệt, phường 14 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7704897,
   "Latitude": 106.6580778
 },
 {
   "STT": 1691,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Thị Thủy",
   "address": "A8/5 khu phố 1, Thị trấn Tân Túc, Huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.674278,
   "Latitude": 106.580551
 },
 {
   "STT": 1692,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Thị Trúc Thanh",
   "address": "282 Nguyễn Văn Nghi, phường 7, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.82562,
   "Latitude": 106.6846
 },
 {
   "STT": 1693,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Thị Tuyết Nga",
   "address": "533 Minh Phụng, phường 10 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.762566,
   "Latitude": 106.644494
 },
 {
   "STT": 1694,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Trọng Văn",
   "address": "489A Nguyễn Đình Chiểu, phường 2 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.769115,
   "Latitude": 106.6814121
 },
 {
   "STT": 1695,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Trung Dũng",
   "address": "Sô 2 Đường 8 Cư Xá Chu Văn An,phường 26 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8112656,
   "Latitude": 106.7111243
 },
 {
   "STT": 1696,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Văn Lâm",
   "address": "73/4C Lê Văn Khương, phường Hiệp Thành , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8653382,
   "Latitude": 106.6497447
 },
 {
   "STT": 1697,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Văn Phước",
   "address": "107/7 Phạm Văn Hai, phường 3, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.792565,
   "Latitude": 106.66294
 },
 {
   "STT": 1698,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Văn Vĩnh",
   "address": "107C Nguyễn kiệm, phường 3 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8185002,
   "Latitude": 106.6787159
 },
 {
   "STT": 1699,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Viết Tùng",
   "address": "75 Tôn Thất Hiệp, phường 13 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764245,
   "Latitude": 106.653479
 },
 {
   "STT": 1700,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Đăng Minh",
   "address": "18/29 Thống Nhất, phường 16, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.848462,
   "Latitude": 106.664478
 },
 {
   "STT": 1701,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Đình Dũng",
   "address": "7B Nguyễn Oanh, phường 10 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8279989,
   "Latitude": 106.6792162
 },
 {
   "STT": 1702,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Đình Hân",
   "address": "80/19 Ba Vân, phường 14, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.7959574,
   "Latitude": 106.6398004
 },
 {
   "STT": 1703,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Đình Long & BS. Lê Huỳnh Mỹ Hạnh",
   "address": "6 Trần Điện, phường 10 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7514946,
   "Latitude": 106.6625031
 },
 {
   "STT": 1704,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Phạm Nguyên Chánh",
   "address": "536/12B Lê Văn Sỹ, phường 11, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.790683,
   "Latitude": 106.674178
 },
 {
   "STT": 1705,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Phạm Thị Hiền",
   "address": "21 Trần Khắc Chân, phường Tân Định , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7930575,
   "Latitude": 106.6911786
 },
 {
   "STT": 1706,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Phạm Thị Kiều",
   "address": "187 Nguyễn Tiểu La, phường 8 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7636742,
   "Latitude": 106.6663313
 },
 {
   "STT": 1707,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Phạm Thị Minh Thư",
   "address": "2295 Huỳnh Tấn Phát, Khu phố 7,Thị trấn Nhà Bè , Huyện Nhà Bè , thành phố Hồ Chí Minh",
   "Longtitude": 10.68796,
   "Latitude": 106.7428
 },
 {
   "STT": 1708,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Phạm Thị Ngọc Thúy",
   "address": "84 Vạn Kiếp, phường 3 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.799925,
   "Latitude": 106.694213
 },
 {
   "STT": 1709,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Phan Ái Hùng",
   "address": "72/15 Duy Tân, phường 15 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.795539,
   "Latitude": 106.679706
 },
 {
   "STT": 1710,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Phan Phước Thụy Như",
   "address": "11D Ấp 4, Xã Bình Chánh, huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.658853,
   "Latitude": 106.68779
 },
 {
   "STT": 1711,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Phan Thị Ngọc Hà",
   "address": "E10/27A Ấp 5 Thới Hòa, Xã Bình Chánh, huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8295216,
   "Latitude": 106.5790527
 },
 {
   "STT": 1712,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Phan Thị Quế Chi",
   "address": "69/9 Nguyễn Đình Chính, phường 15 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.795883,
   "Latitude": 106.679726
 },
 {
   "STT": 1713,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Phan Thị Thanh Yên",
   "address": "62/5 Nguyễn Kim, phường 6 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7628095,
   "Latitude": 106.6620727
 },
 {
   "STT": 1714,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Phan Văn Tĩnh",
   "address": "1441 Đường Ba Tháng Hai,phường 16 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7567997,
   "Latitude": 106.6475256
 },
 {
   "STT": 1715,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Quách Hoàng Gia Nam",
   "address": "231 Bình Long, khu phố 6,phường Bình Hưng Hòa A , quận Bình Tân , thành phố Hồ Chí Minh",
   "Longtitude": 10.7843284,
   "Latitude": 106.6187488
 },
 {
   "STT": 1716,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Thi",
   "address": "56 Đường Số 18, phường Bình Trị Đông B , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.745832,
   "Latitude": 106.6144636
 },
 {
   "STT": 1717,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Toàn",
   "address": "128 Hà Đặc, phường Trung Mỹ Tây , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8583656,
   "Latitude": 106.6139497
 },
 {
   "STT": 1718,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Trần Ngọc Cẩm",
   "address": "167/6 Nguyễn Bửu, phường 2 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7569084,
   "Latitude": 106.679977
 },
 {
   "STT": 1719,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Trần Thị Ánh Minh",
   "address": "8/4 Lê Văn Khương, phường Tân Thới Hiệp , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8696188,
   "Latitude": 106.6492368
 },
 {
   "STT": 1720,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Trần Thị Liễu",
   "address": "51/124 Cao Thắng, phường 3 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7682387,
   "Latitude": 106.6798882
 },
 {
   "STT": 1721,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Trần Thị Nguyệt",
   "address": "470 Lê Văn Sỹ, phường 14 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7893139,
   "Latitude": 106.675554
 },
 {
   "STT": 1722,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Trần Thị Như Ngọc",
   "address": "A11/2 Quốc Lộ 50, Xã Bình Hưng, huyện Bình Chánh, thành phố Hồ Chí Minh",
   "Longtitude": 10.7250367,
   "Latitude": 106.6558347
 },
 {
   "STT": 1723,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Trần Văn Chương",
   "address": "20 Phan Huy Chú, phường 10 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7505529,
   "Latitude": 106.6620268
 },
 {
   "STT": 1724,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Trần Xuân Thông",
   "address": "117/20 Thành Thái, phường 14 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7686251,
   "Latitude": 106.666874
 },
 {
   "STT": 1725,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Trần Thị Long Châu Ngọc",
   "address": "21/4 Nguyễn Thiện Thuật, phường 2 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7664808,
   "Latitude": 106.6815345
 },
 {
   "STT": 1726,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Trịnh Bích Thủy",
   "address": "611/84 Điện Biên Phủ, phường 1 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7748556,
   "Latitude": 106.6834373
 },
 {
   "STT": 1727,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Trương Thị Minh Phương",
   "address": "135/59 Trần Hưng Đạo, phường Cầu Ông Lãnh , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.766491,
   "Latitude": 106.695306
 },
 {
   "STT": 1728,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Tuấn & BS. Dung",
   "address": "50 Đường số 4, phường 16 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7329163,
   "Latitude": 106.6234032
 },
 {
   "STT": 1729,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Ung Thị Phượng",
   "address": "11 Nguyễn Thiện Thuật, phường 2, Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7662927,
   "Latitude": 106.6818415
 },
 {
   "STT": 1730,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Văn Nữ Thiên Trang",
   "address": "149 Chu Văn An, phường 26, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.810839,
   "Latitude": 106.7059297
 },
 {
   "STT": 1731,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Võ Chí Hiếu",
   "address": "5 Tỉnh Lộ 8, Thị trấn Củ Chi , huyện Củ Chi , thành phố Hồ Chí Minh",
   "Longtitude": 10.969716,
   "Latitude": 106.4886009
 },
 {
   "STT": 1732,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Võ Ngọc Thoại",
   "address": "1A/1 Nguyễn Ảnh Thủ, Tổ 19, Khu phố 2, phường Hiệp Thành , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8775711,
   "Latitude": 106.6514973
 },
 {
   "STT": 1733,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Võ Phú Cường",
   "address": "287 Lãnh Binh Thăng, phường 8 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764132,
   "Latitude": 106.648284
 },
 {
   "STT": 1734,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Võ Văn Tự Hiến",
   "address": "73/1 Hồ Hảo Hớn, phường Cô Giang , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.762038,
   "Latitude": 106.691196
 },
 {
   "STT": 1735,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Võ Văn Tuấn",
   "address": "443/6 Lê Văn Sỹ, phường 12 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7893709,
   "Latitude": 106.674594
 },
 {
   "STT": 1736,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Vũ Thị Kiều Diễm",
   "address": "30/4H Hoàng Hoa Thám, phường 12, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.797834,
   "Latitude": 106.647803
 },
 {
   "STT": 1737,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Vương Thị Ngọc Thảo",
   "address": "74 Hàm Nghi, phường Bến Nghé ,Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.771221,
   "Latitude": 106.703522
 },
 {
   "STT": 1738,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Đặng Thị Hoàng Anh",
   "address": "619/13C Hưng Phú, phường 9 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.746333,
   "Latitude": 106.6704589
 },
 {
   "STT": 1739,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Đặng Thị Ngọc An",
   "address": "399/23 Nguyễn Đình Chiểu,phường 5 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7709771,
   "Latitude": 106.6838578
 },
 {
   "STT": 1740,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Đậu Đình Ân",
   "address": "101 Nguyễn Trãi, phường 2 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7579691,
   "Latitude": 106.6804586
 },
 {
   "STT": 1741,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Điền",
   "address": "3/34-36-38 Đường 385, phường Tăng Nhơn Phú A , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8488327,
   "Latitude": 106.7909878
 },
 {
   "STT": 1742,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Đinh Thị Tuyết Trinh",
   "address": "50 Đường số 13, phường 4 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7406847,
   "Latitude": 106.6711194
 },
 {
   "STT": 1743,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Đỗ Hải Độ",
   "address": "43/2 Ấp Tân Tiến, Xã Xuân Thới Đông , huyện Hóc Môn , thành phố Hồ Chí Minh",
   "Longtitude": 10.8606646,
   "Latitude": 106.6032062
 },
 {
   "STT": 1744,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS.Hồng & BS. Hãn",
   "address": "J10 Cư xá Phú Lâm A, phường 12 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7502237,
   "Latitude": 106.6318057
 },
 {
   "STT": 1745,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS.Nguyễn Thành Quang",
   "address": "504/24 Bà Hạt, phường 8 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7671275,
   "Latitude": 106.6735886
 },
 {
   "STT": 1746,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Ths.BS.Nguyễn Ngọc Hải",
   "address": "499-501 Bà Hạt, phường 8 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7654257,
   "Latitude": 106.6664601
 },
 {
   "STT": 1747,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt Én Trắng",
   "address": "196 Lý Thái Tổ, phường 1 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7669628,
   "Latitude": 106.6777003
 },
 {
   "STT": 1748,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt Minh Long",
   "address": "303 Tùng Thiện Vương, phường 11 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7467815,
   "Latitude": 106.6611005
 },
 {
   "STT": 1749,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt Mỹ Đức - BS. Nguyễn Thị Minh Hạnh",
   "address": "642 Trường Chinh, Khu phố 3, Tổ 2, phường Tân Hưng Thuận ,Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8391803,
   "Latitude": 106.6177359
 },
 {
   "STT": 1750,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt Nguyệt Ánh",
   "address": "6 Bùi Đình Túy, phường 14 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8067342,
   "Latitude": 106.6980439
 },
 {
   "STT": 1751,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt Nha Khoa TB.VIỆT Á",
   "address": "F1/8M2 Đường Vĩnh Lộc, Xã Vĩnh Lộc A, huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8113963,
   "Latitude": 106.5757703
 },
 {
   "STT": 1752,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt Tấn Phát",
   "address": "361 Kinh Dương Vương, phường An Lạc , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.74217,
   "Latitude": 106.6198435
 },
 {
   "STT": 1753,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt Thanh Hồng",
   "address": "48 Tân Hóa, phường 14 , Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.7568381,
   "Latitude": 106.6361936
 },
 {
   "STT": 1754,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt Đoàn Kết",
   "address": "125 Bình Chiểu, phường Bình Chiểu , quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8830404,
   "Latitude": 106.7264125
 },
 {
   "STT": 1755,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Phương Nam",
   "address": "507 Tô Ngọc Vân, phường Tam Phú , quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.864513,
   "Latitude": 106.7419761
 },
 {
   "STT": 1756,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Lê Thị Viết",
   "address": "42/32 Hoàng Diệu, phường 12 ,Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.765193,
   "Latitude": 106.705935
 },
 {
   "STT": 1757,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Thị Diệu Lan",
   "address": "243/12 Chu Văn An, phường 12, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8104026,
   "Latitude": 106.7038819
 },
 {
   "STT": 1758,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Nguyễn Thị Lan Anh",
   "address": "31 Ni Sư Huỳnh Liên, phường 10, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.782895,
   "Latitude": 106.6493672
 },
 {
   "STT": 1759,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Đặng Ngọc Hùng",
   "address": "300A Nguyễn Thị Định, Khu Phố 1, phường Thạnh Mỹ Lợi , Quận 2, thành phố Hồ Chí Minh",
   "Longtitude": 10.7909804,
   "Latitude": 106.7286239
 },
 {
   "STT": 1760,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - BS. Đặng Thế Hiển",
   "address": "74 Nguyễn Văn Lạc, phường 19, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.7925778,
   "Latitude": 106.7089895
 },
 {
   "STT": 1761,
   "Name": "Phòng Khám Chuyên Khoa Răng Hùng Châu",
   "address": "146 Hồng bàng, phường 12 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7549787,
   "Latitude": 106.6597201
 },
 {
   "STT": 1762,
   "Name": "Phòng Khám Chuyên Khoa Sản",
   "address": "44 Phạm Ngũ Lão, phường 4 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8206775,
   "Latitude": 106.683312
 },
 {
   "STT": 1763,
   "Name": "Phòng Khám Chuyên Khoa Sản",
   "address": "88 Trần Quý, phường 6 , Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.759301,
   "Latitude": 106.656145
 },
 {
   "STT": 1764,
   "Name": "Phòng Khám Chuyên Khoa Sản",
   "address": "77C Lạc Long Quân, phường 1 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.75872,
   "Latitude": 106.639535
 },
 {
   "STT": 1765,
   "Name": "Phòng Khám Chuyên Khoa Sản & Nhi Khoa - BS. Nguyễn Thành Tâm & BS. Lê Thanh Bảo Quyên",
   "address": "B99 Quang Trung, phường Đông Hưng Thuận , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8460919,
   "Latitude": 106.6372768
 },
 {
   "STT": 1766,
   "Name": "Phòng Khám Chuyên Khoa Sản & Nội Tổng Quát - BS An & BS Tuyết Mai",
   "address": "45/76 Đường Số 20, phường 8, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8397391,
   "Latitude": 106.6531088
 },
 {
   "STT": 1767,
   "Name": "Phòng Khám Chuyên Khoa Sản & Nội Tổng Quát - BS. Lan Phương & BS. Hoàng Tân",
   "address": "369 Lê Hồng Phong, phường 2 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.763609,
   "Latitude": 106.675565
 },
 {
   "STT": 1768,
   "Name": "Phòng Khám Chuyên Khoa Sản - Phụ Khoa Bs. Nguyễn Thị Rảnh",
   "address": "4/12 Quốc lộ 1A, ẤP 3, Xã Tân Quý Tây , Huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.6689281,
   "Latitude": 106.5840206
 },
 {
   "STT": 1769,
   "Name": "Phòng Khám Chuyên Khoa Sản - BS Hoàng Huy Kền",
   "address": "25 Đặng Thị Rành, phường Linh Tây, quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8508199,
   "Latitude": 106.7532602
 },
 {
   "STT": 1770,
   "Name": "Phòng Khám Chuyên Khoa Sản - BS Lê Thị Anh Thư",
   "address": "47C Trần Phú, phường 4 , Quận 5, thành phố Hồ Chí Minh",
   "Longtitude": 10.7619354,
   "Latitude": 106.6787565
 },
 {
   "STT": 1771,
   "Name": "Phòng Khám Chuyên Khoa Sản - BS Nguyễn Song Nguyên",
   "address": "125 Tôn Thất Thuyết, phường 15 ,Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7533411,
   "Latitude": 106.7085314
 },
 {
   "STT": 1772,
   "Name": "Phòng Khám Chuyên Khoa Sản - BS Thùy & BS Thạch",
   "address": "69 Phù Đổng Thiên Vương,phường 11 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7539433,
   "Latitude": 106.662575
 },
 {
   "STT": 1773,
   "Name": "Phòng Khám Chuyên Khoa Sản - BS Trịnh Hữu Phước",
   "address": "529/16 Nguyễn Tri Phương,phường 8 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.763265,
   "Latitude": 106.669169
 },
 {
   "STT": 1774,
   "Name": "Phòng Khám Chuyên Khoa Sản - BS Đặng Thị Hạnh",
   "address": "423A Gia Phú, phường 3 , Quận 6, thành phố Hồ Chí Minh",
   "Longtitude": 10.744003,
   "Latitude": 106.647595
 },
 {
   "STT": 1775,
   "Name": "Phòng Khám Chuyên Khoa Sản - BS. Hoàng Nhạn",
   "address": "194 Phan Văn Hớn, phường Tân Thới Nhất , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8300439,
   "Latitude": 106.6147396
 },
 {
   "STT": 1776,
   "Name": "Phòng Khám Chuyên Khoa Sản - BS. Ngụy Thanh Quang",
   "address": "210/1 Tân Khai, phường 4 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.75827,
   "Latitude": 106.65558
 },
 {
   "STT": 1777,
   "Name": "Phòng Khám Chuyên Khoa Sản - BS. Nguyễn Thị Lãng",
   "address": "115 Nguyễn Chí Thanh, phường 16 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.756365,
   "Latitude": 106.648178
 },
 {
   "STT": 1778,
   "Name": "Phòng Khám Chuyên Khoa Sản - BS. Nguyễn Thu Đông",
   "address": "86/4 Trần Hưng Đạo, phường 7 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7531694,
   "Latitude": 106.6702212
 },
 {
   "STT": 1779,
   "Name": "Phòng Khám Chuyên Khoa Sản - BS. Nguyễn Văn Thịnh",
   "address": "462A/2 Trần Hưng Đạo, phường 2, Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7561007,
   "Latitude": 106.6827678
 },
 {
   "STT": 1780,
   "Name": "Phòng Khám Chuyên Khoa Sản - BS. Nhung & BS. Phúc",
   "address": "76 Vạn Kiếp, phường 3 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.800052,
   "Latitude": 106.694204
 },
 {
   "STT": 1781,
   "Name": "Phòng Khám Chuyên Khoa Sản - BS. Phạm Thành Đức",
   "address": "100/24 An Dương Vương, phường 9 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7578662,
   "Latitude": 106.6704806
 },
 {
   "STT": 1782,
   "Name": "Phòng Khám Chuyên Khoa Sản - BS. Tạ Thị Thanh Thủy",
   "address": "BB2 Trường Sơn, Cư xá Bắc Hải,phường 15 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.784536,
   "Latitude": 106.665868
 },
 {
   "STT": 1783,
   "Name": "Phòng Khám Chuyên Khoa Sản - BS. Trần Thị Như Phương",
   "address": "624 Quốc Lộ 13, phường Hiệp Bình Phước , quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8676095,
   "Latitude": 106.7209839
 },
 {
   "STT": 1784,
   "Name": "Phòng Khám Chuyên Khoa Sản - BS. Đỗ Danh Toàn",
   "address": "218F/17 Trần Hưng Đạo, phường 11 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7531302,
   "Latitude": 106.6659251
 },
 {
   "STT": 1785,
   "Name": "Phòng Khám Chuyên Khoa Sản BS. Nguyễn Thị Thanh Hà",
   "address": "30/12 Phó Đức Chính, phường Nguyễn Thái Bình , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7687519,
   "Latitude": 106.700762
 },
 {
   "STT": 1786,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa",
   "address": "109A Hoàng Diệu 2, phường Linh Chiểu , quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.854329,
   "Latitude": 106.7725804
 },
 {
   "STT": 1787,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa",
   "address": "1275B Ba Tháng Hai, phường 16 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7588487,
   "Latitude": 106.6509275
 },
 {
   "STT": 1788,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa",
   "address": "139 Đào Duy Từ, phường 5, Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7600209,
   "Latitude": 106.6656733
 },
 {
   "STT": 1789,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa",
   "address": "241A Đường 17, phường Tân Quy, Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.744736,
   "Latitude": 106.7102478
 },
 {
   "STT": 1790,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa & Tiêu Hóa - BS. Trịnh Thảo & .BS. Hồng Hạnh",
   "address": "1F2 Nguyễn Thái Sơn, phường 3, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8158832,
   "Latitude": 106.6797352
 },
 {
   "STT": 1791,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS Hoàng Yến",
   "address": "86 - 88 Đường 41, Khu Tân Qui Đông, phường Tân Phong , Quận 7, thành phố Hồ Chí Minh",
   "Longtitude": 10.7366978,
   "Latitude": 106.7108888
 },
 {
   "STT": 1792,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS Nguyễn Thị Hồng Nhung",
   "address": "76 Đường 46, phường Bình Trưng Đông , Quận 2 , thành phố Hồ Chí Minh",
   "Longtitude": 10.788559,
   "Latitude": 106.7757744
 },
 {
   "STT": 1793,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS Nguyễn Thị Như Ngọc",
   "address": "257 Hồng Bàng, phường 11 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7549449,
   "Latitude": 106.6616081
 },
 {
   "STT": 1794,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS Nguyễn Vũ Mỹ Linh",
   "address": "424/1 Nguyễn Văn Luông, phường 12 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.752258,
   "Latitude": 106.635386
 },
 {
   "STT": 1795,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS Quỳ & BS Phượng",
   "address": "G56 Bến Vân Đồn, phường 6 ,Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7630468,
   "Latitude": 106.6985901
 },
 {
   "STT": 1796,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS Sao Lan",
   "address": "141 Trần Bình Trọng, phường 2 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7574802,
   "Latitude": 106.6807438
 },
 {
   "STT": 1797,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS Thanh Minh",
   "address": "315/3 Trần Bình Trọng, phường 4, Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7615506,
   "Latitude": 106.6787571
 },
 {
   "STT": 1798,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS Trần Kim Loan",
   "address": "74 Đường số 17, phường Tân Kiểng , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7449573,
   "Latitude": 106.7129406
 },
 {
   "STT": 1799,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS Trương Thị Xinh",
   "address": "208 Trần Bình Trọng, phường 4 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7606877,
   "Latitude": 106.67988
 },
 {
   "STT": 1800,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Bạch Tuyết Mai",
   "address": "98 Quách Đình Bảo, phường Phú Thạnh, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.780996,
   "Latitude": 106.626109
 },
 {
   "STT": 1801,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Hồ Thị Biên",
   "address": "118 Diên Hồng, phường 1 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.800337,
   "Latitude": 106.698521
 },
 {
   "STT": 1802,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Hoàng Lê Minh Hiền",
   "address": "526 Bà Hạt, phường 8 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7650882,
   "Latitude": 106.664696
 },
 {
   "STT": 1803,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Hoàng Thị Thanh Thảo",
   "address": "460 Hồ Ngọc Lãm, phường An Lạc, quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.72755,
   "Latitude": 106.608
 },
 {
   "STT": 1804,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Huỳnh Thị Thủy",
   "address": "216A Hà Huy Giáp, phường Thạnh Lộc , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8591928,
   "Latitude": 106.6801918
 },
 {
   "STT": 1805,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Lê Thị Hòa Bình",
   "address": "215A/6 Đoàn Thị Điểm, phường 1, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.799602,
   "Latitude": 106.681438
 },
 {
   "STT": 1806,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Ngô Văn Nhắng",
   "address": "409 Lê Trọng Tấn, phường Sơn Kỳ, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.8083679,
   "Latitude": 106.617474
 },
 {
   "STT": 1807,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Nguyễn Ngọc Điệp",
   "address": "68 Dạ Nam, phường 2 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.749029,
   "Latitude": 106.685067
 },
 {
   "STT": 1808,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Nguyễn Thị Minh Tuyết",
   "address": "9/55A Phan Huy Ích, phường 14, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.84134,
   "Latitude": 106.636088
 },
 {
   "STT": 1809,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Nguyễn Thị Ngọc Yến & BS. Hồng Thu",
   "address": "465 Hồ Học Lãm, phường An Lạc, quận Bình Tân, thành phố Hồ Chí Minh",
   "Longtitude": 10.7253453,
   "Latitude": 106.6089803
 },
 {
   "STT": 1810,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Nguyễn Thị Sự",
   "address": "67 Hòa Hưng, phường 12 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.779467,
   "Latitude": 106.6753785
 },
 {
   "STT": 1811,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Nguyễn Thị Thanh Thúy",
   "address": "144 Cây Keo, phường Hiệp Tân, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.772303,
   "Latitude": 106.627821
 },
 {
   "STT": 1812,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Nguyễn Thị Thu",
   "address": "449 Thống Nhất, phường 11 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8451369,
   "Latitude": 106.6648022
 },
 {
   "STT": 1813,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Nguyễn Thị Tươi",
   "address": "525 Huỳnh Văn Bánh, phường 14, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.7906438,
   "Latitude": 106.6683355
 },
 {
   "STT": 1814,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Nguyễn Xuân Trang",
   "address": "103/3 Đặng Chất, phường 2 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.747107,
   "Latitude": 106.683992
 },
 {
   "STT": 1815,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Nguyễn Đình Hào",
   "address": "649/20/6 Điện Biên Phủ, phường 25 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8002224,
   "Latitude": 106.7211485
 },
 {
   "STT": 1816,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Nguyễn Đình Vinh",
   "address": "969 Nguyễn Kiệm, phường 3 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8225826,
   "Latitude": 106.6788156
 },
 {
   "STT": 1817,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Ninh & BS. Hằng",
   "address": "224A Hồ Văn Huê, phường 9 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.805127,
   "Latitude": 106.67809
 },
 {
   "STT": 1818,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Phan Xuân Khoa",
   "address": "155 Tạ Quang Bửu, phường 3 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.744657,
   "Latitude": 106.686772
 },
 {
   "STT": 1819,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Tăng Thường Bản",
   "address": "38 Trương Vĩnh Ký, phường Tân Thành, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.793516,
   "Latitude": 106.636084
 },
 {
   "STT": 1820,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Thủy & Chuyên Khoa Da Liễu - BS. Mười",
   "address": "44 Nguyễn Đình Chính, phường 15, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.796706,
   "Latitude": 106.679426
 },
 {
   "STT": 1821,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Trần Ngọc Diễm",
   "address": "26/9 Bông Sao, phường 5 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.739288,
   "Latitude": 106.662338
 },
 {
   "STT": 1822,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Võ Doãn Mỹ Thạnh",
   "address": "221 Nơ Trang Long, phường 12, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8165603,
   "Latitude": 106.6977116
 },
 {
   "STT": 1823,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Xuân Trang",
   "address": "533 Lê Văn Quới, phường Bình Trị Đông A , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7764043,
   "Latitude": 106.6025915
 },
 {
   "STT": 1824,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Yến Oanh & BS. Thu Đông",
   "address": "21 Huỳnh Đình Hai, phường 14, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8044684,
   "Latitude": 106.7003157
 },
 {
   "STT": 1825,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Đặng Thị Trân Hạnh",
   "address": "90A Hồ Văn Huê, phường 9 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.801679,
   "Latitude": 106.676214
 },
 {
   "STT": 1826,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Đỗ Ngọc Xuân Trang",
   "address": "35/22 Nguyễn Quý Anh, phường Tân Sơn Nhì, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.8020202,
   "Latitude": 106.6281353
 },
 {
   "STT": 1827,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS.Huỳnh Thị Thu Thủy",
   "address": "55/31 Nguyễn Chế Nghĩa, phường 13 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.744122,
   "Latitude": 106.6538039
 },
 {
   "STT": 1828,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa An Tâm",
   "address": "579-581 Nguyễn Đình Chiểu,phường 2 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7675294,
   "Latitude": 106.6799081
 },
 {
   "STT": 1829,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa Bella - Cát Minh",
   "address": "466 Tên Lửa, phường Bình Trị Đông B , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7546428,
   "Latitude": 106.6095793
 },
 {
   "STT": 1830,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa Liên Hương",
   "address": "461 Lê Đức Thọ, phường 16 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.847123,
   "Latitude": 106.667757
 },
 {
   "STT": 1831,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa Phương Ngoan",
   "address": "247 Hồng Bàng, phường 11 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7549423,
   "Latitude": 106.6618356
 },
 {
   "STT": 1832,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa Phương Vy",
   "address": "BB3 Trường Sơn, Cư Xá Bắc Hải,phường 15 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.782042,
   "Latitude": 106.663391
 },
 {
   "STT": 1833,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa Thạc Sỹ - Bs. Đỗ Thị Tuyết Nga",
   "address": "D14/28 Đường 18B, ấp 4, Xã Bình Chánh , Huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.6643619,
   "Latitude": 106.5721409
 },
 {
   "STT": 1834,
   "Name": "Phòng Khám Chuyên Khoa Sản Và Kế Hoạch Hóa Gia Đình Thiện Tâm",
   "address": "186 Nguyễn Ảnh Thủ, phường Hiệp Thành , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8770567,
   "Latitude": 106.6428528
 },
 {
   "STT": 1835,
   "Name": "Phòng Khám Chuyên Khoa Siêu Âm",
   "address": "83 Đường Số 5, phường Linh Xuân, quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8789835,
   "Latitude": 106.7627089
 },
 {
   "STT": 1836,
   "Name": "Phòng Khám Chuyên Khoa Sọ Não Cột Sống - BS. Phạm Anh Tuấn & Chuyên Khoa Chấn Thương Chỉnh Hình - BS. Trang Mạnh Khôi",
   "address": "45 Ngô Quyền, phường 10 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7521398,
   "Latitude": 106.6666229
 },
 {
   "STT": 1837,
   "Name": "Phòng Khám Chuyên Khoa Song Quân",
   "address": "47 Bà Triệu, Khu phố 1, Thị trấn Hóc Môn , huyện Hóc Môn , thành phố Hồ Chí Minh",
   "Longtitude": 10.8807451,
   "Latitude": 106.5933858
 },
 {
   "STT": 1838,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Phan Xuân Khoa",
   "address": "155 Tạ Quang Bửu, phường 3 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.744657,
   "Latitude": 106.686772
 },
 {
   "STT": 1839,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Tăng Thường Bản",
   "address": "38 Trương Vĩnh Ký, phường Tân Thành, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.793516,
   "Latitude": 106.636084
 },
 {
   "STT": 1840,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Thủy & Chuyên Khoa Da Liễu - BS. Mười",
   "address": "44 Nguyễn Đình Chính, phường 15, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.796706,
   "Latitude": 106.679426
 },
 {
   "STT": 1841,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Trần Ngọc Diễm",
   "address": "26/9 Bông Sao, phường 5 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.739288,
   "Latitude": 106.662338
 },
 {
   "STT": 1842,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Võ Doãn Mỹ Thạnh",
   "address": "221 Nơ Trang Long, phường 12, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8165603,
   "Latitude": 106.6977116
 },
 {
   "STT": 1843,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Xuân Trang",
   "address": "533 Lê Văn Quới, phường Bình Trị Đông A , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7764043,
   "Latitude": 106.6025915
 },
 {
   "STT": 1844,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Yến Oanh & BS. Thu Đông",
   "address": "21 Huỳnh Đình Hai, phường 14, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8044684,
   "Latitude": 106.7003157
 },
 {
   "STT": 1845,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Đặng Thị Trân Hạnh",
   "address": "90A Hồ Văn Huê, phường 9 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.801679,
   "Latitude": 106.676214
 },
 {
   "STT": 1846,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS. Đỗ Ngọc Xuân Trang",
   "address": "35/22 Nguyễn Quý Anh, phường Tân Sơn Nhì, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.8020202,
   "Latitude": 106.6281353
 },
 {
   "STT": 1847,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - BS.Huỳnh Thị Thu Thủy",
   "address": "55/31 Nguyễn Chế Nghĩa, phường 13 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.744122,
   "Latitude": 106.6538039
 },
 {
   "STT": 1848,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa An Tâm",
   "address": "579-581 Nguyễn Đình Chiểu,phường 2 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7675294,
   "Latitude": 106.6799081
 },
 {
   "STT": 1849,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa Bella - Cát Minh",
   "address": "466 Tên Lửa, phường Bình Trị Đông B , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7546428,
   "Latitude": 106.6095793
 },
 {
   "STT": 1850,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa Liên Hương",
   "address": "461 Lê Đức Thọ, phường 16 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.847123,
   "Latitude": 106.667757
 },
 {
   "STT": 1851,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa Phương Ngoan",
   "address": "247 Hồng Bàng, phường 11 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7549423,
   "Latitude": 106.6618356
 },
 {
   "STT": 1852,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa Phương Vy",
   "address": "BB3 Trường Sơn, Cư Xá Bắc Hải,phường 15 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.782042,
   "Latitude": 106.663391
 },
 {
   "STT": 1853,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa Thạc Sỹ - Bs. Đỗ Thị Tuyết Nga",
   "address": "D14/28 Đường 18B, ấp 4, Xã Bình Chánh , Huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.6643619,
   "Latitude": 106.5721409
 },
 {
   "STT": 1854,
   "Name": "Phòng Khám Chuyên Khoa Sản Và Kế Hoạch Hóa Gia Đình Thiện Tâm",
   "address": "186 Nguyễn Ảnh Thủ, phường Hiệp Thành , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8770567,
   "Latitude": 106.6428528
 },
 {
   "STT": 1855,
   "Name": "Phòng Khám Chuyên Khoa Siêu Âm",
   "address": "83 Đường Số 5, phường Linh Xuân, quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8789835,
   "Latitude": 106.7627089
 },
 {
   "STT": 1856,
   "Name": "Phòng Khám Chuyên Khoa Sọ Não Cột Sống - BS. Phạm Anh Tuấn & Chuyên Khoa Chấn Thương Chỉnh Hình - BS. Trang Mạnh Khôi",
   "address": "45 Ngô Quyền, phường 10 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7521398,
   "Latitude": 106.6666229
 },
 {
   "STT": 1857,
   "Name": "Phòng Khám Chuyên Khoa Song Quân",
   "address": "47 Bà Triệu, Khu phố 1, Thị trấn Hóc Môn , Huyện Hóc Môn , thành phố Hồ Chí Minh",
   "Longtitude": 10.8807451,
   "Latitude": 106.5933858
 },
 {
   "STT": 1858,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng",
   "address": "129B Lê Thị Bạch Cát, phường 11, Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764573,
   "Latitude": 106.650972
 },
 {
   "STT": 1859,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng",
   "address": "257/24 Lý Thường Kiệt, phường 15 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.772648,
   "Latitude": 106.656858
 },
 {
   "STT": 1860,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng",
   "address": "78C8 Phạm Ngũ Lão, phường 3, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8204843,
   "Latitude": 106.6829661
 },
 {
   "STT": 1861,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng",
   "address": "482 Lê Văn Quới, phường Bình Hưng Hòa A , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.778931,
   "Latitude": 106.604753
 },
 {
   "STT": 1862,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng",
   "address": "9 Tuệ Tĩnh, phường 13 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764474,
   "Latitude": 106.653878
 },
 {
   "STT": 1863,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng",
   "address": "36A Ngô Đức Kế, phường 12, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8023606,
   "Latitude": 106.7161971
 },
 {
   "STT": 1864,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng",
   "address": "664 Nguyễn Chí Thanh, phường 4, Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7577231,
   "Latitude": 106.6575022
 },
 {
   "STT": 1865,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng",
   "address": "64 Nguyễn Lâm, phường 6 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7639856,
   "Latitude": 106.6633522
 },
 {
   "STT": 1866,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng",
   "address": "225 Đào Duy Từ, phường 7 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7593263,
   "Latitude": 106.6621305
 },
 {
   "STT": 1867,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng",
   "address": "184 Nguyễn Chí Thanh, phường 3, Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7602985,
   "Latitude": 106.6703097
 },
 {
   "STT": 1868,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng & Mắt",
   "address": "38A Nguyễn Lâm, phường 6 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7633648,
   "Latitude": 106.6637667
 },
 {
   "STT": 1869,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng & Ung Bướu - BS. Nguyễn Tấn Hiển",
   "address": "38 Bùi Thị Xuân, phường Bến Thành , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7719098,
   "Latitude": 106.6899966
 },
 {
   "STT": 1870,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng (BS Nghị) - Tim Mạch (BS Hoa)",
   "address": "55 Đường 23, phường 10 , Quận 6, thành phố Hồ Chí Minh",
   "Longtitude": 10.7378543,
   "Latitude": 106.6252117
 },
 {
   "STT": 1871,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS Chương",
   "address": "4 Lã Xuân Oai, phường Tăng Nhơn Phú B , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.843561,
   "Latitude": 106.79018
 },
 {
   "STT": 1872,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS Dương Đình Thông",
   "address": "979 Hồng Bàng, phường 9 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7544304,
   "Latitude": 106.6359539
 },
 {
   "STT": 1873,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS Hương & BS Trị",
   "address": "34 Đường Số 1, Khu Phố 4,phường An Phú , Quận 2 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7978317,
   "Latitude": 106.7333039
 },
 {
   "STT": 1874,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS Ngô Minh",
   "address": "367 Tùng Thiện Vương, phường 12 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.745815,
   "Latitude": 106.65934
 },
 {
   "STT": 1875,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS Nguyễn Chính Đại",
   "address": "337 Nguyễn Thiện Thuật, phường 1 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7700975,
   "Latitude": 106.6771634
 },
 {
   "STT": 1876,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS Nguyễn Hữu Dũng",
   "address": "351 Hồng Bàng, phường 11 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7544058,
   "Latitude": 106.659264
 },
 {
   "STT": 1877,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS Nguyễn Thanh Phong",
   "address": "5 Lô K, Cư xá Phú Lâm D, Bình Phú, phường 10 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7399156,
   "Latitude": 106.6322742
 },
 {
   "STT": 1878,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS Nguyễn Văn Minh",
   "address": "318 Nguyễn Văn Luông, phường 12 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7498513,
   "Latitude": 106.6355065
 },
 {
   "STT": 1879,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS Nhan Trừng Sơn",
   "address": "21 - 23 Đỗ Quang Đẩu, phường Phạm Ngũ Lão , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.766622,
   "Latitude": 106.6910089
 },
 {
   "STT": 1880,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS Phan Đình Long",
   "address": "10C Kỳ Đồng, phường 9 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.784411,
   "Latitude": 106.68279
 },
 {
   "STT": 1881,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS Trần Anh Bích",
   "address": "132 Phạm Phú Thứ, phường 4 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7439589,
   "Latitude": 106.6441755
 },
 {
   "STT": 1882,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS Trần Duy Huân",
   "address": "481 Quang Trung, phường 10 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8321137,
   "Latitude": 106.6676391
 },
 {
   "STT": 1883,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS Xà Trường Thành",
   "address": "52 Tân Hòa Đông, phường 14 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.756227,
   "Latitude": 106.632717
 },
 {
   "STT": 1884,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS Đặng Hoàng Sơn",
   "address": "1039 Nguyễn Trãi, phường 14 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7528357,
   "Latitude": 106.6515082
 },
 {
   "STT": 1885,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Bùi Thị Ngọc Thúy",
   "address": "360 Tân Phước, phường 5 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7612476,
   "Latitude": 106.660911
 },
 {
   "STT": 1886,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Bùi Văn Soát",
   "address": "166/15 Đường Số 19, phường 12, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8381654,
   "Latitude": 106.6541724
 },
 {
   "STT": 1887,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Cát Huy Quang",
   "address": "491/32 Huỳnh Văn Bánh, phường 14 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.791592,
   "Latitude": 106.6681854
 },
 {
   "STT": 1888,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Dương Hoàng Thắng",
   "address": "42 Lê Khôi, phường Phú Thạnh, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.7802054,
   "Latitude": 106.6282015
 },
 {
   "STT": 1889,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Dương Thị Thanh Mai",
   "address": "49 Phạm Phú Thứ, phường 11 , Tân Bình , thành phố Hồ Chí Minh",
   "Longtitude": 10.788746,
   "Latitude": 106.648802
 },
 {
   "STT": 1890,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Hiển",
   "address": "216 Phan Văn Trị, phường 10 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8295914,
   "Latitude": 106.6814666
 },
 {
   "STT": 1891,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Hồ Quốc Tuấn",
   "address": "675 Xô Viết Nghệ Tĩnh, phường 26, quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8120806,
   "Latitude": 106.7148142
 },
 {
   "STT": 1892,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Hồ Thành Hải",
   "address": "133 Tân Hòa Đông, phường 14 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.758914,
   "Latitude": 106.629911
 },
 {
   "STT": 1893,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Hoàng Bá Dũng",
   "address": "624 Nguyễn Chí Thanh, phường 4, Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.758034,
   "Latitude": 106.65874
 },
 {
   "STT": 1894,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Hoàng Gia Thịnh",
   "address": "145/36 Nguyễn Thiện Thuật,phường 1 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7673673,
   "Latitude": 106.6791026
 },
 {
   "STT": 1895,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Hoàng Lương",
   "address": "40/32 Calmette, phường Nguyễn Thái Bình , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.767217,
   "Latitude": 106.700236
 },
 {
   "STT": 1896,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Hùng",
   "address": "211 Đường số 29, phường Bình Trị Đông B , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7557038,
   "Latitude": 106.6100342
 },
 {
   "STT": 1897,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Huỳnh Minh Thế & BS. Việt Anh",
   "address": "330/12 Phan Đình Phùng, phường 1 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.796897,
   "Latitude": 106.6821329
 },
 {
   "STT": 1898,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Huỳnh Thùy Quế Hương",
   "address": "262/9 Lạc Long Quân, phường 10, Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.758876,
   "Latitude": 106.641182
 },
 {
   "STT": 1899,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Huỳnh Vĩ Sơn",
   "address": "2/1 Kỳ Đồng, phường 9 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.784147,
   "Latitude": 106.683374
 },
 {
   "STT": 1900,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Khưu Minh Thái",
   "address": "961 Tạ Quang Bửu, phường 6 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.733857,
   "Latitude": 106.655401
 },
 {
   "STT": 1901,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Lâm Hoàng Yến",
   "address": "201 Đường 3 Tháng 2, phường 9 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7678127,
   "Latitude": 106.6674614
 },
 {
   "STT": 1902,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Lâm Huyền Trân",
   "address": "194/46 Võ Văn Tần, phường 5 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7695661,
   "Latitude": 106.6829278
 },
 {
   "STT": 1903,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Lâm Văn Hiệp",
   "address": "441 Bà Hạt, phường 8 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7655668,
   "Latitude": 106.6674264
 },
 {
   "STT": 1904,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Lê Huỳnh Mai",
   "address": "53 Nguyễn Thông, phường 9 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7797709,
   "Latitude": 106.6813921
 },
 {
   "STT": 1905,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Lê Long Hải",
   "address": "322A Điện Biên Phủ, phường 11 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.775252,
   "Latitude": 106.6816913
 },
 {
   "STT": 1906,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Lê Nguyễn Diễm Thi",
   "address": "36 Nguyễn Văn Lạc, phường 19, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.794101,
   "Latitude": 106.709483
 },
 {
   "STT": 1907,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Lê Phương Hà",
   "address": "25 Vườn Chuối, phường 4 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7725081,
   "Latitude": 106.683511
 },
 {
   "STT": 1908,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Lê Thị Tuyết Mai",
   "address": "105/1 Nguyễn Kiệm, phường 3, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8113938,
   "Latitude": 106.6785762
 },
 {
   "STT": 1909,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Lê Trí",
   "address": "13 Đoàn Thị Điểm, phường 1 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.799583,
   "Latitude": 106.6818399
 },
 {
   "STT": 1910,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Lê Tự Thành Nhân",
   "address": "207/6 Lạc Long Quân, phường 3 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.76382,
   "Latitude": 106.641836
 },
 {
   "STT": 1911,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Lê Tuyến Thoại",
   "address": "78 Đường Số 9, Cư Xá Bình Thới,phường 8 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.763013,
   "Latitude": 106.646968
 },
 {
   "STT": 1912,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Lê Tuyết Trâm",
   "address": "74 Nguyễn Biểu, phường 1 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.755135,
   "Latitude": 106.6842015
 },
 {
   "STT": 1913,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Lương Trọng Nghĩa",
   "address": "1263A Tỉnh Lộ 10, phường Tân Tạo , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7562295,
   "Latitude": 106.5927377
 },
 {
   "STT": 1914,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Ngô Thị Thu Thủy",
   "address": "219A An Bình, phường 7 , Quận 5, thành phố Hồ Chí Minh",
   "Longtitude": 10.754629,
   "Latitude": 106.6708687
 },
 {
   "STT": 1915,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Ngô Văn Công & Sản Phụ Khoa - BS. Nguyễn Thị Nhật Phượng",
   "address": "35 Tân Hòa Đông, phường 13 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.756493,
   "Latitude": 106.631866
 },
 {
   "STT": 1916,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Ngô Đức Minh Huy",
   "address": "228/2 Nguyễn Văn Nghi, phường 7 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.824838,
   "Latitude": 106.68607
 },
 {
   "STT": 1917,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Anh Tuấn",
   "address": "1607 Huỳnh Tấn Phát, phường Phú Mỹ , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7018404,
   "Latitude": 106.7381763
 },
 {
   "STT": 1918,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Bá Khoa",
   "address": "22A Bùi Hữu Nghĩa, phường 5 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7518876,
   "Latitude": 106.6749111
 },
 {
   "STT": 1919,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Bích Hạnh",
   "address": "41 Đường số 5 - CX Bình Thới,phường 8 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7609249,
   "Latitude": 106.6504454
 },
 {
   "STT": 1920,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Hải Tùng",
   "address": "419 Hòa Hảo, phường 5 , Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7612966,
   "Latitude": 106.6667289
 },
 {
   "STT": 1921,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Hoàng Linh",
   "address": "53/18 Trần Khánh Dư, phường Tân Định , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.794848,
   "Latitude": 106.690155
 },
 {
   "STT": 1922,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Hồng Sơn",
   "address": "115/1A7 Nguyễn Kiệm, phường 3, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8130847,
   "Latitude": 106.6789006
 },
 {
   "STT": 1923,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Khánh Nho",
   "address": "216/4 Lạc Long Quân, phường 10, Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.761526,
   "Latitude": 106.642498
 },
 {
   "STT": 1924,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Kim Ca",
   "address": "40/13A Trần Quang Diệu, phường 14 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.788645,
   "Latitude": 106.6786149
 },
 {
   "STT": 1925,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Mạnh Cường",
   "address": "163/6B Bến Chương Dương,phường Cầu Ông Lãnh , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7579335,
   "Latitude": 106.6893018
 },
 {
   "STT": 1926,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Minh Hảo Hớn",
   "address": "48/4 Hồ Biểu Chánh, phường 11, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.792272,
   "Latitude": 106.674813
 },
 {
   "STT": 1927,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Minh Quang",
   "address": "23/3A Ấp Hưng Lân, Xã Bà Điểm , huyện Hóc Môn , thành phố Hồ Chí Minh",
   "Longtitude": 10.850368,
   "Latitude": 106.602394
 },
 {
   "STT": 1928,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Ngọc Hậu",
   "address": "135/116 Hồng Bàng, phường 6 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.75518,
   "Latitude": 106.640108
 },
 {
   "STT": 1929,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Phước Huy",
   "address": "118 Tên Lửa, phường Bình Trị Đông B , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7424104,
   "Latitude": 106.6140103
 },
 {
   "STT": 1930,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Quang Quý",
   "address": "376/68 Nguyễn Đình Chiểu,phường 4 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7749622,
   "Latitude": 106.6845548
 },
 {
   "STT": 1931,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Quảng Đại",
   "address": "174 Ngô Quyền, phường 5 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7629393,
   "Latitude": 106.6648667
 },
 {
   "STT": 1932,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Quốc Thanh",
   "address": "(Lô A Chung Cư Sư Vạn Hạnh) 3 Sư vạn hạnh, phường 9 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7594886,
   "Latitude": 106.6744111
 },
 {
   "STT": 1933,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Tấn Dũng",
   "address": "1 Trường Sơn, Cư Xá Bắc Hải,phường 15 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7857682,
   "Latitude": 106.6664479
 },
 {
   "STT": 1934,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Tấn Đức",
   "address": "1003 Hồng Bàng, phường 12 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7542894,
   "Latitude": 106.6363997
 },
 {
   "STT": 1935,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Thanh Hải",
   "address": "142 Bình Thới, phường 14 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.767162,
   "Latitude": 106.649035
 },
 {
   "STT": 1936,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Thanh Tâm",
   "address": "71 Hoàng Xuân Nhị, phường Phú Trung, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.776642,
   "Latitude": 106.644026
 },
 {
   "STT": 1937,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Thanh Vũ",
   "address": "696A Nguyễn Duy Trinh, phường Bình Trưng Đông , Quận 2 , thành phố Hồ Chí Minh",
   "Longtitude": 10.78829,
   "Latitude": 106.7723
 },
 {
   "STT": 1938,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Thế Huy",
   "address": "30/101 Tuệ Tĩnh, phường 13 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.763917,
   "Latitude": 106.653795
 },
 {
   "STT": 1939,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Thị Bích Thủy",
   "address": "112 Trần Quốc Thảo, phường 7 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7836039,
   "Latitude": 106.6860811
 },
 {
   "STT": 1940,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Thị Ngọc Dung",
   "address": "27 Thạch Thị Thanh, phường Tân Định , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.790194,
   "Latitude": 106.692628
 },
 {
   "STT": 1941,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Thị Thanh Thúy",
   "address": "172 Lương Nhữ Học, phường 11 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7534234,
   "Latitude": 106.6602117
 },
 {
   "STT": 1942,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Thị Thịnh",
   "address": "31A Nguyễn Kiệm, phường 3 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8128861,
   "Latitude": 106.6785145
 },
 {
   "STT": 1943,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Trần Hải",
   "address": "99/13 Hiệp Thành 35, phường Hiệp Thành , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8695322,
   "Latitude": 106.6450166
 },
 {
   "STT": 1944,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Trương Khương",
   "address": "456/62 Cao Thắng, phường 12 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7700106,
   "Latitude": 106.6824127
 },
 {
   "STT": 1945,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Tường Minh",
   "address": "34A Đường số 9, phường Bình Chiểu , quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8749811,
   "Latitude": 106.7234857
 },
 {
   "STT": 1946,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Tường Đức",
   "address": "120/11 Trần Đình Xu, phường Nguyễn Cư Trinh , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.762739,
   "Latitude": 106.68937
 },
 {
   "STT": 1947,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Văn Bửu",
   "address": "152/5 Nguyễn Ãnh Thủ, Xã Trung Chánh , huyện Hóc Môn , thành phố Hồ Chí Minh",
   "Longtitude": 10.8575403,
   "Latitude": 106.6086392
 },
 {
   "STT": 1948,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Văn Hải",
   "address": "1075 Nguyễn Duy Trinh, phường Long Trường , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.79968,
   "Latitude": 106.8094
 },
 {
   "STT": 1949,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Văn Thức",
   "address": "144A Phan Văn Trị, phường 12, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.81066,
   "Latitude": 106.695947
 },
 {
   "STT": 1950,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Văn Út",
   "address": "237A Tân Kỳ Tân Quý, phường Tây Thạnh, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.8122907,
   "Latitude": 106.6261831
 },
 {
   "STT": 1951,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Văn Đức",
   "address": "400/3 Lê Văn Sỹ, phường 14 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7890219,
   "Latitude": 106.676632
 },
 {
   "STT": 1952,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Vĩnh Phước",
   "address": "91 Tân Vĩnh, phường 6 , Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.759624,
   "Latitude": 106.70013
 },
 {
   "STT": 1953,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Nguyễn Đăng Khuê",
   "address": "78/C18 Phạm Ngũ Lão, phường 3, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8204843,
   "Latitude": 106.6829661
 },
 {
   "STT": 1954,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Phạm Hữu Quốc",
   "address": "151/141A Lũy Bán Bích, phường Hòa Thạnh, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.7794438,
   "Latitude": 106.635535
 },
 {
   "STT": 1955,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Phạm Kiên Hữu",
   "address": "118-120 Nguyễn Văn Cừ, phường Nguyễn Cư Trinh , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.758387,
   "Latitude": 106.684666
 },
 {
   "STT": 1956,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Phạm Ngọc Chất",
   "address": "273 Lãnh Binh Thăng, phường 12, Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.762601,
   "Latitude": 106.651414
 },
 {
   "STT": 1957,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Phạm Phương Mai",
   "address": "19 Lê Đại Hành, phường 4 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.758986,
   "Latitude": 106.658987
 },
 {
   "STT": 1958,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Phạm Thái Quốc Bửu",
   "address": "164D Điện Biên Phủ, phường 6 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.785899,
   "Latitude": 106.692944
 },
 {
   "STT": 1959,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Phạm Thanh Sơn",
   "address": "241 Nguyễn Đình Chiểu, phường 5, Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7736251,
   "Latitude": 106.6855395
 },
 {
   "STT": 1960,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Phạm Tuấn Khoa",
   "address": "36 Đội Cung, phường 11 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7668586,
   "Latitude": 106.649997
 },
 {
   "STT": 1961,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Phạm Đoàn Tấn Tài",
   "address": "C7/6 Ấp 4A, Xã Bình Hưng, huyện Bình Chánh, thành phố Hồ Chí Minh",
   "Longtitude": 10.659865,
   "Latitude": 106.687479
 },
 {
   "STT": 1962,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Phan Công Ánh",
   "address": "124 Đặng Văn Ngữ, phường 14, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.793484,
   "Latitude": 106.668727
 },
 {
   "STT": 1963,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Phan Dư Lê Lợi & Sản Phụ Khoa - BS. Lê Ngọc Anh Thư",
   "address": "9-11 Tuệ Tĩnh, phường 13 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764474,
   "Latitude": 106.653878
 },
 {
   "STT": 1964,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Phan Gia Duy Linh",
   "address": "287/42 Nguyễn Đình Chiểu,phường 5 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.772459,
   "Latitude": 106.6856049
 },
 {
   "STT": 1965,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Phan Ngọc Toàn",
   "address": "150 Lý Thái Tổ, phường 2 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7664565,
   "Latitude": 106.6789865
 },
 {
   "STT": 1966,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Quách Ngọc Minh",
   "address": "363 Quang Trung, phường 10 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8299749,
   "Latitude": 106.6703896
 },
 {
   "STT": 1967,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Quách Tập",
   "address": "237 Lê văn Sỹ, phường 13 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.787044,
   "Latitude": 106.6789299
 },
 {
   "STT": 1968,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Quang",
   "address": "90A Nguyễn Ảnh Thủ, phường Hiệp Thành , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8778189,
   "Latitude": 106.6253775
 },
 {
   "STT": 1969,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Quốc Hưng",
   "address": "367C Bình Đông, phường 15 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.733886,
   "Latitude": 106.635645
 },
 {
   "STT": 1970,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Tạ Văn Thuận",
   "address": "595/36 Nguyễn Đình Chiểu, phường 2 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7675788,
   "Latitude": 106.6810855
 },
 {
   "STT": 1971,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Thái Hữu Dũng",
   "address": "348 Tân Phước, phường 7 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7613201,
   "Latitude": 106.6610794
 },
 {
   "STT": 1972,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Thiều Vĩ Tuấn",
   "address": "22 Nguyễn Huy Lượng, phường 14, quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8057108,
   "Latitude": 106.696346
 },
 {
   "STT": 1973,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Thu Hồng",
   "address": "22 Đường Số 144, phường Tân Phú , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8672708,
   "Latitude": 106.8052763
 },
 {
   "STT": 1974,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Tô Văn Hiền",
   "address": "348/38 Cách Mạng Tháng Tám,phường 10 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.783254,
   "Latitude": 106.67368
 },
 {
   "STT": 1975,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Tống Thị Hồng Lương",
   "address": "140/1 Lý Thường Kiệt, phường 7 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.763675,
   "Latitude": 106.660269
 },
 {
   "STT": 1976,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Trần Anh Tuấn",
   "address": "151 Hòa Bình, phường Tân Thành, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.76624,
   "Latitude": 106.6319
 },
 {
   "STT": 1977,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Trần Kim Phương",
   "address": "736 Quốc Lộ 22, Thị trấn Củ Chi , huyện Củ Chi , thành phố Hồ Chí Minh",
   "Longtitude": 10.972192,
   "Latitude": 106.4965434
 },
 {
   "STT": 1978,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Trần Lê Dung",
   "address": "264 Điện Biên Phủ, phường 17, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.7989877,
   "Latitude": 106.7063198
 },
 {
   "STT": 1979,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Trần Minh Tường",
   "address": "14 Lý Nam Đế, phường 7 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7587702,
   "Latitude": 106.6605558
 },
 {
   "STT": 1980,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Trần Quỳnh Lãm",
   "address": "124 Lê Văn Việt, phường Hiệp Phú, Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8455733,
   "Latitude": 106.7794678
 },
 {
   "STT": 1981,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Trần Thị Mai Phương",
   "address": "33 Đường O, phường Tân Phong ,Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7219153,
   "Latitude": 106.7161585
 },
 {
   "STT": 1982,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Trần Thị Minh Nguyệt",
   "address": "28/49 Nguyễn Cảnh Chân, phường Cầu Kho , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7586352,
   "Latitude": 106.6886436
 },
 {
   "STT": 1983,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Trần Thiện Hảo",
   "address": "40 Lê Đình Cẩn, phường Tân Tạo, quận Bình Tân, thành phố Hồ Chí Minh",
   "Longtitude": 10.7583888,
   "Latitude": 106.6050632
 },
 {
   "STT": 1984,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Trần Đình Khả",
   "address": "24-26 Vũ Huy Tấn, phường 3, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.7957265,
   "Latitude": 106.6949433
 },
 {
   "STT": 1985,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Trịnh Thị Minh Hải",
   "address": "552 Điện Biên Phủ, phường 11 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7718197,
   "Latitude": 106.6777416
 },
 {
   "STT": 1986,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Trọng Minh & Chuyên Khoa Nhi - BS. Phượng Linh",
   "address": "423/18 Nguyễn Kiệm, phường 9, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.8114614,
   "Latitude": 106.678581
 },
 {
   "STT": 1987,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Trương Thị Kim Dung",
   "address": "99B Phạm Văn Chí, phường 3 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7460712,
   "Latitude": 106.6490551
 },
 {
   "STT": 1988,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Từ Thị Kim Phượng",
   "address": "41D Nguyễn Lâm, phường 6 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.762267,
   "Latitude": 106.662825
 },
 {
   "STT": 1989,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Võ Ngọc Hoàn",
   "address": "516 Hậu Giang, phường 12 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7486402,
   "Latitude": 106.6369093
 },
 {
   "STT": 1990,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Võ Ngọc Thảo",
   "address": "638A Hậu Giang, phường 12 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.748065,
   "Latitude": 106.63416
 },
 {
   "STT": 1991,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Vũ Công Trực",
   "address": "310 Hòa Hảo, phường 4 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.762178,
   "Latitude": 106.66923
 },
 {
   "STT": 1992,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Vũ Hải Bằng",
   "address": "359 Huỳnh Văn Bánh, phường 11, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.7919904,
   "Latitude": 106.6723537
 },
 {
   "STT": 1993,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Vũ Hải Long",
   "address": "122/10 Trần Đình Xu, phường Nguyễn Cư Trinh , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.762986,
   "Latitude": 106.688539
 },
 {
   "STT": 1994,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Vũ Văn Khánh",
   "address": "373 Khu Phố 3, Lê Hồng Phong,phường 1 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.766959,
   "Latitude": 106.674357
 },
 {
   "STT": 1995,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Vũ Đình Truy",
   "address": "474/2 Nguyễn Tri Phương, phường 9 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7515539,
   "Latitude": 106.6693503
 },
 {
   "STT": 1996,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Đặng Xuân Hùng",
   "address": "1105 Cách Mạng Tháng 8, phường 7, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.791934,
   "Latitude": 106.6548339
 },
 {
   "STT": 1997,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Đào Thị Ngọc Anh & BS. Đào Trung Trực",
   "address": "18A/34B Nguyễn Thị Minh Khai,phường Đa Kao , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.784342,
   "Latitude": 106.699257
 },
 {
   "STT": 1998,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Đinh Thị Nguyệt",
   "address": "73A Thuận Kiều, phường 12 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7575178,
   "Latitude": 106.6581666
 },
 {
   "STT": 1999,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Đỗ Công Hân",
   "address": "166 Quốc lộ 13, phường Hiệp Bình Chánh , quận Thủ Đức, thành phố Hồ Chí Minh",
   "Longtitude": 10.8457747,
   "Latitude": 106.7312875
 },
 {
   "STT": 2000,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - BS. Đoàn Thế Giáp",
   "address": "67 Đường Số 5, phường Phước Bình , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.817885,
   "Latitude": 106.771858
 },
 {
   "STT": 2001,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng BS Bùi Thái Vi",
   "address": "129B Lê Thị Bạch Cát, phường 11, Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764573,
   "Latitude": 106.650972
 },
 {
   "STT": 2002,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng Hải Ngọc",
   "address": "29/4 Hà Huy Giáp, phường Thạnh Xuân , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8908305,
   "Latitude": 106.6858908
 },
 {
   "STT": 2003,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng Kỹ Thuật Cao - BS. Huỳnh Kiến",
   "address": "87 Thuận Kiều, phường 4 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.758114,
   "Latitude": 106.658252
 },
 {
   "STT": 2004,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng Thiện Tâm",
   "address": "993 Phạm Văn Bạch, phường 12, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8358719,
   "Latitude": 106.6444319
 },
 {
   "STT": 2005,
   "Name": "Phòng Khám Chuyên Khoa Tâm Gia An",
   "address": "122B Trần Đình Xu, phường Nguyễn Cư Trinh , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.762403,
   "Latitude": 106.6886125
 },
 {
   "STT": 2006,
   "Name": "Phòng Khám Chuyên Khoa Tâm Thần",
   "address": "36 Đường số 16 - CX Lữ Gia,phường 15 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7715899,
   "Latitude": 106.6567052
 },
 {
   "STT": 2007,
   "Name": "Phòng Khám Chuyên Khoa Tâm Thần",
   "address": "168/9 Bùi Thị Xuân, phường 3, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.7932783,
   "Latitude": 106.6622899
 },
 {
   "STT": 2008,
   "Name": "Phòng Khám Chuyên Khoa Tâm Thần",
   "address": "13 Hàn Hải Nguyên, phường 16 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.757776,
   "Latitude": 106.650782
 },
 {
   "STT": 2009,
   "Name": "Phòng Khám Chuyên Khoa Tâm Thần - BS. Lê Quốc Nam",
   "address": "5/35 Nơ Trang Long, phường 7, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.805331,
   "Latitude": 106.694266
 },
 {
   "STT": 2010,
   "Name": "Phòng Khám Chuyên Khoa Tâm Thần - BS. Lê Văn Hiến",
   "address": "20 Đường số 10 , Khu phố 1,phường Linh Tây , Thủ Đức , thành phố Hồ Chí Minh",
   "Longtitude": 10.8788912,
   "Latitude": 106.7736987
 },
 {
   "STT": 2011,
   "Name": "Phòng Khám Chuyên Khoa Tâm Thần - BS. Nguyễn Tiến Thành",
   "address": "51/1 Phan Văn Hân, phường 17, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.7924025,
   "Latitude": 106.7080722
 },
 {
   "STT": 2012,
   "Name": "Phòng Khám Chuyên Khoa Tâm Thần - BS. Nguyễn Trung Hoàng",
   "address": "64/2 Lê Trực, phường 7 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8072725,
   "Latitude": 106.6925062
 },
 {
   "STT": 2013,
   "Name": "Phòng Khám Chuyên Khoa Tâm Thần - BS. Trần Minh Khuê",
   "address": "96 Phú Thọ, phường 1 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7575035,
   "Latitude": 106.6417188
 },
 {
   "STT": 2014,
   "Name": "Phòng Khám Chuyên Khoa Tâm Thần Kinh - BS Nguyễn Tú",
   "address": "233/12 Nguyễn Trãi, phường Nguyễn Cư Trinh , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.765048,
   "Latitude": 106.688079
 },
 {
   "STT": 2015,
   "Name": "Phòng Khám Chuyên Khoa Tâm Thần Kinh - BS Đăng Đức",
   "address": "14 - 16 Phạm Đình Hổ, phường 1 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7469768,
   "Latitude": 106.649589
 },
 {
   "STT": 2016,
   "Name": "Phòng Khám Chuyên Khoa Tâm Thần Kinh - BS. Ngô Tích Linh",
   "address": "60/13 Nguyễn Trãi, phường 3 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.758539,
   "Latitude": 106.6799908
 },
 {
   "STT": 2017,
   "Name": "Phòng Khám Chuyên Khoa Tâm Thần Kinh - BS. Lê Hiếu",
   "address": "20 Đào tấn, phường 5 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7507977,
   "Latitude": 106.6750993
 },
 {
   "STT": 2018,
   "Name": "Phòng Khám Chuyên Khoa Tâm Thần Kinh - BS. Nguyễn Thị Giang",
   "address": "139 Đường số 8, phường Tân Thuận Đông , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7496094,
   "Latitude": 106.7401833
 },
 {
   "STT": 2019,
   "Name": "Phòng Khám Chuyên Khoa Tâm Thần Kinh - BS. Nguyễn Đức Tuệ",
   "address": "284/25/8 Lý Thường Kiệt, phường 7 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7571026,
   "Latitude": 106.6617669
 },
 {
   "STT": 2020,
   "Name": "Phòng Khám Chuyên Khoa Tạo Hình & Thẩm Mỹ Thanh Tuyền",
   "address": "247 Đường 3 Tháng 2, phường 10, Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7706881,
   "Latitude": 106.6727134
 },
 {
   "STT": 2021,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh & Động Kinh Trẻ Em - BS. Nguyễn Minh Tuấn",
   "address": "014 Lô D, Chung Cư Lạc Long Quân, phường 5 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7709037,
   "Latitude": 106.6424458
 },
 {
   "STT": 2022,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS Huỳnh Hồng Châu",
   "address": "60/4 Phước Hưng, phường 8 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7557705,
   "Latitude": 106.6688976
 },
 {
   "STT": 2023,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS Lê Thị Cẩm Dung",
   "address": "168/73 Nguyễn Cư Trinh, phường Nguyễn Cư Trinh , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764331,
   "Latitude": 106.689247
 },
 {
   "STT": 2024,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS Nguyễn Văn Tuấn",
   "address": "217 Bãi Sậy, phường 4 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7470883,
   "Latitude": 106.6444867
 },
 {
   "STT": 2025,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS. Bùi Phú Minh",
   "address": "592 Sư Vạn Hạnh, phường 10 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7690461,
   "Latitude": 106.6713736
 },
 {
   "STT": 2026,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS. Hường",
   "address": "17 Phạm Ngũ Lão, phường 7 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.825169,
   "Latitude": 106.68116
 },
 {
   "STT": 2027,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS. Huỳnh Thị Liễu",
   "address": "72 Hưng Phú, phường 8 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.75075,
   "Latitude": 106.681077
 },
 {
   "STT": 2028,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS. Lê Thị Kim Hạnh",
   "address": "225 Hùng Vương, phường 9 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7564303,
   "Latitude": 106.6669767
 },
 {
   "STT": 2029,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS. Lê Xuân Long",
   "address": "696 Nguyễn Chí Thanh, phường 4, Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.757637,
   "Latitude": 106.656739
 },
 {
   "STT": 2030,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS. Lục Đan Mỹ",
   "address": "93 Trần Quang Diệu, phường 14 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.788873,
   "Latitude": 106.677839
 },
 {
   "STT": 2031,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS. Nguyễn Cửu Di",
   "address": "362/1 Lý Thái Tổ, phường 1 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7678978,
   "Latitude": 106.6750647
 },
 {
   "STT": 2032,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS. Nguyễn Tập",
   "address": "171 Nguyễn Đình Chiểu, phường 6, Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7756143,
   "Latitude": 106.6875582
 },
 {
   "STT": 2033,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS. Nguyễn Thị Kim Liên",
   "address": "520C/10 Bis Nguyễn Tri Phương,phường 12 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7626081,
   "Latitude": 106.6682109
 },
 {
   "STT": 2034,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS. Nguyễn Đình Tùng",
   "address": "97A Tân Hòa Đông, phường 14 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.758093,
   "Latitude": 106.630652
 },
 {
   "STT": 2035,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS. Phạm Quỳnh Diệp",
   "address": "125 Lê Quang Định, phường 14, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8059464,
   "Latitude": 106.6976934
 },
 {
   "STT": 2036,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS. Phạm Ý",
   "address": "20 Thuận Kiều, phường 12 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7553638,
   "Latitude": 106.6585944
 },
 {
   "STT": 2037,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS. Tô Phương Vũ",
   "address": "117/14 Bàn Cờ, phường 3 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7696573,
   "Latitude": 106.6807285
 },
 {
   "STT": 2038,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS. Trần Như Thành",
   "address": "1B9 Nguyễn Thái Sơn, phường 3, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8158832,
   "Latitude": 106.6797352
 },
 {
   "STT": 2039,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS. Trần Quang Thắng",
   "address": "62/8 Lữ Gia, phường 15 , Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.771555,
   "Latitude": 106.654006
 },
 {
   "STT": 2040,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS. Trần Tấn Thuyết",
   "address": "296 Phan Đình Phùng, phường 1, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.79766,
   "Latitude": 106.68113
 },
 {
   "STT": 2041,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS. Trương Đình Liêm",
   "address": "137/4 Lê Văn Sỹ, phường 13 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.791569,
   "Latitude": 106.671291
 },
 {
   "STT": 2042,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS. Võ Văn Nho",
   "address": "28/38 Cư Xá Lữ Gia, phường 15 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.773685,
   "Latitude": 106.654793
 },
 {
   "STT": 2043,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS. Vũ Đình Vương",
   "address": "21C5 Đường D1, phường 25 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.801549,
   "Latitude": 106.718778
 },
 {
   "STT": 2044,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS. Đặng Hoàng Hải",
   "address": "18 Trần Bình Trọng, phường 1 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7587539,
   "Latitude": 106.6797501
 },
 {
   "STT": 2045,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS. Đặng Văn Đãi",
   "address": "511 Phan Văn Trị, phường 7 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7545829,
   "Latitude": 106.6747715
 },
 {
   "STT": 2046,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh - BS. Đào Trần Thái",
   "address": "2B Bà Triệu, phường 12 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7575412,
   "Latitude": 106.662697
 },
 {
   "STT": 2047,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh BS Nguyễn Hữu Hồng",
   "address": "74/5 Lê Đại Hành, phường 7 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7593757,
   "Latitude": 106.6592049
 },
 {
   "STT": 2048,
   "Name": "Phòng Khám Chuyên Khoa Thần Kinh Cột Sống 3C Care Chiropractic",
   "address": "B1-32A, Lầu B1 Vincom B Center, 72 Lê Thánh Tôn, phường Bến Nghé , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7780263,
   "Latitude": 106.7020418
 },
 {
   "STT": 2049,
   "Name": "Phòng Khám Chuyên Khoa Thận Tiết Niệu - BS. Nguyễn Văn Ân",
   "address": "49 Đường Số 3, phường 4 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7726281,
   "Latitude": 106.6824804
 },
 {
   "STT": 2050,
   "Name": "Phòng Khám Chuyên Khoa Thấp Khớp - Bác Sĩ Đại Phi Vân",
   "address": "24 Trần Hưng Đạo B, phường 7 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7530515,
   "Latitude": 106.6713022
 },
 {
   "STT": 2051,
   "Name": "Phòng Khám Chuyên Khoa Thiên Ân",
   "address": "33 Bùi Văn Ba, phường Tân Thuận Đông , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7492931,
   "Latitude": 106.7323463
 },
 {
   "STT": 2052,
   "Name": "Phòng Khám Chuyên Khoa Tiết Niệu",
   "address": "56/29 Cư Xá Lữ Gia, phường 15 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.772216,
   "Latitude": 106.653993
 },
 {
   "STT": 2053,
   "Name": "Phòng Khám Chuyên Khoa Tiết Niệu & Nội Tiết - BS. Trần Văn Sáng",
   "address": "17K/30-32 Dương Đình Nghệ,phường 8 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.760065,
   "Latitude": 106.649469
 },
 {
   "STT": 2054,
   "Name": "Phòng Khám Chuyên Khoa Tiết Niệu - BS. Nguyễn Minh Quang",
   "address": "202 – 204 Bà Hạt, phường 9 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7664512,
   "Latitude": 106.6705
 },
 {
   "STT": 2055,
   "Name": "Phòng Khám Chuyên Khoa Tiết Niệu - BS. Nguyễn Phúc Cẩm Hoàng",
   "address": "57/650 Nguyễn Oanh, phường 17, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.848348,
   "Latitude": 106.678287
 },
 {
   "STT": 2056,
   "Name": "Phòng Khám Chuyên Khoa Tiết Niệu - BS. Nguyễn Tiến Đệ",
   "address": "287/49 Nguyễn Đình Chiểu,phường 5 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7725266,
   "Latitude": 106.6857728
 },
 {
   "STT": 2057,
   "Name": "Phòng Khám Chuyên Khoa Tiết Niệu - BS. Nguyễn Tuấn Vinh",
   "address": "95 Trần Quý, phường 4 , Quận 11, thành phố Hồ Chí Minh",
   "Longtitude": 10.75899,
   "Latitude": 106.65637
 },
 {
   "STT": 2058,
   "Name": "Phòng Khám Chuyên Khoa Tiết Niệu - BS. Trương Hoàng Minh",
   "address": "429/3 Hoàng văn Thụ, phường 2, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.798728,
   "Latitude": 106.659622
 },
 {
   "STT": 2059,
   "Name": "Phòng Khám Chuyên Khoa Tiết Niệu - BS. Đào Quang Oánh",
   "address": "243/9/11 Tô Hiến Thành, phường 13 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7784915,
   "Latitude": 106.6677229
 },
 {
   "STT": 2060,
   "Name": "Phòng Khám Chuyên Khoa Tiết Niệu - BS. Đỗ Hoàng Dũng",
   "address": "575/56 Điện Biên Phủ, phường 1 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.768066,
   "Latitude": 106.6777898
 },
 {
   "STT": 2061,
   "Name": "Phòng Khám Chuyên Khoa Tiết Niệu - Nội Tổng Quát - BS. Nguyễn Xuân Huy",
   "address": "387 Hàn Hải Nguyên, phường 2 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.758354,
   "Latitude": 106.642342
 },
 {
   "STT": 2062,
   "Name": "Phòng Khám Chuyên Khoa Tiêu Hóa & Gan Mật - BS. Đỗ Quang Huy",
   "address": "371 Hàn Hải Nguyên, phường 6 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7583439,
   "Latitude": 106.642676
 },
 {
   "STT": 2063,
   "Name": "Phòng Khám Chuyên Khoa Tiêu Hóa - BS. Lâm Việt Trung",
   "address": "210 Nguyễn Chí Thanh, phường 4 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7605357,
   "Latitude": 106.6702234
 },
 {
   "STT": 2064,
   "Name": "Phòng Khám Chuyên Khoa Tiêu Hóa - BS. Lê Thành Lý",
   "address": "53 Lê Đại Hành, phường 6 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7600242,
   "Latitude": 106.6584658
 },
 {
   "STT": 2065,
   "Name": "Phòng Khám Chuyên Khoa Tiểu Đường - BS Lê Bạch Lan",
   "address": "927/4 Cách Mạng Tháng Tám,phường 7, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.7896439,
   "Latitude": 106.658758
 },
 {
   "STT": 2066,
   "Name": "Phòng Khám Chuyên Khoa Tim - BS. Lê Kim Tuyến",
   "address": "676/40 Lê Quang Định, phường 1, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8211242,
   "Latitude": 106.6903406
 },
 {
   "STT": 2067,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch",
   "address": "61B Bình Giã, phường 13, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.80132,
   "Latitude": 106.644619
 },
 {
   "STT": 2068,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch",
   "address": "726 Tân Kỳ Tân Quý, phường Bình Hưng Hòa , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.8026884,
   "Latitude": 106.6026064
 },
 {
   "STT": 2069,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch",
   "address": "14 Võ Thành Trang, phường 11, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.789094,
   "Latitude": 106.6470329
 },
 {
   "STT": 2070,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch",
   "address": "8B Cư xá Phan Đăng Lưu, phường 3 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.802113,
   "Latitude": 106.6958391
 },
 {
   "STT": 2071,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch",
   "address": "58 Lê Đại Hành, phường 7 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7591961,
   "Latitude": 106.6591243
 },
 {
   "STT": 2072,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch & Chuyên Khoa Cơ Xương Khớp - BS. Trần Thị Thúy Hằng",
   "address": "30 Phạm Đôn, phường 10 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.751567,
   "Latitude": 106.6634395
 },
 {
   "STT": 2073,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch & Nội Tổng Quát - BS. Nguyễn Hữu Bách",
   "address": "23 Dương Đình Hội, phường Phước Long B , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8174571,
   "Latitude": 106.7754982
 },
 {
   "STT": 2074,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Hồ Hữu Phước",
   "address": "202 Nguyễn Trọng Tuyển, phường 8 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.7977907,
   "Latitude": 106.674108
 },
 {
   "STT": 2075,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Hoàng Chương",
   "address": "6/10E Lê Lợi, Xã Tân Hiệp , huyện Hóc Môn , thành phố Hồ Chí Minh",
   "Longtitude": 10.8971921,
   "Latitude": 106.581217
 },
 {
   "STT": 2076,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Huỳnh Thị Hồng Anh",
   "address": "328 Trần Hưng Đạo, phường 11 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7522648,
   "Latitude": 106.6622255
 },
 {
   "STT": 2077,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Lê Quan Minh",
   "address": "529/44 Huỳnh Văn Bánh, phường 14 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.791592,
   "Latitude": 106.6681854
 },
 {
   "STT": 2078,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Lê Thành Khánh Vân",
   "address": "40/20 Tân Hưng, phường 12 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7555079,
   "Latitude": 106.6596992
 },
 {
   "STT": 2079,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Nguyễn Minh Thu",
   "address": "163/3 Lý Nam Đế, phường 7 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7619417,
   "Latitude": 106.6590968
 },
 {
   "STT": 2080,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Nguyễn Ngọc Quế",
   "address": "183/20 Hậu Giang, phường 5 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.748956,
   "Latitude": 106.645072
 },
 {
   "STT": 2081,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Nguyễn Phú",
   "address": "J4 Kinh Dương Vương, Cư Xá Phú Lâm A, phường 12 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7502237,
   "Latitude": 106.6318057
 },
 {
   "STT": 2082,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Nguyễn Thị Thanh Lan",
   "address": "34/41 Lữ Gia, phường 15 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7706046,
   "Latitude": 106.6560934
 },
 {
   "STT": 2083,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Nguyễn Thị Thúy Hằng",
   "address": "188/1 Thành Thái, phường 12 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7690393,
   "Latitude": 106.6657766
 },
 {
   "STT": 2084,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Nguyễn Văn Mai",
   "address": "10 Đỗ Đức Dục, phường Phú Thạnh, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.7806735,
   "Latitude": 106.6272192
 },
 {
   "STT": 2085,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Nguyễn Văn Yêm",
   "address": "83 Nguyễn Thị Tú, phường Bình Hưng Hòa B , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.8158389,
   "Latitude": 106.5982647
 },
 {
   "STT": 2086,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Nguyễn Văn Đông",
   "address": "49/18 Huỳnh Mẫn Đạt, phường 19, quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.790568,
   "Latitude": 106.709124
 },
 {
   "STT": 2087,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Nguyễn Xuân Tuấn Anh",
   "address": "87A Tô Hiến Thành, phường 13 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.781604,
   "Latitude": 106.670644
 },
 {
   "STT": 2088,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Phạm Nguyễn Vinh",
   "address": "9 Đông Sơn, phường 7 , quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.786744,
   "Latitude": 106.6549919
 },
 {
   "STT": 2089,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Phạm Xuân Hậu",
   "address": "336A Phan Văn Trị, phường 11, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8193753,
   "Latitude": 106.6946195
 },
 {
   "STT": 2090,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Phạm Đức Đạt",
   "address": "118/5 Trần Quang Diệu, phường 14 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.790119,
   "Latitude": 106.6783729
 },
 {
   "STT": 2091,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Thân Trọng Minh",
   "address": "559/18 Trần Hưng Đạo, phường Cầu Kho , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7595966,
   "Latitude": 106.6894191
 },
 {
   "STT": 2092,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Trần Hòa",
   "address": "43/11 Lý Nam Đế, phường 7 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7585931,
   "Latitude": 106.6604315
 },
 {
   "STT": 2093,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Trần Thị Tuyết Lan",
   "address": "16/63 Tuệ Tĩnh, phường 13 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764102,
   "Latitude": 106.653544
 },
 {
   "STT": 2094,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Trương Quang Bình",
   "address": "89 Nơ Trang Long, phường 11, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8096046,
   "Latitude": 106.6950226
 },
 {
   "STT": 2095,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Võ Mỹ Phượng",
   "address": "58 Trần Bình Trọng, phường 1 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7539886,
   "Latitude": 106.6820403
 },
 {
   "STT": 2096,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Võ Văn Phan",
   "address": "4 Nguyễn Trọng Tuyển, phường 15 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.797254,
   "Latitude": 106.680818
 },
 {
   "STT": 2097,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Đào Thị Hoài Nguyên",
   "address": "66/58D Lãnh Binh Thăng, phường 13 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.762632,
   "Latitude": 106.655141
 },
 {
   "STT": 2098,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Đào Thị Thanh Bình",
   "address": "69-71 Bàu Cát 2, phường 10, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.794611,
   "Latitude": 106.640905
 },
 {
   "STT": 2099,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Đỗ Kim Quế",
   "address": "187/10 Lương Nhữ Học, phường 11 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.754331,
   "Latitude": 106.6599184
 },
 {
   "STT": 2100,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Đỗ Quang Huân",
   "address": "528/5/47 Điện Biên Phủ, phường 11 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7707069,
   "Latitude": 106.6770595
 },
 {
   "STT": 2101,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - BS. Đỗ Thị Kim Chi",
   "address": "5A Trần Đình Xu, phường Nguyễn Cư Trinh , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.763576,
   "Latitude": 106.68769
 },
 {
   "STT": 2102,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - GS. TS. BS. Nguyễn Khánh Dư & TS. BS. Nguyễn Khánh Việt",
   "address": "15 Lê Đại Hành, phường 4 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.758834,
   "Latitude": 106.659012
 },
 {
   "STT": 2103,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch - The Beauty Heart Clinic",
   "address": "162 Nguyễn Thiện Thuật, phường 3 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7684288,
   "Latitude": 106.6795918
 },
 {
   "STT": 2104,
   "Name": "Phòng Khám Chuyên Khoa Tim Mạch Hồng Tâm",
   "address": "105 Thành Thái, phường 14 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7739519,
   "Latitude": 106.6644503
 },
 {
   "STT": 2105,
   "Name": "Phòng Khám Chuyên Khoa Tổng Quát",
   "address": "99 - 101 Lãnh Binh Thăng,phường 12 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.762287,
   "Latitude": 106.654323
 },
 {
   "STT": 2106,
   "Name": "Phòng Khám Chuyên Khoa Trĩ - BS. Cao Ngọc Khánh",
   "address": "706F Tên Lửa, phường Bình Trị Đông B , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7558215,
   "Latitude": 106.6061189
 },
 {
   "STT": 2107,
   "Name": "Phòng Khám Chuyên Khoa Trị Liệu Thần Kinh Cột Sống Hoa Kỳ (ACC) - CS Nguyễn Trãi",
   "address": "133 Nguyễn Trãi, phường 2 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7575424,
   "Latitude": 106.6792784
 },
 {
   "STT": 2108,
   "Name": "Phòng Khám Chuyên Khoa Trị Liệu Thần Kinh Cột Sống Hoa Kỳ (ACC) - CS Nguyễn Du",
   "address": "99 Nguyễn Du, phường Bến Thành, Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7750494,
   "Latitude": 106.6968165
 },
 {
   "STT": 2109,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu",
   "address": "138 Nhật Tảo, phường 8 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7647615,
   "Latitude": 106.6676062
 },
 {
   "STT": 2110,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS Nguyễn Hồng Ri",
   "address": "366 Hai Bà Trưng, phường Tân Định , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.790503,
   "Latitude": 106.688907
 },
 {
   "STT": 2111,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Bùi Xuân Trường",
   "address": "187 Lê Quang Định, phường 7, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.80854,
   "Latitude": 106.694276
 },
 {
   "STT": 2112,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Cao Anh Tiến",
   "address": "100 Trần Văn Kỷ, phường 14, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8042923,
   "Latitude": 106.6959617
 },
 {
   "STT": 2113,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Cúc",
   "address": "126 Trần Văn Kỷ, phường 14, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8042941,
   "Latitude": 106.6959446
 },
 {
   "STT": 2114,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Diệp Bảo Tuấn",
   "address": "154 Nguyễn Thượng Hiền, phường 5 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.809575,
   "Latitude": 106.683873
 },
 {
   "STT": 2115,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Huỳnh Hồng Hạnh",
   "address": "186/1 Trần Kế Xương, phường 7, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.8008515,
   "Latitude": 106.690384
 },
 {
   "STT": 2116,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Lâm Đức Hoàng",
   "address": "81/9 Hoàng Hoa Thám, phường 6, quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.806339,
   "Latitude": 106.6898439
 },
 {
   "STT": 2117,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Lê Hoàng Chương",
   "address": "32 Nguyễn Huy Lượng, phường 14, quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8056666,
   "Latitude": 106.696059
 },
 {
   "STT": 2118,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Lê Hồng Cúc",
   "address": "206 Trần Bình Trọng, phường 4 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7606345,
   "Latitude": 106.6800459
 },
 {
   "STT": 2119,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bs. Liêm & BS. Sung",
   "address": "26 Nơ Trang Long, phường 14, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8050964,
   "Latitude": 106.694995
 },
 {
   "STT": 2120,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Lưu Văn Minh",
   "address": "138 Nhật Tảo, phường 8 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7647615,
   "Latitude": 106.6676062
 },
 {
   "STT": 2121,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Nguyễn Châu Hiếu",
   "address": "64 Âu Dương Lân, phường 3 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7470087,
   "Latitude": 106.6821769
 },
 {
   "STT": 2122,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Nguyễn Hải Nam",
   "address": "90 Bàu Cát, phường 14 , quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.7923971,
   "Latitude": 106.6422167
 },
 {
   "STT": 2123,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Nguyễn Hữu Phúc",
   "address": "144 Nguyễn Trọng Tuyển, phường 8 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.7976379,
   "Latitude": 106.676393
 },
 {
   "STT": 2124,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Nguyễn Quốc Điền",
   "address": "101 Đặng Văn Ngữ, phường 14, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.792563,
   "Latitude": 106.668304
 },
 {
   "STT": 2125,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Nguyễn Thái Bình",
   "address": "48 Nguyễn Huy Lượng, phường 14, quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8054969,
   "Latitude": 106.6952828
 },
 {
   "STT": 2126,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Nguyễn Thế Hiển",
   "address": "16 Nguyễn Huy Lượng, phường 14, quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8058035,
   "Latitude": 106.6965375
 },
 {
   "STT": 2127,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Nguyễn Đỗ Thùy Giang",
   "address": "27/24 Đường Số 4, phường 10, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.828346,
   "Latitude": 106.6774939
 },
 {
   "STT": 2128,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Phạm Duy Hoàng",
   "address": "20 Nguyễn Huy Lượng, phường 14, quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8056856,
   "Latitude": 106.6964561
 },
 {
   "STT": 2129,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Phạm Hùng Cường",
   "address": "41 Nguyễn Phi Khanh, phường Tân Định , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.791653,
   "Latitude": 106.694443
 },
 {
   "STT": 2130,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Phạm Thiên Hương",
   "address": "48/10C Nguyễn Thái Sơn, phường 3 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.82007,
   "Latitude": 106.684144
 },
 {
   "STT": 2131,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Phạm Văn Bùng",
   "address": "106 đường số 5, phường Bình Trị Đông B , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7502166,
   "Latitude": 106.6138092
 },
 {
   "STT": 2132,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Phạm Xuân Dũng",
   "address": "492 Lê Quang Định, phường 11, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8147428,
   "Latitude": 106.6896154
 },
 {
   "STT": 2133,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Phan Văn Bùng",
   "address": "106 Đường số 5, phường Bình Trị Đông B , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7502166,
   "Latitude": 106.6138092
 },
 {
   "STT": 2134,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Phó Đức Mẫn",
   "address": "180A Nguyễn Đình Chính, phường 11 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.794472,
   "Latitude": 106.6739149
 },
 {
   "STT": 2135,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Trần Thanh Phương",
   "address": "38/88C Lãnh Binh Thăng, phường 13 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.76277,
   "Latitude": 106.6557909
 },
 {
   "STT": 2136,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Trần Thị Anh Tường",
   "address": "43/14 Nơ Trang Long, phường 7, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.807916,
   "Latitude": 106.694567
 },
 {
   "STT": 2137,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Trần Việt Thế Phương",
   "address": "47/11 Phan Văn Trị, phường 14, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.8066376,
   "Latitude": 106.6975202
 },
 {
   "STT": 2138,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Trịnh Thị Minh Châu",
   "address": "531 Điện Biên Phủ, phường 3 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7709052,
   "Latitude": 106.6772363
 },
 {
   "STT": 2139,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Trương Công Gia Thuận & BS. Nguyễn Thị Hồng Ánh",
   "address": "204 Trường Chinh, phường Tân Hưng Thuận , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8336208,
   "Latitude": 106.6208684
 },
 {
   "STT": 2140,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Võ Kim Điền",
   "address": "65 Triệu Quang Phục, phường 10 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7513089,
   "Latitude": 106.661399
 },
 {
   "STT": 2141,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Võ Ngọc Đức",
   "address": "8/4 Ký Con, phường 7 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.803298,
   "Latitude": 106.6850879
 },
 {
   "STT": 2142,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Võ Thanh Nhân",
   "address": "529/3 Nguyễn Tri Phương, phường 8 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764593,
   "Latitude": 106.6684439
 },
 {
   "STT": 2143,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Võ Đức Hiếu",
   "address": "51 Cao Thắng, phường 11 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7704573,
   "Latitude": 106.6815564
 },
 {
   "STT": 2144,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Vũ Văn Vũ",
   "address": "220/42 Lê Văn Sỹ, phường 14 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.787024,
   "Latitude": 106.682414
 },
 {
   "STT": 2145,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Đỗ Tường Huân",
   "address": "182/15A Lê Văn Sỹ, phường 10, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.793596,
   "Latitude": 106.670657
 },
 {
   "STT": 2146,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS. Đoàn Minh Trông",
   "address": "450 Lê Hồng Phong, phường 1 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7656288,
   "Latitude": 106.6752605
 },
 {
   "STT": 2147,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - BS.Trần Đặng Ngọc Linh",
   "address": "380/6 Nơ Trang Long, phường 13, quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8223293,
   "Latitude": 106.7078675
 },
 {
   "STT": 2148,
   "Name": "Phòng Khám Chuyên Khoa Vật Lý Trị Liệu - BS. Lê Hữu Lợi",
   "address": "950 Nguyễn Kiệm, phường 3 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8243738,
   "Latitude": 106.6793667
 },
 {
   "STT": 2149,
   "Name": "Phòng Khám Chuyên Khoa Vật Lý Trị Liệu Bs. Lê Hữu Lợi",
   "address": "788/15E Nguyễn Kiệm, phường 3, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8150121,
   "Latitude": 106.6786247
 },
 {
   "STT": 2150,
   "Name": "Phòng Khám Chuyên Khoa Xét Nghiệm",
   "address": "255 Lý Thường Kiệt, phường 15 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7727153,
   "Latitude": 106.6573292
 },
 {
   "STT": 2151,
   "Name": "Phòng Khám Chuyên Khoa Xét Nghiệm",
   "address": "32/16 Nguyễn Huy Lượng, phường 14 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8058035,
   "Latitude": 106.6965375
 },
 {
   "STT": 2152,
   "Name": "Phòng Khám Chuyên Khoa Xét Nghiệm",
   "address": "410 Hòa Hảo, phường 5 , Quận 10, thành phố Hồ Chí Minh",
   "Longtitude": 10.7616729,
   "Latitude": 106.6671078
 },
 {
   "STT": 2153,
   "Name": "Phòng Khám Chuyên Khoa Xét Nghiệm - BS. Vũ Thường Vụ",
   "address": "824/10 Sư Vạn Hạnh, phường 12 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7748226,
   "Latitude": 106.6682775
 },
 {
   "STT": 2154,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp",
   "address": "257/27 Lý Thường Kiệt, phường 15 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.77244,
   "Latitude": 106.65686
 },
 {
   "STT": 2155,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp",
   "address": "143/2/26 Phạm Huy Thông,phường 6 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8337696,
   "Latitude": 106.682176
 },
 {
   "STT": 2156,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp",
   "address": "175 Tân Phước, phường 6 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7619578,
   "Latitude": 106.6643661
 },
 {
   "STT": 2157,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp",
   "address": "442/1 Sư Vạn Hạnh, phường 9 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7697406,
   "Latitude": 106.6706615
 },
 {
   "STT": 2158,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - BS Ngô Mạnh Hùng",
   "address": "48 Nguyễn Văn Công, phường 3, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8245713,
   "Latitude": 106.6778181
 },
 {
   "STT": 2159,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - BS Ngô Thành Ý",
   "address": "57 Bàu Cát 1, phường 14, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.794355,
   "Latitude": 106.6428882
 },
 {
   "STT": 2160,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - BS. Chữ",
   "address": "343 Vườn Lài, phường Phú Thọ Hòa, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.787677,
   "Latitude": 106.626046
 },
 {
   "STT": 2161,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - BS. Hiếu",
   "address": "235/10 Thích Quảng Đức, phường 4 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.807411,
   "Latitude": 106.679066
 },
 {
   "STT": 2162,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - BS. Huỳnh Văn Khoa",
   "address": "104/55 Thành Thái, phường 12 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7726323,
   "Latitude": 106.6649801
 },
 {
   "STT": 2163,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - BS. Nguyễn Quốc Trị",
   "address": "75 Bàn Cờ, phường 3 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7704706,
   "Latitude": 106.6794433
 },
 {
   "STT": 2164,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - BS. Nguyễn Văn Thái",
   "address": "1340/5 Ba Tháng Hai, phường 2 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7569619,
   "Latitude": 106.6467764
 },
 {
   "STT": 2165,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - BS. Nguyễn Văn Thanh",
   "address": "25/17 Trần Quý, phường 4 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.759346,
   "Latitude": 106.658195
 },
 {
   "STT": 2166,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - BS. Phan Dư Lê Thắng",
   "address": "142 Đặng Nguyên Cẩn, phường 13 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7547631,
   "Latitude": 106.631826
 },
 {
   "STT": 2167,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - BS. Trần Văn Tân & Mắt - BS. Lâm Kim Phụng",
   "address": "270K Võ Thị Sáu, phường 7 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.780359,
   "Latitude": 106.6844825
 },
 {
   "STT": 2168,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - BS. Trang Mạnh Khôi",
   "address": "248 Nhật Tảo, phường 8 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7638321,
   "Latitude": 106.6643729
 },
 {
   "STT": 2169,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - BS. Trung",
   "address": "139 Đặng Chất, phường 2 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.747465,
   "Latitude": 106.68498
 },
 {
   "STT": 2170,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - BS. Trương Minh Hổ",
   "address": "442 Sư Vạn Hạnh, phường 9 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7657203,
   "Latitude": 106.672002
 },
 {
   "STT": 2171,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - BS. Trương Trọng Tín",
   "address": "191 Nguyễn Lâm, phường 6 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7650669,
   "Latitude": 106.6630685
 },
 {
   "STT": 2172,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - BS. Vũ Văn Nghĩa",
   "address": "375A Bến Bình Đông, phường 15 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.732822,
   "Latitude": 106.634764
 },
 {
   "STT": 2173,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - BS. Đặng Ngọc Hải",
   "address": "91M Lý Thường Kiệt, phường 7 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7612104,
   "Latitude": 106.6606605
 },
 {
   "STT": 2174,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - BS. Đỗ Thế Tản",
   "address": "141 Trần Nhân Tông, phường 2 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.762465,
   "Latitude": 106.674656
 },
 {
   "STT": 2175,
   "Name": "Phòng Khám Chuyên Khoa Y Học Cổ Truyền - BS. Nguyễn Thị Kim Ngân",
   "address": "181 Thái Phiên, phường 9 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.75993,
   "Latitude": 106.646085
 },
 {
   "STT": 2176,
   "Name": "Phòng Khám Chuyên Khoa Y Học Cổ Truyền - Lương Y Phòng Minh Đường",
   "address": "113G/1Bis-3 Lạc Long Quân,phường 3 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764313,
   "Latitude": 106.64199
 },
 {
   "STT": 2177,
   "Name": "Phòng Khám Chuyên Khoa Y Học Dân Tộc BS. Đỗ Hữu Định",
   "address": "181 Nam Kỳ Khởi Nghĩa, phường 7, Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.785606,
   "Latitude": 106.688042
 },
 {
   "STT": 2178,
   "Name": "Phòng Khám Chuyên Khoa Điều Trị Kết Hợp Vật Lý Trị Liệu - BS. Năm",
   "address": "860/24 Xô Viết Nghệ Tĩnh,phường 25 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8025179,
   "Latitude": 106.71249
 },
 {
   "STT": 2179,
   "Name": "Phòng Khám Chuyên Khoa Đột Quỵ - BS. Trần Chí Cường",
   "address": "53 Phạm Hữu Chí, phường 12 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.755753,
   "Latitude": 106.6591057
 },
 {
   "STT": 2180,
   "Name": "Phòng Khám Chuyên Nội Khoa Nam Hiếm Muộn - BS. Võ Thanh Đạm",
   "address": "257 Vĩnh Viễn, phường 4 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.763791,
   "Latitude": 106.668507
 },
 {
   "STT": 2181,
   "Name": "Phòng Khám Chuyên Phụ Khoa - BS. Hồ Thị Thanh",
   "address": "103/68 Đặng Chất, phường 2 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.746662,
   "Latitude": 106.684621
 },
 {
   "STT": 2182,
   "Name": "Phòng Khám Cơ Xương Khớp - BS. Trần Văn Dương",
   "address": "145 Vườn Lài, phường Phú Thọ Hòa, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.787824,
   "Latitude": 106.631505
 },
 {
   "STT": 2183,
   "Name": "Phòng Khám Dạ Dày-Ruột-Gan Mật - BS Trần Thiện Trung",
   "address": "44 Phước Hưng, phường 8 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7555543,
   "Latitude": 106.6688286
 },
 {
   "STT": 2184,
   "Name": "Phòng Khám Da Grace - Grace Skincare Clinic",
   "address": "Mayfair Suites – WMC Tower, 102C Cống Quỳnh, phường Phạm Ngũ Lão , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7650137,
   "Latitude": 106.6906477
 },
 {
   "STT": 2185,
   "Name": "Phòng Khám Da Liễu - BS Nguyễn Thị Diệu My",
   "address": "47A Lê Bình, phường 4 , quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.7937117,
   "Latitude": 106.6568769
 },
 {
   "STT": 2186,
   "Name": "Phòng Khám Da Liễu - BS Trịnh Văn Chức",
   "address": "63 Trường Chinh, phường 12, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.793803,
   "Latitude": 106.65132
 },
 {
   "STT": 2187,
   "Name": "Phòng Khám Da Liễu - BS. Nguyễn Thị Bích Liên",
   "address": "58B Cao Thắng, phường 5 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7697855,
   "Latitude": 106.6827173
 },
 {
   "STT": 2188,
   "Name": "Phòng Khám Da Liễu - BS. Trần Thị Thùy Linh",
   "address": "11 Trần Khánh Dư, phường Tân Định , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.793867,
   "Latitude": 106.688799
 },
 {
   "STT": 2189,
   "Name": "Phòng Khám Da Liễu 199",
   "address": "199 Huỳnh Văn Bánh, phường 12, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.7920459,
   "Latitude": 106.6780859
 },
 {
   "STT": 2190,
   "Name": "Phòng Khám Da Liễu Aurora",
   "address": "A408TM Hưng Phát Silver Star, 156A Nguyễn Hữu Thọ, Xã Phước Kiển , Huyện Nhà Bè , thành phố Hồ Chí Minh",
   "Longtitude": 10.70719,
   "Latitude": 106.7057733
 },
 {
   "STT": 2191,
   "Name": "Phòng Khám Da Liễu BS. Trần Quốc Long",
   "address": "6/15 Nguyễn Thị Minh Khai,phường Đa Kao , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7855535,
   "Latitude": 106.7007414
 },
 {
   "STT": 2192,
   "Name": "Phòng Khám Da Liễu Dermatol Master",
   "address": "51/4/5 Thành Thái, phường 14 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7681902,
   "Latitude": 106.6668809
 },
 {
   "STT": 2193,
   "Name": "Phòng Khám Da Liễu Doctor Scar",
   "address": "SS1N Hồng Lĩnh, phường 15 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7799988,
   "Latitude": 106.6636273
 },
 {
   "STT": 2194,
   "Name": "Phòng Khám Da Liễu Hưng Thịnh",
   "address": "139 Tam Châu, Khu phố 2,phường Tam Bình , Thủ Đức , thành phố Hồ Chí Minh",
   "Longtitude": 10.8694571,
   "Latitude": 106.743726
 },
 {
   "STT": 2195,
   "Name": "Phòng Khám Da Liễu SkinOne",
   "address": "200 Tô Hiến Thành, phường 15 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.781734,
   "Latitude": 106.668482
 },
 {
   "STT": 2196,
   "Name": "Phòng Khám Da Liễu Đồng Diều",
   "address": "202 Phạm Hùng, phường 5 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7408475,
   "Latitude": 106.6687817
 },
 {
   "STT": 2197,
   "Name": "Phòng Khám Dr.Heckmann",
   "address": "4 Đoàn Thị Điểm, phường 1 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.7992927,
   "Latitude": 106.6812885
 },
 {
   "STT": 2198,
   "Name": "Phòng Khám FV",
   "address": "Lầu 3, tòa nhà Bitexco Financial Tower 2, 2 Hải Triều, phường Bến Nghé , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7716446,
   "Latitude": 106.7046532
 },
 {
   "STT": 2199,
   "Name": "Phòng Khám Gan - Mật Sài Gòn",
   "address": "31-33 Phạm Hữu Chí, phường 12 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7558245,
   "Latitude": 106.6594544
 },
 {
   "STT": 2200,
   "Name": "Phòng Khám Hải Thoa",
   "address": "6 Nguyễn Văn Nghi, phường 5, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.821339,
   "Latitude": 106.689049
 },
 {
   "STT": 2201,
   "Name": "Phòng Khám Hạnh Lâm",
   "address": "378 Phạm Hùng, phường 5 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7365681,
   "Latitude": 106.6714765
 },
 {
   "STT": 2202,
   "Name": "Phòng Khám Hiếm Muộn & Sản Phụ Khoa - BS. Cù Thị Kim Loan",
   "address": "52 Đường số 11, Khu dân cư Him Lam, phường Tân Hưng , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7392301,
   "Latitude": 106.6959206
 },
 {
   "STT": 2203,
   "Name": "Phòng Khám Hiền Lương",
   "address": "755 Trường Chinh, phường Tây Thạnh, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.8186186,
   "Latitude": 106.6310521
 },
 {
   "STT": 2204,
   "Name": "Phòng Khám Hiện Đại Quốc Tế GMCC",
   "address": "65 Hoàng Hoa Thám, phường 6, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.806085,
   "Latitude": 106.690199
 },
 {
   "STT": 2205,
   "Name": "Phòng Khám Hoàn Mỹ Sài Gòn",
   "address": "4A Hoàng Việt, phường 4, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.7977292,
   "Latitude": 106.6587109
 },
 {
   "STT": 2206,
   "Name": "Phòng Khám Hoàng Diệu",
   "address": "390 Hoàng Diệu, phường 5 , Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7599485,
   "Latitude": 106.6986432
 },
 {
   "STT": 2207,
   "Name": "Phòng Khám Hoàng Gia",
   "address": "44A Nguyễn Văn Đậu, phường 6, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.806495,
   "Latitude": 106.6871409
 },
 {
   "STT": 2208,
   "Name": "Phòng Khám Hoàng Mai",
   "address": "1 Trần Thị Nghỉ, phường 7 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.827086,
   "Latitude": 106.681102
 },
 {
   "STT": 2209,
   "Name": "Phòng Khám Hoàng Nguyên",
   "address": "294 Huỳnh Tấn Phát, phường Tân Thuận Tây , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7513772,
   "Latitude": 106.7285546
 },
 {
   "STT": 2210,
   "Name": "Phòng Khám Hoàng Vũ",
   "address": "630-632 Hưng Phú, phường 10 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.746321,
   "Latitude": 106.668594
 },
 {
   "STT": 2211,
   "Name": "Phòng Khám Hội Chữ Thập Đỏ Quận 5",
   "address": "251B Trần Phú, phường 9 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7580677,
   "Latitude": 106.6749994
 },
 {
   "STT": 2212,
   "Name": "Phòng Khám Hội Chữ Thập Đỏ Quận 8",
   "address": "183 Đường Số 8, phường 4 , Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.741842,
   "Latitude": 106.669951
 },
 {
   "STT": 2213,
   "Name": "Phòng Khám Hồng Châu",
   "address": "133 Nguyễn Thị Định, phường Bình Trưng Tây , Quận 2 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7881014,
   "Latitude": 106.7553413
 },
 {
   "STT": 2214,
   "Name": "Phòng Khám Hồng Sáng",
   "address": "425 Mã Lò, phường Bình Trị Đông A , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7788,
   "Latitude": 106.600035
 },
 {
   "STT": 2215,
   "Name": "Phòng Khám Hồng Đức",
   "address": "178A Thới An 32, phường Thới An, Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8787829,
   "Latitude": 106.6510838
 },
 {
   "STT": 2216,
   "Name": "Phòng Khám Huệ Đức",
   "address": "52 Cao Xuân Dục, phường 13 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7460126,
   "Latitude": 106.658964
 },
 {
   "STT": 2217,
   "Name": "Phòng Khám Huy Liệu",
   "address": "(Lô D chung cư Hùng Vương) 014B Mạc Thiên Tích, phường 11 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7548258,
   "Latitude": 106.6644546
 },
 {
   "STT": 2218,
   "Name": "Phòng Khám Huỳnh Quang",
   "address": "810 An Dương Vương, phường 13, Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7548775,
   "Latitude": 106.6244435
 },
 {
   "STT": 2219,
   "Name": "Phòng Khám Khánh Minh",
   "address": "28 Tăng Bạt Hổ, phường 12 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7580725,
   "Latitude": 106.6622194
 },
 {
   "STT": 2220,
   "Name": "Phòng Khám Khoa Kiểm Soát Dịch Bệnh",
   "address": "21 Nguyễn Văn Nghi, phường 7, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.824162,
   "Latitude": 106.682209
 },
 {
   "STT": 2221,
   "Name": "Phòng Khám Khoa Mắt - BS Lê Minh Thông",
   "address": "22 Nguyễn Thị Nghĩa, phường Bến Thành , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7705622,
   "Latitude": 106.6935953
 },
 {
   "STT": 2222,
   "Name": "Phòng Khám Khớp Gối Khớp Háng",
   "address": "21-23 Tống Văn Trân, phường 5 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7704,
   "Latitude": 106.643299
 },
 {
   "STT": 2223,
   "Name": "Phòng Khám Khớp Háng Khớp Gối - BS. Nguyễn Thành Chơn",
   "address": "15 Tống Văn Trân, phường 5 ,Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.770339,
   "Latitude": 106.643393
 },
 {
   "STT": 2224,
   "Name": "Phòng Khám Kiều Thu",
   "address": "12 Nơ Trang Long, phường 14, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.804672,
   "Latitude": 106.6951354
 },
 {
   "STT": 2225,
   "Name": "Phòng Khám Kỹ Thuật Cao PHOENIX MEDICAL CENTER",
   "address": "18 - 20 Phước Hưng, phường 8 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7550705,
   "Latitude": 106.6689275
 },
 {
   "STT": 2226,
   "Name": "Phòng Khám Lao & Bệnh Phổi - BS Lê Văn Nhi",
   "address": "103A Bùi Thị Xuân, phường Phạm Ngũ Lão , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.767693,
   "Latitude": 106.686615
 },
 {
   "STT": 2227,
   "Name": "Phòng Khám Lao & Bệnh Phổi - BS. Phan An",
   "address": "219 Nguyễn Trãi, phường Nguyễn Cư Trinh , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7652678,
   "Latitude": 106.6878883
 },
 {
   "STT": 2228,
   "Name": "Phòng Khám Lão Khoa - BS. Lương Văn Đến",
   "address": "107 Điện Biên Phủ, phường Đa Kao , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.789151,
   "Latitude": 106.696538
 },
 {
   "STT": 2229,
   "Name": "Phòng Khám Lão Khoa - BS. Thân Hà Ngọc Thể",
   "address": "37 Nguyễn Phi Khanh, phường Tân Định , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.791634,
   "Latitude": 106.694681
 },
 {
   "STT": 2230,
   "Name": "Phòng Khám Lão Khoa Medvie",
   "address": "79/21 Âu Cơ, phường 14 , Quận 11 , thành phố Hồ Chí Minh",
   "Longtitude": 10.769825,
   "Latitude": 106.649917
 },
 {
   "STT": 2231,
   "Name": "Phòng Khám Lao Và Bệnh Phổi - BS. Nguyễn Tấn Toàn",
   "address": "549 Trường Chinh, phường Tân Sơn Nhì, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.8021173,
   "Latitude": 106.6365466
 },
 {
   "STT": 2232,
   "Name": "Phòng Khám Liên Khoa Lao - HIV",
   "address": "29/5 Lê Đức Thọ, phường 17, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.843985,
   "Latitude": 106.6740389
 },
 {
   "STT": 2233,
   "Name": "Phòng Khám Liên Tâm",
   "address": "A14 Bà Hom, phường 13 , Quận 6, thành phố Hồ Chí Minh",
   "Longtitude": 10.754821,
   "Latitude": 106.629595
 },
 {
   "STT": 2234,
   "Name": "Phòng Khám Loãng Xương Trần Văn Đức",
   "address": "540/5 Vĩnh Viễn, phường 6 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.763755,
   "Latitude": 106.669056
 },
 {
   "STT": 2235,
   "Name": "Phòng Khám Maple Healthcare - CS Quận 3",
   "address": "107B Trương Định, phường 6 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.778138,
   "Latitude": 106.688321
 },
 {
   "STT": 2236,
   "Name": "Phòng Khám Mắt - BS Nguyễn Chí Trung Thế Truyền",
   "address": "158 Lê Lai, phường Bến Thành ,Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7692634,
   "Latitude": 106.6914119
 },
 {
   "STT": 2237,
   "Name": "Phòng Khám Mắt - BS Nguyễn Ngọc Phùng",
   "address": "271 Lê Quang Sung, phường 6 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.751688,
   "Latitude": 106.643521
 },
 {
   "STT": 2238,
   "Name": "Phòng Khám Mắt - BS Nguyễn Phát Trước Tiên",
   "address": "12 Đường số 9, phường 11 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7514615,
   "Latitude": 106.6279303
 },
 {
   "STT": 2239,
   "Name": "Phòng Khám Mắt - BS Phạm Nguyên Huân",
   "address": "3 Trần Khánh Dư, phường Tân Định , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.793919,
   "Latitude": 106.689284
 },
 {
   "STT": 2240,
   "Name": "Phòng Khám Mắt - BS Trường Xuân",
   "address": "275A Hậu Giang, phường 5 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.749547,
   "Latitude": 106.6425919
 },
 {
   "STT": 2241,
   "Name": "Phòng Khám Mắt - BS Xuân Đào",
   "address": "291 Nguyễn Tiểu La, phường 8 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7659046,
   "Latitude": 106.6657677
 },
 {
   "STT": 2242,
   "Name": "Phòng Khám Mắt - BS. Vũ Anh Lê",
   "address": "98 Cô Bắc, phường Cô Giang ,Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.764839,
   "Latitude": 106.69457
 },
 {
   "STT": 2243,
   "Name": "Phòng Khám Mắt - Răng Hàm Mặt",
   "address": "60 Ngô Quyền, phường 5 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7606308,
   "Latitude": 106.6654077
 },
 {
   "STT": 2244,
   "Name": "Phòng Khám Mắt Anh Khoa",
   "address": "3/23 Quốc Lộ 22, Khu Phố 2, Thị trấn Hóc Môn , huyện Hóc Môn , thành phố Hồ Chí Minh",
   "Longtitude": 10.8863934,
   "Latitude": 106.5922924
 },
 {
   "STT": 2245,
   "Name": "Phòng Khám Mắt Hoàng Mai",
   "address": "40 Đường số 3, phường Thạnh Mỹ Lợi , Quận 2 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7803681,
   "Latitude": 106.7596492
 },
 {
   "STT": 2246,
   "Name": "Phòng Khám Mắt Nhi - BS. Nguyễn Thành Danh",
   "address": "19 Đinh Tiên Hoàng, phường Đa Kao , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.788664,
   "Latitude": 106.699152
 },
 {
   "STT": 2247,
   "Name": "Phòng Khám Mê Kông",
   "address": "78C4 Phạm Ngũ Lão, phường 3, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8204843,
   "Latitude": 106.6829661
 },
 {
   "STT": 2248,
   "Name": "Phòng Khám Minh Châu",
   "address": "1236 Tỉnh Lộ 10, phường Bình Trị Đông, quận Bình Tân, thành phố Hồ Chí Minh",
   "Longtitude": 10.7558489,
   "Latitude": 106.5947359
 },
 {
   "STT": 2249,
   "Name": "Phòng Khám Mỹ Kim",
   "address": "8 - 1A Mỹ An, Hà Huy Tập (Phú Mỹ Hưng), phường Tân Phong , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7266353,
   "Latitude": 106.7127814
 },
 {
   "STT": 2250,
   "Name": "Phòng Khám Nam Khoa",
   "address": "99 Thành Thái, phường 14 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7737879,
   "Latitude": 106.6644956
 },
 {
   "STT": 2251,
   "Name": "Phòng Khám Nam Khoa - BS Lê Anh Tuấn",
   "address": "23 Nguyễn Văn Đậu, phường 5, quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.8049782,
   "Latitude": 106.6869718
 },
 {
   "STT": 2252,
   "Name": "Phòng Khám Nam Khoa - BS. Mai Bá Tiến Dũng",
   "address": "39 Đường số 3 Cư Xá Đô Thành,phường 4 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7727178,
   "Latitude": 106.6826371
 },
 {
   "STT": 2253,
   "Name": "Phòng Khám Nam Khoa Âu Á",
   "address": "425 Nguyễn Văn Luông, phường 12 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7520328,
   "Latitude": 106.6347026
 },
 {
   "STT": 2254,
   "Name": "Phòng Khám Nam Khoa Chuyên Khoa Da Liễu Quận Tân Bình",
   "address": "372 (394 số mới) Phạm Văn Bạch,phường 15, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.8254026,
   "Latitude": 106.6402719
 },
 {
   "STT": 2255,
   "Name": "Phòng Khám Ngoài Giờ - BS Bảo",
   "address": "191 Đường số 17, phường Tân Quy , Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.744736,
   "Latitude": 106.710248
 },
 {
   "STT": 2256,
   "Name": "Phòng Khám Ngoài Giờ BS Nguyễn Thanh Nam",
   "address": "47/1B Trần Phú, phường 4 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7620027,
   "Latitude": 106.6787869
 },
 {
   "STT": 2257,
   "Name": "Phòng Khám Ngoài Giờ BS Thu Hà",
   "address": "532 Ngô Gia Tự, phường 9 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7584682,
   "Latitude": 106.6678759
 },
 {
   "STT": 2258,
   "Name": "Phòng Khám Ngoài Giờ BS Trần Minh Hải",
   "address": "20 Phạm Hữu Chí, phường 12 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7558252,
   "Latitude": 106.6579764
 },
 {
   "STT": 2259,
   "Name": "Phòng Khám Ngoài Giờ Bs.Nguyễn Thanh Liêm",
   "address": "D12/2 Ấp 4, Xã Tân Quý Tây, huyện Bình Chánh, thành phố Hồ Chí Minh",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 2260,
   "Name": "Phòng Khám Ngoài Giờ Chuyên Khoa Nội Tổng Hợp (Bs.Võ Thanh Hùng)",
   "address": "Quốc lộ 50, Xã Bình Hưng , Huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.72833,
   "Latitude": 106.6559
 },
 {
   "STT": 2261,
   "Name": "Phòng Khám Ngoài Giờ Khải - Tú",
   "address": "81/3A Tân Thới Hiệp 22, phường Tân Thới Hiệp , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8560638,
   "Latitude": 106.6424773
 },
 {
   "STT": 2262,
   "Name": "Phòng Khám Ngoài Giờ Lam Anh",
   "address": "66 Bế Văn Đàn, phường 14, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.7894063,
   "Latitude": 106.6426551
 },
 {
   "STT": 2263,
   "Name": "Phòng Khám Ngoài Giờ Sản Phụ Khoa - BS. Phương Ngoan",
   "address": "2G Lý Thường Kiệt, phường 12 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7564221,
   "Latitude": 106.6627029
 },
 {
   "STT": 2264,
   "Name": "Phòng Khám Ngoại Khoa - BS. Trần Văn Phú",
   "address": "2F Nguyễn Thị Tần, phường 2 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7470459,
   "Latitude": 106.686012
 },
 {
   "STT": 2265,
   "Name": "Phòng Khám Ngoại Khoa - BS. Phan Huấn",
   "address": "1237 Nguyễn Xiển, phường Long Bình , Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8634398,
   "Latitude": 106.8375852
 },
 {
   "STT": 2266,
   "Name": "Phòng Khám Ngoại Khoa - BS. Trần Duy Công",
   "address": "201 Nguyễn Xí, phường 26 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8150937,
   "Latitude": 106.7076851
 },
 {
   "STT": 2267,
   "Name": "Phòng Khám Ngoại Niệu & Nam Khoa - BS Phạm Hữu Đương",
   "address": "4 Tôn Đản, phường 13 , Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.762483,
   "Latitude": 106.708148
 },
 {
   "STT": 2268,
   "Name": "Phòng Khám Ngoại Thận Niệu & Sản Phụ Khoa - BS. Chung Tuấn Khiêm & BS. Nguyễn Vũ Đông Hằng",
   "address": "24B Quách Vũ, phường Hiệp Tân, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.773929,
   "Latitude": 106.631529
 },
 {
   "STT": 2269,
   "Name": "Phòng Khám Ngoại Tổng Quát - BS.Kim",
   "address": "B7/19 Trịnh Như Khuê, Xã Bình Chánh, huyện Bình Chánh , thành phố Hồ Chí Minh",
   "Longtitude": 10.6656062,
   "Latitude": 106.5721106
 },
 {
   "STT": 2270,
   "Name": "Phòng Khám Ngoài Giờ - BS. Hằng",
   "address": "2A Đường 4, Khu phố 5, phường Trường Thọ , Thủ Đức , thành phố Hồ Chí Minh",
   "Longtitude": 10.836449,
   "Latitude": 106.7593699
 },
 {
   "STT": 2271,
   "Name": "Phòng Khám Ngọc Lan",
   "address": "55/25-27 Lê Thị Hồng Gấm,phường Nguyễn Thái Bình , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.768471,
   "Latitude": 106.698192
 },
 {
   "STT": 2272,
   "Name": "Phòng Khám Ngọc Đức",
   "address": "218 Nguyễn Tiểu La, phường 8 ,Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7646003,
   "Latitude": 106.6661835
 },
 {
   "STT": 2273,
   "Name": "Phòng Khám Ngọc Đức",
   "address": "323 Ngô Gia Tự, phường 3 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.762007,
   "Latitude": 106.67061
 },
 {
   "STT": 2274,
   "Name": "Phòng Khám Nha Khoa",
   "address": "214 Nguyễn Văn Đậu, phường 11, quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8115763,
   "Latitude": 106.691933
 },
 {
   "STT": 2275,
   "Name": "Phòng Khám Nha Khoa & Nhãn Khoa - BS. Ngô Thị Thùy Lụa & BS. Trần Mùi",
   "address": "11 Đường Số 68, phường Hiệp Phú, Quận 9 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8466585,
   "Latitude": 106.7763428
 },
 {
   "STT": 2276,
   "Name": "Phòng Khám Nha Khoa - BS Nguyễn Thị Bích Loan",
   "address": "77/4 Bành Văn Trân, phường 7, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.78807,
   "Latitude": 106.65901
 },
 {
   "STT": 2277,
   "Name": "Phòng Khám Nha Khoa - BS. Bách Hợp",
   "address": "637 Quang Trung, phường 11 , quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8351915,
   "Latitude": 106.6627176
 },
 {
   "STT": 2278,
   "Name": "Phòng Khám Nha Khoa - BS. Cang Hồng Thái",
   "address": "46 Bàn Cờ, phường 2 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7701287,
   "Latitude": 106.680146
 },
 {
   "STT": 2279,
   "Name": "Phòng Khám Nha Khoa - BS. Cao Bá Tri",
   "address": "194/1 Võ Văn Tần, phường 5 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.771238,
   "Latitude": 106.6850971
 },
 {
   "STT": 2280,
   "Name": "Phòng Khám Nha Khoa - BS. Hứa Thị Xuân Hòa",
   "address": "38 An Điềm, phường 10 , Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.75172,
   "Latitude": 106.665954
 },
 {
   "STT": 2281,
   "Name": "Phòng Khám Nha Khoa - BS. Huỳnh Ngọc Long",
   "address": "26/3 Nguyễn Cảnh Chân, phường Cầu Kho , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7606013,
   "Latitude": 106.6870395
 },
 {
   "STT": 2282,
   "Name": "Phòng Khám Nha Khoa - BS. Lê Thanh Hùng",
   "address": "302 Lê Văn Sỹ, phường 14 , Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.787178,
   "Latitude": 106.679452
 },
 {
   "STT": 2283,
   "Name": "Phòng Khám Nha Khoa - BS. Lê Thị Hiệp Xuân",
   "address": "45 Võ Thị Sáu, phường Đa Kao ,Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7902596,
   "Latitude": 106.6947919
 },
 {
   "STT": 2284,
   "Name": "Phòng Khám Nha Khoa - BS. Lê Thị Xuân Đào",
   "address": "1006 Nguyễn Trãi, phường 14 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7530249,
   "Latitude": 106.6524661
 },
 {
   "STT": 2285,
   "Name": "Phòng Khám Nha Khoa - BS. Lê Văn Khôi",
   "address": "125A Bình Tiên, phường 7 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.742574,
   "Latitude": 106.643215
 },
 {
   "STT": 2286,
   "Name": "Phòng Khám Nha Khoa - BS. Lê Đình Đại",
   "address": "534C Lê Văn Sỹ, phường 14 ,Quận 3 , thành phố Hồ Chí Minh",
   "Longtitude": 10.79015,
   "Latitude": 106.674085
 },
 {
   "STT": 2287,
   "Name": "Phòng Khám Nha Khoa - BS. Mai Phương",
   "address": "47/42/11A Bùi Đình Túy, phường 24 , quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.806478,
   "Latitude": 106.706386
 },
 {
   "STT": 2288,
   "Name": "Phòng Khám Nha Khoa - BS. Nguyễn Ngọc Duy",
   "address": "151 Bis Trần Quang Khải, phường Tân Định , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7920322,
   "Latitude": 106.6921942
 },
 {
   "STT": 2289,
   "Name": "Phòng Khám Nha Khoa - BS. Nguyễn Ngọc Quang",
   "address": "54/16 Lê Quang Định, phường 14, quận Bình Thạnh , thành phố Hồ Chí Minh",
   "Longtitude": 10.8049449,
   "Latitude": 106.6976655
 },
 {
   "STT": 2290,
   "Name": "Phòng Khám Nha Khoa - BS. Nguyễn Thanh Tâm",
   "address": "38/1 An Dương Vương, phường 10, Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.738635,
   "Latitude": 106.622836
 },
 {
   "STT": 2291,
   "Name": "Phòng Khám Nha Khoa - BS. Nguyễn Thị Xuân Đài",
   "address": "489A/1 Huỳnh Văn Bánh, phường 13 , quận Phú Nhuận, thành phố Hồ Chí Minh",
   "Longtitude": 10.7904651,
   "Latitude": 106.6696185
 },
 {
   "STT": 2292,
   "Name": "Phòng Khám Nha Khoa - BS. Nguyễn Văn Sện",
   "address": "372/B3 Phan Văn Khoẻ, phường 5, Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.747278,
   "Latitude": 106.643741
 },
 {
   "STT": 2293,
   "Name": "Phòng Khám Nha Khoa - BS. Phú",
   "address": "539 Nguyễn Trãi, phường 7 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7542379,
   "Latitude": 106.6686873
 },
 {
   "STT": 2294,
   "Name": "Phòng Khám Nha Khoa - BS. Quốc Huy",
   "address": "384 Bà Hạt, phường 8 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.765685,
   "Latitude": 106.6673739
 },
 {
   "STT": 2295,
   "Name": "Phòng Khám Nha Khoa - BS. Trần Thị Phương",
   "address": "33 Đường 24A, phường 10 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7424315,
   "Latitude": 106.6227489
 },
 {
   "STT": 2296,
   "Name": "Phòng Khám Nha Khoa - BS. Đỗ Thanh Hải",
   "address": "28 Lò Gốm, phường 7 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.739726,
   "Latitude": 106.6342371
 },
 {
   "STT": 2297,
   "Name": "Phòng Khám Nha Khoa An - Pha",
   "address": "52 Đường số 6, khu phố Hưng Phước 3, phường Tân Phong ,Quận 7 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7302085,
   "Latitude": 106.7078669
 },
 {
   "STT": 2298,
   "Name": "Phòng Khám Nha Khoa BS Đoàn Thị Thu",
   "address": "109/27 Lê Quốc Hưng, phường 12, Quận 4 , thành phố Hồ Chí Minh",
   "Longtitude": 10.765451,
   "Latitude": 106.704336
 },
 {
   "STT": 2299,
   "Name": "Phòng Khám Nha Khoa Lý Hồng Sơn",
   "address": "569 Hồng Bàng, phường 2 , Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7532726,
   "Latitude": 106.6504401
 },
 {
   "STT": 2300,
   "Name": "Phòng Khám Nha Khoa Nha Sĩ Nguyễn Văn Hạnh",
   "address": "553 Trần Hưng Đạo, phường Cầu Kho , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7571239,
   "Latitude": 106.6861801
 },
 {
   "STT": 2301,
   "Name": "Phòng Khám Nha Khoa Nhật Huy",
   "address": "B90 Bạch Đằng, phường 2, quận Tân Bình, thành phố Hồ Chí Minh",
   "Longtitude": 10.815833,
   "Latitude": 106.669506
 },
 {
   "STT": 2302,
   "Name": "Phòng Khám Nha Khoa Như Ý",
   "address": "125/4 Âu Dương Lân, phường 2 ,Quận 8 , thành phố Hồ Chí Minh",
   "Longtitude": 10.743643,
   "Latitude": 106.684429
 },
 {
   "STT": 2303,
   "Name": "Phòng Khám Nha Khoa OXY",
   "address": "40/1 Tô Ký, Xã Thới Tam Thôn , huyện Hóc Môn , thành phố Hồ Chí Minh",
   "Longtitude": 10.8874031,
   "Latitude": 106.6032016
 },
 {
   "STT": 2304,
   "Name": "Phòng Khám Nha Khoa Phước Lộc",
   "address": "53G Lý Chiêu Hoàng, phường 10 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.739421,
   "Latitude": 106.63154
 },
 {
   "STT": 2305,
   "Name": "Phòng Khám Nha Khoa Toàn Cầu",
   "address": "228 Hòa Hảo, phường 3 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7626257,
   "Latitude": 106.6716369
 },
 {
   "STT": 2306,
   "Name": "Phòng Khám Nha Khoa Đạt Trang",
   "address": "188C Phan Văn Trị, phường 12, quận Bình Thạnh, thành phố Hồ Chí Minh",
   "Longtitude": 10.813762,
   "Latitude": 106.695372
 },
 {
   "STT": 2307,
   "Name": "Phòng Khám Nha Khoa Đồng Khánh",
   "address": "908 Trần Hưng Đạo, phường 7 ,Quận 5 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7547664,
   "Latitude": 106.6790452
 },
 {
   "STT": 2308,
   "Name": "Phòng Khám Nhà Thuốc 99",
   "address": "99 Cây Keo, phường Hiệp Tân, quận Tân Phú, thành phố Hồ Chí Minh",
   "Longtitude": 10.771686,
   "Latitude": 106.628724
 },
 {
   "STT": 2309,
   "Name": "Phòng Khám Nhãn Khoa - BS. Tùng Lâm & Nha Khoa - BS. Lê Vân",
   "address": "788/46A Nguyễn Kiệm, phường 3, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8128315,
   "Latitude": 106.6790126
 },
 {
   "STT": 2310,
   "Name": "Phòng Khám Nhân Nghĩa",
   "address": "52 Nguyễn Ảnh Thủ, phường Hiệp Thành , Quận 12 , thành phố Hồ Chí Minh",
   "Longtitude": 10.8764959,
   "Latitude": 106.647237
 },
 {
   "STT": 2311,
   "Name": "Phòng Khám Nhân Đạo Hội Chữ Thập Đỏ Quận Bình Tân",
   "address": "784 Tỉnh lộ 10, phường Bình Trị Đông , quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.7579343,
   "Latitude": 106.6097004
 },
 {
   "STT": 2312,
   "Name": "Phòng Khám Nhân Đức",
   "address": "229/11 Bình Phú, phường 11 ,Quận 6 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7462654,
   "Latitude": 106.6310607
 },
 {
   "STT": 2313,
   "Name": "Phòng Khám Nhật Phương",
   "address": "108/1/24 Đường Số 10, phường 9, quận Gò Vấp, thành phố Hồ Chí Minh",
   "Longtitude": 10.8406885,
   "Latitude": 106.6559604
 },
 {
   "STT": 2314,
   "Name": "Phòng Khám Nhật Tảo",
   "address": "162 Nhật Tảo, phường 8 , Quận 10 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7646134,
   "Latitude": 106.6671531
 },
 {
   "STT": 2315,
   "Name": "Phòng Khám Nhi & Nội Khoa - BS Văn Bích",
   "address": "72D Nguyễn Hữu Cầu, phường Tân Định , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.790507,
   "Latitude": 106.6907014
 },
 {
   "STT": 2316,
   "Name": "Phòng Khám Nhi & Ung Bướu - BS Đức & BS Thịnh",
   "address": "66 Đinh Công Tráng, phường Tân Định , Quận 1 , thành phố Hồ Chí Minh",
   "Longtitude": 10.7892309,
   "Latitude": 106.691052
 },
 {
   "STT": 2317,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu  Bác sĩ. Nguyễn Thị Đào",
   "address": " 134/48 Thành Thái, Phường 12, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7689272,
   "Latitude": 106.6666533
 },
 {
   "STT": 2318,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - Bác sĩ. Phạm Thị Kim Anh",
   "address": " 437 Huỳnh Văn Bánh, Phường 13, Phú Nhuận, Hồ Chí Minh",
   "Longtitude": 10.7917816,
   "Latitude": 106.6712986
 },
 {
   "STT": 2319,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Lê Hồng Cúc",
   "address": "206 Trần Bình Trọng, Phường 4, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7606345,
   "Latitude": 106.6800459
 },
 {
   "STT": 2320,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Liêm & Bác sĩ. Sung",
   "address": "26 Nơ Trang Long, Phường 14, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.8050964,
   "Latitude": 106.694995
 },
 {
   "STT": 2321,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Lưu Văn Minh",
   "address": "138 Nhật Tảo, Phường 8, Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7647615,
   "Latitude": 106.6676062
 },
 {
   "STT": 2322,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Nguyễn Châu Hiếu",
   "address": "64 Âu Dương Lân, Phường 3 , Quận 8 , Hồ Chí Minh",
   "Longtitude": 10.7470091,
   "Latitude": 106.6821767
 },
 {
   "STT": 2323,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Nguyễn Hải Nam",
   "address": "90 Bàu Cát, Phường 14 ,Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.7923971,
   "Latitude": 106.6422167
 },
 {
   "STT": 2324,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Nguyễn Hữu Phúc",
   "address": "144 Nguyễn Trọng Tuyển, Phường 8, Phú Nhuận, Hồ Chí Minh",
   "Longtitude": 10.7976379,
   "Latitude": 106.676393
 },
 {
   "STT": 2325,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Nguyễn Quốc Điền",
   "address": "101 Đặng Văn Ngữ,Phường 14 , Phú Nhuận , Hồ Chí Minh",
   "Longtitude": 10.792563,
   "Latitude": 106.668304
 },
 {
   "STT": 2326,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Nguyễn Thái Bình",
   "address": "48 Nguyễn Huy Lượng, Phường 14 , Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.8054969,
   "Latitude": 106.6952828
 },
 {
   "STT": 2327,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Nguyễn Thế Hiển",
   "address": "16 Nguyễn Huy Lượng, Phường 14 , Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.8058035,
   "Latitude": 106.6965375
 },
 {
   "STT": 2328,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Nguyễn Đỗ Thùy Giang",
   "address": "27/24 Đường Số 4,Phường 10 , Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.848072,
   "Latitude": 106.658541
 },
 {
   "STT": 2329,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Phạm Duy Hoàng",
   "address": "20 Nguyễn Huy Lượng,Phường 14 , Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.8056856,
   "Latitude": 106.6964561
 },
 {
   "STT": 2330,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Phạm Hùng Cường",
   "address": "41 Nguyễn Phi Khanh, Phường Tân Định , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.791653,
   "Latitude": 106.694443
 },
 {
   "STT": 2331,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Phạm Thiên Hương",
   "address": "48/10C Nguyễn Thái Sơn, Phường 3 , Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.82007,
   "Latitude": 106.684144
 },
 {
   "STT": 2332,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Phạm Văn Bùng",
   "address": "106 đường số 5, Phường Bình Trị Đông B , Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.7502166,
   "Latitude": 106.6138092
 },
 {
   "STT": 2333,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Phạm Xuân Dũng",
   "address": "492 Lê Quang Định,Phường 11 , Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.8152808,
   "Latitude": 106.6901475
 },
 {
   "STT": 2334,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Phó Đức Mẫn",
   "address": "180A Nguyễn Đình Chính,Phường 11 , Phú Nhuận , Hồ Chí Minh",
   "Longtitude": 10.794472,
   "Latitude": 106.6739149
 },
 {
   "STT": 2335,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Trần Thanh Phương",
   "address": "38/88C Lãnh Binh Thăng,Phường 13 , Quận 11 , Hồ Chí Minh",
   "Longtitude": 10.76277,
   "Latitude": 106.6557909
 },
 {
   "STT": 2336,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Trần Thị Anh Tường",
   "address": "43/14 Nơ Trang Long, Phường 7, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.807916,
   "Latitude": 106.694567
 },
 {
   "STT": 2337,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Trần Việt Thế Phương",
   "address": "47/11 Phan Văn Trị,Phường 14 , Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.8066376,
   "Latitude": 106.6975202
 },
 {
   "STT": 2338,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Trịnh Thị Minh Châu",
   "address": "531 Điện Biên Phủ, Phường 3, Quận 3, Hồ Chí Minh",
   "Longtitude": 10.7709052,
   "Latitude": 106.6772363
 },
 {
   "STT": 2339,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Trương Công Gia Thuận & Bác sĩ. Nguyễn Thị Hồng Ánh",
   "address": "204 Trường Chinh, Phường Tân Hưng Thuận , Quận 12 , Hồ Chí Minh",
   "Longtitude": 10.8336208,
   "Latitude": 106.6208684
 },
 {
   "STT": 2340,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Võ Kim Điền",
   "address": "65 Triệu Quang Phục,Phường 10 , Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7513089,
   "Latitude": 106.661399
 },
 {
   "STT": 2341,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Võ Ngọc Đức",
   "address": "8/4 Ký Con, Phường 7 ,Phú Nhuận , Hồ Chí Minh",
   "Longtitude": 10.803298,
   "Latitude": 106.6850879
 },
 {
   "STT": 2342,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Võ Thanh Nhân",
   "address": "529/3 Nguyễn Tri Phương,Phường 8 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.764593,
   "Latitude": 106.6684439
 },
 {
   "STT": 2343,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Võ Đức Hiếu",
   "address": "51 Cao Thắng, Phường 11 , Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.7704573,
   "Latitude": 106.6815564
 },
 {
   "STT": 2344,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Vũ Văn Vũ",
   "address": "220/42 Lê Văn Sỹ, Phường 14 , Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.787024,
   "Latitude": 106.682414
 },
 {
   "STT": 2345,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Đỗ Bình Minh",
   "address": "117A Hòa Hưng, Phường 12 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7786133,
   "Latitude": 106.6731001
 },
 {
   "STT": 2346,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Đỗ Tường Huân",
   "address": "182/15A Lê Văn Sỹ, Phường 10, Phú Nhuận, Hồ Chí Minh",
   "Longtitude": 10.793596,
   "Latitude": 106.670657
 },
 {
   "STT": 2347,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ. Đoàn Minh Trông",
   "address": "450 Lê Hồng Phong,Phường 1 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7656288,
   "Latitude": 106.6752605
 },
 {
   "STT": 2348,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu - Bác sĩ.Trần Đặng Ngọc Linh",
   "address": "380/6 Nơ Trang Long, Phường 13, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.8223293,
   "Latitude": 106.7078675
 },
 {
   "STT": 2349,
   "Name": "Phòng Khám Chuyên Khoa Vật Lý Trị Liệu - Bác sĩ. Lê Hữu Lợi",
   "address": "950 Nguyễn Kiệm, Phường 3 , Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.8243738,
   "Latitude": 106.6793667
 },
 {
   "STT": 2350,
   "Name": "Phòng Khám Chuyên Khoa Xét Nghiệm",
   "address": "255 Lý Thường Kiệt,Phường 15 , Quận 11 , Hồ Chí Minh",
   "Longtitude": 10.7727153,
   "Latitude": 106.6573292
 },
 {
   "STT": 2351,
   "Name": "Phòng Khám Chuyên Khoa Xét Nghiệm",
   "address": "32/16 Nguyễn Huy Lượng,Phường 14 , Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.8058035,
   "Latitude": 106.6965375
 },
 {
   "STT": 2352,
   "Name": "Phòng Khám Chuyên Khoa Xét Nghiệm",
   "address": "410 Hòa Hảo, Phường 5 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7616729,
   "Latitude": 106.6671078
 },
 {
   "STT": 2353,
   "Name": "Phòng Khám Chuyên Khoa Xét Nghiệm - Bác sĩ. Vũ Thường Vụ",
   "address": "824/10 Sư Vạn Hạnh, Phường 12 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7748226,
   "Latitude": 106.6682775
 },
 {
   "STT": 2354,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp",
   "address": "257/27 Lý Thường Kiệt,Phường 15 , Quận 11 , Hồ Chí Minh",
   "Longtitude": 10.77244,
   "Latitude": 106.65686
 },
 {
   "STT": 2355,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp",
   "address": "143/2/26 Phạm Huy Thông, Phường 6 , Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.832599,
   "Latitude": 106.681037
 },
 {
   "STT": 2356,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp",
   "address": "175 Tân Phước, Phường 6, Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7619578,
   "Latitude": 106.6643661
 },
 {
   "STT": 2357,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp",
   "address": "442/1 Sư Vạn Hạnh,Phường 9 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7697406,
   "Latitude": 106.6706615
 },
 {
   "STT": 2358,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - Bác sĩ Ngô Mạnh Hùng",
   "address": "48 Nguyễn Văn Công,Phường 3 , Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.8245713,
   "Latitude": 106.6778181
 },
 {
   "STT": 2359,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - Bác sĩ Ngô Thành Ý",
   "address": "57 Bàu Cát 1, Phường 14 ,Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.794355,
   "Latitude": 106.6428882
 },
 {
   "STT": 2360,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - Bác sĩ. Chữ",
   "address": "343 Vườn Lài, Phường Phú Thọ Hòa , Tân Phú , Hồ Chí Minh",
   "Longtitude": 10.787677,
   "Latitude": 106.626046
 },
 {
   "STT": 2361,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - Bác sĩ. Hiếu",
   "address": "235/10 Thích Quảng Đức,Phường 4 , Phú Nhuận , Hồ Chí Minh",
   "Longtitude": 10.807411,
   "Latitude": 106.679066
 },
 {
   "STT": 2362,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - Bác sĩ. Huỳnh Văn Khoa",
   "address": "104/55 Thành Thái,Phường 12 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7726323,
   "Latitude": 106.6649801
 },
 {
   "STT": 2363,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - Bác sĩ. Nguyễn Quốc Trị",
   "address": "75 Bàn Cờ, Phường 3 ,Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.7704706,
   "Latitude": 106.6794433
 },
 {
   "STT": 2364,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - Bác sĩ. Nguyễn Văn Thái",
   "address": "1340/5 Ba Tháng Hai,Phường 2 , Quận 11 , Hồ Chí Minh",
   "Longtitude": 10.7569619,
   "Latitude": 106.6467764
 },
 {
   "STT": 2365,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - Bác sĩ. Nguyễn Văn Thanh",
   "address": "25/17 Trần Quý, Phường 4, Quận 11 , Hồ Chí Minh",
   "Longtitude": 10.759346,
   "Latitude": 106.658195
 },
 {
   "STT": 2366,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - Bác sĩ. Phan Dư Lê Thắng",
   "address": "142 Đặng Nguyên Cẩn,Phường 13 , Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.7547631,
   "Latitude": 106.631826
 },
 {
   "STT": 2367,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - Bác sĩ. Trần Văn Tân & Mắt - Bác sĩ. Lâm Kim Phụng",
   "address": "270K Võ Thị Sáu, Phường 7 , Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.780359,
   "Latitude": 106.6844825
 },
 {
   "STT": 2368,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - Bác sĩ. Trang Mạnh Khôi",
   "address": "248 Nhật Tảo, Phường 8 ,Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7638321,
   "Latitude": 106.6643729
 },
 {
   "STT": 2369,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - Bác sĩ. Trung",
   "address": "139 Đặng Chất, Phường 2, Quận 8 , Hồ Chí Minh",
   "Longtitude": 10.747465,
   "Latitude": 106.68498
 },
 {
   "STT": 2370,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - Bác sĩ. Trương Minh Hổ",
   "address": "442 Sư Vạn Hạnh, Phường 9 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7657203,
   "Latitude": 106.672002
 },
 {
   "STT": 2371,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - Bác sĩ. Trương Trọng Tín",
   "address": "191 Nguyễn Lâm, Phường 6 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7650669,
   "Latitude": 106.6630685
 },
 {
   "STT": 2372,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - Bác sĩ. Vũ Văn Nghĩa",
   "address": "375A Bến Bình Đông,Phường 15 , Quận 8 , Hồ Chí Minh",
   "Longtitude": 10.732822,
   "Latitude": 106.634764
 },
 {
   "STT": 2373,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - Bác sĩ. Đặng Ngọc Hải",
   "address": "91M Lý Thường Kiệt,Phường 7 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7612104,
   "Latitude": 106.6606605
 },
 {
   "STT": 2374,
   "Name": "Phòng Khám Chuyên Khoa Xương Khớp - Bác sĩ. Đỗ Thế Tản",
   "address": "141 Trần Nhân Tông,Phường 2 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.762465,
   "Latitude": 106.674656
 },
 {
   "STT": 2375,
   "Name": "Phòng Khám Chuyên Khoa Y Học Cổ Truyền - Bác sĩ. Nguyễn Thị Kim Ngân",
   "address": "181 Thái Phiên, Phường 9, Quận 11, Hồ Chí Minh",
   "Longtitude": 10.75993,
   "Latitude": 106.646085
 },
 {
   "STT": 2376,
   "Name": "Phòng Khám Chuyên Khoa Y Học Cổ Truyền - Lương Y Phòng Minh Đường",
   "address": "113G/1Bis-3 Lạc Long Quân, Phường 3 , Quận 11, Hồ Chí Minh",
   "Longtitude": 10.7616703,
   "Latitude": 106.6418524
 },
 {
   "STT": 2377,
   "Name": "Phòng Khám Chuyên Khoa Y Học Dân Tộc Bác sĩ. Đỗ Hữu Định",
   "address": "181 Nam Kỳ Khởi Nghĩa, Phường 7, Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.785606,
   "Latitude": 106.688042
 },
 {
   "STT": 2378,
   "Name": "Phòng Khám Chuyên Khoa Điều Trị Kết Hợp Vật Lý Trị Liệu - Bác sĩ. Năm",
   "address": "860/24 Xô Viết Nghệ Tĩnh,Phường 25 , Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.8025179,
   "Latitude": 106.71249
 },
 {
   "STT": 2379,
   "Name": "Phòng Khám Chuyên Khoa Đột Quỵ - Bác sĩ. Trần Chí Cường",
   "address": "53 Phạm Hữu Chí, Phường 12 ,Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.755753,
   "Latitude": 106.6591057
 },
 {
   "STT": 2380,
   "Name": "Phòng Khám Chuyên Nội Khoa Nam Hiếm Muộn - Bác sĩ. Võ Thanh Đạm",
   "address": "257 Vĩnh Viễn, Phường 4 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.763791,
   "Latitude": 106.668507
 },
 {
   "STT": 2381,
   "Name": "Phòng Khám Chuyên Phụ Khoa - Bác sĩ. Hồ Thị Thanh",
   "address": "103/68 Đặng Chất, Phường 2 ,Quận 8 , Hồ Chí Minh",
   "Longtitude": 10.746662,
   "Latitude": 106.684621
 },
 {
   "STT": 2382,
   "Name": "Phòng Khám Cơ Xương Khớp - Bác sĩ. Trần Văn Dương",
   "address": "145 Vườn Lài, Phường Phú Thọ Hòa , Tân Phú , Hồ Chí Minh",
   "Longtitude": 10.787824,
   "Latitude": 106.631505
 },
 {
   "STT": 2383,
   "Name": "Phòng Khám Dạ Dày-Ruột-Gan Mật - Bác sĩ Trần Thiện Trung",
   "address": "44 Phước Hưng, Phường 8 , Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7555543,
   "Latitude": 106.6688286
 },
 {
   "STT": 2384,
   "Name": "Phòng Khám Da Grace - Grace Skincare Clinic",
   "address": "Mayfair Suites – WMC Tower, 102C Cống Quỳnh, Phường Phạm Ngũ Lão , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7650137,
   "Latitude": 106.6906477
 },
 {
   "STT": 2385,
   "Name": "Phòng Khám Da Liễu - Bác sĩ Nguyễn Thị Diệu My",
   "address": "47A Lê Bình, Phường 4 , Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.7937117,
   "Latitude": 106.6568769
 },
 {
   "STT": 2386,
   "Name": "Phòng Khám Da Liễu - Bác sĩ Trịnh Văn Chức",
   "address": "63 Trường Chinh, Phường 12 , Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.793803,
   "Latitude": 106.65132
 },
 {
   "STT": 2387,
   "Name": "Phòng Khám Da Liễu - Bác sĩ. Nguyễn Thị Bích Liên",
   "address": "58B Cao Thắng, Phường 5 , Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.7697855,
   "Latitude": 106.6827173
 },
 {
   "STT": 2388,
   "Name": "Phòng Khám Da Liễu - Bác sĩ. Trần Thị Thùy Linh",
   "address": "11 Trần Khánh Dư, Phường Tân Định , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.793867,
   "Latitude": 106.688799
 },
 {
   "STT": 2389,
   "Name": "Phòng Khám Da Liễu 199",
   "address": "199 Huỳnh Văn Bánh, Phường 12 ,Phú Nhuận , Hồ Chí Minh",
   "Longtitude": 10.7920459,
   "Latitude": 106.6780859
 },
 {
   "STT": 2390,
   "Name": "Phòng Khám Da Liễu Aurora",
   "address": "A408TM Hưng Phát Silver Star, 156A Nguyễn Hữu Thọ, Xã Phước Kiển , Huyện Nhà Bè , Hồ Chí Minh",
   "Longtitude": 10.7138292,
   "Latitude": 106.7001992
 },
 {
   "STT": 2391,
   "Name": "Phòng Khám Da Liễu Bác sĩ. Trần Quốc Long",
   "address": "6/15 Nguyễn Thị Minh Khai,Phường Đa Kao , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7855535,
   "Latitude": 106.7007414
 },
 {
   "STT": 2392,
   "Name": "Phòng Khám Da Liễu Dermatol Master",
   "address": "51/4/5 Thành Thái, Phường 14 ,Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7681902,
   "Latitude": 106.6668809
 },
 {
   "STT": 2393,
   "Name": "Phòng Khám Da Liễu Doctor Scar",
   "address": "SS1N Hồng Lĩnh, Phường 15 ,Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7799988,
   "Latitude": 106.6636273
 },
 {
   "STT": 2394,
   "Name": "Phòng Khám Da Liễu Hưng Thịnh",
   "address": "139 Tam Châu, Khu phố 2,Phường Tam Bình , Thủ Đức , Hồ Chí Minh",
   "Longtitude": 10.8683995,
   "Latitude": 106.7441247
 },
 {
   "STT": 2395,
   "Name": "Phòng Khám Da Liễu SkinOne",
   "address": "200 Tô Hiến Thành, Phường 15 ,Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.781734,
   "Latitude": 106.668482
 },
 {
   "STT": 2396,
   "Name": "Phòng Khám Da Liễu Đồng Diều",
   "address": "202 Phạm Hùng, Phường 5 , Quận 8 , Hồ Chí Minh",
   "Longtitude": 10.7408475,
   "Latitude": 106.6687817
 },
 {
   "STT": 2397,
   "Name": "Phòng Khám Dr.Heckmann",
   "address": "4 Đoàn Thị Điểm, Phường 1 , Phú Nhuận , Hồ Chí Minh",
   "Longtitude": 10.7992927,
   "Latitude": 106.6812885
 },
 {
   "STT": 2398,
   "Name": "Phòng Khám FV",
   "address": "Lầu 3, tòa nhà Bitexco Financial Tower 2, 2 Hải Triều, Phường Bến Nghé , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7716446,
   "Latitude": 106.7046532
 },
 {
   "STT": 2399,
   "Name": "Phòng Khám Gan - Mật Sài Gòn",
   "address": "31-33 Phạm Hữu Chí, Phường 12 ,Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7558245,
   "Latitude": 106.6594544
 },
 {
   "STT": 2400,
   "Name": "Phòng Khám Hải Thoa",
   "address": "6 Nguyễn Văn Nghi, Phường 5 ,Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.821339,
   "Latitude": 106.689049
 },
 {
   "STT": 2401,
   "Name": "Phòng Khám Hạnh Lâm",
   "address": "378 Phạm Hùng, Phường 5 , Quận 8 , Hồ Chí Minh",
   "Longtitude": 10.7365681,
   "Latitude": 106.6714765
 },
 {
   "STT": 2402,
   "Name": "Phòng Khám Hiếm Muộn & Sản Phụ Khoa - Bác sĩ. Cù Thị Kim Loan",
   "address": "52 Đường số 11, Khu dân cư Him Lam, Phường Tân Hưng , Quận 7 , Hồ Chí Minh",
   "Longtitude": 10.7392301,
   "Latitude": 106.6959206
 },
 {
   "STT": 2403,
   "Name": "Phòng Khám Hiền Lương",
   "address": "755 Trường Chinh, Phường Tây Thạnh , Tân Phú , Hồ Chí Minh",
   "Longtitude": 10.8186186,
   "Latitude": 106.6310521
 },
 {
   "STT": 2404,
   "Name": "Phòng Khám Hiện Đại Quốc Tế GMCC",
   "address": "65 Hoàng Hoa Thám, Phường 6 ,Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.806085,
   "Latitude": 106.690199
 },
 {
   "STT": 2405,
   "Name": "Phòng Khám Hoàn Mỹ Sài Gòn",
   "address": "4A Hoàng Việt, Phường 4 , Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.7977292,
   "Latitude": 106.6587109
 },
 {
   "STT": 2406,
   "Name": "Phòng Khám Hoàng Diệu",
   "address": "390 Hoàng Diệu, Phường 5 , Quận 4 , Hồ Chí Minh",
   "Longtitude": 10.7599485,
   "Latitude": 106.6986432
 },
 {
   "STT": 2407,
   "Name": "Phòng Khám Hoàng Gia",
   "address": "44A Nguyễn Văn Đậu, Phường 6 ,Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.806495,
   "Latitude": 106.6871409
 },
 {
   "STT": 2408,
   "Name": "Phòng Khám Hoàng Mai",
   "address": "1 Trần Thị Nghỉ, Phường 7 , Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.827086,
   "Latitude": 106.681102
 },
 {
   "STT": 2409,
   "Name": "Phòng Khám Hoàng Nguyên",
   "address": "294 Huỳnh Tấn Phát, Phường Tân Thuận Tây , Quận 7 , Hồ Chí Minh",
   "Longtitude": 10.7513772,
   "Latitude": 106.7285546
 },
 {
   "STT": 2410,
   "Name": "Phòng Khám Hoàng Vũ",
   "address": "630-632 Hưng Phú, Phường 10 ,Quận 8 , Hồ Chí Minh",
   "Longtitude": 10.746321,
   "Latitude": 106.668594
 },
 {
   "STT": 2411,
   "Name": "Phòng Khám Hội Chữ Thập Đỏ Quận 5",
   "address": "251B Trần Phú, Phường 9 , Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7580677,
   "Latitude": 106.6749994
 },
 {
   "STT": 2412,
   "Name": "Phòng Khám Hội Chữ Thập Đỏ Quận 8",
   "address": "183 Đường Số 8, Phường 4 , Quận 8 , Hồ Chí Minh",
   "Longtitude": 10.741842,
   "Latitude": 106.669951
 },
 {
   "STT": 2413,
   "Name": "Phòng Khám Hồng Châu",
   "address": "133 Nguyễn Thị Định, Phường Bình Trưng Tây , Quận 2 , Hồ Chí Minh",
   "Longtitude": 10.778965,
   "Latitude": 106.7671639
 },
 {
   "STT": 2414,
   "Name": "Phòng Khám Hồng Sáng",
   "address": "425 Mã Lò, Phường Bình Trị Đông A , Bình Tân , Hồ Chí Minh",
   "Longtitude": 10.7788,
   "Latitude": 106.600035
 },
 {
   "STT": 2415,
   "Name": "Phòng Khám Hồng Đức",
   "address": "178A Thới An 32, Phường Thới An, Quận 12 , Hồ Chí Minh",
   "Longtitude": 10.8787829,
   "Latitude": 106.6510838
 },
 {
   "STT": 2416,
   "Name": "Phòng Khám Huệ Đức",
   "address": "52 Cao Xuân Dục, Phường 13, Quận 8, Hồ Chí Minh",
   "Longtitude": 10.7460126,
   "Latitude": 106.658964
 },
 {
   "STT": 2417,
   "Name": "Phòng Khám Huy Liệu",
   "address": "014B Mạc Thiên Tích, Phường 11 ,Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7550697,
   "Latitude": 106.6647309
 },
 {
   "STT": 2418,
   "Name": "Phòng Khám Huỳnh Quang",
   "address": "810 An Dương Vương, Phường 13, Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.7548775,
   "Latitude": 106.6244435
 },
 {
   "STT": 2419,
   "Name": "Phòng Khám Khánh Minh",
   "address": "28 Tăng Bạt Hổ, Phường 12 ,Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7580725,
   "Latitude": 106.6622194
 },
 {
   "STT": 2420,
   "Name": "Phòng Khám Khoa Kiểm Soát Dịch Bệnh",
   "address": "21 Nguyễn Văn Nghi, Phường 7 ,Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.824162,
   "Latitude": 106.682209
 },
 {
   "STT": 2421,
   "Name": "Phòng Khám Khoa Mắt - Bác sĩ Lê Minh Thông",
   "address": "22 Nguyễn Thị Nghĩa, Phường Bến Thành , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7705622,
   "Latitude": 106.6935953
 },
 {
   "STT": 2422,
   "Name": "Phòng Khám Khớp Gối Khớp Háng",
   "address": "21-23 Tống Văn Trân, Phường 5 ,Quận 11 , Hồ Chí Minh",
   "Longtitude": 10.7704,
   "Latitude": 106.643299
 },
 {
   "STT": 2423,
   "Name": "Phòng Khám Khớp Háng Khớp Gối - Bác sĩ. Nguyễn Thành Chơn",
   "address": "15 Tống Văn Trân, Phường 5 ,Quận 11 , Hồ Chí Minh",
   "Longtitude": 10.770339,
   "Latitude": 106.643393
 },
 {
   "STT": 2424,
   "Name": "Phòng Khám Kiều Thu",
   "address": "12 Nơ Trang Long, Phường 14 ,Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.804672,
   "Latitude": 106.6951354
 },
 {
   "STT": 2425,
   "Name": "Phòng Khám Kỹ Thuật Cao PHOENIX MEDICAL CENTER",
   "address": "18 - 20 Phước Hưng, Phường 8, Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7550705,
   "Latitude": 106.6689275
 },
 {
   "STT": 2426,
   "Name": "Phòng Khám Lao & Bệnh Phổi - Bác sĩ Lê Văn Nhi",
   "address": "103A Bùi Thị Xuân, Phường Phạm Ngũ Lão , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.767693,
   "Latitude": 106.686615
 },
 {
   "STT": 2427,
   "Name": "Phòng Khám Lao & Bệnh Phổi - Bác sĩ. Phan An",
   "address": "219 Nguyễn Trãi, Phường Nguyễn Cư Trinh , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7652678,
   "Latitude": 106.6878883
 },
 {
   "STT": 2428,
   "Name": "Phòng Khám Lão Khoa - Bác sĩ. Lương Văn Đến",
   "address": "107 Điện Biên Phủ, Phường Đa Kao , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.789151,
   "Latitude": 106.696538
 },
 {
   "STT": 2429,
   "Name": "Phòng Khám Lão Khoa - Bác sĩ. Thân Hà Ngọc Thể",
   "address": "37 Nguyễn Phi Khanh, Phường Tân Định , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.791634,
   "Latitude": 106.694681
 },
 {
   "STT": 2430,
   "Name": "Phòng Khám Lão Khoa Medvie",
   "address": "79/21 Âu Cơ, Phường 14 , Quận 11 , Hồ Chí Minh",
   "Longtitude": 10.769825,
   "Latitude": 106.649917
 },
 {
   "STT": 2431,
   "Name": "Phòng Khám Lao Và Bệnh Phổi - Bác sĩ. Nguyễn Tấn Toàn",
   "address": "549 Trường Chinh, Phường Tân Sơn Nhì , Tân Phú , Hồ Chí Minh",
   "Longtitude": 10.8021173,
   "Latitude": 106.6365466
 },
 {
   "STT": 2432,
   "Name": "Phòng Khám Liên Khoa Lao - HIV",
   "address": "29/5 Lê Đức Thọ, Phường 17 , Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.843985,
   "Latitude": 106.6740389
 },
 {
   "STT": 2433,
   "Name": "Phòng Khám Liên Tâm",
   "address": "A14 Bà Hom, Phường 13 , Quận 6, Hồ Chí Minh",
   "Longtitude": 10.754821,
   "Latitude": 106.629595
 },
 {
   "STT": 2434,
   "Name": "Phòng Khám Loãng Xương Trần Văn Đức",
   "address": "540/5 Vĩnh Viễn, Phường 6 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.763755,
   "Latitude": 106.669056
 },
 {
   "STT": 2435,
   "Name": "Phòng Khám Maple Healthcare - CS Quận 3",
   "address": "107B Trương Định, Phường 6 ,Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.778138,
   "Latitude": 106.688321
 },
 {
   "STT": 2436,
   "Name": "Phòng Khám Mắt - Bác sĩ Nguyễn Chí Trung Thế Truyền",
   "address": "158 Lê Lai, Phường Bến Thành ,Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7692634,
   "Latitude": 106.6914119
 },
 {
   "STT": 2437,
   "Name": "Phòng Khám Mắt - Bác sĩ Nguyễn Ngọc Phùng",
   "address": "271 Lê Quang Sung, Phường 6 ,Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.751688,
   "Latitude": 106.643521
 },
 {
   "STT": 2438,
   "Name": "Phòng Khám Mắt - Bác sĩ Nguyễn Phát Trước Tiên",
   "address": "12 Đường số 9, Phường 11 , Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.7452692,
   "Latitude": 106.6292798
 },
 {
   "STT": 2439,
   "Name": "Phòng Khám Mắt - Bác sĩ Phạm Nguyên Huân",
   "address": "3 Trần Khánh Dư, Phường Tân Định , Quận 1 , Hồ Chí Min",
   "Longtitude": 10.793919,
   "Latitude": 106.689284
 },
 {
   "STT": 2440,
   "Name": "Phòng Khám Mắt - Bác sĩ Trường Xuân",
   "address": "275A Hậu Giang, Phường 5 , Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.749547,
   "Latitude": 106.6425919
 },
 {
   "STT": 2441,
   "Name": "Phòng Khám Mắt - Bác sĩ Xuân Đào",
   "address": "291 Nguyễn Tiểu La, Phường 8 ,Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7659046,
   "Latitude": 106.6657677
 },
 {
   "STT": 2442,
   "Name": "Phòng Khám Mắt - Bác sĩ. Vũ Anh Lê",
   "address": "98 Cô Bắc, Phường Cô Giang ,Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.764839,
   "Latitude": 106.69457
 },
 {
   "STT": 2443,
   "Name": "Phòng Khám Mắt - Răng Hàm Mặt",
   "address": "60 Ngô Quyền, Phường 5 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7606308,
   "Latitude": 106.6654077
 },
 {
   "STT": 2444,
   "Name": "Phòng Khám Mắt Hoàng Mai",
   "address": "40 Đường số 3, Phường Thạnh Mỹ Lợi , Quận 2 , Hồ Chí Minh",
   "Longtitude": 10.7803681,
   "Latitude": 106.7596492
 },
 {
   "STT": 2445,
   "Name": "Phòng Khám Mắt Nhi - Bác sĩ. Nguyễn Thành Danh",
   "address": "19 Đinh Tiên Hoàng, Phường Đa Kao , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.788664,
   "Latitude": 106.699152
 },
 {
   "STT": 2446,
   "Name": "Phòng Khám Mê Kông",
   "address": "78C4 Phạm Ngũ Lão, Phường 3 ,Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.8204843,
   "Latitude": 106.6829661
 },
 {
   "STT": 2447,
   "Name": "Phòng Khám Minh Châu",
   "address": "1236 Tỉnh Lộ 10, Phường Bình Trị Đông , Bình Tân , Hồ Chí Minh",
   "Longtitude": 10.7558489,
   "Latitude": 106.5947359
 },
 {
   "STT": 2448,
   "Name": "Phòng Khám Mỹ Kim",
   "address": "8 - 1A Mỹ An, Hà Huy Tập (Phú Mỹ Hưng), Phường Tân Phong ,Quận 7 , Hồ Chí Minh",
   "Longtitude": 10.7266353,
   "Latitude": 106.7127814
 },
 {
   "STT": 2449,
   "Name": "Phòng Khám Nam Khoa",
   "address": "99 Thành Thái, Phường 14 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7737879,
   "Latitude": 106.6644956
 },
 {
   "STT": 2450,
   "Name": "Phòng Khám Nam Khoa - Bác sĩ Lê Anh Tuấn",
   "address": "23 Nguyễn Văn Đậu, Phường 5 ,Phú Nhuận , Hồ Chí Minh",
   "Longtitude": 10.8049782,
   "Latitude": 106.6869718
 },
 {
   "STT": 2451,
   "Name": "Phòng Khám Nam Khoa - Bác sĩ. Mai Bá Tiến Dũng",
   "address": "39 Đường số 3 Cư Xá Đô Thành,Phường 4 , Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.7727178,
   "Latitude": 106.6826371
 },
 {
   "STT": 2452,
   "Name": "Phòng Khám Nam Khoa Âu Á",
   "address": "425 Nguyễn Văn Luông, Phường 12 , Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.7520328,
   "Latitude": 106.6347026
 },
 {
   "STT": 2453,
   "Name": "Phòng Khám Nam Khoa Chuyên Khoa Da Liễu Quận Tân Bình",
   "address": "372 (394 số mới) Phạm Văn Bạch,Phường 15 , Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.8254026,
   "Latitude": 106.6402719
 },
 {
   "STT": 2454,
   "Name": "Phòng Khám Ngoài Giờ - Bác sĩ Bảo",
   "address": "191 Đường số 17, Phường Tân Quy , Quận 7 , Hồ Chí Minh",
   "Longtitude": 10.744736,
   "Latitude": 106.710248
 },
 {
   "STT": 2455,
   "Name": "Phòng Khám Ngoài Giờ Bác sĩ Nguyễn Thanh Nam",
   "address": "47/1B Trần Phú, Phường 4 , Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7620027,
   "Latitude": 106.6787869
 },
 {
   "STT": 2456,
   "Name": "Phòng Khám Ngoài Giờ Bác sĩ Thu Hà",
   "address": "532 Ngô Gia Tự, Phường 9 , Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7584682,
   "Latitude": 106.6678759
 },
 {
   "STT": 2457,
   "Name": "Phòng Khám Ngoài Giờ Bác sĩ Trần Minh Hải",
   "address": "20 Phạm Hữu Chí, Phường 12 ,Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7558252,
   "Latitude": 106.6579764
 },
 {
   "STT": 2458,
   "Name": "Phòng Khám Ngoài Giờ Bác sĩ.Nguyễn Thanh Liêm",
   "address": "D12/2 Ấp 4, Xã Tân Quý Tây ,Huyện Bình Chánh , Hồ Chí Minh",
   "Longtitude": 10.6575819,
   "Latitude": 106.5885656
 },
 {
   "STT": 2459,
   "Name": "Phòng Khám Ngoài Giờ Chuyên Khoa Nội Tổng Hợp (Bác sĩ.Võ Thanh Hùng)",
   "address": "Quốc lộ 50, Xã Bình Hưng , Huyện Bình Chánh , Hồ Chí Minh",
   "Longtitude": 10.7203301,
   "Latitude": 106.6557487
 },
 {
   "STT": 2460,
   "Name": "Phòng Khám Ngoài Giờ Khải - Tú",
   "address": "81/3A Tân Thới Hiệp 22, Phường Tân Thới Hiệp , Quận 12 , Hồ Chí Minh",
   "Longtitude": 10.8560638,
   "Latitude": 106.6424773
 },
 {
   "STT": 2461,
   "Name": "Phòng Khám Ngoài Giờ Lam Anh",
   "address": "66 Bế Văn Đàn, Phường 14 , Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.7894063,
   "Latitude": 106.6426551
 },
 {
   "STT": 2462,
   "Name": "Phòng Khám Ngoài Giờ Sản Phụ Khoa - Bác sĩ. Phương Ngoan",
   "address": "2G Lý Thường Kiệt, Phường 12 ,Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7564221,
   "Latitude": 106.6627029
 },
 {
   "STT": 2463,
   "Name": "Phòng Khám Ngoại Khoa - Bác sĩ. Trần Văn Phú",
   "address": "2F Nguyễn Thị Tần, Phường 2 ,Quận 8 , Hồ Chí Minh",
   "Longtitude": 10.7470459,
   "Latitude": 106.686012
 },
 {
   "STT": 2464,
   "Name": "Phòng Khám Ngoại Khoa - Bác sĩ. Phan Huấn",
   "address": "1237 Nguyễn Xiển, Phường Long Bình , Quận 9 , Hồ Chí Minh",
   "Longtitude": 10.8634398,
   "Latitude": 106.8375852
 },
 {
   "STT": 2465,
   "Name": "Phòng Khám Ngoại Khoa - Bác sĩ. Trần Duy Công",
   "address": "201 Nguyễn Xí, Phường 26 , Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.8145464,
   "Latitude": 106.7062085
 },
 {
   "STT": 2466,
   "Name": "Phòng Khám Ngoại Niệu & Nam Khoa - Bác sĩ Phạm Hữu Đương",
   "address": "4 Tôn Đản, Phường 13 , Quận 4 , Hồ Chí Minh",
   "Longtitude": 10.762483,
   "Latitude": 106.708148
 },
 {
   "STT": 2467,
   "Name": "Phòng Khám Ngoại Thận Niệu & Sản Phụ Khoa - Bác sĩ. Chung Tuấn Khiêm & Bác sĩ. Nguyễn Vũ Đông Hằng",
   "address": "24B Quách Vũ, Phường Hiệp Tân ,Tân Phú , Hồ Chí Minh",
   "Longtitude": 10.773929,
   "Latitude": 106.631529
 },
 {
   "STT": 2468,
   "Name": "Phòng Khám Ngoại Tổng Quát - Bác sĩ.Kim",
   "address": "B7/19 Trịnh Như Khuê, Xã Bình Chánh , Huyện Bình Chánh , Hồ Chí Minh",
   "Longtitude": 10.6656062,
   "Latitude": 106.5721106
 },
 {
   "STT": 2469,
   "Name": "Phòng Khám Ngoài Giờ - Bác sĩ. Hằng",
   "address": "2A Đường 4, Khu phố 5, Phường Trường Thọ , Thủ Đức , Hồ Chí Minh",
   "Longtitude": 10.8604597,
   "Latitude": 106.7557161
 },
 {
   "STT": 2470,
   "Name": "Phòng Khám Ngọc Lan",
   "address": "55/25-27 Lê Thị Hồng Gấm,Phường Nguyễn Thái Bình , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.768471,
   "Latitude": 106.698192
 },
 {
   "STT": 2471,
   "Name": "Phòng Khám Ngọc Đức",
   "address": "218 Nguyễn Tiểu La, Phường 8 ,Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7646003,
   "Latitude": 106.6661835
 },
 {
   "STT": 2472,
   "Name": "Phòng Khám Ngọc Đức",
   "address": "323 Ngô Gia Tự, Phường 3 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.762007,
   "Latitude": 106.67061
 },
 {
   "STT": 2473,
   "Name": "Phòng Khám Nha Khoa",
   "address": "214 Nguyễn Văn Đậu, Phường 11, Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.8115763,
   "Latitude": 106.691933
 },
 {
   "STT": 2474,
   "Name": "Phòng Khám Nha Khoa & Nhãn Khoa - Bác sĩ. Ngô Thị Thùy Lụa & Bác sĩ. Trần Mùi",
   "address": "11 Đường Số 68, Phường Hiệp Phú, Quận 9 , Hồ Chí Minh",
   "Longtitude": 10.8466585,
   "Latitude": 106.7763428
 },
 {
   "STT": 2475,
   "Name": "Phòng Khám Nha Khoa - Bác sĩ Nguyễn Thị Bích Loan",
   "address": "77/4 Bành Văn Trân, Phường 7 ,Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.78807,
   "Latitude": 106.65901
 },
 {
   "STT": 2476,
   "Name": "Phòng Khám Nha Khoa - Bác sĩ. Bách Hợp",
   "address": "637 Quang Trung, Phường 11 , Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.8351915,
   "Latitude": 106.6627176
 },
 {
   "STT": 2477,
   "Name": "Phòng Khám Nha Khoa - Bác sĩ. Cang Hồng Thái",
   "address": "46 Bàn Cờ, Phường 2 , Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.7701287,
   "Latitude": 106.680146
 },
 {
   "STT": 2478,
   "Name": "Phòng Khám Nha Khoa - Bác sĩ. Cao Bá Tri",
   "address": "194/1 Võ Văn Tần, Phường 5 ,Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.771238,
   "Latitude": 106.6850971
 },
 {
   "STT": 2479,
   "Name": "Phòng Khám Nha Khoa - Bác sĩ. Hứa Thị Xuân Hòa",
   "address": "38 An Điềm, Phường 10 , Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.75172,
   "Latitude": 106.665954
 },
 {
   "STT": 2480,
   "Name": "Phòng Khám Nha Khoa - Bác sĩ. Huỳnh Ngọc Long",
   "address": "26/3 Nguyễn Cảnh Chân, Phường Cầu Kho , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7606013,
   "Latitude": 106.6870395
 },
 {
   "STT": 2481,
   "Name": "Phòng Khám Nha Khoa - Bác sĩ. Lê Thanh Hùng",
   "address": "302 Lê Văn Sỹ, Phường 14 , Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.787178,
   "Latitude": 106.679452
 },
 {
   "STT": 2482,
   "Name": "Phòng Khám Nha Khoa - Bác sĩ. Lê Thị Hiệp Xuân",
   "address": "45 Võ Thị Sáu, Phường Đa Kao ,Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7902596,
   "Latitude": 106.6947919
 },
 {
   "STT": 2483,
   "Name": "Phòng Khám Nha Khoa - Bác sĩ. Lê Thị Xuân Đào",
   "address": "1006 Nguyễn Trãi, Phường 14 ,Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7530249,
   "Latitude": 106.6524661
 },
 {
   "STT": 2484,
   "Name": "Phòng Khám Nha Khoa - Bác sĩ. Lê Văn Khôi",
   "address": "125A Bình Tiên, Phường 7 , Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.742574,
   "Latitude": 106.643215
 },
 {
   "STT": 2485,
   "Name": "Phòng Khám Nha Khoa - Bác sĩ. Lê Đình Đại",
   "address": "534C Lê Văn Sỹ, Phường 14 ,Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.79015,
   "Latitude": 106.674085
 },
 {
   "STT": 2486,
   "Name": "Phòng Khám Nha Khoa - Bác sĩ. Mai Phương",
   "address": "47/42/11A Bùi Đình Túy, Phường 24 , Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.806478,
   "Latitude": 106.706386
 },
 {
   "STT": 2487,
   "Name": "Phòng Khám Nha Khoa - Bác sĩ. Nguyễn Ngọc Duy",
   "address": "151 Bis Trần Quang Khải, Phường Tân Định , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7916587,
   "Latitude": 106.6904104
 },
 {
   "STT": 2488,
   "Name": "Phòng Khám Nha Khoa - Bác sĩ. Nguyễn Ngọc Quang",
   "address": "54/16 Lê Quang Định, Phường 14, Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.805916,
   "Latitude": 106.698218
 },
 {
   "STT": 2489,
   "Name": "Phòng Khám Nha Khoa - Bác sĩ. Nguyễn Thanh Tâm",
   "address": "38/1 An Dương Vương, Phường 10, Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.738635,
   "Latitude": 106.622836
 },
 {
   "STT": 2490,
   "Name": "Phòng Khám Nha Khoa - Bác sĩ. Nguyễn Thị Xuân Đài",
   "address": "489A/1 Huỳnh Văn Bánh, Phường 13 , Phú Nhuận , Hồ Chí Minh",
   "Longtitude": 10.791112,
   "Latitude": 106.669918
 },
 {
   "STT": 2491,
   "Name": "Phòng Khám Nha Khoa - Bác sĩ. Nguyễn Văn Sện",
   "address": "372/B3 Phan Văn Khoẻ, Phường 5, Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.747278,
   "Latitude": 106.643741
 },
 {
   "STT": 2492,
   "Name": " Phòng Khám Nha Khoa - Bác sĩ. Phú",
   "address": "539 Nguyễn Trãi, Phường 7 ,Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7542379,
   "Latitude": 106.6686873
 },
 {
   "STT": 2493,
   "Name": "Phòng Khám Nha Khoa - Bác sĩ. Quốc Huy",
   "address": "384 Bà Hạt, Phường 8 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.765685,
   "Latitude": 106.6673739
 },
 {
   "STT": 2494,
   "Name": "Phòng Khám Nha Khoa - Bác sĩ. Trần Thị Phương",
   "address": "33 Đường 24A, Phường 10 , Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.7424315,
   "Latitude": 106.6227489
 },
 {
   "STT": 2495,
   "Name": "Phòng Khám Nha Khoa - Bác sĩ. Đỗ Thanh Hải",
   "address": "28 Lò Gốm, Phường 7 , Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.739726,
   "Latitude": 106.6342371
 },
 {
   "STT": 2496,
   "Name": "Phòng Khám Nha Khoa An - Pha",
   "address": "52 Đường số 6, khu phố Hưng Phước 3, Phường Tân Phong ,Quận 7 , Hồ Chí Minh",
   "Longtitude": 10.7302085,
   "Latitude": 106.7078669
 },
 {
   "STT": 2497,
   "Name": "Phòng Khám Nha Khoa Bác sĩ Đoàn Thị Thu",
   "address": "109/27 Lê Quốc Hưng, Phường 12, Quận 4 , Hồ Chí Minh",
   "Longtitude": 10.765451,
   "Latitude": 106.704336
 },
 {
   "STT": 2498,
   "Name": "Phòng Khám Nha Khoa Lý Hồng Sơn",
   "address": "569 Hồng Bàng, Phường 2 , Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.7532726,
   "Latitude": 106.6504401
 },
 {
   "STT": 2499,
   "Name": "Phòng Khám Nha Khoa Nha Sĩ Nguyễn Văn Hạnh",
   "address": "553 Trần Hưng Đạo, Phường Cầu Kho , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7571239,
   "Latitude": 106.6861801
 },
 {
   "STT": 2500,
   "Name": "Phòng Khám Nha Khoa Nhật Huy",
   "address": "B90 Bạch Đằng, Phường 2 , Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.815833,
   "Latitude": 106.669506
 },
 {
   "STT": 2501,
   "Name": "Phòng Khám Nha Khoa Như Ý",
   "address": "125/4 Âu Dương Lân, Phường 2 ,Quận 8 , Hồ Chí Minh",
   "Longtitude": 10.743643,
   "Latitude": 106.684429
 },
 {
   "STT": 2502,
   "Name": "Phòng Khám Nha Khoa OXY",
   "address": "40/1 Tô Ký, Xã Thới Tam Thôn ,Huyện Hóc Môn , Hồ Chí Minh",
   "Longtitude": 10.8761798,
   "Latitude": 106.6129606
 },
 {
   "STT": 2503,
   "Name": "Phòng Khám Nha Khoa Phước Lộc",
   "address": "53G Lý Chiêu Hoàng, Phường 10 ,Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.739421,
   "Latitude": 106.63154
 },
 {
   "STT": 2504,
   "Name": "Phòng Khám Nha Khoa Toàn Cầu",
   "address": "228 Hòa Hảo, Phường 3 , Quận 10, Hồ Chí Minh",
   "Longtitude": 10.7626257,
   "Latitude": 106.6716369
 },
 {
   "STT": 2505,
   "Name": "Phòng Khám Nha Khoa Đạt Trang",
   "address": "188C Phan Văn Trị, Phường 12 ,Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.813762,
   "Latitude": 106.695372
 },
 {
   "STT": 2506,
   "Name": "Phòng Khám Nha Khoa Đồng Khánh",
   "address": "908 Trần Hưng Đạo, Phường 7 ,Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7547664,
   "Latitude": 106.6790452
 },
 {
   "STT": 2507,
   "Name": "Phòng Khám Nhà Thuốc 99",
   "address": "99 Cây Keo, Phường Hiệp Tân ,Tân Phú , Hồ Chí Minh",
   "Longtitude": 10.771686,
   "Latitude": 106.628724
 },
 {
   "STT": 2508,
   "Name": "Phòng Khám Nhãn Khoa - Bác sĩ. Tùng Lâm & Nha Khoa - Bác sĩ. Lê Vân",
   "address": "788/46A Nguyễn Kiệm, Phường 3, Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.8128315,
   "Latitude": 106.6790126
 },
 {
   "STT": 2509,
   "Name": "Phòng Khám Nhân Nghĩa",
   "address": "52 Nguyễn Ảnh Thủ, Phường Hiệp Thành , Quận 12 , Hồ Chí Minh",
   "Longtitude": 10.8764959,
   "Latitude": 106.647237
 },
 {
   "STT": 2510,
   "Name": "Phòng Khám Nhân Đạo Hội Chữ Thập Đỏ Quận Bình Tân",
   "address": "784 Tỉnh lộ 10, Phường Bình Trị Đông , Bình Tân , Hồ Chí Minh",
   "Longtitude": 10.7579343,
   "Latitude": 106.6097004
 },
 {
   "STT": 2511,
   "Name": "Phòng Khám Nhân Đức",
   "address": "229/11 Bình Phú, Phường 11 ,Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.7462654,
   "Latitude": 106.6310607
 },
 {
   "STT": 2512,
   "Name": "Phòng Khám Nhật Phương",
   "address": "108/1/24 Đường Số 10, Phường 9, Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.8406885,
   "Latitude": 106.6559604
 },
 {
   "STT": 2513,
   "Name": "Phòng Khám Nhật Tảo",
   "address": "162 Nhật Tảo, Phường 8 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7646134,
   "Latitude": 106.6671531
 },
 {
   "STT": 2514,
   "Name": "Phòng Khám Nhi & Nội Khoa - Bác sĩ Văn Bích",
   "address": "72D Nguyễn Hữu Cầu, Phường Tân Định , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.790507,
   "Latitude": 106.6907014
 },
 {
   "STT": 2515,
   "Name": "Phòng Khám Nhi & Ung Bướu - Bác sĩ Đức & Bác sĩ Thịnh",
   "address": "66 Đinh Công Tráng, Phường Tân Định , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7892309,
   "Latitude": 106.691052
 },
 {
   "STT": 2516,
   "Name": "Phòng Khám Nhi - Bác sĩ Huỳnh Thị Diễm Kiều",
   "address": "276 Thạch Lam, Phường Phú Thạnh , Tân Phú , Hồ Chí Minh",
   "Longtitude": 10.7789357,
   "Latitude": 106.6248856
 },
 {
   "STT": 2517,
   "Name": "Phòng Khám Nhi - Bác sĩ. Lê Tròn Vuông",
   "address": "105A Phạm Hữu Lầu, Phường Phú Mỹ , Quận 7 , Hồ Chí Minh",
   "Longtitude": 10.7046435,
   "Latitude": 106.7343087
 },
 {
   "STT": 2518,
   "Name": "Phòng Khám Nhi Khoa & Nội Khoa - Bác sĩ. Nguyễn Thị Kim Cúc",
   "address": "17E1 Cư Xá 304 Đường D1,Phường 25 , Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.8012294,
   "Latitude": 106.7176468
 },
 {
   "STT": 2519,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Ánh Tuyết",
   "address": "171 Đường Số 265, Phường Hiệp Phú , Quận 9 , Hồ Chí Minh",
   "Longtitude": 10.8466042,
   "Latitude": 106.7865407
 },
 {
   "STT": 2520,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Bùi Hữu An",
   "address": "59 Bình Chiểu, Phường Bình Chiểu, Thủ Đức , Hồ Chí Minh",
   "Longtitude": 10.8535586,
   "Latitude": 106.764747
 },
 {
   "STT": 2521,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Bùi Thị Mai Phương",
   "address": "533B Cách Mạng Tháng 8, Phường 15 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7829529,
   "Latitude": 106.671896
 },
 {
   "STT": 2522,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Dư Tuấn Quy",
   "address": "41 Đường 9, Phường 11 , Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.7445871,
   "Latitude": 106.629138
 },
 {
   "STT": 2523,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Dương Ngọc Ánh",
   "address": "375 Nguyễn Duy Trinh, Phường Bình Trưng Tây , Quận 2 , Hồ Chí Minh",
   "Longtitude": 10.7883707,
   "Latitude": 106.7677473
 },
 {
   "STT": 2524,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Hoàng Ngọc Quý",
   "address": "19 Lý Thường Kiệt, Phường 4 , Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.824145,
   "Latitude": 106.685135
 },
 {
   "STT": 2525,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Hoàng Trọng",
   "address": "10 Nam Quốc Cang, Phường Phạm Ngũ Lão , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.768023,
   "Latitude": 106.687869
 },
 {
   "STT": 2526,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Khưu Đức Thành",
   "address": "492 Thống Nhất, Phường 16 , Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.8465884,
   "Latitude": 106.6646772
 },
 {
   "STT": 2527,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Lâm Mỹ Dung",
   "address": "296 Khánh Hội, Phường 5 , Quận 4 , Hồ Chí Minh",
   "Longtitude": 10.761134,
   "Latitude": 106.6974827
 },
 {
   "STT": 2528,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Lê Công Thiên",
   "address": "46/3 Minh Phụng, Phường 5 ,Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.748212,
   "Latitude": 106.643471
 },
 {
   "STT": 2529,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Lê Hồng Dũng",
   "address": "32M Cư Xá Vĩnh Hội Bến Vân Đồn,Phường 6 , Quận 4 , Hồ Chí Minh",
   "Longtitude": 10.7611037,
   "Latitude": 106.6997136
 },
 {
   "STT": 2530,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Lê Nguyễn Nhật Trung",
   "address": "184 Trần Não, Phường Bình An ,Quận 2 , Hồ Chí Minh",
   "Longtitude": 10.782775,
   "Latitude": 106.7291244
 },
 {
   "STT": 2531,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Lê Thị Minh Hồng",
   "address": "250 Nguyễn Xí, Phường 13 , Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.8182104,
   "Latitude": 106.7052422
 },
 {
   "STT": 2532,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Lê Thị Thu Thảo",
   "address": "70 Ngô Đức Kế, Phường Bến Nghé, Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.772385,
   "Latitude": 106.70423
 },
 {
   "STT": 2533,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Nguyễn Hoàng Châu",
   "address": "989 Phạm Thế Hiển, Phường 5 ,Quận 8 , Hồ Chí Minh",
   "Longtitude": 10.7384506,
   "Latitude": 106.6633606
 },
 {
   "STT": 2534,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Nguyễn Huỳnh Bảo Ngân",
   "address": "525 Huỳnh Tấn Phát, Phường Tân Thuận Đông , Quận 7 , Hồ Chí Minh",
   "Longtitude": 10.7438176,
   "Latitude": 106.7295062
 },
 {
   "STT": 2535,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Nguyễn Ngọc Thành",
   "address": "10 Tân Vĩnh, Phường 4 , Quận 4 , Hồ Chí Minh",
   "Longtitude": 10.7566982,
   "Latitude": 106.7021155
 },
 {
   "STT": 2536,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Nguyễn Phúc Thịnh",
   "address": "81/105/3 Nguyễn Cửu Vân,Phường 17 , Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.796839,
   "Latitude": 106.707077
 },
 {
   "STT": 2537,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Nguyễn Phước An",
   "address": "295 Hà Huy Giáp, Phường Thạnh Lộc , Quận 12 , Hồ Chí Minh",
   "Longtitude": 10.8643377,
   "Latitude": 106.6803789
 },
 {
   "STT": 2538,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Nguyễn Thanh Hương",
   "address": "107/35 Đường Số 11, Phường 11 ,Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.840252,
   "Latitude": 106.661874
 },
 {
   "STT": 2539,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Nguyễn Thị Ái Xuân",
   "address": "549/85/34 Lê Văn Thọ, Phường 14, Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.84706,
   "Latitude": 106.655319
 },
 {
   "STT": 2540,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Nguyễn Thị Hồng Loan",
   "address": "261/15 Chu Văn An, Phường 12 ,Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.8113495,
   "Latitude": 106.697134
 },
 {
   "STT": 2541,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Nguyễn Thị Quỳnh Lan",
   "address": "57/4 Nguyễn Tuân, Phường 3 , Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.8162495,
   "Latitude": 106.6825507
 },
 {
   "STT": 2542,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Nguyễn Thị Tường Vân",
   "address": "132 Trần Quang Khải, Phường Tân Định , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7926497,
   "Latitude": 106.6915363
 },
 {
   "STT": 2543,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Nguyễn Trọng Trí",
   "address": "24/9 Phan Văn Hớn, Khu phố 4,Phường Tân Thới Nhất , Quận 12 , Hồ Chí Minh",
   "Longtitude": 10.827368,
   "Latitude": 106.621913
 },
 {
   "STT": 2544,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Phạm Lê Thanh Bình",
   "address": "107/18 Đinh Tiên Hoàng,, Phường 3 , Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.798745,
   "Latitude": 106.695818
 },
 {
   "STT": 2545,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Phạm Minh Thu",
   "address": "80 Calmette, Phường Nguyễn Thái Bình , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.767916,
   "Latitude": 106.699425
 },
 {
   "STT": 2546,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Phạm Thị Minh Hồng",
   "address": "145 Trần Não, Phường Bình An ,Quận 2 , Hồ Chí Minh",
   "Longtitude": 10.791511,
   "Latitude": 106.7305813
 },
 {
   "STT": 2547,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Phạm Trung Dũng",
   "address": "553/6 Lũy Bán Bích, Phường Phú Thạnh , Tân Phú , Hồ Chí Minh",
   "Longtitude": 10.778044,
   "Latitude": 106.634348
 },
 {
   "STT": 2548,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Phạm Đình Hòa",
   "address": "349 Nguyễn Duy Trinh, Phường Bình Trưng Tây , Quận 2 , Hồ Chí Minh",
   "Longtitude": 10.788454,
   "Latitude": 106.76685
 },
 {
   "STT": 2549,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Phan Hà",
   "address": "19 Trần Văn Khánh, Phường Tân Thuận Đông , Quận 7 , Hồ Chí Minh",
   "Longtitude": 10.7561416,
   "Latitude": 106.7228997
 },
 {
   "STT": 2550,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Tạ Tích",
   "address": "128/4 Đinh Tiên Hoàng, Phường 1, Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.79771,
   "Latitude": 106.697269
 },
 {
   "STT": 2551,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Trần Anh",
   "address": "137A3 Nguyễn Thị Minh Khai,Phường Bến Thành , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7713815,
   "Latitude": 106.6875827
 },
 {
   "STT": 2552,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Trần Anh Huy",
   "address": "317 Phạm Hữu Lầu, Phường Phú Mỹ , Quận 7 , Hồ Chí Minh",
   "Longtitude": 10.705713,
   "Latitude": 106.7407058
 },
 {
   "STT": 2553,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Trần Thị Kim Nhung",
   "address": "13 Lô B Cư Xá Thanh Đa, Phường 27 , Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.8174339,
   "Latitude": 106.7223859
 },
 {
   "STT": 2554,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Trần Thị Minh Nguyệt",
   "address": "22 Nguyễn Đình Chiểu, Phường Đa Kao , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.790556,
   "Latitude": 106.701251
 },
 {
   "STT": 2555,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Trần Thị Xuân Hương",
   "address": "67 Cô Bắc, Phường Cô Giang ,Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.764675,
   "Latitude": 106.694761
 },
 {
   "STT": 2556,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Trần Văn Thảo",
   "address": "Tầng trệt, Era 7-9 (B2-B4, CC Kỷ Nguyên – Era Town), Đường 15B, Phạm Hữu Lầu, Phường Phú Mỹ ,Quận 7 , Hồ Chí Minh",
   "Longtitude": 10.7000542,
   "Latitude": 106.7322589
 },
 {
   "STT": 2557,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Trần Văn Định",
   "address": "465 Tân Hòa Đông, Phường Bình Trị Đông , Bình Tân , Hồ Chí Minh",
   "Longtitude": 10.7678777,
   "Latitude": 106.6151435
 },
 {
   "STT": 2558,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Trịnh Thị Thanh Ngân",
   "address": "29 Lý Văn Phức, Phường Tân Định, Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7911914,
   "Latitude": 106.6937263
 },
 {
   "STT": 2559,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Trương Thị Ngọc Anh",
   "address": "130 Nguyễn Thị Định, Phường Bình Trưng Tây , Quận 2 , Hồ Chí Minh",
   "Longtitude": 10.778685,
   "Latitude": 106.766999
 },
 {
   "STT": 2560,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Văn Bích",
   "address": "72D Nguyễn Hữu Cầu, Phường Tân Định , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.790507,
   "Latitude": 106.6907014
 },
 {
   "STT": 2561,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Võ Thị Ngọc Thúy",
   "address": "529 Trần Xuân Soạn, Phường Tân Kiểng , Quận 7 , Hồ Chí Minh",
   "Longtitude": 10.7517399,
   "Latitude": 106.6986284
 },
 {
   "STT": 2562,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Võ Thị Phương Liên",
   "address": "177 Bùi Đình Tuý, Phường 24 ,Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.8082562,
   "Latitude": 106.7078324
 },
 {
   "STT": 2563,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Vũ Minh Phúc",
   "address": "150 Cách Mạng Tháng Tám,Phường 12 , Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.779546,
   "Latitude": 106.673961
 },
 {
   "STT": 2564,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Vũ Đình Phương Ân",
   "address": "14M Quốc Hương, Khu phố 2,Phường Thảo Điền , Quận 2 , Hồ Chí Minh",
   "Longtitude": 10.8006113,
   "Latitude": 106.7321349
 },
 {
   "STT": 2565,
   "Name": "Phòng Khám Nhi Khoa - Bác sĩ. Đặng Kim Quan",
   "address": "834 Hồng Bàng, Phường 14 ,Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.754754,
   "Latitude": 106.635298
 },
 {
   "STT": 2566,
   "Name": "Phòng Khám Nhi Khoa 339",
   "address": "88B Đường 339, Phường Phước Long B , Quận 9 , Hồ Chí Minh",
   "Longtitude": 10.8236528,
   "Latitude": 106.7757822
 },
 {
   "STT": 2567,
   "Name": "Phòng Khám Nhi Nội Khoa - Bác sĩ. Phạm Văn Hoàng",
   "address": "116 Bùi Đình Túy, Phường 12 ,Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.809498,
   "Latitude": 106.7032399
 },
 {
   "STT": 2568,
   "Name": "Phòng Khám Nhi Quận 7",
   "address": "76 đường số 37, Phường Tân Quy, Quận 7 , Hồ Chí Minh",
   "Longtitude": 10.7444614,
   "Latitude": 106.7146098
 },
 {
   "STT": 2569,
   "Name": "Phòng Khám Nhi Đồng & Chấn Thương Chỉnh Hình Bác sĩ. Liên - Bác sĩ. Khoan",
   "address": "57/6 Nguyễn Văn Bứa, Ấp 1, Xã Xuân Thới Sơn , Huyện Hóc Môn , Hồ Chí Minh",
   "Longtitude": 10.8725839,
   "Latitude": 106.5385641
 },
 {
   "STT": 2570,
   "Name": "Phòng Khám Nhi Đồng - Bác sĩ Truyền - BV Nhi Đồng 1",
   "address": "135 Dương Thị Mười, Phường Tân Chánh Hiệp , Quận 12 , Hồ Chí Minh",
   "Longtitude": 10.8617178,
   "Latitude": 106.6321178
 },
 {
   "STT": 2571,
   "Name": "Phòng Khám Nhi Đồng - Bác sĩ. Cường",
   "address": "410 Phan Huy Ích, Phường 12 ,Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.833225,
   "Latitude": 106.6378469
 },
 {
   "STT": 2572,
   "Name": "Phòng Khám Nhi Đồng - Bác sĩ. Nguyễn Phước Mỹ Linh & Bác sĩ. Nguyễn Đỗ Trọng",
   "address": "62 Lý Phục Man, Phường Bình Thuận , Quận 7 , Hồ Chí Minh",
   "Longtitude": 10.741737,
   "Latitude": 106.725142
 },
 {
   "STT": 2573,
   "Name": "Phòng Khám Nhi Đồng - Bác sĩ. Nguyễn Phương Khanh",
   "address": "92 Ỷ Lan, Phường Hiệp Tân , Tân Phú , Hồ Chí Minh",
   "Longtitude": 10.776329,
   "Latitude": 106.629981
 },
 {
   "STT": 2574,
   "Name": "Phòng Khám Nhi Đồng - Bác sĩ. Thuôi",
   "address": "224 Nguyễn Tư Giản, Phường 12 ,Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.831693,
   "Latitude": 106.6405437
 },
 {
   "STT": 2575,
   "Name": "Phòng Khám Nhi Đồng - Bác sĩ. Tuyết & Bác sĩ. Phượng - CS1",
   "address": "49 Đường 6, Phường Phước Bình ,Quận 9 , Hồ Chí Minh",
   "Longtitude": 10.8366223,
   "Latitude": 106.7794265
 },
 {
   "STT": 2576,
   "Name": "Phòng Khám Nhi Đồng - Bác sĩ. Tuyết & Bác sĩ. Phượng - CS2",
   "address": "2A Lê Văn Thịnh, Phường Bình Trưng Tây , Quận 2 , Hồ Chí Minh",
   "Longtitude": 10.7879902,
   "Latitude": 106.7690877
 },
 {
   "STT": 2577,
   "Name": "Phòng Khám Nhi Đồng - Bác sĩ. Đinh Hồng Duyên",
   "address": "91/2/5 Phạm Văn Chiêu, Phường 14 , Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.850987,
   "Latitude": 106.654279
 },
 {
   "STT": 2578,
   "Name": "Phòng Khám Nhi Đồng Bác sĩ.Trần Thị Bích Kim",
   "address": "sô 1 Đường số 4, Phường Tam Phú, Thủ Đức , Hồ Chí Minh",
   "Longtitude": 10.8397971,
   "Latitude": 106.7631894
 },
 {
   "STT": 2579,
   "Name": "Phòng Khám Nhi Đồng MISA",
   "address": "292 Đặng Thúc Vịnh, Xã Thới Tam Thôn , Huyện Hóc Môn , Hồ Chí Minh",
   "Longtitude": 10.888918,
   "Latitude": 106.607219
 },
 {
   "STT": 2580,
   "Name": "Phòng Khám Nhi Đồng Thạc Sĩ Bác sĩ.Nguyễn Thị Kim Nhi",
   "address": "E12/346 Quốc lộ 50, Xã Phong Phú , Huyện Bình Chánh , Hồ Chí Minh",
   "Longtitude": 10.6995307,
   "Latitude": 106.6468148
 },
 {
   "STT": 2581,
   "Name": "Phòng Khám Nhi Đồng Thành Phố",
   "address": "31 Lý Tự Trọng, Phường Bến Nghé, Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7795135,
   "Latitude": 106.7026522
 },
 {
   "STT": 2582,
   "Name": "Phòng Khám Nhi Đồng Yêu Trẻ - Bác sĩ. Lê Thị Ngọc Kim",
   "address": "10/5 Hà Huy Giáp, Phường Thạnh Lộc , Quận 12 , Hồ Chí Minh",
   "Longtitude": 10.8517779,
   "Latitude": 106.6788822
 },
 {
   "STT": 2583,
   "Name": "Phòng Khám Nhi Đồng Yêu Trẻ Củ Chi - CS1",
   "address": "968 Quốc lộ 22, Xã Tân An Hội ,Huyện Củ Chi , Hồ Chí Minh",
   "Longtitude": 10.9736312,
   "Latitude": 106.478148
 },
 {
   "STT": 2584,
   "Name": "Phòng Khám Nhơn Nghĩa Đường",
   "address": "86C Hải Thượng Lãn Ông, Phường 10 , Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7513462,
   "Latitude": 106.6623039
 },
 {
   "STT": 2585,
   "Name": "Phòng Khám Nhũ - Bác sĩ. Nguyễn Trần Bảo Chi",
   "address": "144D Lý Chính Thắng, Phường 7 ,Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.786702,
   "Latitude": 106.684498
 },
 {
   "STT": 2586,
   "Name": "Phòng Khám Nội - Hô Hấp",
   "address": "167 Ngô Quyền, Phường 6 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.763691,
   "Latitude": 106.6644977
 },
 {
   "STT": 2587,
   "Name": "Phòng Khám Nội Bác sĩ Lê Thiên Sơn",
   "address": "201/1 Hiệp Thành 13, Phường Hiệp Thành , Quận 12 , Hồ Chí Minh",
   "Longtitude": 10.8722061,
   "Latitude": 106.6340872
 },
 {
   "STT": 2588,
   "Name": "Phòng Khám Nội Khoa & Da Liễu - Bác Sĩ Bùi Văn Quang",
   "address": "14/5 Thống Nhất, Phường 16 , Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.8421109,
   "Latitude": 106.66594
 },
 {
   "STT": 2589,
   "Name": "Phòng Khám Nội Khoa & Siêu Âm - Bác sĩ Quang",
   "address": "361 Nguyễn Thị Thập, Phường Tân Phong , Quận 7 , Hồ Chí Minh",
   "Longtitude": 10.7383233,
   "Latitude": 106.7138421
 },
 {
   "STT": 2590,
   "Name": "Phòng Khám Nội Khoa & Tiểu Đường - Bác sĩ Hà Từ Hồng",
   "address": "709 Bến Bình Đông, Phường 13 ,Quận 8 , Hồ Chí Minh",
   "Longtitude": 10.744045,
   "Latitude": 106.652305
 },
 {
   "STT": 2591,
   "Name": "Phòng Khám Nội Khoa - Bác sĩ Trần Văn Hội",
   "address": "21/2 Tự Lập, Phường 4 , Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.7919721,
   "Latitude": 106.6555041
 },
 {
   "STT": 2592,
   "Name": "Phòng Khám Nội Khoa - Bác sĩ Vĩnh",
   "address": "15/62 Hoàng Hoa Thám, Phường 13 , Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.808216,
   "Latitude": 106.647702
 },
 {
   "STT": 2593,
   "Name": "Phòng Khám Nội Khoa - Bác sĩ. Lê Kim Thạch",
   "address": "190 Bình Lợi, Phường 13 , Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.8334449,
   "Latitude": 106.7047226
 },
 {
   "STT": 2594,
   "Name": "Phòng Khám Nội Khoa - Bác sĩ. Đạo",
   "address": "12 Đường số 19, Phường Tân Quy, Quận 7 , Hồ Chí Minh",
   "Longtitude": 10.7448512,
   "Latitude": 106.7097887
 },
 {
   "STT": 2595,
   "Name": "Phòng Khám Nội Khoa Hiệp Bình",
   "address": "14 Hiệp Bình, Phường Hiệp Bình Chánh , Thủ Đức , Hồ Chí Minh",
   "Longtitude": 10.8295453,
   "Latitude": 106.7133781
 },
 {
   "STT": 2596,
   "Name": "Phòng Khám Nội Khoa Medi Sài Gòn",
   "address": "92/4 Hà Huy Giáp, Phường Thạnh Xuân , Quận 12 , Hồ Chí Minh",
   "Longtitude": 10.8908305,
   "Latitude": 106.6858908
 },
 {
   "STT": 2597,
   "Name": "Phòng Khám Nội Khoa Nhi - Bác sĩ. Trần Thi Như Thủy",
   "address": "F16 Cư xá Phú Lâm, Phường 13 ,Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.753866,
   "Latitude": 106.629653
 },
 {
   "STT": 2598,
   "Name": "Phòng Khám Nội Khoa Tổng Quát",
   "address": "142 Nguyễn Chí Thanh, Phường 3, Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.760672,
   "Latitude": 106.671734
 },
 {
   "STT": 2599,
   "Name": "Phòng Khám Nội Ngoại Khoa - Bác sĩ Sơn",
   "address": "44 Tân Hải, Phường 13 , Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.8017193,
   "Latitude": 106.6391563
 },
 {
   "STT": 2600,
   "Name": "Phòng Khám Nội Nhi - Bác sĩ Nguyễn Minh Ngọc",
   "address": "2 Chân Lý, Phường Bình Thọ , Thủ Đức , Hồ Chí Minh",
   "Longtitude": 10.848918,
   "Latitude": 106.769668
 },
 {
   "STT": 2601,
   "Name": "Phòng Khám Nội Nhi - Bác sĩ Đỗ Thị Thúy Nga",
   "address": "19 Trần Nhật Duật, Phường Tân Định , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.792851,
   "Latitude": 106.689783
 },
 {
   "STT": 2602,
   "Name": "Phòng Khám Nội Nhi - Bác sĩ. Nguyễn Quốc Khánh",
   "address": "125 Nguyễn Thị Định, Phường Bình Trưng Tây , Quận 2 , Hồ Chí Minh",
   "Longtitude": 10.7884597,
   "Latitude": 106.7552286
 },
 {
   "STT": 2603,
   "Name": "Phòng Khám Nội Nhi - Bác sĩ. Nguyễn Văn Hùng",
   "address": "283/3 Nơ Trang Long, Phường 13, Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.8190337,
   "Latitude": 106.7024864
 },
 {
   "STT": 2604,
   "Name": "Phòng Khám Nội Nhi - Bác sĩ. Trần Châu Thái",
   "address": "151 Tân Kỳ Tân Quý, Phường Tân Sơn Nhì , Tân Phú , Hồ Chí Minh",
   "Longtitude": 10.8026751,
   "Latitude": 106.6282191
 },
 {
   "STT": 2605,
   "Name": "Phòng Khám Nội Nhi - Bác sĩ. Vũ Thanh Xuân",
   "address": "F2/2, Xã Vĩnh Lộc B , Huyện Bình Chánh , Hồ Chí Minh",
   "Longtitude": 10.8180932,
   "Latitude": 106.574041
 },
 {
   "STT": 2606,
   "Name": "Phòng Khám Nội Nhi Tổng Hợp - Bác sĩ Huỳnh Thị Bích Huyền",
   "address": "311 Võ Thành Trang, Phường 11 ,Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.7888532,
   "Latitude": 106.6468999
 },
 {
   "STT": 2607,
   "Name": "Phòng Khám Nội Phổi - Bác sĩ. Nguyễn Thanh Xuân",
   "address": "774 Thống Nhất, Phường 15 , Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.8550888,
   "Latitude": 106.6655515
 },
 {
   "STT": 2608,
   "Name": "Phòng Khám Nội Soi Tai Mũi Họng - Bác sĩ La Thị Kim Liên",
   "address": "271 Phan Đình Phùng, Phường 15, Phú Nhuận , Hồ Chí Minh",
   "Longtitude": 10.796946,
   "Latitude": 106.6811
 },
 {
   "STT": 2609,
   "Name": "Phòng Khám Nội Soi Tai Mũi Họng - Bác sĩ. Trương Tam Phong",
   "address": "137 Trần Thủ Độ, Phường Phú Thạnh , Tân Phú , Hồ Chí Minh",
   "Longtitude": 10.782279,
   "Latitude": 106.6262409
 },
 {
   "STT": 2610,
   "Name": "Phòng Khám Nội Thần Kinh - Bác sĩ Lê Minh",
   "address": "7A Đặng Tất, Phường Tân Định ,Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7928286,
   "Latitude": 106.6891781
 },
 {
   "STT": 2611,
   "Name": "Phòng Khám Nội Thần Kinh - Ths Bác sĩ Thân Thị Minh Trung",
   "address": "63 Nguyễn Hồng Đào, Phường 14, Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.796376,
   "Latitude": 106.643298
 },
 {
   "STT": 2612,
   "Name": "Phòng Khám Nội Tiêu Hóa Bác sĩ. Nguyễn Bá Sơn",
   "address": "586 Phan Văn Trị, Phường 7 , Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.8302904,
   "Latitude": 106.6794121
 },
 {
   "STT": 2613,
   "Name": "Phòng Khám Nội Tiêu Hóa Gan Mật Bác sĩ Phương - Thận Nội Tiết Bác sĩ Kim Chi",
   "address": "357A/25 Nguyễn Trọng Tuyển,Phường 1 , Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.797195,
   "Latitude": 106.666023
 },
 {
   "STT": 2614,
   "Name": "Phòng Khám Nội Tim Mạch - Bác sĩ Hồ Minh",
   "address": "92B Bùi Thị Xuân, Phường Bến Thành , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7712324,
   "Latitude": 106.6892343
 },
 {
   "STT": 2615,
   "Name": "Phòng Khám Nội Tim Mạch - Bác sĩ Huỳnh Ngọc Long",
   "address": "52 Trịnh Văn Cấn, Phường Cầu Ông Lãnh , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.767257,
   "Latitude": 106.696122
 },
 {
   "STT": 2616,
   "Name": "Phòng Khám Nội Tim Mạch - Bác sĩ Võ Văn Niên",
   "address": "164 Bùi Thị Xuân, Phường 3 , Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.794383,
   "Latitude": 106.664475
 },
 {
   "STT": 2617,
   "Name": "Phòng Khám Nội Tim Mạch - PGS.TS.Bác sĩ Phạm Nguyễn Vinh - Bác sĩ Tạ Đình Việt Phương",
   "address": "9 Đông Sơn, Phường 7 , Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.786744,
   "Latitude": 106.6549919
 },
 {
   "STT": 2618,
   "Name": "Phòng Khám Nội Tim Mạch - Tổng Quát - Bác sĩ Lê Thanh Bình",
   "address": "39A Hồng Lạc, Phường 10 , Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.7852575,
   "Latitude": 106.6498623
 },
 {
   "STT": 2619,
   "Name": "Phòng Khám Nội Tim Mạch Bác sĩ Toàn Khoa - Nhi Khoa Bác sĩ Như Uyên",
   "address": "148 Võ Thành Trang, Phường 11 ,Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.791108,
   "Latitude": 106.6492297
 },
 {
   "STT": 2620,
   "Name": "Phòng Khám Nội Tổng Hợp & Tim Mạch - TS.Bác sĩ. Tạ Thị Thanh Hương",
   "address": "545/6 Ba Tháng Hai, Phường 8 ,Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7772017,
   "Latitude": 106.6811454
 },
 {
   "STT": 2621,
   "Name": "Phòng Khám Nội Tổng Hợp - Bác sĩ Cao Phi Phong",
   "address": "437C Phạm Văn Chí, Phường 7 ,Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.742322,
   "Latitude": 106.642551
 },
 {
   "STT": 2622,
   "Name": "Phòng Khám Nội Tổng Hợp - Bác sĩ Hà Tuấn Khánh",
   "address": "349/1 Nguyễn Trãi, Phường 7 ,Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7542123,
   "Latitude": 106.6691542
 },
 {
   "STT": 2623,
   "Name": "Phòng Khám Nội Tổng Hợp - Bác sĩ Nguyễn Mạnh Tuấn",
   "address": "108/8 Nguyễn Thiện Thuật,Phường 2 , Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.767648,
   "Latitude": 106.6806293
 },
 {
   "STT": 2624,
   "Name": "Phòng Khám Nội Tổng Hợp - Bác sĩ Nguyễn Năng Viện",
   "address": "1480 Trường Sa, Phường 3 , Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.793118,
   "Latitude": 106.6599046
 },
 {
   "STT": 2625,
   "Name": "Phòng Khám Nội Tổng Hợp - Bác sĩ Quách Đức Hộ",
   "address": "686/16 Cách Mạng Tháng Tám,Phường 5 , Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.787428,
   "Latitude": 106.664098
 },
 {
   "STT": 2626,
   "Name": "Phòng Khám Nội Tổng Hợp - Bác sĩ Tiếng",
   "address": "88 An Bình, Phường 5 , Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7516134,
   "Latitude": 106.6728851
 },
 {
   "STT": 2627,
   "Name": "Phòng Khám Nội Tổng Hợp - Bác sĩ Triệu Thị Thái",
   "address": "171 Bàu Cát, Phường 14 , Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.7922042,
   "Latitude": 106.6433354
 },
 {
   "STT": 2628,
   "Name": "Phòng Khám Nội Tổng Hợp - Bác sĩ Trương Quốc Dũng",
   "address": "802 Lạc Long Quân, Phường 9 ,Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.78132,
   "Latitude": 106.6500475
 },
 {
   "STT": 2629,
   "Name": "Phòng Khám Nội Tổng Hợp - Bác sĩ Võ Văn Thắng",
   "address": "732G Hương Lộ 2, Phường Bình Trị Đông A , Bình Tân , Hồ Chí Minh",
   "Longtitude": 10.7656942,
   "Latitude": 106.6045689
 },
 {
   "STT": 2630,
   "Name": "Phòng Khám Nội Tổng Hợp - Bác sĩ. Mộc Thiên Hưng",
   "address": "437 Lê Quang Sung, Phường 9 ,Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.751937,
   "Latitude": 106.639726
 },
 {
   "STT": 2631,
   "Name": "Phòng Khám Nội Tổng Hợp - Bác sĩ. Nguyễn Văn Hồng Vũ",
   "address": "14 Phạm Đôn, Phường 10 , Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7512255,
   "Latitude": 106.6633802
 },
 {
   "STT": 2632,
   "Name": "Phòng Khám Nội Tổng Hợp& Nội Cơ Xương Khớp - Bác sĩ Lê Anh Thư",
   "address": "680/94 Nguyễn Trãi, Phường 11 ,Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7534296,
   "Latitude": 106.6618635
 },
 {
   "STT": 2633,
   "Name": "Phòng Khám Nội Tổng Quát & Nhi - Bác sĩ. Nguyễn Hùng Diệt - Bác sĩ. Lê Thị Chưa",
   "address": "A17/35 Ấp 1, Xã Bình Chánh ,Huyện Bình Chánh , Hồ Chí Minh",
   "Longtitude": 10.776099,
   "Latitude": 106.5780894
 },
 {
   "STT": 2634,
   "Name": "Phòng Khám Nội Tổng Quát & Nhi Khoa - Bác sĩ. Hảo Bác sĩ. Điệp",
   "address": "70B Trần Đình Xu, Phường Cô Giang , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.760744,
   "Latitude": 106.691366
 },
 {
   "STT": 2635,
   "Name": "Phòng Khám Nội Tổng Quát & Tiêu Hóa - Bác sĩ. Vương Thừa Đức",
   "address": "491/19 Nguyễn Đình Chiểu,Phường 2 , Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.7684423,
   "Latitude": 106.6821858
 },
 {
   "STT": 2636,
   "Name": "Phòng Khám Nội Tổng Quát & Tim Mạch - Bác sĩ Đoàn Thái",
   "address": "169 Nguyễn Thị Nhỏ, Phường 9 ,Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.772389,
   "Latitude": 106.6528161
 },
 {
   "STT": 2637,
   "Name": "Phòng Khám Nội Tổng Quát & Tim Mạch - Bác sĩ. Trần Phi Sơn",
   "address": "158 Võ Văn Ngân, Phường Bình Thọ , Thủ Đức , Hồ Chí Minh",
   "Longtitude": 10.850155,
   "Latitude": 106.761835
 },
 {
   "STT": 2638,
   "Name": "Phòng Khám Nội Tổng Quát - Bác Sĩ Bùi Văn Sen",
   "address": "220 Nguyễn Chí Thanh, Phường 3, Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7602408,
   "Latitude": 106.6700175
 },
 {
   "STT": 2639,
   "Name": "Phòng Khám Nội Tổng Quát - Bảo Sanh Vạn Hanh",
   "address": "185 - 189 Quang Trung, Phường 10 , Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.8347749,
   "Latitude": 106.664115
 },
 {
   "STT": 2640,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ Hoàng Công Thành",
   "address": "87 Phạm Phú Thứ, Phường 11 ,Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.7888648,
   "Latitude": 106.6477589
 },
 {
   "STT": 2641,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ Lê Minh Trí",
   "address": "1 Phan Văn Sửu, Phường 13 , Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.805508,
   "Latitude": 106.6370918
 },
 {
   "STT": 2642,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ Lê Viết Hiếu",
   "address": "437/7 Hoàng Văn Thụ, Phường 4 ,Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.79668,
   "Latitude": 106.658022
 },
 {
   "STT": 2643,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ Nguyễn Thị Mộng Trang",
   "address": "444 An Dương Vương, Phường 10, Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.7403696,
   "Latitude": 106.6238525
 },
 {
   "STT": 2644,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ Điền Hòa Lễ",
   "address": "15/14 Ngô Quyền, Phường 10 ,Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7511614,
   "Latitude": 106.6662347
 },
 {
   "STT": 2645,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ Đỗ Trung Ngọc",
   "address": "55 Phạm Văn Chí, Phường 1 ,Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.7470391,
   "Latitude": 106.6506423
 },
 {
   "STT": 2646,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ. Bùi Đắc Chí",
   "address": "62/10 Điện Biên Phủ, Phường Đa Kao , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.791254,
   "Latitude": 106.69766
 },
 {
   "STT": 2647,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ. Dương Thị Vân Anh",
   "address": "434 Phan Văn Trị, Phường 7 ,Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.755292,
   "Latitude": 106.6760763
 },
 {
   "STT": 2648,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ. Dương Văn Ninh",
   "address": "2/1A Đường số 4, Phường Bình Chiểu , Thủ Đức , Hồ Chí Minh",
   "Longtitude": 10.8775502,
   "Latitude": 106.7491454
 },
 {
   "STT": 2649,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ. Hồ Thị Minh Hồng",
   "address": "42/480B Lê Đức Thọ, Phường 17 ,Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.8422699,
   "Latitude": 106.676366
 },
 {
   "STT": 2650,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ. Hoà",
   "address": "31 Đường số 15, Phường Bình Chiểu , Thủ Đức , Hồ Chí Minh",
   "Longtitude": 10.8769902,
   "Latitude": 106.7444958
 },
 {
   "STT": 2651,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ. Hoàng Hải",
   "address": "161 Nguyễn Văn Nghi, Phường 7 ,Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.8262227,
   "Latitude": 106.6829974
 },
 {
   "STT": 2652,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ. Hứa Khắc Vũ",
   "address": "D7/80 Trịnh Như Khuê, Xã Bình Chánh , Huyện Bình Chánh , Hồ Chí Minh",
   "Longtitude": 10.6630405,
   "Latitude": 106.5692428
 },
 {
   "STT": 2653,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ. Lâm Anh Minh",
   "address": "70 Đường số 8, Phường Linh Trung , Thủ Đức , Hồ Chí Minh",
   "Longtitude": 10.8572777,
   "Latitude": 106.7746422
 },
 {
   "STT": 2654,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ. Lê Hoàng",
   "address": "816 Tỉnh lộ 7, Xã Phước Thạnh ,Huyện Củ Chi , Hồ Chí Minh",
   "Longtitude": 11.0068812,
   "Latitude": 106.4281472
 },
 {
   "STT": 2655,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ. Lê Tự Quốc Tuấn",
   "address": "848 Trường Chinh, Phường 15 ,Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.8228916,
   "Latitude": 106.6295166
 },
 {
   "STT": 2656,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ. Nguyễn Trí Dũng",
   "address": "33 Nguyễn Trung Ngạn, Phường Bến Nghé , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7828689,
   "Latitude": 106.7044977
 },
 {
   "STT": 2657,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ. Nguyễn Văn Châu",
   "address": "59 Bùi Thị Xuân, Phường Bến Thành , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7708327,
   "Latitude": 106.6896174
 },
 {
   "STT": 2658,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ. Nguyễn Vĩnh Bình",
   "address": "351 Thống Nhất, Phường 11 , Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.838814,
   "Latitude": 106.662007
 },
 {
   "STT": 2659,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ. Nguyễn Đình Liêm",
   "address": "107/4Bis Thống Nhất, Phường 11 ,Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.8394135,
   "Latitude": 106.6653068
 },
 {
   "STT": 2660,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ. Phạm Quý Hiệp",
   "address": "110 Trần Đình Xu, Phường Nguyễn Cư Trinh , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.762043,
   "Latitude": 106.689313
 },
 {
   "STT": 2661,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ. Phạm Văn Luyện",
   "address": "75 Đường số 7, Phường Linh Trung , Thủ Đức , Hồ Chí Minh",
   "Longtitude": 10.8566341,
   "Latitude": 106.7661364
 },
 {
   "STT": 2662,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ. Phan Ngọc Thạch",
   "address": "48/2 Điện Biên Phủ, Phường Đa Kao , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.791506,
   "Latitude": 106.698018
 },
 {
   "STT": 2663,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ. Phùng Đức Thắng",
   "address": "63/13 Pasteur, Phường Bến Nghé, Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.774901,
   "Latitude": 106.7004709
 },
 {
   "STT": 2664,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ. Sửu",
   "address": "59 Dương Quảng Hàm, Phường 5 ,Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.8247002,
   "Latitude": 106.6952044
 },
 {
   "STT": 2665,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ. Tố Hoài",
   "address": "74 Trần Thị Nghĩ, Phường 7 , Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.8276448,
   "Latitude": 106.6818065
 },
 {
   "STT": 2666,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ. Vinh",
   "address": "10 Quang Trung, Phường 11 , Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.8351723,
   "Latitude": 106.6631906
 },
 {
   "STT": 2667,
   "Name": "Phòng Khám Nội Tổng Quát - Bác sĩ. Võ Văn Nhanh",
   "address": "299 Lê Trọng Tấn, Phường Sơn Kỳ, Tân Phú , Hồ Chí Minh",
   "Longtitude": 10.807796,
   "Latitude": 106.6202507
 },
 {
   "STT": 2668,
   "Name": "Phòng Khám Nội Tổng Quát Bác sĩ Công Thành - Nội Thần Kinh Bác sĩ Cẩm Linh",
   "address": "102 Hiệp Nhất, Phường 4 , Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.7930299,
   "Latitude": 106.6560906
 },
 {
   "STT": 2669,
   "Name": "Phòng Khám Nội Tổng Quát Bác sĩ. Nguyễn Hữu Chí",
   "address": "112 Cống Quỳnh, Phường Phạm Ngũ Lão , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.765195,
   "Latitude": 106.690447
 },
 {
   "STT": 2670,
   "Name": "Phòng Khám Nội Tổng Quát Và Nhi Khoa Hoàng Lâm",
   "address": "29 Liêu Bình Hương, Xã Tân Thông Hội , Huyện Củ Chi , Hồ Chí Minh",
   "Longtitude": 10.9640084,
   "Latitude": 106.5038547
 },
 {
   "STT": 2671,
   "Name": "Phòng Khám Phạm Kiều",
   "address": "5C Đặng Thúc Vịnh, Xã Đông Thạnh , Huyện Hóc Môn , Hồ Chí Minh",
   "Longtitude": 10.9062782,
   "Latitude": 106.6439152
 },
 {
   "STT": 2672,
   "Name": "Phòng Khám Phẫu Thuật Thẩm Mỹ - Bác sĩ. Võ Thiện Lai",
   "address": "",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 2673,
   "Name": "Phòng Khám Phổi & Nội Khoa Bà Hom",
   "address": "",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 2674,
   "Name": "Phòng Khám Phổi - Sản Khoa",
   "address": "",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 2675,
   "Name": "Phòng Khám Phẫu Thuật Thẩm Mỹ - Bác sĩ. Võ Thiện Lai",
   "address": "181/29 Ba Tháng Hai, Phường 11, Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7654743,
   "Latitude": 106.662969
 },
 {
   "STT": 2676,
   "Name": "Phòng Chống Chấn Thương & Các Bệnh Không Lây",
   "address": "104 Huỳnh Văn Bánh, Phường 17 ,Phú Nhuận , Hồ Chí Minh",
   "Longtitude": 10.793386,
   "Latitude": 106.680148
 },
 {
   "STT": 2677,
   "Name": "Phòng Khám Phong Vân",
   "address": "653/31 Quang Trung, Phường 11 ,Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.837935,
   "Latitude": 106.65948
 },
 {
   "STT": 2678,
   "Name": "Phòng Khám Phụ Khoa - Bác sĩ Bùi Thanh Thảo",
   "address": "1118 Tỉnh Lộ 10, Phường Tân Tạo, Bình Tân , Hồ Chí Minh",
   "Longtitude": 10.7549476,
   "Latitude": 106.5993362
 },
 {
   "STT": 2679,
   "Name": "Phòng Khám Phụ Khoa - Bác sĩ Nguyễn Hoàng Lệ",
   "address": "27 Lô E Đường Số 14, Phường 11 ,Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.7458167,
   "Latitude": 106.6303386
 },
 {
   "STT": 2680,
   "Name": "Phòng Khám Phụ Sản Hoàng Gia",
   "address": "284B Nguyễn Trọng Tuyển,Phường 10 , Phú Nhuận , Hồ Chí Minh",
   "Longtitude": 10.7982119,
   "Latitude": 106.6694258
 },
 {
   "STT": 2681,
   "Name": "Phòng Khám Phụ Sản Minh Khai",
   "address": "430 Nguyễn Thị Minh Khai,Phường 5 , Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.7698587,
   "Latitude": 106.6855731
 },
 {
   "STT": 2682,
   "Name": "Phòng Khám Phụ Sản Minh Phúc",
   "address": "137A1 Nguyễn Chí Thanh, Phường 8 , Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7594911,
   "Latitude": 106.6670859
 },
 {
   "STT": 2683,
   "Name": "Phòng Khám Phú Đa",
   "address": "485 Tỉnh Lộ 10, Phường Bình Trị Đông B , Bình Tân , Hồ Chí Minh",
   "Longtitude": 10.7572803,
   "Latitude": 106.6188837
 },
 {
   "STT": 2684,
   "Name": "Phòng Khám Phục Hồi Chức Năng - Bác sĩ Võ Thị Mảnh",
   "address": "220 Bãi Sậy, Phường 4 , Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.7470665,
   "Latitude": 106.6443358
 },
 {
   "STT": 2685,
   "Name": "Phòng Khám Phục Hồi Chức Năng - Bác sĩ. Nguyễn Đình Trường",
   "address": "24/48A Lê Thị Hồng, Phường 17 ,Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.8331337,
   "Latitude": 106.6777667
 },
 {
   "STT": 2686,
   "Name": "Phòng Khám Phục Hồi Chức Năng Phúc An",
   "address": "552A Tân Kỳ Tân Quý, Phường Bình Hưng Hòa , Bình Tân , Hồ Chí Minh",
   "Longtitude": 10.7946829,
   "Latitude": 106.6102106
 },
 {
   "STT": 2687,
   "Name": "Phòng Khám Phước Lê",
   "address": "249/29 Nguyễn Tiểu La, Phường 8, Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7605254,
   "Latitude": 106.6668583
 },
 {
   "STT": 2688,
   "Name": "Phòng Khám Phước Long",
   "address": "22 Đường Số 61, Phường Phước Long B , Quận 9 , Hồ Chí Minh",
   "Longtitude": 10.8299445,
   "Latitude": 106.7719388
 },
 {
   "STT": 2689,
   "Name": "Phòng Khám Phương Uyên",
   "address": "595 Lê Trọng Tấn, Phường Bình Hưng Hòa , Bình Tân , Hồ Chí Minh",
   "Longtitude": 10.8091912,
   "Latitude": 106.6151838
 },
 {
   "STT": 2690,
   "Name": "Phòng Khám Quốc Tế Ánh Nga - Chuyên Khoa Nội Ký Sinh Trùng",
   "address": "402 An Dương Vương, Phường 4 ,Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7592676,
   "Latitude": 106.6779836
 },
 {
   "STT": 2691,
   "Name": "Phòng Khám Quốc Tế Careplus",
   "address": "107 Tân Hải, Phường 13 , Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.8025612,
   "Latitude": 106.6384214
 },
 {
   "STT": 2692,
   "Name": "Phòng Khám Quốc Tế Careplus - CN2",
   "address": "105 Tôn Dật Tiên (Tầng 2, Crescent Plaza), Phường Tân Phú , Quận 7 , Hồ Chí Minh",
   "Longtitude": 10.7272787,
   "Latitude": 106.7201185
 },
 {
   "STT": 2693,
   "Name": "Phòng Khám Quốc Tế EXSON",
   "address": "722 Sư Vạn Hạnh, Phường 12 ,Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7718,
   "Latitude": 106.6697429
 },
 {
   "STT": 2694,
   "Name": "Phòng Khám Quốc Tế Victoria Healthcare - Cơ Sở 1",
   "address": "79 Điện Biên Phủ, Phường Đa Kao, Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7895506,
   "Latitude": 106.6974753
 },
 {
   "STT": 2695,
   "Name": "Phòng Khám Quốc Tế Victoria Healthcare - Cơ Sở 2",
   "address": "135A Nguyễn Văn Trỗi, Phường 11, Phú Nhuận , Hồ Chí Minh",
   "Longtitude": 10.794281,
   "Latitude": 106.677444
 },
 {
   "STT": 2696,
   "Name": "Phòng Khám Quốc Tế Victoria Healthcare - Cơ Sở 3",
   "address": "Tòa nhà Broadway B, 152 Nguyễn Lương Bằng, Phường Tân Phú ,Quận 7 , Hồ Chí Minh",
   "Longtitude": 10.7293017,
   "Latitude": 106.721126
 },
 {
   "STT": 2697,
   "Name": "Phòng Khám Quốc Tế Victoria Healthcare - Cơ Sở 4",
   "address": "20 - 20 Bis-22 Đinh Tiên Hoàng,Phường Đa Kao , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7888655,
   "Latitude": 106.6992761
 },
 {
   "STT": 2698,
   "Name": "Phòng Khám Quốc Tế Việt Mỹ",
   "address": "829 Nguyễn Văn Quá, Phường Đông Hưng Thuận , Quận 12 , Hồ Chí Minh",
   "Longtitude": 10.8470206,
   "Latitude": 106.6345683
 },
 {
   "STT": 2699,
   "Name": "Phòng Khám Quốc Tế Đa Khoa Seasain",
   "address": "12 Nguyễn Bá Tuyển, Phường 12 ,Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.7998103,
   "Latitude": 106.6479081
 },
 {
   "STT": 2700,
   "Name": "Phòng Khám Răng - Bác sĩ. Mai Ngọc Dung",
   "address": "174 Đề Thám, Phường Cầu Ông Lãnh , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.765914,
   "Latitude": 106.69471
 },
 {
   "STT": 2701,
   "Name": "Phòng Khám Răng - Bác sĩ. Nguyễn Văn Khánh",
   "address": "348 Phạm Thế Hiển, Phường 3 ,Quận 8 , Hồ Chí Minh",
   "Longtitude": 10.7465276,
   "Latitude": 106.6824374
 },
 {
   "STT": 2702,
   "Name": "Phòng Khám Răng Gia Đình - Bác sĩ. Phương",
   "address": "1050/4 Quang Trung, Phường 12 ,Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.841695,
   "Latitude": 106.641375
 },
 {
   "STT": 2703,
   "Name": "Phòng Khám Răng Hàm Mặt",
   "address": "315 Hòa Hảo, Phường 4 , Quận 10, Hồ Chí Minh",
   "Longtitude": 10.761905,
   "Latitude": 106.669121
 },
 {
   "STT": 2704,
   "Name": "Phòng Khám Răng Hàm Mặt - Bác sĩ Hồng Quân",
   "address": "339 Nguyễn Trọng Tuyển, Phường 1 , Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.798141,
   "Latitude": 106.668305
 },
 {
   "STT": 2705,
   "Name": "Phòng Khám Răng Hàm Mặt - Bác sĩ Phan Thị Thanh Xuân",
   "address": "13 Trần Mai Ninh, Phường 12 ,Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.7941239,
   "Latitude": 106.648101
 },
 {
   "STT": 2706,
   "Name": "Phòng Khám Răng Hàm Mặt - Bác sĩ. Hương Giang",
   "address": "405-C15 Xô Viết Nghệ Tĩnh,Phường 24 , Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.8053329,
   "Latitude": 106.71132
 },
 {
   "STT": 2707,
   "Name": "Phòng Khám Răng Hàm Mặt - Nha Sĩ Nguyễn Thanh Thảo",
   "address": "281/52/12 Lê Văn Sỹ, Phường 1 ,Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.793347,
   "Latitude": 106.666586
 },
 {
   "STT": 2708,
   "Name": "Phòng Khám Răng Hàm Mặt - Nha Sĩ Vũ Mỹ Linh",
   "address": "158/262/10 Phạm Văn Hai,Phường 3 , Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.79282,
   "Latitude": 106.660933
 },
 {
   "STT": 2709,
   "Name": "Phòng Khám Răng Hàm Mặt An Phú - Bác sĩ. Ngô Thị Thúy Phượng",
   "address": "50 Nguyễn Hoàng, Kp 5, Phường An Phú , Quận 2 , Hồ Chí Minh",
   "Longtitude": 10.7963269,
   "Latitude": 106.7449066
 },
 {
   "STT": 2710,
   "Name": "Phòng Khám Răng Hàm Mặt Ngô Quyền",
   "address": "111D Ngô Quyền, Phường 6 ,Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7627511,
   "Latitude": 106.6646688
 },
 {
   "STT": 2711,
   "Name": "Phòng Khám Răng Hàm Mặt Sài Gòn",
   "address": "216 Tỉnh Lộ 15, Xã Phú Hòa Đông, Huyện Củ Chi , Hồ Chí Minh",
   "Longtitude": 11.0203175,
   "Latitude": 106.5642998
 },
 {
   "STT": 2712,
   "Name": "Phòng Khám Răng Hàm Mặt Sài Gòn - CS1",
   "address": "1256 Võ Văn Kiệt, Phường 10 ,Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7495439,
   "Latitude": 106.6612193
 },
 {
   "STT": 2713,
   "Name": "Phòng Khám Răng Hàm Mặt Sài Gòn - CS2",
   "address": "352 Trường Chinh, Phường 13 ,Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.7992797,
   "Latitude": 106.6416695
 },
 {
   "STT": 2714,
   "Name": "Phòng Khám Răng Hàm Mặt Sài Gòn - CS3",
   "address": "400C Lê Hồng Phong, Phường 1 ,Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7644261,
   "Latitude": 106.6756067
 },
 {
   "STT": 2715,
   "Name": "Phòng Khám Răng Hàm Mặt Tân Quý",
   "address": "B2/10 Ấp 2, Xã Tân Quý Tây ,Huyện Bình Chánh , Hồ Chí Minh",
   "Longtitude": 10.666887,
   "Latitude": 106.5967126
 },
 {
   "STT": 2716,
   "Name": "Phòng Khám Răng Hàm Mặt Trọng Phúc",
   "address": "483 Huỳnh Tấn Phát, Phường Tân Thuận Đông , Quận 7 , Hồ Chí Minh",
   "Longtitude": 10.7453988,
   "Latitude": 106.7293346
 },
 {
   "STT": 2717,
   "Name": "Phòng Khám Sài Gòn Quốc Tế - Chi Nhánh Nhi Đồng",
   "address": "D6/60 ấp 4, Xã Bình Chánh ,Huyện Bình Chánh , Hồ Chí Minh",
   "Longtitude": 10.6671791,
   "Latitude": 106.572107
 },
 {
   "STT": 2718,
   "Name": "Phòng Khám Sài Gòn Y Khoa",
   "address": "99 - 109 Thuận Kiều, Phường 4 ,Quận 11 , Hồ Chí Minh",
   "Longtitude": 10.7584959,
   "Latitude": 106.658372
 },
 {
   "STT": 2719,
   "Name": "Phòng Khám Sản Khoa & Di Truyền Học - Bác sĩ. Phùng Như Toàn",
   "address": "109/33 Nguyễn Thiện Thuật,Phường 2 , Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.7669573,
   "Latitude": 106.6800198
 },
 {
   "STT": 2720,
   "Name": "Phòng Khám Sản Khoa - Bác sĩ Nguyễn Thái Hà",
   "address": "115 Nguyễn Thị Minh Khai,Phường Bến Thành , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7719535,
   "Latitude": 106.6879002
 },
 {
   "STT": 2721,
   "Name": "Phòng Khám Sản Khoa - Bác sĩ. Lâm Quốc Bảo",
   "address": "57/22 Hòa Hảo, Phường 5 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7609998,
   "Latitude": 106.6661275
 },
 {
   "STT": 2722,
   "Name": "Phòng Khám Sản Khoa - Bác sĩ. Nguyễn Thị Mai",
   "address": "9B Thạch Thị Thanh, Phường Tân Định , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.789777,
   "Latitude": 106.692947
 },
 {
   "STT": 2723,
   "Name": "Phòng Khám Sản Khoa Bác sĩ. Mỹ Duyên",
   "address": "27 Lê Lợi, Thị trấn Hóc Môn ,Huyện Hóc Môn , Hồ Chí Minh",
   "Longtitude": 10.889656,
   "Latitude": 106.5935142
 },
 {
   "STT": 2724,
   "Name": "Phòng Khám Sản Nhi Bảo Sanh Sài Gòn",
   "address": "722 - 724 Huỳnh Tấn Phát,Phường Tân Phú , Quận 7 , Hồ Chí Minh",
   "Longtitude": 10.731691,
   "Latitude": 106.731518
 },
 {
   "STT": 2725,
   "Name": "Phòng Khám Sản Phụ Khoa & Da Liễu - Bác sĩ. Xuân",
   "address": "423 Đỗ Xuân Hợp, Phường Phước Long B , Quận 9 , Hồ Chí Minh",
   "Longtitude": 10.8126145,
   "Latitude": 106.775724
 },
 {
   "STT": 2726,
   "Name": "Phòng Khám Sản Phụ Khoa & Hiếm Muộn - Bác sĩ. Vũ Minh Ngọc",
   "address": "436B/71A/19 Đường 3 Tháng 2,Phường 12 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.7688404,
   "Latitude": 106.6689382
 },
 {
   "STT": 2727,
   "Name": "Phòng Khám Sản Phụ Khoa & KHHGĐ - Bác sĩ Đặng Huệ Linh",
   "address": "50 Bình Tiên, Phường 3 , Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.7412375,
   "Latitude": 106.6435086
 },
 {
   "STT": 2728,
   "Name": "Phòng Khám Sản Phụ Khoa & Siêu Âm - Bác sĩ. Hồ Thị Phượng",
   "address": "14 Ỷ Lan, Phường Hòa Thạnh ,Tân Phú , Hồ Chí Minh",
   "Longtitude": 10.775645,
   "Latitude": 106.63211
 },
 {
   "STT": 2729,
   "Name": "Phòng Khám Sản Phụ Khoa & Siêu Âm - Bác sĩ. Lê Thị Thu Cúc",
   "address": "308 Đường Số 8, Phường 11, Gò Vấp, Hồ Chí Minh",
   "Longtitude": 10.8447761,
   "Latitude": 106.6606438
 },
 {
   "STT": 2730,
   "Name": "Phòng Khám Sản Phụ Khoa & Siêu Âm - Bác sĩ. Lương Bạch Lan",
   "address": "300 Hiền Vương, Phường Phú Thạnh , Tân Phú , Hồ Chí Minh",
   "Longtitude": 10.783097,
   "Latitude": 106.621543
 },
 {
   "STT": 2731,
   "Name": "Phòng Khám Sản Phụ Khoa & Siêu Âm - Bác sĩ. Nguyễn Thị Chi",
   "address": "91 Đường Số 5, Phường Phước Bình , Quận 9 , Hồ Chí Minh",
   "Longtitude": 10.817724,
   "Latitude": 106.7713
 },
 {
   "STT": 2732,
   "Name": "Phòng Khám Sản Phụ Khoa & Siêu Âm - Bác sĩ. Phạm Nguyễn Ngọc Quang & Bác sĩ. Nguyễn Phương Thúy",
   "address": "130 Đường số 8, KDC Nam Long,Phường Tân Thuận Đông , Quận 7, Hồ Chí Minh",
   "Longtitude": 10.7448243,
   "Latitude": 106.7323494
 },
 {
   "STT": 2733,
   "Name": "Phòng Khám Sản Phụ Khoa & Siêu Âm - Bác sĩ. Trần Thị Kim Phượng",
   "address": "107/7 Huỳnh Tấn Phát, Khu phố 2, Phường Tân Thuận Tây , Quận 7 , Hồ Chí Minh",
   "Longtitude": 10.7143535,
   "Latitude": 106.7369983
 },
 {
   "STT": 2734,
   "Name": "Phòng Khám Sản Phụ Khoa & Siêu Âm - Bác sĩ. Trần Thị Trúc Phương",
   "address": "285B Phạm Hữu Lầu, Phường Phú Mỹ , Quận 7 , Hồ Chí Minh",
   "Longtitude": 10.7042767,
   "Latitude": 106.7288832
 },
 {
   "STT": 2735,
   "Name": "Phòng Khám Sản Phụ Khoa & Siêu Âm - Bác sĩ. Trần Thị Tú Uyên",
   "address": "44 Lê Văn Khương, Phường Thới An , Quận 12 , Hồ Chí Minh",
   "Longtitude": 10.8636045,
   "Latitude": 106.6498145
 },
 {
   "STT": 2736,
   "Name": "Phòng Khám Sản Phụ Khoa & Siêu Âm - Bác sĩ. Trương Thị Ánh Tuyết",
   "address": "239/85 Khuông Việt, Phường Phú Trung , Tân Phú , Hồ Chí Minh",
   "Longtitude": 10.771621,
   "Latitude": 106.638736
 },
 {
   "STT": 2737,
   "Name": "Phòng Khám Sản Phụ Khoa & Siêu Âm Hạnh Phúc - Bác sĩ. Nguyễn Thanh Thúy",
   "address": "160B/1C Vườn Lài, Phường Phú Thọ Hòa , Tân Phú , Hồ Chí Minh",
   "Longtitude": 10.786486,
   "Latitude": 106.6364526
 },
 {
   "STT": 2738,
   "Name": "Phòng Khám Sản Phụ Khoa (Bác sĩ Kim Anh) - Nội Tổng Quát (Bác sĩ Võ Tuấn)",
   "address": "255 Cao Văn Lầu, Phường 2 ,Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.750793,
   "Latitude": 106.648376
 },
 {
   "STT": 2739,
   "Name": "Phòng Khám Sản Phụ Khoa - 3 Tăng Bạt Hổ",
   "address": "3 Tăng Bạt Hổ, Phường 12 , Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7577886,
   "Latitude": 106.6626008
 },
 {
   "STT": 2740,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác Sĩ Vũ Thanh Dung",
   "address": "46 Bùi Hữu Nghĩa, Phường 5 ,Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7529536,
   "Latitude": 106.6751259
 },
 {
   "STT": 2741,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ Bùi Trúc Giang",
   "address": "165/56 Nguyễn Thái Bình, Phường Nguyễn Thái Bình , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7674749,
   "Latitude": 106.698756
 },
 {
   "STT": 2742,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ Dư Liễu Huệ",
   "address": "318 Âu Cơ, Phường 10 , Tân Bình, Hồ Chí Minh",
   "Longtitude": 10.7764779,
   "Latitude": 106.6468639
 },
 {
   "STT": 2743,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ Hồ Hoa",
   "address": "348/12 Hoàng Văn Thụ, Phường 4, Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.797296,
   "Latitude": 106.657013
 },
 {
   "STT": 2744,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ Hồ Việt Thu",
   "address": "159/52/21D Trần Văn Đang,Phường 11 , Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.785819,
   "Latitude": 106.674337
 },
 {
   "STT": 2745,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ Hoàng Thị Mỹ Ý",
   "address": "453/9 Lê Văn Sỹ, Phường 12 ,Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.788865,
   "Latitude": 106.671694
 },
 {
   "STT": 2746,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ Huỳnh Thị Hiếu",
   "address": "9 Lê Duy Nhuận, Phường 12 , Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.7975438,
   "Latitude": 106.6477181
 },
 {
   "STT": 2747,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ Lê Nguyễn Anh Thi",
   "address": "357A Nguyễn Trọng Tuyển,Phường 1 , Tân Bình , Hồ Chí Minh",
   "Longtitude": 10.798352,
   "Latitude": 106.666224
 },
 {
   "STT": 2748,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ Minh Phương",
   "address": "71 Đường số 1, Xã Bình Hưng ,Huyện Bình Chánh , Hồ Chí Minh",
   "Longtitude": 10.7263854,
   "Latitude": 106.6540785
 },
 {
   "STT": 2749,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ Nguyễn Hồng Châu",
   "address": "251 Dương Bá Trạc, Phường 1 ,Quận 8 , Hồ Chí Minh",
   "Longtitude": 10.7457879,
   "Latitude": 106.689464
 },
 {
   "STT": 2750,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ Nguyễn Thị Vĩnh Thành",
   "address": "125 Đề Thám, Phường Cô Giang ,Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.765019,
   "Latitude": 106.694753
 },
 {
   "STT": 2751,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ Nguyễn Viết Minh Đức",
   "address": "D7/63 Trịnh Như Khuê, Xã Bình Chánh , Huyện Bình Chánh , Hồ Chí Minh",
   "Longtitude": 10.6630405,
   "Latitude": 106.5692428
 },
 {
   "STT": 2752,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ Trần Thị Miền",
   "address": "3/1 Nguyễn Văn Thủ, Phường Đa Kao , Quận 1 , Hồ Chí Minh",
   "Longtitude": 10.7926227,
   "Latitude": 106.7019653
 },
 {
   "STT": 2753,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ Trương Thị Thảo",
   "address": "79 Cao Thắng, Phường 3 , Quận 3, Hồ Chí Minh",
   "Longtitude": 10.7720324,
   "Latitude": 106.6796955
 },
 {
   "STT": 2754,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ Tuấn & Bác sĩ Hường",
   "address": "85 Lê Văn Thịnh, Phường Bình Trưng Đông , Quận 2 , Hồ Chí Minh",
   "Longtitude": 10.7844157,
   "Latitude": 106.7691043
 },
 {
   "STT": 2755,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ Vũ Thị Kim Chi",
   "address": "3 Nguyễn Văn Mai, Phường 8 ,Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.7905094,
   "Latitude": 106.688267
 },
 {
   "STT": 2756,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ. Âu Nhựt Luân",
   "address": "164 Nguyễn Văn Đậu, Phường 7 ,Bình Thạnh , Hồ Chí Minh",
   "Longtitude": 10.810009,
   "Latitude": 106.689901
 },
 {
   "STT": 2757,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ. Bùi Ngọc Phượng",
   "address": "131 Hà Tôn Quyền, Phường 4 ,Quận 11 , Hồ Chí Minh",
   "Longtitude": 10.757058,
   "Latitude": 106.652678
 },
 {
   "STT": 2758,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ. Bùi Thị Bích Hội",
   "address": "165 Lê Hồng Phong, Phường 3 ,Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7577689,
   "Latitude": 106.6777548
 },
 {
   "STT": 2759,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ. Bùi Thị Phương Nga",
   "address": "K12 Cư Xá Phú Lâm A, Phường 12, Quận 6 , Hồ Chí Minh",
   "Longtitude": 10.748798,
   "Latitude": 106.630905
 },
 {
   "STT": 2760,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ. Bùi Thị Thu Hương",
   "address": "184 Bùi Văn Ba, Phường Tân Thuận Đông , Quận 7 , Hồ Chí Minh",
   "Longtitude": 10.7502922,
   "Latitude": 106.7345169
 },
 {
   "STT": 2761,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ. Cao Hữu Thịnh",
   "address": "141 Trần Bình Trọng, Phường 2 ,Quận 5 , Hồ Chí Minh",
   "Longtitude": 10.7574802,
   "Latitude": 106.6807438
 },
 {
   "STT": 2762,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ. Cao Minh Đức",
   "address": "272C Võ Thị Sáu, Phường 7 ,Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.7798069,
   "Latitude": 106.6832667
 },
 {
   "STT": 2763,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ. Cao Thị Xuyến",
   "address": "55 Nghĩa Thục, Phường 5 , Quận 5, Hồ Chí Minh",
   "Longtitude": 10.7528406,
   "Latitude": 106.6757404
 },
 {
   "STT": 2764,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ. Châu Thị Hoàng",
   "address": "33 Đường Số 17, Phường Bình Trị Đông B , Bình Tân , Hồ Chí Minh",
   "Longtitude": 10.744467,
   "Latitude": 106.6142682
 },
 {
   "STT": 2765,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ. Chi & Bác sĩ. Thủy",
   "address": "2 Trương Văn Thành, Phường Hiệp Phú , Quận 9 , Hồ Chí Minh",
   "Longtitude": 10.850891,
   "Latitude": 106.779762
 },
 {
   "STT": 2766,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ. Cù Thị Kim Loan",
   "address": "781/A1 Lê Hồng Phong, Phường 12 , Quận 10 , Hồ Chí Minh",
   "Longtitude": 10.772357,
   "Latitude": 106.6720141
 },
 {
   "STT": 2767,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ. Dương Mộng Thu Hà",
   "address": "235 Nguyễn Văn Luông, Phường 11, Quận 6, Hồ Chí Minh",
   "Longtitude": 10.745417,
   "Latitude": 106.635137
 },
 {
   "STT": 2768,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ. Dương Mỹ Phụng",
   "address": "351/44 Lê Vǎn Sỹ, Phường 13 ,Quận 3 , Hồ Chí Minh",
   "Longtitude": 10.786427,
   "Latitude": 106.676469
 },
 {
   "STT": 2769,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ. Giang Châu Võ",
   "address": "297/37 Lê Đức Thọ, Phường 17 ,Gò Vấp , Hồ Chí Minh",
   "Longtitude": 10.840142,
   "Latitude": 106.677475
 },
 {
   "STT": 2770,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ. Hồ Thị Ngọc",
   "address": "2/1A Cư Xá Lữ Gia, Phường 15 ,Quận 11 , Hồ Chí Minh",
   "Longtitude": 10.7736056,
   "Latitude": 106.6550748
 },
 {
   "STT": 2771,
   "Name": "Phòng Khám Sản Phụ Khoa - Bác sĩ. Hồ Viết Thắng",
   "address": "44 đường 27, Phường Bình Trị Đông B , Bình Tân , Hồ Chí Minh",
   "Longtitude": 10.7553701,
   "Latitude": 106.6139835
 }
];